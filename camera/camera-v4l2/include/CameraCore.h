/*
 * Camera HAL for Ingenic android 4.1
 *
 * Copyright 2012 Ingenic Semiconductor LTD.
 *
 * author:
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */

#ifndef __CAMERA_CORE_H_
#define __CAMERA_CORE_H_

#include <fcntl.h>
#include <sys/select.h>
#include <sys/ioctl.h>
#include <sys/time.h>
#include <sys/mman.h>
#include <linux/videodev2.h>
#include <linux/jz_cim.h>
#include <linux/android_pmem.h>
#include <utils/Timers.h>
#include <utils/Log.h>
#include <utils/Errors.h>
#include <utils/String8.h>
#include <utils/threads.h>
/* #include <utils/WorkQueue.h> */
#include <WorkQueue.h>
#include <utils/List.h>
#include <binder/IMemory.h>
#include <binder/MemoryBase.h>
#include <binder/MemoryHeapBase.h>
#include <cutils/properties.h>
#include <cmath>
#include <ui/PixelFormat.h>
#include <ui/Rect.h>
#include <ui/GraphicBufferMapper.h>
#include <media/stagefright/foundation/ADebug.h>

#include <hardware/camera.h>
#ifdef CAMERA_VERSION2
#include <hardware/camera2.h>
#endif
#undef PAGE_SIZE
#include "android_jz_ipu.h"
#include "gc_gralloc_priv.h"
#include "gralloc_priv.h"

#define PMEMDEVICE "/dev/pmem_camera"

#define START_ADDR_ALIGN 0x1000 /* 4096 byte */
#define STRIDE_ALIGN 0x800 /* 2048 byte */

#define PREVIEW_BUFFER_CONUT 5
#define PREVIEW_BUFFER_SIZE 0x400000 //4M
#define CAPTURE_BUFFER_COUNT 1

#define MIN_WIDTH 640
#define MIN_HEIGHT 480
#define SIZE_640X480      (MIN_WIDTH*MIN_HEIGHT)
#define SIZE_1600X1200    (1600*1200)
#define SIZE_1024X768     (1024*768)

#define RECORDING_BUFFER_NUM 2

#define LOG_FUNCTION_NAME ALOGV("%d: %s() ENTER", __LINE__, __FUNCTION__);
#define LOG_FUNCTION_NAME_EXIT ALOGV("%d: %s() EXIT", __LINE__, __FUNCTION__);

//camera param cmd
#define CPCMD_SET_RESOLUTION         0x01
#define CPCMD_SET_OUTPUT_FORMAT      0x03

struct resolution_info {
  struct frm_size ptable[16];
  struct frm_size ctable[16];
};

struct camera_param {
  unsigned int cmd;
  struct resolution_info param;
};

/**
 * The metadata of the video frame data.
 */

typedef struct CameraYUVMeta {

    /**
     *  The index in the frame buffers.
     */

	int32_t index;
    /**
     *  video  frame width and height.
     */

	int32_t width;
	int32_t height;

    /**
     *  if use pmem, this is y phys addr of video frame of yuv data .
     */

	int32_t yPhy;

    /**
     *  if use pmem, this is u phys addr of video frame of yuv data .
     */

	int32_t uPhy;


    /**
     *  if use pmem, this is v phys addr of video frame of yuv data .
     */

	int32_t vPhy;


    /**
     *  this is y virtual addr of video frame of yuv data .
     */

	int32_t yAddr;


    /**
     *  this is u virtual addr of video frame of yuv data .
     */

	int32_t uAddr;


    /**
     *  this is v virtual addr of video frame of yuv data .
     */

	int32_t vAddr;


    /**
     *  this is y stride in byte of video frame of yuv data .
     */

	int32_t yStride;


    /**
     *  this is uv stride in byte of video frame of yuv data .
     */

	int32_t uStride;
	int32_t vStride;


    /**
     *  this is buffer number of video frames.
     */

	int32_t count;


    /**
     *  this is format of video frames, it is define in
     *  system/core/include/system/graphics.h
     *  pixel format definitions
     */

	int32_t format;
}CameraYUVMeta;

#endif
