/**
 *
 * audio_hw.h
 *
 **/

#include <hardware/audio.h>
#include <hardware/hardware.h>

#include <system/audio.h>

#include <tinyalsa/asoundlib.h>

#include <audio_utils/resampler.h>
#include <audio_route/audio_route.h>

#define PCM_CARD 0
#define PCM_DEVICE 0
#define PCM_DEVICE_SCO	1
#define MIXER_CARD 0
#define OUT_PERIOD_SIZE 2048
#define OUT_PERIOD_COUNT 8
#define OUT_SAMPLING_RATE 44100

#define IN_PERIOD_SIZE 1024
#define IN_PERIOD_COUNT 4
#define IN_SAMPLING_RATE 8000

#define BT_PERIOD_CHANNEL 1
#define BT_PERIOD_SIZE	512
#define BT_PERIOD_COUNT 4
#define BT_SAMPLING_RATE 8000
#define DMIC_DEVICE     "/dev/jz-dmic"

struct stream_out;
struct stream_in;
struct audio_device {
    struct audio_hw_device hw_device;
    pthread_mutex_t lock; /* see note below on mutex acquisition order */
    unsigned int out_device;
    unsigned int in_device;
    bool standby;
    bool mic_mute;
    bool btMode;
    bool dmicMode;
    bool is_in_call;
    bool route_keep;

    int dev_fd;
    /*device route*/
    struct audio_route *ar;

    /*primary stream*/
    struct stream_out *active_out;
    struct stream_in *active_in;

    /*bt stream*/
    struct stream_out *sco_out;		/*soc end*/
    struct stream_in *sco_in;
    struct stream_out *voc_out;		/*voice end*/
    struct stream_in *voc_in;
};

struct stream_out {
    struct audio_stream_out stream;

    pthread_mutex_t lock; /* see note below on mutex acquisition order */
    struct pcm *pcm;
    struct pcm_config *pcm_config;
    bool standby;

    unsigned int requested_rate;
    struct resampler_itfe *resampler;
    int16_t *buffer;
    size_t buffer_frames;
    struct audio_device *dev;
};

struct stream_in {
    struct audio_stream_in stream;

    pthread_mutex_t lock; /* see note below on mutex acquisition order */
    struct pcm *pcm;
    struct pcm_config *pcm_config;
    bool standby;

    unsigned int requested_rate;
    struct resampler_itfe *resampler;
    struct resampler_buffer_provider buf_provider;
    int16_t *buffer;
    size_t buffer_size;
    size_t frames_in;
    int read_status;
    struct audio_device *dev;
};
