/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define LOG_TAG "audio_hw_primary"
#define LOG_NDEBUG 0

#include <errno.h>
#include <pthread.h>
#include <stdint.h>
#include <stdlib.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <cutils/log.h>
#include <cutils/properties.h>
#include <hardware/audio.h>
#include <hardware/hardware.h>
#include <system/audio.h>
#include <tinyalsa/asoundlib.h>
#include <audio_utils/resampler.h>
#include <audio_route/audio_route.h>

#ifdef __cplusplus
extern "C" {
#endif
#include <cutils/str_parms.h>
#ifdef __cplusplus
}
#endif
#include "audio_hw.h"
#include "webrtc_aec.h"
extern int dmic_open(struct audio_device *alsadev);
extern ssize_t dmic_read(struct stream_in *stream, void* buffer, size_t bytes);
extern void dmic_close(struct audio_device *alsadev);
static int output_debug_fd = -1;
static int output_debug_resample_fd = -1;
static int input_debug_resample_fd = -1;
static int input_debug_fd = -1;
static struct timeval dbg_tv;

struct pcm_config pcm_config_out = {
    .channels = 2,
    .rate = OUT_SAMPLING_RATE,
    .period_size = OUT_PERIOD_SIZE,
    .period_count = OUT_PERIOD_COUNT,
    .format = PCM_FORMAT_S16_LE,
    .start_threshold = OUT_PERIOD_SIZE,
};

struct pcm_config pcm_config_in = {
    .channels = 2,
    .rate = IN_SAMPLING_RATE,
    .period_size = IN_PERIOD_SIZE,
    .period_count = IN_PERIOD_COUNT,
    .format = PCM_FORMAT_S16_LE,
    .start_threshold = 1,
    .stop_threshold = (IN_PERIOD_SIZE * IN_PERIOD_COUNT),
};

static uint32_t out_get_sample_rate(const struct audio_stream *stream);
static size_t out_get_buffer_size(const struct audio_stream *stream);
static audio_format_t out_get_format(const struct audio_stream *stream);
static uint32_t in_get_sample_rate(const struct audio_stream *stream);
static size_t in_get_buffer_size(const struct audio_stream *stream);
static audio_format_t in_get_format(const struct audio_stream *stream);
static int get_next_buffer(struct resampler_buffer_provider *buffer_provider,
                                   struct resampler_buffer* buffer);
static void release_buffer(struct resampler_buffer_provider *buffer_provider,
                                  struct resampler_buffer* buffer);
extern int bt_stream_init(struct audio_device *adev);
extern void bt_end_call(struct audio_device *adev);
extern int bt_start_call(struct audio_device *adev);
/*
 * NOTE: when multiple mutexes have to be acquired, always take the
 * audio_device mutex first, followed by the stream_in and/or
 * stream_out mutexes.
 */

/* Helper functions */
static void select_devices(struct audio_device *adev)
{
    int headphone_on;
    int speaker_on;
    int buildin_mic_on;
    int headset_mic_on;
    int bt_slave_sco_on = 0;

    char *error_msg;
    headphone_on = adev->out_device & (AUDIO_DEVICE_OUT_WIRED_HEADSET |
                                    AUDIO_DEVICE_OUT_WIRED_HEADPHONE);
    speaker_on = adev->out_device & AUDIO_DEVICE_OUT_SPEAKER;
    buildin_mic_on = adev->in_device & AUDIO_DEVICE_IN_BUILTIN_MIC;
    headset_mic_on = adev->in_device & AUDIO_DEVICE_IN_WIRED_HEADSET;

    if (adev->is_in_call)
	    bt_slave_sco_on = 1;

    audio_route_reset(adev->ar);

    if (speaker_on && !headphone_on)
        audio_route_apply_path(adev->ar, "speaker");
    if (headphone_on && !speaker_on)
        audio_route_apply_path(adev->ar, "headphone");
    if (speaker_on && headphone_on)
	audio_route_apply_path(adev->ar, "speaker-headphone");
    if (buildin_mic_on){
	if(access(DMIC_DEVICE, R_OK) == -1){
	    error_msg = strerror(errno);
	    ALOGE("ret_access is %s",error_msg);
	}

	if(access(DMIC_DEVICE, R_OK) == 0){
		if (adev->dmicMode != true) {
			ALOGE("#############dmic on###############");
			dmic_open(adev);
		}
	}else{
		ALOGE("##############buildin mic on###############");
		audio_route_apply_path(adev->ar, "buildin-mic");
	}
    }else if(!buildin_mic_on){

		dmic_close(adev);
	    ALOGE("#############dmic off###############");
    }
    if (headset_mic_on)
	    audio_route_apply_path(adev->ar, "headset-mic");

    if (!bt_slave_sco_on && adev->btMode != false)
	    bt_end_call(adev);

    if (!adev->route_keep)
	    audio_route_update_mixer(adev->ar);

    if (bt_slave_sco_on && adev->btMode == false)
	    bt_start_call(adev);

    ALOGV("hp=%c speaker=%c headset_mic_on=%c buildin_mic_on=%c bt_slave_sco_on=%c\n",
		    headphone_on ? 'y' : 'n', speaker_on ? 'y' : 'n',
		    headset_mic_on ? 'y' : 'n', buildin_mic_on ? 'y' : 'n',
		    bt_slave_sco_on ? 'y' : 'n');
}

/* must be called with hw device and output stream mutexes locked */
void do_out_standby(struct stream_out *out)
{
    struct audio_device *adev = out->dev;

    if (!out->standby) {
        pcm_close(out->pcm);
        out->pcm = NULL;
        adev->active_out = NULL;
        if (out->resampler) {
            release_resampler(out->resampler);
            out->resampler = NULL;
        }
        if (out->buffer) {
            free(out->buffer);
            out->buffer = NULL;
        }
	if (output_debug_fd > 0) {
		close(output_debug_fd);
		output_debug_fd = -1;
	}
	if (output_debug_resample_fd > 0) {
		close(output_debug_resample_fd);
		output_debug_resample_fd = -1;
	}
        out->standby = true;
    }
}

/* must be called with hw device and input stream mutexes locked */
void do_in_standby(struct stream_in *in)
{
    struct audio_device *adev = in->dev;

    if (!in->standby && in->pcm != NULL) {
        pcm_close(in->pcm);
        in->pcm = NULL;
        adev->active_in = NULL;
        if (in->resampler) {
            release_resampler(in->resampler);
            in->resampler = NULL;
        }
        if (in->buffer) {
            free(in->buffer);
            in->buffer = NULL;
        }
	if (input_debug_fd > 0) {
		close(input_debug_fd);
		input_debug_fd = -1;
	}
	if (input_debug_resample_fd > 0) {
		close(input_debug_resample_fd);
		input_debug_resample_fd = -1;
	}
        in->standby = true;
    }
}

/* must be called with hw device and output stream mutexes locked */
static int start_output_stream(struct stream_out *out)
{
    struct audio_device *adev = out->dev;
    unsigned int device;
    int ret;

    ALOGV("%s \n", __func__);

    device = PCM_DEVICE;
    out->pcm_config = &pcm_config_out;
    out->pcm = pcm_open(PCM_CARD, device, PCM_OUT | PCM_NORESTART, out->pcm_config);

    if (out->pcm && !pcm_is_ready(out->pcm)) {
        ALOGE("pcm_open(out) failed: %s", pcm_get_error(out->pcm));
        pcm_close(out->pcm);
        return -ENOMEM;
    }

    /*
     * If the stream rate differs from the PCM rate, we need to
     * create a resampler.
     */
    if (out_get_sample_rate(&out->stream.common) != out->pcm_config->rate) {
	    ret = create_resampler(out_get_sample_rate(&out->stream.common),
                               out->pcm_config->rate,
                               out->pcm_config->channels,
                               RESAMPLER_QUALITY_DEFAULT,
                               NULL,
                               &out->resampler);
        out->buffer_frames = (pcm_config_out.period_size * out->pcm_config->rate) /
                out_get_sample_rate(&out->stream.common) + 1;

        out->buffer = (int16_t *)malloc(pcm_frames_to_bytes(out->pcm, out->buffer_frames));
    }

    output_debug_fd = open("/data/output.pcm", O_WRONLY | O_APPEND | O_TRUNC);
    output_debug_resample_fd = open("/data/resample.pcm", O_WRONLY | O_APPEND | O_TRUNC);
    adev->active_out = out;
    out->standby = false;
    return 0;
}

/* must be called with hw device and input stream mutexes locked */
static int start_input_stream(struct stream_in *in)
{
    struct audio_device *adev = in->dev;
    unsigned int device;
    int ret;

    ALOGV("%s enter \n", __func__);
    device = PCM_DEVICE;
    in->pcm_config = &pcm_config_in;

    in->pcm = pcm_open(PCM_CARD, device, PCM_IN, in->pcm_config);
    if (in->pcm && !pcm_is_ready(in->pcm)) {
        ALOGE("pcm_open(in) failed: %s", pcm_get_error(in->pcm));
        pcm_close(in->pcm);
        return -ENOMEM;
    }

    /*
     * If the stream rate differs from the PCM rate, we need to
     * create a resampler.
     */
    if (in_get_sample_rate(&in->stream.common) != in->pcm_config->rate) {
        in->buf_provider.get_next_buffer = get_next_buffer;
        in->buf_provider.release_buffer = release_buffer;

        ret = create_resampler(in->pcm_config->rate,
                               in_get_sample_rate(&in->stream.common),
                               1,
                               RESAMPLER_QUALITY_DEFAULT,
                               &in->buf_provider,
                               &in->resampler);
    }
    in->buffer_size = pcm_frames_to_bytes(in->pcm,
                                          in->pcm_config->period_size);
    in->buffer = (int16_t *)malloc(in->buffer_size);
    in->frames_in = 0;
    input_debug_fd = open("/data/input.pcm", O_WRONLY | O_APPEND | O_TRUNC);
    input_debug_resample_fd = open("/data/inresample.pcm", O_WRONLY | O_APPEND | O_TRUNC);
    adev->active_in = in;
    in->standby = false;
    ALOGV("%s leave \n", __func__);
    return 0;
}

static int get_next_buffer(struct resampler_buffer_provider *buffer_provider,
                                   struct resampler_buffer* buffer)
{
    struct stream_in *in;

    if (buffer_provider == NULL || buffer == NULL)
        return -EINVAL;

    in = (struct stream_in *)((char *)buffer_provider -
                                   offsetof(struct stream_in, buf_provider));

    if (in->pcm == NULL) {
        buffer->raw = NULL;
        buffer->frame_count = 0;
        in->read_status = -ENODEV;
        return -ENODEV;
    }

    if (in->frames_in == 0) {
        in->read_status = pcm_read(in->pcm,
                                   (void*)in->buffer,
                                   in->buffer_size);
        if (in->read_status != 0) {
            ALOGE("get_next_buffer() pcm_read error %d", in->read_status);
            buffer->raw = NULL;
            buffer->frame_count = 0;
            return in->read_status;
        }
        in->frames_in = in->pcm_config->period_size;
        if (in->pcm_config->channels == 2) {
            unsigned int i;
            /* Discard right channel */
            for (i = 1; i < in->frames_in; i++)
                in->buffer[i] = in->buffer[i * 2];
        }
    }

    buffer->frame_count = (buffer->frame_count > in->frames_in) ?
                                in->frames_in : buffer->frame_count;
    buffer->i16 = in->buffer + (in->pcm_config->period_size - in->frames_in);

    return in->read_status;

}

static void release_buffer(struct resampler_buffer_provider *buffer_provider,
                                  struct resampler_buffer* buffer)
{
    struct stream_in *in;

    if (buffer_provider == NULL || buffer == NULL)
        return;

    in = (struct stream_in *)((char *)buffer_provider -
                                   offsetof(struct stream_in, buf_provider));

    in->frames_in -= buffer->frame_count;
}

/* read_frames() reads frames from kernel driver, down samples to capture rate
 * if necessary and output the number of frames requested to the buffer specified */
static ssize_t read_frames(struct stream_in *in, void *buffer, ssize_t frames)
{
    ssize_t frames_wr = 0;

    while (frames_wr < frames) {
        size_t frames_rd = frames - frames_wr;
        if (in->resampler != NULL) {
            in->resampler->resample_from_provider(in->resampler,
                    (int16_t *)((char *)buffer +
                            frames_wr * audio_stream_in_frame_size(&in->stream)),
                    &frames_rd);
        } else {
            struct resampler_buffer buf = {
                    { raw : NULL, },
                    frame_count : frames_rd,
            };
            get_next_buffer(&in->buf_provider, &buf);
            if (buf.raw != NULL) {
                memcpy((char *)buffer +
                           frames_wr * audio_stream_in_frame_size(&in->stream),
                        buf.raw,
                        buf.frame_count * audio_stream_in_frame_size(&in->stream));
                frames_rd = buf.frame_count;
            }
            release_buffer(&in->buf_provider, &buf);
        }
        /* in->read_status is updated by getNextBuffer() also called by
         * in->resampler->resample_from_provider() */
        if (in->read_status != 0)
            return in->read_status;

        frames_wr += frames_rd;
    }
    return frames_wr;
}

/* API functions */

static uint32_t out_get_sample_rate(const struct audio_stream *stream __unused)
{
    return pcm_config_out.rate;
}

static int out_set_sample_rate(struct audio_stream *stream __unused, uint32_t rate __unused)
{
    return -ENOSYS;
}

static size_t out_get_buffer_size(const struct audio_stream *stream)
{
    return pcm_config_out.period_size *
               audio_stream_out_frame_size((const struct audio_stream_out *)stream);
}

static uint32_t out_get_channels(const struct audio_stream *stream __unused)
{
    return AUDIO_CHANNEL_OUT_STEREO;
}

static audio_format_t out_get_format(const struct audio_stream *stream __unused)
{
    return AUDIO_FORMAT_PCM_16_BIT;
}

static int out_set_format(struct audio_stream *stream __unused, audio_format_t format __unused)
{
    return -ENOSYS;
}

static int out_standby(struct audio_stream *stream)
{
    struct stream_out *out = (struct stream_out *)stream;
#if 1		/*open close is waste time, we ignore it*/
    ALOGV("%s enter \n", __func__);

    pthread_mutex_lock(&out->dev->lock);
    pthread_mutex_lock(&out->lock);
    do_out_standby(out);
    pthread_mutex_unlock(&out->lock);
    pthread_mutex_unlock(&out->dev->lock);

    ALOGV("%s leave \n", __func__);
#endif
    return 0;
}

static int out_dump(const struct audio_stream *stream __unused, int fd __unused)
{
    return 0;
}

static int out_set_parameters(struct audio_stream *stream, const char *kvpairs)
{
	struct stream_out *out = (struct stream_out *)stream;
	struct audio_device *adev = out->dev;
	struct str_parms *parms;
	char value[32];
	int ret;
	unsigned int val;

   parms = str_parms_create_str(kvpairs);
   ret = str_parms_get_str(parms, AUDIO_PARAMETER_STREAM_ROUTING,
						value, sizeof(value));

   ALOGV("%s enter\n", __func__);
   pthread_mutex_lock(&adev->lock);
   if (ret >= 0) {
	   val = atoi(value);
	   if (((adev->out_device != val) && (val != 0)) || (adev->is_in_call != adev->btMode)) {
		   if (adev->out_device == val) {
			   adev->route_keep = 1;
		   } else {
			   adev->route_keep = 0;
			   adev->out_device = val;
		   }
		   select_devices(adev);
	   }
   }
   pthread_mutex_unlock(&adev->lock);
   str_parms_destroy(parms);
   ALOGV("%s leave\n", __func__);
   return ret;
}

static char * out_get_parameters(const struct audio_stream *stream __unused, const char *keys __unused)
{
    return strdup("");
}

static uint32_t out_get_latency(const struct audio_stream_out *stream __unused)
{
    return (pcm_config_out.period_size * pcm_config_out.period_count * 1000) / pcm_config_out.rate;
}

static int out_set_volume(struct audio_stream_out *stream __unused, float left __unused,
                          float right __unused)
{
    return -ENOSYS;
}

static ssize_t out_write(struct audio_stream_out *stream, const void* buffer,
                         size_t bytes)
{
    int ret = 0;
    struct stream_out *out = (struct stream_out *)stream;
    struct audio_device *adev = out->dev;
    size_t frame_size = audio_stream_out_frame_size(stream);
    int16_t *in_buffer = (int16_t *)buffer;
    size_t in_frames = bytes / frame_size;
    size_t out_frames;

    /*
     * acquiring hw device mutex systematically is useful if a low
     * priority thread is waiting on the output stream mutex - e.g.
     * executing out_set_parameters() while holding the hw device
     * mutex
     */
    /*ALOGV("%s enter \n", __func__);*/
    pthread_mutex_lock(&adev->lock);
    pthread_mutex_lock(&out->lock);
    if (adev->btMode == true) {
	    ret = -EPERM;
	    pthread_mutex_unlock(&adev->lock);
	    goto exit;
    } else if (out->standby) {
	    ret = start_output_stream(out);
	    if (ret) {
		    pthread_mutex_unlock(&adev->lock);
		    goto exit;
	    }
    }
    pthread_mutex_unlock(&adev->lock);

    if (output_debug_fd > 0) {
	    if (write(output_debug_fd, buffer,
				    (in_frames*frame_size)) != (ssize_t)(in_frames*frame_size)) {
		    ALOGE("[dump] write error !");
	    }
    }

    /* Change sample rate, if necessary */
    if (out_get_sample_rate(&stream->common) != out->pcm_config->rate) {
        out_frames = out->buffer_frames;
        out->resampler->resample_from_input(out->resampler,
                                            in_buffer, &in_frames,
                                            out->buffer, &out_frames);
        in_buffer = out->buffer;
	if (output_debug_resample_fd > 0) {
		if (write(output_debug_resample_fd, in_buffer,
					(out_frames*frame_size)) != (ssize_t)(out_frames*frame_size)) {
			ALOGE("[dump] write error !");
		}
	}
    } else {
        out_frames = in_frames;
    }

    ret = pcm_write(out->pcm, in_buffer, out_frames * frame_size);
    if (ret == -EPIPE) {
	    /* In case of underrun, don't sleep since we want to catch up asap */
	    pthread_mutex_unlock(&out->lock);
	    return ret;
    }
exit:
    pthread_mutex_unlock(&out->lock);
    if (ret != 0) {
        usleep(bytes * 1000000 / audio_stream_out_frame_size(stream) /
               out_get_sample_rate(&stream->common));
    }
    /*ALOGV("%s leave \n", __func__);*/
    return bytes;
}

static int out_get_render_position(const struct audio_stream_out *stream __unused,
                                   uint32_t *dsp_frames __unused)
{
    return -EINVAL;
}

static int out_add_audio_effect(const struct audio_stream *stream __unused, effect_handle_t effect __unused)
{
    return 0;
}

static int out_remove_audio_effect(const struct audio_stream *stream __unused, effect_handle_t effect __unused)
{
    return 0;
}

static int out_get_next_write_timestamp(const struct audio_stream_out *stream __unused,
                                        int64_t *timestamp __unused)
{
    return -EINVAL;
}

/** audio_stream_in implementation **/
static uint32_t in_get_sample_rate(const struct audio_stream *stream)
{
    struct stream_in *in = (struct stream_in *)stream;

    return in->requested_rate;
}

static int in_set_sample_rate(struct audio_stream *stream __unused, uint32_t rate __unused)
{
    return 0;
}

static size_t in_get_buffer_size(const struct audio_stream *stream)
{
    struct stream_in *in = (struct stream_in *)stream;
    size_t size;

    /*
     * take resampling into account and return the closest majoring
     * multiple of 16 frames, as audioflinger expects audio buffers to
     * be a multiple of 16 frames
     */
    size = (in->pcm_config->period_size * in_get_sample_rate(stream)) /
            in->pcm_config->rate;
    size = ((size + 15) / 16) * 16;

    size = size * audio_stream_in_frame_size((const struct audio_stream_in *)stream);
    return size;
}

static uint32_t in_get_channels(const struct audio_stream *stream __unused)
{
    return AUDIO_CHANNEL_IN_MONO;
}

static audio_format_t in_get_format(const struct audio_stream *stream __unused)
{
    return AUDIO_FORMAT_PCM_16_BIT;
}

static int in_set_format(struct audio_stream *stream __unused, audio_format_t format __unused)
{
    return -ENOSYS;
}

static int in_standby(struct audio_stream *stream)
{
    struct stream_in *in = (struct stream_in *)stream;

    ALOGV("%s enter\n", __func__);
    pthread_mutex_lock(&in->dev->lock);
    pthread_mutex_lock(&in->lock);
    do_in_standby(in);
    pthread_mutex_unlock(&in->lock);
    pthread_mutex_unlock(&in->dev->lock);
    ALOGV("%s leave\n", __func__);

    return 0;
}

static int in_dump(const struct audio_stream *stream __unused, int fd __unused)
{
    return 0;
}

static int in_set_parameters(struct audio_stream *stream, const char *kvpairs)
{
    struct stream_in *in = (struct stream_in *)stream;
    struct audio_device *adev = in->dev;
    struct str_parms *parms;
    char value[32];
    int ret;
    unsigned int val;

    ALOGV("%s enter\n", __func__);
    parms = str_parms_create_str(kvpairs);
    ret = str_parms_get_str(parms, AUDIO_PARAMETER_STREAM_ROUTING,
		    value, sizeof(value));
    pthread_mutex_lock(&adev->lock);
    if (ret >= 0) {
	    /* strip AUDIO_DEVICE_BIT_IN to allow bitwise comparisons */
	    val = atoi(value) & ~AUDIO_DEVICE_BIT_IN;
	    ALOGV("adev->in_device = %x new val = %x  adev->dev_fd = %d\n", adev->in_device, val, adev->dev_fd);
	    if (((adev->in_device != val) && (val != 0))
			    ||(adev->in_device = AUDIO_DEVICE_IN_BUILTIN_MIC && access(DMIC_DEVICE, R_OK) == 0
				    && (adev->dev_fd == -1 || adev->dev_fd == 0))) {
		    adev->in_device = val;
		    select_devices(adev);
	    }
    }
    pthread_mutex_unlock(&adev->lock);
    str_parms_destroy(parms);
    ALOGV("%s leave\n", __func__);
    return ret;
}

static char * in_get_parameters(const struct audio_stream *stream __unused,
                                const char *keys __unused)
{
    ALOGV("%s\n", __func__);
    return strdup("");
}

static int in_set_gain(struct audio_stream_in *stream __unused, float gain __unused)
{
    ALOGV("%s\n", __func__);
    return 0;
}

static ssize_t in_read(struct audio_stream_in *stream, void* buffer,
                       size_t bytes)
{
    int ret = 0;
    struct stream_in *in = (struct stream_in *)stream;
    struct audio_device *adev = in->dev;
    size_t frames_rq = bytes / audio_stream_in_frame_size(stream);

    /*ALOGV("%s enter \n", __func__);*/
    /*
     * acquiring hw device mutex systematically is useful if a low
     * priority thread is waiting on the input stream mutex - e.g.
     * executing in_set_parameters() while holding the hw device
     * mutex
     */
    pthread_mutex_lock(&adev->lock);
    pthread_mutex_lock(&in->lock);
    if (adev->btMode == true) {
	    ret = -EPERM;
	    pthread_mutex_unlock(&adev->lock);
	    goto exit;
    } else if (adev->dmicMode == true){
	pthread_mutex_unlock(&adev->lock);
	ret = (int)dmic_read(in, buffer, bytes);
	ALOGE("dmic read !");
	goto exit;
    } else if (in->standby) {
	    ret = start_input_stream(in);
	    if (ret) {
		    pthread_mutex_unlock(&adev->lock);
		    goto exit;
	    }
    }
    pthread_mutex_unlock(&adev->lock);

    if (in->resampler != NULL) {
        ret = read_frames(in, buffer, frames_rq);
    } else if (in->pcm_config->channels == 2) {
        /*
         * If the PCM is stereo, capture twice as many frames and
         * discard the right channel.
         */
        unsigned int i;
        int16_t *in_buffer = (int16_t *)buffer;

        ret = pcm_read(in->pcm, in->buffer, bytes * 2);
	if (!ret && input_debug_fd > 0) {
		if (write(input_debug_fd, in->buffer,
					bytes * 2) != (ssize_t)(bytes * 2)) {
			ALOGE("[dump] write error !");
		}
	}
        /* Discard right channel */
        for (i = 0; i < frames_rq; i++)
            in_buffer[i] = in->buffer[i * 2];
    } else {
        ret = pcm_read(in->pcm, buffer, bytes);
	if (!ret && input_debug_fd > 0) {
		if (write(input_debug_fd, in->buffer,
					bytes * 2) != (ssize_t)(bytes * 2)) {
			ALOGE("[dump] write error !");
		}
	}
    }

    if (ret > 0)
        ret = 0;
    /*
     * Instead of writing zeroes here, we could trust the hardware
     * to always provide zeroes when muted.
     */
    if (ret == 0 && adev->mic_mute)
        memset(buffer, 0, bytes);

exit:
    if (ret < 0)
        usleep(bytes * 1000000 / audio_stream_in_frame_size(stream) /
               in_get_sample_rate(&stream->common));
    pthread_mutex_unlock(&in->lock);
    /*ALOGV("%s leave %d\n", __func__, bytes);*/
    return bytes;
}

static uint32_t in_get_input_frames_lost(struct audio_stream_in *stream __unused)
{
    return 0;
}

static int in_add_audio_effect(const struct audio_stream *stream __unused,
                               effect_handle_t effect __unused)
{
    ALOGV("%s\n", __func__);
    return 0;
}

static int in_remove_audio_effect(const struct audio_stream *stream __unused,
                                  effect_handle_t effect __unused)
{
    ALOGV("%s\n", __func__);
    return 0;
}

static int adev_open_output_stream(struct audio_hw_device *dev,
                              audio_io_handle_t handle __unused,
                              audio_devices_t devices __unused,
                              audio_output_flags_t flags __unused,
                              struct audio_config *config,
                              struct audio_stream_out **stream_out,
                              const char *address __unused)
{
    struct audio_device *adev = (struct audio_device *)dev;
    struct stream_out *out;
    int ret;

    ALOGV("%s enter \n", __func__);
    out = (struct stream_out *)calloc(1, sizeof(struct stream_out));
    if (!out)
        return -ENOMEM;

    out->stream.common.get_sample_rate = out_get_sample_rate;
    out->stream.common.set_sample_rate = out_set_sample_rate;
    out->stream.common.get_buffer_size = out_get_buffer_size;
    out->stream.common.get_channels = out_get_channels;
    out->stream.common.get_format = out_get_format;
    out->stream.common.set_format = out_set_format;
    out->stream.common.standby = out_standby;
    out->stream.common.dump = out_dump;
    out->stream.common.set_parameters = out_set_parameters;
    out->stream.common.get_parameters = out_get_parameters;
    out->stream.common.add_audio_effect = out_add_audio_effect;
    out->stream.common.remove_audio_effect = out_remove_audio_effect;
    out->stream.get_latency = out_get_latency;
    out->stream.set_volume = out_set_volume;
    out->stream.write = out_write;
    out->stream.get_render_position = out_get_render_position;
    out->stream.get_next_write_timestamp = out_get_next_write_timestamp;

    out->dev = adev;

    config->format = out_get_format(&out->stream.common);
    config->channel_mask = out_get_channels(&out->stream.common);
    config->sample_rate = out_get_sample_rate(&out->stream.common);

    out->standby = true;

    *stream_out = &out->stream;

    ALOGV("%s leave \n", __func__);
    return 0;

err_open:
    free(out);
    *stream_out = NULL;
    return ret;
}

static void adev_close_output_stream(struct audio_hw_device *dev __unused,
                                     struct audio_stream_out *stream)
{
    struct stream_out *out = (struct stream_out *)stream;
    pthread_mutex_lock(&out->dev->lock);
    pthread_mutex_lock(&out->lock);
    do_out_standby(out);
    pthread_mutex_unlock(&out->lock);
    pthread_mutex_unlock(&out->dev->lock);
    free(stream);
    ALOGV("%s leave\n", __func__);
}

static int adev_set_parameters(struct audio_hw_device *dev __unused, const char *kvpairs __unused)
{
    ALOGV("%s\n", __func__);
    return 0;
}

static char * adev_get_parameters(const struct audio_hw_device *dev __unused,
                                  const char *keys __unused)
{
    return strdup("");
}

static int adev_init_check(const struct audio_hw_device *dev __unused)
{
    return 0;
}

static int adev_set_voice_volume(struct audio_hw_device *dev __unused, float volume __unused)
{
    return -ENOSYS;
}

static int adev_set_master_volume(struct audio_hw_device *dev __unused, float volume __unused)
{
    return -ENOSYS;
}

static int adev_set_mode(struct audio_hw_device *dev, audio_mode_t mode)
{
    struct audio_device *adev = (struct audio_device *)dev;
    if  (mode == AUDIO_MODE_IN_CALL) {
	    ALOGV("%s adev mode bt is in call\n", __func__);
	    adev->is_in_call = true;
    } else if (adev->is_in_call == true){
	    ALOGV("%s adev mode bt leave call\n", __func__);
	    adev->is_in_call = false;
    }
    return 0;
}

static int adev_set_mic_mute(struct audio_hw_device *dev, bool state)
{
    struct audio_device *adev = (struct audio_device *)dev;

    adev->mic_mute = state;

    return 0;
}

static int adev_get_mic_mute(const struct audio_hw_device *dev, bool *state)
{
    struct audio_device *adev = (struct audio_device *)dev;

    *state = adev->mic_mute;

    return 0;
}

static size_t adev_get_input_buffer_size(const struct audio_hw_device *dev __unused,
                                         const struct audio_config *config)
{
    size_t size;

    /*
     * take resampling into account and return the closest majoring
     * multiple of 16 frames, as audioflinger expects audio buffers to
     * be a multiple of 16 frames
     */
    size = (pcm_config_in.period_size * config->sample_rate) / pcm_config_in.rate;
    size = ((size + 15) / 16) * 16;

    size =  (size * audio_channel_count_from_in_mask(config->channel_mask) *
                audio_bytes_per_sample(config->format));
    return size;
}

static int adev_open_input_stream(struct audio_hw_device *dev,
				audio_io_handle_t handle __unused,
				audio_devices_t devices __unused,
				struct audio_config *config,
				struct audio_stream_in **stream_in,
				audio_input_flags_t flags __unused,
				const char *address __unused,
				audio_source_t source __unused)
{
    struct audio_device *adev = (struct audio_device *)dev;
    struct stream_in *in;
    int ret;

    *stream_in = NULL;

    /* Respond with a request for mono if a different format is given. */
    if (config->channel_mask != AUDIO_CHANNEL_IN_MONO) {
        config->channel_mask = AUDIO_CHANNEL_IN_MONO;
        return -EINVAL;
    }

    in = (struct stream_in *)calloc(1, sizeof(struct stream_in));
    if (!in)
        return -ENOMEM;

    in->stream.common.get_sample_rate = in_get_sample_rate;
    in->stream.common.set_sample_rate = in_set_sample_rate;
    in->stream.common.get_buffer_size = in_get_buffer_size;
    in->stream.common.get_channels = in_get_channels;
    in->stream.common.get_format = in_get_format;
    in->stream.common.set_format = in_set_format;
    in->stream.common.standby = in_standby;
    in->stream.common.dump = in_dump;
    in->stream.common.set_parameters = in_set_parameters;
    in->stream.common.get_parameters = in_get_parameters;
    in->stream.common.add_audio_effect = in_add_audio_effect;
    in->stream.common.remove_audio_effect = in_remove_audio_effect;
    in->stream.set_gain = in_set_gain;
    in->stream.read = in_read;
    in->stream.get_input_frames_lost = in_get_input_frames_lost;

    in->dev = adev;
    in->standby = true;
    in->requested_rate = config->sample_rate;
    in->pcm_config = &pcm_config_in; /* default PCM config */

    *stream_in = &in->stream;
    return 0;
}

static void adev_close_input_stream(struct audio_hw_device *dev __unused,
		struct audio_stream_in *stream)
{
	struct stream_in *in = (struct stream_in *)stream;
	ALOGV("%s enter  dev_fd = %d\n", __func__, in->dev->dev_fd);

	in_standby(&stream->common);
	if (in->dev->dmicMode == true)
		dmic_close(in->dev);
	else {
		close(in->dev->dev_fd);
		in->dev->dev_fd = -1;
	}
	ALOGV("close dmic !\n");
	free(stream);
}

static int adev_dump(const audio_hw_device_t *device __unused, int fd __unused)
{
    return 0;
}

static int adev_close(hw_device_t *device)
{
    struct audio_device *adev = (struct audio_device *)device;

    audio_route_free(adev->ar);

    free(device);
    return 0;
}

static int adev_open(const hw_module_t* module, const char* name,
                     hw_device_t** device)
{
    struct audio_device *adev;
    int ret;

    if (strcmp(name, AUDIO_HARDWARE_INTERFACE) != 0)
        return -EINVAL;

    adev = (audio_device*)calloc(1, sizeof(struct audio_device));
    if (!adev)
        return -ENOMEM;

    adev->hw_device.common.tag = HARDWARE_DEVICE_TAG;
    adev->hw_device.common.version = AUDIO_DEVICE_API_VERSION_2_0;
    adev->hw_device.common.module = (struct hw_module_t *) module;
    adev->hw_device.common.close = adev_close;

    adev->hw_device.init_check = adev_init_check;
    adev->hw_device.set_voice_volume = adev_set_voice_volume;
    adev->hw_device.set_master_volume = adev_set_master_volume;
    adev->hw_device.set_mode = adev_set_mode;
    adev->hw_device.set_mic_mute = adev_set_mic_mute;
    adev->hw_device.get_mic_mute = adev_get_mic_mute;
    adev->hw_device.set_parameters = adev_set_parameters;
    adev->hw_device.get_parameters = adev_get_parameters;
    adev->hw_device.get_input_buffer_size = adev_get_input_buffer_size;
    adev->hw_device.open_output_stream = adev_open_output_stream;
    adev->hw_device.close_output_stream = adev_close_output_stream;
    adev->hw_device.open_input_stream = adev_open_input_stream;
    adev->hw_device.close_input_stream = adev_close_input_stream;
    adev->hw_device.dump = adev_dump;

    adev->ar = audio_route_init(MIXER_CARD, NULL);
    adev->out_device = AUDIO_DEVICE_NONE;
    adev->in_device = AUDIO_DEVICE_IN_BUILTIN_MIC & ~AUDIO_DEVICE_BIT_IN;
    adev->is_in_call = false;

    *device = &adev->hw_device.common;
    bt_stream_init(adev);
    return 0;
}

static struct hw_module_methods_t hal_module_methods = {
    .open = adev_open,
};

struct audio_module HAL_MODULE_INFO_SYM = {
    .common = {
        .tag = HARDWARE_MODULE_TAG,
        .module_api_version = AUDIO_MODULE_API_VERSION_0_1,
        .hal_api_version = HARDWARE_HAL_API_VERSION,
        .id = AUDIO_HARDWARE_MODULE_ID,
        .name = "Ingenic Audio Alsa HW HAL",
        .author = "Ingenic Semiconductor Co.,Ltd",
        .methods = &hal_module_methods,
    },
};
