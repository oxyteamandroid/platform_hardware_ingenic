
#include <stdio.h>
#include <time.h>
#include <sys/time.h>
#include "audio_processing.h"
#include "audio_buffer.h"
#include "webrtc_vad.h"
#include "module_common_types.h"
#include "tick_util.h"
#include "webrtc_aec.h"

#include <cutils/log.h>

using webrtc::AudioFrame;
using webrtc::AudioBuffer;
using webrtc::GainControl;
using webrtc::AudioProcessing;
using webrtc::NoiseSuppression;
using webrtc::EchoControlMobile;
using webrtc::VoiceDetection;
using webrtc::VadModes;

static AudioProcessing *apm = NULL;
static AudioFrame *far_frame = NULL;
static AudioFrame *near_frame = NULL;

static VadInst *vadInst;

int echo;
int count = 0;
int close_count = 0;
int isopen = 0;

int ingenic_apm_init()
{
	apm = AudioProcessing::Create(0);
	assert(apm != NULL);

	int32_t sample_rate_hz = 8000;
	int32_t device_sample_rate_hz = 8000;

	int num_capture_input_channels = 1;
	int num_capture_output_channels = 1;
	int num_render_channels = 1;
	int samples_per_channel = 160;

	apm->Initialize();

	apm->set_sample_rate_hz(sample_rate_hz);
	apm->set_num_channels(num_capture_input_channels, num_capture_output_channels);
	apm->set_num_reverse_channels(num_render_channels);

	//enable ns
	apm->noise_suppression()->Enable(true);
	apm->noise_suppression()->set_level(NoiseSuppression::kVeryHigh);  //kModerate default

    apm->high_pass_filter()->Enable(true);

	near_frame = new AudioFrame();

	near_frame->_audioChannel = 1;
	near_frame->_frequencyInHz = 8000;
	near_frame->_payloadDataLengthInSamples = samples_per_channel;


	WebRtcVad_Create(&vadInst);
	WebRtcVad_Init(vadInst);

	WebRtcVad_set_mode(vadInst, 2);

	return 0;
}

int ingenic_apm_set_far_frame(short *buf)
{
	near_frame->_audioChannel = 1;
	near_frame->_frequencyInHz = 8000;
	near_frame->_payloadDataLengthInSamples = 160;

	for(int i=0; i< 160; i++) {
		near_frame->_payloadData[i] = buf[i] >> 6;
	}
	apm->set_stream_delay_ms(50);
	apm->ProcessStream(near_frame);

	echo = WebRtcVad_Process(vadInst, 8000, near_frame->_payloadData, 160);

	if (isopen == 0) {
		if (echo == 0) {
			count++;
			if (count == 20) {
				count = 0;
				close_count = 0;
				isopen = 1;
			}
		}
	} else {
		if (echo == 1) {
				isopen = 0;
		}
	}

	return 0;
}

int ingenic_apm_set_near_frame(short *input, short *output){
    static int cnt = 0;
    static int old = 0;
    static int start = 0;
    static int end = 0;

    start = isopen ^ old;

    old = isopen;

	if (isopen) {
	    if (start) {
	        ALOGE("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
	        end = 0;
	        cnt = 0;
	    }

	    if (end != 1) {
            if (cnt <= 1) {
                cnt++;
                memset(output, 0, 160 * sizeof(short));
                return 0;
            } else {
                end = 1;
            }
	    }

		near_frame->_audioChannel = 1;
		near_frame->_frequencyInHz = 8000;
		near_frame->_payloadDataLengthInSamples = 160;

		for(int i=0; i< 160; i++) {
			near_frame->_payloadData[i] = input[i];
		}

		apm->ProcessStream(near_frame);

		memcpy(output, near_frame->_payloadData, 160 * sizeof(short));
	} else {

		memset(output, 0, 160 * sizeof(short));
	}

	return 0;
}

void ingenic_apm_destroy(){
        AudioProcessing::Destroy(apm);
	apm = NULL;

	if (near_frame != NULL) {
	      delete near_frame;
	      near_frame = NULL;
	}

	WebRtcVad_Free(vadInst);
}

