#include <errno.h>
#include <pthread.h>
#include <stdint.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdlib.h>

#include <cutils/log.h>
#include <cutils/properties.h>
#include <cutils/str_parms.h>

#include <hardware/hardware.h>
#include <system/audio.h>
#include <hardware/audio.h>
#include <tinyalsa/asoundlib.h>

#include <math.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <linux/llist.h>
#include <linux/soundcard.h>
#include <audio_utils/resampler.h>
#include <audio_route/audio_route.h>
#include "audio_hw.h"

/*
 *once we know exactly what device we want , we open real device here by the order
 *  of 1.dmic,  2.codec.
 *  if dmic support, don't open codec.
 * */

#define DMIC_SET_SAMPLERATE 0x103

int dmic_open(struct audio_device *alsadev)
{
	int kdevice = -1;
	int ret = -ENODEV;
	int status = -1;

		if(access(DMIC_DEVICE, R_OK) == 0) {
			alsadev->dev_fd = open(DMIC_DEVICE, O_RDONLY);
			ALOGE("dmic dev_fd = %d ",alsadev->dev_fd);
			if(alsadev->dev_fd < 0){
				status = -errno;
				ALOGE("%s(line:%d): open dmic audio device error %d", __func__, __LINE__,status);
				alsadev->dev_fd = -1;
			} else {
					/* config dmic parameters */
					uint32_t temp;
					temp = 8000;
					ret = ioctl(alsadev->dev_fd, DMIC_SET_SAMPLERATE, &temp);
					if(ret < 0) {
						ALOGE("%s(line:%d): dmic set samplerate failed. does not support.SR(%d)",
								__func__, __LINE__,
								temp);
					}
					/* dmic fix to 16bit, 16KsampleRate, */
					ALOGE("dmic open success  !");
					alsadev->dmicMode = true;
					return ret;
				}
			}

	return ret;
}

ssize_t dmic_read(struct stream_in *in, void* buffer, size_t bytes)
{

	/* read data from device */
	size_t count;
	int bytesRead = 0;
	uint8_t* p = (uint8_t*)buffer;
	ALOGE("dmic_read, dev_fd:%d", in->dev->dev_fd);

	if (in == NULL)
	    return -ENODEV;
	ALOGV("count %d",bytes);

	if (bytes%2 != 0)
		count = bytes -1;
	else
		count = bytes;

	while (count) {
		bytesRead = read(in->dev->dev_fd, p, count);
		if (bytesRead < 0) {
			ALOGE("%s(line:%d): read error while recording, return -EIO!", __func__, __LINE__);
			close(in->dev->dev_fd);
			return -EIO;
		}

		count -= bytesRead;
		p += bytesRead;
	}

	if (bytes%2 !=0)
		return bytes - 1;
	else
		return bytes;
}


ssize_t dmic_read_2(struct audio_device *alsadev, void* buffer, size_t bytes)
{
    /* read data from device */
    size_t count;
    int bytesRead = 0;
    uint8_t* p = (uint8_t*)buffer;

    if (alsadev == NULL)
        return -ENODEV;

    if (bytes%2 != 0)
        count = bytes -1;
    else
        count = bytes;

    while (count) {
        bytesRead = read(alsadev->dev_fd, p, count);
        if (bytesRead < 0) {
            ALOGE("%s(line:%d): read error while recording, return -EIO!", __func__, __LINE__);
            close(alsadev->dev_fd);
            return -EIO;
        }

        count -= bytesRead;
        p += bytesRead;
    }

    if (bytes%2 !=0)
        return bytes - 1;
    else
        return bytes;
}

void dmic_close(struct audio_device *alsadev)
{
	ALOGE("dmic_close, dev_fd:%d", alsadev->dev_fd);
	if (alsadev->dev_fd >= 0) {
		close(alsadev->dev_fd);
	}
	alsadev->dmicMode = false;
	alsadev->dev_fd = -1;
}
