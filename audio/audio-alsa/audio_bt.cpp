/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Base on /audio-oss/primary/audio_hw.c
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define LOG_TAG "audio_hw_primary"
#define LOG_NDEBUG 0
#include <errno.h>
#include <pthread.h>
#include <stdint.h>
#include <sys/time.h>
#include <stdlib.h>
#include <cutils/log.h>
#include <math.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <linux/llist.h>
#include <linux/soundcard.h>
#include "audio_hw.h"
#include "webrtc_aec.h"

#define WEBRTC_ECHO_CANCELLATION
#ifdef WEBRTC_ECHO_CANCELLATION
#include "audio_processing.h"
#include "audio_buffer.h"
#include "webrtc_vad.h"
#include "module_common_types.h"
#include "tick_util.h"

using webrtc::AudioFrame;
using webrtc::AudioBuffer;
using webrtc::GainControl;
using webrtc::AudioProcessing;
using webrtc::NoiseSuppression;
using webrtc::EchoControlMobile;
using webrtc::VoiceDetection;
using webrtc::VadModes;

static AudioProcessing *apm = NULL;
static AudioFrame *far_frame = NULL;
static AudioFrame *near_frame = NULL;

#endif

extern int dmic_open(struct audio_device *alsadev);
extern ssize_t dmic_read_2(struct audio_device *alsadev, void* buffer, size_t bytes);
extern void dmic_close(struct audio_device *alsadev);
void do_out_standby(struct stream_out *out);
void do_in_standby(struct stream_in *in);

struct pcm_config pcm_config_bt_sco_out = {
	.channels = 1,
	.rate = BT_SAMPLING_RATE,
	.period_size = BT_PERIOD_SIZE,
	.period_count = BT_PERIOD_COUNT,
	.format = PCM_FORMAT_S16_LE,
	.start_threshold = (BT_PERIOD_SIZE * (BT_PERIOD_COUNT - 1)),
};
struct pcm_config pcm_config_bt_sco_in = {
	.channels = 1,
	.rate = BT_SAMPLING_RATE,
	.period_size = BT_PERIOD_SIZE,
	.period_count = BT_PERIOD_COUNT,
	.format = PCM_FORMAT_S16_LE,
	.start_threshold = BT_PERIOD_SIZE,
	.stop_threshold = BT_PERIOD_SIZE * (BT_PERIOD_COUNT - 1),
};
struct pcm_config pcm_config_bt_voc_out = {
	.channels = 2,
	.rate = BT_SAMPLING_RATE,
	.period_size = BT_PERIOD_SIZE,
	.period_count = BT_PERIOD_COUNT,
	.format = PCM_FORMAT_S16_LE,
	.start_threshold = (BT_PERIOD_SIZE * (BT_PERIOD_COUNT - 1)),
};

struct pcm_config pcm_config_bt_voc_in = {
	.channels = 2,
	.rate = BT_SAMPLING_RATE,
	.period_size = BT_PERIOD_SIZE,
	.period_count = BT_PERIOD_COUNT,
	.format = PCM_FORMAT_S16_LE,
	.start_threshold = BT_PERIOD_SIZE,
	.stop_threshold = BT_PERIOD_SIZE * (BT_PERIOD_COUNT - 1),
};

typedef struct {
	struct list_head list;
	unsigned int size;
	char *addr;
} ADnode;

typedef struct {
	struct list_head    free_node_list;
	struct list_head    use_node_list;
	ADnode node[BT_PERIOD_COUNT];
	int free_cnt;
	int use_cnt;
	char *mem;
	pthread_mutex_t     slock;
} ADstream;
ADstream bt_dstream, bt_ustream;

pthread_cond_t dstream_rdy_sig = PTHREAD_COND_INITIALIZER;
pthread_cond_t ustream_rdy_sig = PTHREAD_COND_INITIALIZER;
pthread_mutex_t dstream_rdy_lock;
pthread_mutex_t ustream_rdy_lock;

struct bt_context {
	pthread_t dr_thread;
	pthread_t dw_thread;
	pthread_t ur_thread;
	pthread_t uw_thread;
} bt_dev;

#ifdef WEBRTC_ECHO_CANCELLATION
#define ECHO_FIFO_DEPTH      (10)
typedef struct {
	char * mem;
        int fifo_rp;
        int fifo_wp;
        pthread_cond_t fifo_condition;
        pthread_mutex_t fifo_lock;
} EchoFifo;
EchoFifo echo_stream={0}, ref_stream={0};
#endif

static ADnode *get_free_node(ADstream *stream)
{
	struct list_head *phead = &stream->free_node_list;
	ADnode *node = NULL;

	pthread_mutex_lock(&stream->slock);
	if (list_empty(phead)) {
		pthread_mutex_unlock(&stream->slock);
		return NULL;
	}

	node = list_entry(phead->next, ADnode, list);
	list_del(phead->next);
	stream->free_cnt--;
	pthread_mutex_unlock(&stream->slock);
	return node;
}

static void put_free_node(ADstream *stream, ADnode *node)
{
	struct list_head *phead = &stream->free_node_list;

	pthread_mutex_lock(&stream->slock);
	node->size = 0;
	list_add_tail(&node->list, phead);
	stream->free_cnt++;
	pthread_mutex_unlock(&stream->slock);
}

static void put_use_node(ADstream *stream, ADnode *node, int size)
{
	struct list_head *phead = &stream->use_node_list;

	pthread_mutex_lock(&stream->slock);
	node->size = size;
	list_add_tail(&node->list, phead);
	stream->use_cnt++;
	pthread_mutex_unlock(&stream->slock);
}

static void put_use_node_head(ADstream *stream,  ADnode *node)
{
	struct list_head *phead = &stream->use_node_list;

	pthread_mutex_lock(&stream->slock);
	list_add(&node->list, phead);
	stream->use_cnt++;
	pthread_mutex_unlock(&stream->slock);
}
static ADnode *get_use_node(ADstream *stream)
{
	struct list_head *phead = &stream->use_node_list;
	ADnode *node = NULL;

	pthread_mutex_lock(&stream->slock);
	if (list_empty(phead)) {
		pthread_mutex_unlock(&stream->slock);
		return NULL;
	}

	node = list_entry(phead->next, ADnode, list);
	list_del(phead->next);
	stream->use_cnt--;
	pthread_mutex_unlock(&stream->slock);

	return node;
}

static void *dr_thread_func(void *_dev)
{
	struct audio_device *adev = (audio_device *)_dev;
	ADnode *node = NULL;
	int ret;

	if (!adev->voc_in->pcm)
		return NULL;

#ifdef WEBRTC_ECHO_CANCELLATION
	apm = AudioProcessing::Create(0);
	assert(apm != NULL);

	int32_t sample_rate_hz = 8000;
	int32_t device_sample_rate_hz = 8000;

	int num_capture_input_channels = 1;
	int num_capture_output_channels = 1;
	int num_render_channels = 1;
	int samples_per_channel = 160;

	apm->Initialize();

	apm->set_sample_rate_hz(sample_rate_hz);
	apm->set_num_channels(num_capture_input_channels, num_capture_output_channels);
	apm->set_num_reverse_channels(num_render_channels);

	//enable ns
	apm->noise_suppression()->Enable(true);
	apm->noise_suppression()->set_level(NoiseSuppression::kVeryHigh);  //kModerate default


	apm->level_estimator()->Enable(true);
	apm->high_pass_filter()->Enable(true);
	apm->gain_control()->Enable(true);
	apm->gain_control()->set_mode(GainControl::kFixedDigital);  //kAdaptiveAnalog default
	apm->gain_control()->set_compression_gain_db(9);
	apm->gain_control()->set_target_level_dbfs(9);    //Limited to [0, 31];


	near_frame = new AudioFrame();

	near_frame->_audioChannel = 1;
	near_frame->_frequencyInHz = 8000;
	near_frame->_payloadDataLengthInSamples = samples_per_channel;
	ingenic_apm_init();

	int frame_size = 160;
	char *result_buf = (char *)calloc(1, BT_PERIOD_SIZE*10);
	char *result_buf1 = (char *)calloc(1, BT_PERIOD_SIZE*10);
	int total_size = 0, process_size = 0, pos= 0;
	int sub_cnt = 0, res_cnt = 0, tag = 0;
	int result_cnt = 0;
	int frame_len = frame_size * sizeof(short);
	int i = 0, size = 0;
#endif

	ALOGV("%s enter\n", __func__);
	while (adev->btMode) {
		node = get_free_node(&bt_dstream);
		if (node != NULL) {
#ifdef WEBRTC_ECHO_CANCELLATION
			while(1) {
			if (adev->dmicMode && (adev->dev_fd >= 0)) {
				ret = dmic_read_2(adev, ref_stream.mem+ref_stream.fifo_wp*BT_PERIOD_SIZE, BT_PERIOD_SIZE);
				size = ret;
			} else {
				ret = pcm_read(adev->voc_in->pcm, node->addr, BT_PERIOD_SIZE);
				if (!ret) {
					size = BT_PERIOD_SIZE;
				} else {
					ALOGV("%s %d pcm_read error\n",__func__, __LINE__);
				}
			}
				total_size += size;
				if (total_size >= BT_PERIOD_SIZE) {
					pthread_mutex_lock(&echo_stream.fifo_lock);
					//ALOGV("lockfifo %d echo_stream.fifo_wp = %d echo_stream.fifo_rp = %d\n", __LINE__, echo_stream.fifo_wp, echo_stream.fifo_rp);
					if(echo_stream.fifo_wp == echo_stream.fifo_rp){
						if(adev->btMode){
							pthread_cond_wait(&echo_stream.fifo_condition, &echo_stream.fifo_lock);
						}else{
							pthread_mutex_unlock(&echo_stream.fifo_lock);
							break;
						}
					}
					sub_cnt = total_size / frame_len;
					res_cnt = total_size % frame_len;

					short*ref_buf=(short *)(ref_stream.mem + pos);
					short*echo_buf=(short *)(echo_stream.mem + pos);
					short*e_buf=(short *)(result_buf1 + pos);

					for(i=0; i<sub_cnt; i++){
						ingenic_apm_set_far_frame(echo_buf);
						ingenic_apm_set_near_frame(ref_buf, e_buf);
						ref_buf+=frame_size;
						echo_buf+=frame_size;
						e_buf+=frame_size;
					}

					process_size += sub_cnt * frame_len;
					total_size -= sub_cnt * frame_len;

					pos += sub_cnt * frame_len;
					if (pos >= 10 * BT_PERIOD_SIZE) pos =  0;

					int tmp_fifowp_n1=(echo_stream.fifo_wp==(ECHO_FIFO_DEPTH-1))?0:(echo_stream.fifo_wp+1);
					int tmp_fifowp_n2=(tmp_fifowp_n1==(ECHO_FIFO_DEPTH-1))?0:(tmp_fifowp_n1+1);
					if(tmp_fifowp_n2==echo_stream.fifo_rp)
						pthread_cond_signal(&echo_stream.fifo_condition);

					echo_stream.fifo_rp=(echo_stream.fifo_rp==(ECHO_FIFO_DEPTH-1))?0:(echo_stream.fifo_rp+1);
					pthread_mutex_unlock(&echo_stream.fifo_lock);
					//ALOGV("unlock fifo%d\n", __LINE__);

					ref_stream.fifo_wp=(ref_stream.fifo_wp==(ECHO_FIFO_DEPTH-1))?0:(ref_stream.fifo_wp+1);
					if (process_size >= BT_PERIOD_SIZE){
						memcpy(node->addr,result_buf1 + result_cnt * BT_PERIOD_SIZE,BT_PERIOD_SIZE);
						process_size -= BT_PERIOD_SIZE;
						put_use_node(&bt_dstream, node, size);
						result_cnt = (result_cnt==(ECHO_FIFO_DEPTH-1))?0:(result_cnt+1);
						if (bt_dstream.use_cnt == 1)
							pthread_cond_signal(&dstream_rdy_sig);
						break;
					}
				}
				}//end of while(1)
#else
			if (adev->dmicMode && (adev->dev_fd >= 0)) {
				ret = dmic_read_2(adev, node->addr, (BT_PERIOD_SIZE));
				if (ret >= 0) {
					put_use_node(&bt_dstream, node, (BT_PERIOD_SIZE));
					if (bt_dstream.use_cnt == 1) {
						pthread_cond_signal(&dstream_rdy_sig);
					}
				}
			} else {
				ret = pcm_read(adev->voc_in->pcm, node->addr, (BT_PERIOD_SIZE * 2 * 2));
				if (!ret) {
					int i;
					/*just read stereo data from jz i2s interface*/
					for (i = 0; i < BT_PERIOD_SIZE; i++) {
						((unsigned short *)node->addr)[i] =
							((unsigned short *)node->addr)[(2*i)+1];
					}
					put_use_node(&bt_dstream, node, (BT_PERIOD_SIZE * 2));
					if (bt_dstream.use_cnt == 1)
						pthread_cond_signal(&dstream_rdy_sig);
				}
			}
#endif
		} else {
			pthread_mutex_lock(&dstream_rdy_lock);
			if (bt_dstream.free_cnt <= 0)
				pthread_cond_wait(&dstream_rdy_sig, &dstream_rdy_lock);
			pthread_mutex_unlock(&dstream_rdy_lock);
		}
	}
#ifdef WEBRTC_ECHO_CANCELLATION
	ingenic_apm_destroy();
	AudioProcessing::Destroy(apm);
	apm = NULL;
	if (near_frame != NULL) {
		  delete near_frame;
		  near_frame = NULL;
	}
	free(result_buf);
	free(result_buf1);
#endif
	ALOGV("%s leave\n", __func__);
	return NULL;
}

static void *dw_thread_func(void *_dev)
{
	struct audio_device *adev = (audio_device *)_dev;
	ADnode *node = NULL;

	if (!adev->sco_out->pcm)
		return NULL;

	ALOGV("%s enter\n", __func__);
	while (adev->btMode) {
		node = get_use_node(&bt_dstream);
		if (node != NULL) {
			if (pcm_write(adev->sco_out->pcm, node->addr, node->size)) {
				put_use_node_head(&bt_dstream, node);
			} else {
				put_free_node(&bt_dstream, node);
			}
			if (bt_dstream.free_cnt == 1)
				pthread_cond_signal(&dstream_rdy_sig);
		} else {
			pthread_mutex_lock(&dstream_rdy_lock);
			if (bt_dstream.use_cnt <= 0)
				pthread_cond_wait(&dstream_rdy_sig,&dstream_rdy_lock);
			pthread_mutex_unlock(&dstream_rdy_lock);
		}
	}

	ALOGV("%s leave\n", __func__);
	return NULL;
}

static void *ur_thread_func(void *_dev)
{
	struct audio_device *adev = (audio_device *)_dev;
	ADnode *node = NULL;
	int ret;

	if (!adev->sco_in->pcm)
		return NULL;

	ALOGV("%s enter\n", __func__);
	while (adev->btMode) {
		node = get_free_node(&bt_ustream);
		if (node != NULL) {
			ret = pcm_read(adev->sco_in->pcm, node->addr, (BT_PERIOD_SIZE));
			if (!ret) {
				int i;
				for (i = BT_PERIOD_SIZE - 1; i >= 0  ;i--) {
					((unsigned short *)node->addr)[i * 2] = ((unsigned short *)node->addr)[i];
					((unsigned short *)node->addr)[i * 2 + 1] = ((unsigned short *)node->addr)[i];
				}
				put_use_node(&bt_ustream, node, (BT_PERIOD_SIZE * 2));
				if (bt_ustream.use_cnt == 1)
					pthread_cond_signal(&ustream_rdy_sig);
			}
		} else {
			pthread_mutex_lock(&ustream_rdy_lock);
			if (bt_ustream.free_cnt <= 0)
				pthread_cond_wait(&ustream_rdy_sig, &ustream_rdy_lock);
			pthread_mutex_unlock(&ustream_rdy_lock);
		}
	}

	ALOGV("%s leave\n", __func__);
	return NULL;
}

static void *uw_thread_func(void *_dev)
{
	struct audio_device *adev = (audio_device *)_dev;
	ADnode *node = NULL;

	if (!adev->voc_out->pcm)
		return NULL;

	ALOGV("%s enter\n", __func__);
	while (adev->btMode) {
		node = get_use_node(&bt_ustream);
		if (node != NULL) {
#ifdef WEBRTC_ECHO_CANCELLATION
			pthread_mutex_lock(&echo_stream.fifo_lock);
			memcpy(echo_stream.mem+echo_stream.fifo_wp*BT_PERIOD_SIZE,node->addr,(node->size));
			if(echo_stream.fifo_wp==echo_stream.fifo_rp)
			        pthread_cond_signal(&echo_stream.fifo_condition);

			int tmp_fifowp_n1=(echo_stream.fifo_wp==(ECHO_FIFO_DEPTH-1))?0:(echo_stream.fifo_wp+1);
			int tmp_fifowp_n2=(tmp_fifowp_n1==(ECHO_FIFO_DEPTH-1))?0:(tmp_fifowp_n1+1);

			if(tmp_fifowp_n2==echo_stream.fifo_rp){
			        pthread_cond_wait(&echo_stream.fifo_condition, &echo_stream.fifo_lock);
			}
			echo_stream.fifo_wp=tmp_fifowp_n1;
			pthread_mutex_unlock(&echo_stream.fifo_lock);
/*
			short *this_buffer =(short *) node->addr;
			for(int i = 0; i < node->size; i++) {
				this_buffer[i] <<= 1;
			}
*/
#endif
			if (pcm_write(adev->voc_out->pcm, node->addr, (node->size))) {
				ALOGE("%s write failed: %s", __func__,
						pcm_get_error(adev->voc_out->pcm));
				put_use_node_head(&bt_ustream, node);
			} else {
				put_free_node(&bt_ustream, node);
			}
			if (bt_ustream.free_cnt == 1)
				pthread_cond_signal(&ustream_rdy_sig);
		} else {
			pthread_mutex_lock(&ustream_rdy_lock);
			if (bt_ustream.use_cnt <= 0)
				pthread_cond_wait(&ustream_rdy_sig, &ustream_rdy_lock);
			pthread_mutex_unlock(&ustream_rdy_lock);
		}
	}
	ALOGV("%s leave\n", __func__);

	return NULL;
}

int bt_stream_init(struct audio_device *adev)
{
	struct stream_out *sco_out = NULL;
	struct stream_in *sco_in = NULL;
	ADnode *node = NULL;
	int i = 0;

	ALOGV("%s enter\n", __func__);
	adev->btMode = false;
	adev->sco_out = (struct stream_out *)calloc(1, sizeof(struct stream_out));
	if (!adev->sco_out)
		return -ENOMEM;
	adev->sco_out->dev = adev;
	adev->sco_out->pcm = NULL;

	adev->sco_in = (struct stream_in *)calloc(1, sizeof(struct stream_in));
	if (!adev->sco_in)
		return -ENOMEM;
	adev->sco_in->dev = adev;
	adev->sco_in->pcm = NULL;

	adev->voc_out = (struct stream_out *)calloc(1, sizeof(struct stream_out));
	if (!adev->voc_out)
		return -ENOMEM;
	adev->voc_out->dev = adev;
	adev->voc_out->pcm = NULL;

	adev->voc_in = (struct stream_in *)calloc(1, sizeof(struct stream_in));
	if (!adev->voc_in)
		return -ENOMEM;
	adev->voc_in->dev = adev;
	adev->voc_in->pcm = NULL;

	if (bt_dstream.mem == NULL) {
		/*mem period_size * period_count * format_size * max_channel * dirs */
		bt_dstream.mem = (char *)calloc(sizeof(char), (BT_PERIOD_SIZE * BT_PERIOD_COUNT * 2 * 2 * 2));
		if (!bt_dstream.mem) {
			ALOGE(" calloc bt mem error! ");
			return -ENOMEM;
		} else {
			bt_ustream.mem = bt_dstream.mem + (BT_PERIOD_SIZE * BT_PERIOD_COUNT * 2 * 2);
		}
	}

#ifdef WEBRTC_ECHO_CANCELLATION
	if (echo_stream.mem == NULL) {
		echo_stream.mem = (char *)calloc(1, ECHO_FIFO_DEPTH*BT_PERIOD_SIZE);
		if (!echo_stream.mem) {
			ALOGE(" calloc echo_fifo error! ");
			return -ENOMEM;
		}
		memset(echo_stream.mem, 0, ECHO_FIFO_DEPTH*BT_PERIOD_SIZE);
	}
	echo_stream.fifo_rp=0;
	echo_stream.fifo_wp=0;

	if (ref_stream.mem == NULL) {
		ref_stream.mem = (char *)calloc(1, ECHO_FIFO_DEPTH*BT_PERIOD_SIZE);
		if (!ref_stream.mem) {
			ALOGE(" calloc ref_fifo error! ");
			return -ENOMEM;
		}
		memset(ref_stream.mem, 0, ECHO_FIFO_DEPTH*BT_PERIOD_SIZE);
	}
	ref_stream.fifo_rp=0;
	ref_stream.fifo_wp=0;
#endif

	ALOGV("%s leave\n", __func__);
	return 0;
}

int bt_start_call(struct audio_device *adev)
{
	int ret = -ENODEV;
	int i;
	ADnode *node = NULL;

	ALOGV("%s enter\n", __func__);
	adev->btMode = true;
	list_init(&bt_dstream.free_node_list);
	list_init(&bt_ustream.free_node_list);
	list_init(&bt_dstream.use_node_list);
	list_init(&bt_ustream.use_node_list);

	for (i=0; i < BT_PERIOD_COUNT; i++) {
		node = &bt_dstream.node[i];
		node->addr = bt_dstream.mem + i * (BT_PERIOD_SIZE * 2 * 2);
		node->size = 0;
		list_add(&node->list, &bt_dstream.free_node_list);
	}

	for (i=0; i < BT_PERIOD_COUNT; i++) {
		node = &bt_ustream.node[i];
		node->addr = bt_ustream.mem + i * (BT_PERIOD_SIZE * 2 * 2);
		node->size = 0;
		list_add(&node->list, &bt_ustream.free_node_list);
	}

	bt_dstream.free_cnt = BT_PERIOD_COUNT;
	bt_ustream.free_cnt = BT_PERIOD_COUNT;
	bt_dstream.use_cnt = 0;
	bt_ustream.use_cnt = 0;

	if (adev->active_out && adev->active_out->pcm)
		do_out_standby(adev->active_out);
	if (adev->active_in && adev->active_in->pcm)
		do_in_standby(adev->active_in);

	adev->sco_in->pcm_config = &pcm_config_bt_sco_in;
	adev->sco_in->pcm = pcm_open(PCM_CARD, PCM_DEVICE_SCO, PCM_IN, adev->sco_in->pcm_config);
	if (!adev->sco_in->pcm || !pcm_is_ready(adev->sco_in->pcm)) {
		ALOGE("pcm_open bt in failed: %s", pcm_get_error(adev->sco_in->pcm));
		goto err_sco_in;
	}

	adev->sco_out->pcm_config = &pcm_config_bt_sco_out;
	adev->sco_out->pcm = pcm_open(PCM_CARD, PCM_DEVICE_SCO, PCM_OUT, adev->sco_out->pcm_config);
	if (!adev->sco_out->pcm || !pcm_is_ready(adev->sco_out->pcm)) {
		ALOGE("pcm_open bt out failed: %s", pcm_get_error(adev->sco_out->pcm));
		goto err_sco_out;
	}

	adev->voc_out->pcm_config = &pcm_config_bt_voc_out;
	adev->voc_out->pcm = pcm_open(PCM_CARD, PCM_DEVICE, PCM_OUT, adev->voc_out->pcm_config);
	if (!adev->voc_out->pcm || !pcm_is_ready(adev->voc_out->pcm)) {
		ALOGE("pcm_open voc out failed: %s", pcm_get_error(adev->voc_out->pcm));
		goto err_voc_out;
	}

	if (adev->dmicMode && (adev->dev_fd >= 0)) {
		ALOGE("dmic already open");
	} else {
		dmic_open(adev);
	}
	adev->voc_in->pcm_config = &pcm_config_bt_voc_in;
	adev->voc_in->pcm = pcm_open(PCM_CARD, PCM_DEVICE, PCM_IN, adev->voc_in->pcm_config);
	if (!adev->voc_in->pcm || !pcm_is_ready(adev->voc_in->pcm)) {
		ALOGE("pcm_open voc in failed: %s", pcm_get_error(adev->voc_in->pcm));
		goto err_voc_in;
	}

#ifdef WEBRTC_ECHO_CANCELLATION
	if (echo_stream.mem == NULL) {
		echo_stream.mem = (char *)calloc(1, ECHO_FIFO_DEPTH*BT_PERIOD_SIZE);
		if (!echo_stream.mem) {
			ALOGE(" calloc echo_fifo error! ");
			goto err_voc_in;
		}
		memset(echo_stream.mem, 0, ECHO_FIFO_DEPTH*BT_PERIOD_SIZE);
	}
	echo_stream.fifo_rp=0;
	echo_stream.fifo_wp=0;

	if (ref_stream.mem == NULL) {
		ref_stream.mem = (char *)calloc(1, ECHO_FIFO_DEPTH*BT_PERIOD_SIZE);
		if (!ref_stream.mem) {
			ALOGE(" calloc ref_fifo error! ");
			goto err_voc_in;
		}
		memset(ref_stream.mem, 0, ECHO_FIFO_DEPTH*BT_PERIOD_SIZE);
	}
	ref_stream.fifo_rp=0;
	ref_stream.fifo_wp=0;
#endif

	pthread_create(&bt_dev.dr_thread, NULL, dr_thread_func, adev);
	pthread_create(&bt_dev.ur_thread, NULL, ur_thread_func, adev);
	pthread_create(&bt_dev.dw_thread, NULL, dw_thread_func, adev);
	pthread_create(&bt_dev.uw_thread, NULL, uw_thread_func, adev);
	ALOGV("%s leave\n", __func__);
	return 0;
err_voc_in:
	adev->voc_in->pcm = NULL;
	pcm_close(adev->voc_out->pcm);
err_voc_out:
	adev->voc_out->pcm == NULL;
	pcm_close(adev->sco_out->pcm);
err_sco_out:
	adev->sco_out->pcm = NULL;
	pcm_close(adev->sco_in->pcm);
err_sco_in:
	adev->sco_in->pcm = NULL;
	return ret;
}

void bt_end_call(struct audio_device *adev)
{
	ALOGV("%s enter\n", __func__);

	adev->btMode = false;
#ifdef WEBRTC_ECHO_CANCELLATION
	pthread_cond_signal(&echo_stream.fifo_condition);
#endif

	pthread_cond_signal(&dstream_rdy_sig);
	pthread_cond_signal(&ustream_rdy_sig);

	if (adev->sco_in->pcm) {
		/*
		 * Hardware bus(like i2s, pcm) may drop the clock, when we read,
		 * so the read will not comming back soon, may be 10s or more longger
		 * whe stop it here.
		 */
		pcm_stop(adev->sco_in->pcm);
		pthread_join(bt_dev.ur_thread, NULL);
		pcm_close(adev->sco_in->pcm);
		adev->sco_in->pcm = NULL;
	}

	if (adev->sco_out->pcm) {
		pcm_stop(adev->sco_out->pcm);
		pthread_join(bt_dev.dw_thread, NULL);
		pcm_close(adev->sco_out->pcm);
		adev->sco_out->pcm = NULL;
	}

	if (adev->voc_in->pcm) {
		pcm_stop(adev->voc_in->pcm);
		pthread_join(bt_dev.dr_thread, NULL);
		pcm_close(adev->voc_in->pcm);
		adev->voc_in->pcm = NULL;
		dmic_close(adev);
	}

	if (adev->voc_out->pcm) {
		pcm_stop(adev->voc_out->pcm);
		pthread_join(bt_dev.uw_thread, NULL);
		pcm_close(adev->voc_out->pcm);
		adev->voc_out->pcm = NULL;
	}
	if (bt_dstream.mem != NULL)
		memset(bt_dstream.mem, 0, BT_PERIOD_SIZE*BT_PERIOD_COUNT * 8);

#ifdef WEBRTC_ECHO_CANCELLATION
	if (echo_stream.mem != NULL) {
	        echo_stream.fifo_rp=0;
		echo_stream.fifo_wp=0;
	        free(echo_stream.mem);
		echo_stream.mem = NULL;
	}
	if (ref_stream.mem != NULL) {
	        ref_stream.fifo_rp=0;
		ref_stream.fifo_wp=0;
	        free(ref_stream.mem);
		ref_stream.mem = NULL;
	}
#endif

	ALOGV("%s leave\n",__func__);
}
