# Copyright (C) 2012 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

ifeq ($(AUDIO_HAL_TYPE),alsa)
LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)
LOCAL_MODULE := audio.primary.$(TARGET_BOOTLOADER_BOARD_NAME)
LOCAL_MODULE_PATH := $(TARGET_OUT_SHARED_LIBRARIES)/hw
LOCAL_SRC_FILES := \
	audio_bt.cpp audio_hw.cpp dmic.cpp webrtc_watch_aec.cpp
LOCAL_C_INCLUDES += \
	external/tinyalsa/include \
	$(call include-path-for, audio-utils) \
	$(call include-path-for, audio-route) \
	external/webrtc/src  \
			external/webrtc/src/modules/audio_processing \
			external/webrtc/src/common_audio/signal_processing/include \
			external/webrtc/src/modules/audio_processing/include \
			external/webrtc/src/modules/audio_processing/aec  \
			external/webrtc/src/modules/interface  \
			external/webrtc/src/modules/audio_processing/interface  \
			external/webrtc/src/modules/audio_processing/aecm/include \
			external/webrtc/src/modules/audio_processing/aecm \
			external/webrtc/src/system_wrappers/interface  \
			external/webrtc/src/common_audio/vad/include

LOCAL_SHARED_LIBRARIES := liblog libcutils libtinyalsa libaudioutils libaudioroute libwebrtc_audio_preprocessing
LOCAL_MODULE_TAGS := optional
include $(BUILD_SHARED_LIBRARY)
endif

