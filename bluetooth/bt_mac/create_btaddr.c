/******************************************************************************/

// TODO: create a file about bt_addr , bt mac address

#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <errno.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <stdlib.h>
#include <time.h>
#include <sys/time.h>

#ifdef ANDROID
#include <termios.h>
#else
#include <sys/termios.h>
#include <sys/ioctl.h>
#include <limits.h>
#endif

#include <string.h>
#include <signal.h>

#ifdef ANDROID
#include <cutils/properties.h>
#define LOG_TAG "brcm_patchram_plus"
#include <cutils/log.h>
#undef printf
#define printf ALOGD
#undef fprintf
#define fprintf(x, ...) \
  { if(x==stderr) ALOGE(__VA_ARGS__); else fprintf(x, __VA_ARGS__); }

#endif //ANDROID


#define BT_MAC_PATH 	"/data/misc/bluetooth/bt_addr"


int bt_mac[6];


int set_mac_address_random(void)
{
	unsigned char bdaddr[18];
//	unsigned char bdaddr_path[PROPERTY_VALUE_MAX];
//	int fd;
	unsigned rand_mac = 0;
	int i;
	struct timeval t_rand;

	gettimeofday(&t_rand, NULL);
	i = t_rand.tv_usec & 0xFFFF;
	//ALOGE("a11 %d %lld", i, t_rand.tv_usec);

	srandom((unsigned)i);
	i = random();
	usleep(12*1000);
	gettimeofday(&t_rand, NULL);
	i = i ^ (t_rand.tv_usec & 0xFFFF);
	//ALOGE("a11 %d %lld", i, t_rand.tv_usec);
	srandom((unsigned)i);
	rand_mac = random();

	bt_mac[0] = 0xD0;
	bt_mac[1] = 0x31;
	bt_mac[2] = 0x10;
	bt_mac[3] = (unsigned char)(rand_mac & 0xFF);
	bt_mac[4] = (unsigned char)(rand_mac >> 8);
	bt_mac[5] = (unsigned char)(rand_mac >> 16);

	sprintf(bdaddr, "%02x:%02x:%02x:%02x:%02x:%02x", bt_mac[0], bt_mac[1],
			bt_mac[2], bt_mac[3], bt_mac[4], bt_mac[5]);
	ALOGD("bluetooth mac address %s\n", bdaddr);
	return 0;
}

int create_BT_MAC_file(void)
{
	int fd;
	char bt_addr[18];
	fd = open(BT_MAC_PATH, O_WRONLY | O_CREAT,0444);
	if(fd < 0){
		fprintf(stderr,"can't create the file :%s\n",BT_MAC_PATH);
		return errno;
	}

	sprintf(bt_addr, "%02x:%02x:%02x:%02x:%02x:%02x", bt_mac[0], bt_mac[1],
			bt_mac[2], bt_mac[3], bt_mac[4], bt_mac[5]);

	write(fd,bt_addr,18);
	close(fd);

	return 0;
}


int get_mac_address(void)
{
	int fd,ret,count = 0,i = 0,len;
	char* path = BT_MAC_PATH;
	char bt_addr[PROPERTY_VALUE_MAX];

	fd = open(path,O_RDONLY);
	if(fd < 0){
		if(errno == ENOENT){   
			/*if the mac_file does not exist ,then create it by two ways
			 * 1. get property ro.bt.mac
			 * 2. generate the random mac address
			 */
			ret = property_get("ro.bt.mac", bt_addr, NULL);
			if (ret > 0) {
				sscanf(bt_addr, "%02x%02x%02x%02x%02x%02x", 
						&bt_mac[0], &bt_mac[1],&bt_mac[2], &bt_mac[3], &bt_mac[4], &bt_mac[5]);
				ALOGD("bluetooth mac address from property.\n") ;
			} else {
				set_mac_address_random();
			}

			ret = create_BT_MAC_file();
			return ret;
		}else{
			fprintf(stderr,"can't open the file :%s,%d\n",path,errno);
			return errno;
		}
	}

	close(fd);
	return 0;
}


int main (int argc, char **argv)
{
	ALOGD("create BT MAC ADDR\n");
	get_mac_address();

	return 0;
}
