#
# brcm_patchram_plus.c
#

LOCAL_PATH:= $(call my-dir)

ifeq ($(BTMAC_INTERFACE),efuse) #use efuse crc32 as bt mac addr

include $(CLEAR_VARS)
LOCAL_SRC_FILES := create_btaddr_efuse.c crc32.cpp
LOCAL_MODULE := create_btaddr
LOCAL_MODULE_TAGS := optional
LOCAL_SHARED_LIBRARIES := libcutils liblog
LOCAL_C_FLAGS := \
	-DANDROID
include $(BUILD_EXECUTABLE)

else

ifeq ($(BTMAC_INTERFACE),read_from_flash) #read form flash use for bt mac addr
$(error BTMAC_INTERFACE defined read_from_flash not support now)
else #default use random bt mac
include $(CLEAR_VARS)
LOCAL_SRC_FILES := create_btaddr.c
LOCAL_MODULE := create_btaddr
LOCAL_MODULE_TAGS := optional
LOCAL_SHARED_LIBRARIES := libcutils liblog
LOCAL_C_FLAGS := \
	-DANDROID
include $(BUILD_EXECUTABLE)
endif
endif
