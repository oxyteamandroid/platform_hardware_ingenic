#
# brcm_patchram_plus.c
#

LOCAL_PATH:= $(call my-dir)

ifeq ($(WIFIMAC_INTERFACE),efuse) #use efuse crc32 as wifi mac addr

include $(CLEAR_VARS)
LOCAL_SRC_FILES := create_wifiaddr_efuse.c crc32.cpp
LOCAL_MODULE := create_wifiaddr
LOCAL_MODULE_TAGS := optional
LOCAL_SHARED_LIBRARIES := libcutils liblog
LOCAL_C_FLAGS := \
	-DANDROID
include $(BUILD_EXECUTABLE)

else

ifeq ($(WIFIMAC_INTERFACE),read_from_flash) #read form flash use for wifi mac addr
$(error WIFIMAC_INTERFACE defined read_from_flash not support now)
else #default use random wifi mac
include $(CLEAR_VARS)
LOCAL_SRC_FILES := create_wifiaddr.c
LOCAL_MODULE := create_wifiaddr
LOCAL_MODULE_TAGS := optional
LOCAL_SHARED_LIBRARIES := libcutils liblog
LOCAL_C_FLAGS := \
	-DANDROID
include $(BUILD_EXECUTABLE)
endif
endif
