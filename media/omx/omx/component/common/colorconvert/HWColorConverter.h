/*
 * Copyright (C) 2009 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef HW_COLOR_CONVERTER_H_
#define HW_COLOR_CONVERTER_H_

#include <utils/RefBase.h>
#include <sys/types.h>
#include <stdint.h>
#include <utils/Errors.h>
#include <OMX_Video.h>

#include "IPUConverter.h"
#include "MXUConverter.h"
#include "BitmapParams.h"

namespace android {


class HWColorConverter {
 public:
    HWColorConverter();
    ~HWColorConverter();

    bool setFormat(OMX_COLOR_FORMATTYPE src, OMX_COLOR_FORMATTYPE dst);
    bool getFormat(OMX_COLOR_FORMATTYPE* src, OMX_COLOR_FORMATTYPE* dst);

    struct Range {
	Range();
	Range(uint32_t width, uint32_t height);

	uint32_t mStride;
	uint32_t mWidth;
	uint32_t mHeight;
	uint32_t mCropLeft;
	uint32_t mCropTop;
	uint32_t mCropRight;
	uint32_t mCropBottom;
    };

    bool setSrcRange(const Range& src);
    bool setDstRange(const Range& dst);
    bool setRange(const Range& src, const Range& dst);
    bool getRange(Range* src, Range* dst);

    bool setUseDstGraphicBuffer(bool enableGraphicBuffer);
    bool getUseDstGraphicBuffer(bool* graphicBufferEnabled);

    bool start();
    bool stop();

    struct Params {
	void* mData;
	uint32_t mLength;
	bool mIsRangeSet;
	Range mRange;
    };
    bool convert(Params& in, Params& out);

    enum CONVERTER_TYPE {
	UNKNOWN_CONVERTER,
	SOFT_MXU_CONVERTER,
	HARD_IPU_CONVERTER,
	HARD_X2D_CONVERTER,//to be supported.
	SOFT_420TILE_420PLANAR_CONVERTER,
	SOFT_420TILE_RGB565_CONVERTER,
	SOFT_420ARRAY_420PLANAR_CONVERTER,
	PASS_THROUGH_CONVERTER,
    };

    struct Convertion {
	OMX_COLOR_FORMATTYPE mSrcFmt;
	OMX_COLOR_FORMATTYPE mDstFmt;
	CONVERTER_TYPE mType;
    };

 private:
    Convertion mConvertion;
    uint8_t *mClip;

    IPUConverter* mIPUConverter;
    MXUConverter* mMXUConverter;

    Range mSrcRange;
    Range mDstRange;

    bool mUseDstGraphicBuffer;

    bool mStarted;

    bool convert(
	    const void *srcBits, size_t srcStride,
            size_t srcWidth, size_t srcHeight,
            size_t srcCropLeft, size_t srcCropTop,
            size_t srcCropRight, size_t srcCropBottom,
            void *dstBits, size_t dstStride,
            size_t dstWidth, size_t dstHeight,
            size_t dstCropLeft, size_t dstCropTop,
            size_t dstCropRight, size_t dstCropBottom);

    uint8_t *initClip();

    bool convertFromYUV420TileTo420Planar_select(const BitmapParams& src,
						 const BitmapParams& dst);

    int convertFromYUV420TileTo420Planar(
            const BitmapParams &src, const BitmapParams &dst);

    int convertFromYUV420TileTo420Planar_half(
            const BitmapParams &src, const BitmapParams &dst);

    int convertFromYUV420TileTo420Planar_quarter(
            const BitmapParams &src, const BitmapParams &dst);

    bool convertFromYUV420ArrayTo420Planar_select(const BitmapParams &src,
						  const BitmapParams &dst);
    bool convertFromYUV420ArrayTo420Planar(
            const BitmapParams &src, const BitmapParams &dst);

    bool convertFromYUV420ArrayTo420Planar_half(
	   const BitmapParams &src, const BitmapParams &dst);

    bool convertFromYUV420ArrayTo420Planar_quarter(
      const BitmapParams &src, const BitmapParams &dst);

    bool convertFromYUV420TileToRGB565(
            const BitmapParams &src, const BitmapParams &dst);

    HWColorConverter(const HWColorConverter &);
    HWColorConverter &operator=(const HWColorConverter &);
};

}  // namespace android

#endif  // COLOR_CONVERTER_H_
