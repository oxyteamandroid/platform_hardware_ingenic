/*
 * Copyright (C) 2009 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef MXU_CONVERTER_H_
#define MXU_CONVERTER_H_

#include <utils/RefBase.h>

#include <OMX_Video.h>
#include "Ingenic_OMX_Def.h"

#include "BitmapParams.h"

namespace android {

    class MXUConverter {
    public:
	MXUConverter(OMX_COLOR_FORMATTYPE from, OMX_COLOR_FORMATTYPE to);
	virtual ~MXUConverter();

	bool convert(const BitmapParams& src, const BitmapParams& dst, size_t dstStride);

    private:
	OMX_COLOR_FORMATTYPE mSrcFormat, mDstFormat;

	int mHalFormat;
	int mBytesPerDstPixel;

	bool isValid() const;
    };

}  // namespace android

#endif  // HARDWARE_RENDERER_H_
