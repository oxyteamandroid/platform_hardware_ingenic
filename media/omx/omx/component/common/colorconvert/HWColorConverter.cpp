/*
 * Copyright (C) 2009 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define LOG_TAG "HWColorConverter"
#include <utils/Log.h>
#include "jzmedia.h"

#include "HWColorConverter.h"

#include <ui/GraphicBufferMapper.h>
#include <gui/Surface.h>

#include "gc_gralloc_priv.h"
// #include "hal_public.h"
#define MXU_CONVERT

#ifdef MXU_CONVERT
#define i_pref(hint,base,offset)					\
    ({ __asm__ __volatile__("pref %0,%2(%1)"::"i"(hint),"r"(base),"i"(offset):"memory");})
#endif

namespace android {

static const HWColorConverter::Convertion kSupportedConvertions[] = {
    { (OMX_COLOR_FORMATTYPE)OMX_COLOR_FormatYUV420Tile,           (OMX_COLOR_FORMATTYPE)OMX_COLOR_Format32bitRGBA8888,    HWColorConverter::SOFT_MXU_CONVERTER }, // HARD_IPU_CONVERTER
    { (OMX_COLOR_FORMATTYPE)OMX_COLOR_FormatYUV420Tile,           OMX_COLOR_Format16bitRGB565,                            HWColorConverter::SOFT_420TILE_RGB565_CONVERTER }, // HARD_IPU_CONVERTER
    { (OMX_COLOR_FORMATTYPE)OMX_COLOR_FormatYUV420Tile,           OMX_COLOR_FormatYUV420Planar,                           HWColorConverter::SOFT_420TILE_420PLANAR_CONVERTER },
    { (OMX_COLOR_FORMATTYPE)OMX_COLOR_FormatYUV420ArrayPlanar,    (OMX_COLOR_FORMATTYPE)OMX_COLOR_Format32bitRGBA8888,    HWColorConverter::HARD_IPU_CONVERTER },
    { (OMX_COLOR_FORMATTYPE)OMX_COLOR_FormatYUV420ArrayPlanar,    OMX_COLOR_FormatYUV420Planar,                           HWColorConverter::SOFT_420ARRAY_420PLANAR_CONVERTER },
    { OMX_COLOR_FormatYUV420Planar,                               (OMX_COLOR_FORMATTYPE)OMX_COLOR_Format32bitRGBA8888,    HWColorConverter::HARD_IPU_CONVERTER },
};


HWColorConverter::Range::Range()
    :mStride(0),
     mWidth(0),
     mHeight(0),
     mCropLeft(0),
     mCropTop(0),
     mCropRight(0),
     mCropBottom(0){
}

HWColorConverter::Range::Range(uint32_t width, uint32_t height) {//default one
    mStride = mWidth = width;
    mHeight = height;
    mCropLeft = mCropTop = 0;
    mCropRight = mWidth - 1;
    mCropBottom = mHeight - 1;
}

HWColorConverter::HWColorConverter()
    : mClip(NULL),
      mIPUConverter(NULL),
      mMXUConverter(NULL),
      mUseDstGraphicBuffer(false),
      mStarted(false){
    ALOGE("HWColorConverter::HWColorConverter this:%p", this);

    memset(&mConvertion, 0, sizeof(Convertion));
    memset(&mSrcRange, 0, sizeof(Range));
    memset(&mDstRange, 0, sizeof(Range));
}

HWColorConverter::~HWColorConverter() {
    ALOGE("HWColorConverter::~HWColorConverter this:%p in", this);
    if(mClip != NULL){
	delete[] mClip;
	mClip = NULL;
    }

    if(mIPUConverter != NULL){
	delete mIPUConverter;
	mIPUConverter = NULL;
    }
    if(mMXUConverter != NULL){
	delete mMXUConverter;
	mMXUConverter = NULL;
    }

    ALOGE("HWColorConverter::~HWColorConverter this:%p out", this);
}

//setxxx only works when not started yet.
bool HWColorConverter::setFormat(OMX_COLOR_FORMATTYPE src, OMX_COLOR_FORMATTYPE dst) {
    if(mStarted)
	return false;

    mConvertion.mSrcFmt = src;
    mConvertion.mDstFmt = dst;

    return true;
}

bool HWColorConverter::getFormat(OMX_COLOR_FORMATTYPE* src, OMX_COLOR_FORMATTYPE* dst) {
    *src = mConvertion.mSrcFmt;
    *dst = mConvertion.mDstFmt;

    return true;
}

bool HWColorConverter::setSrcRange(const Range& src) {
    if(mStarted)
	return false;

    memcpy(&mSrcRange, &src, sizeof(Range));

    return true;
}

bool HWColorConverter::setDstRange(const Range& dst) {
    if(mStarted)
	return false;

    memcpy(&mDstRange, &dst, sizeof(Range));

    return true;
}

bool HWColorConverter::setRange(const Range& src, const Range& dst) {
    if(mStarted)
	return false;

    memcpy(&mSrcRange, &src, sizeof(Range));
    memcpy(&mDstRange, &dst, sizeof(Range));

    return true;
}

bool HWColorConverter::getRange(Range* src, Range* dst) {
    memcpy(src, &mSrcRange, sizeof(Range));
    memcpy(dst, &mDstRange, sizeof(Range));

    return true;
}

bool HWColorConverter::setUseDstGraphicBuffer(bool enableGraphicBuffer) {
    if(mStarted)
	return false;

    mUseDstGraphicBuffer = enableGraphicBuffer;

    return true;
}

bool HWColorConverter::getUseDstGraphicBuffer(bool* graphicBufferEnabled) {
    *graphicBufferEnabled = mUseDstGraphicBuffer;

    return true;
}

bool HWColorConverter::start() {
    if(mStarted)
	return true;

    if((mConvertion.mSrcFmt == (OMX_COLOR_FORMATTYPE)OMX_COLOR_FormatYUV420Tile ||
	mConvertion.mSrcFmt == (OMX_COLOR_FORMATTYPE)OMX_COLOR_FormatYUV420ArrayPlanar) &&
       (mConvertion.mDstFmt == (OMX_COLOR_FORMATTYPE)OMX_COLOR_FormatYUV420Tile ||
	mConvertion.mDstFmt == (OMX_COLOR_FORMATTYPE)OMX_COLOR_FormatYUV420ArrayPlanar ||
	mConvertion.mDstFmt == (OMX_COLOR_FORMATTYPE)OMX_COLOR_FormatGeneralHW)){
	//do nothing for the convertion among the hw formats. and re-value the dst fmt with exactly the same with src one.
	//a case that user wants to get the hw buffer directly. by setformat outside with OMX_COLOR_FormatGeneralHW/xxx, then omx component may output detailed fmt after get the first decoded frame with later getparam.
	mConvertion.mType = PASS_THROUGH_CONVERTER;
	mConvertion.mDstFmt = mConvertion.mSrcFmt;
    }else{
	bool found = false;
	for(uint32_t i = 0; i < sizeof(kSupportedConvertions); ++i){
	    Convertion convertion = kSupportedConvertions[i];

	    if(convertion.mSrcFmt == mConvertion.mSrcFmt &&
	       convertion.mDstFmt == mConvertion.mDstFmt){
		mConvertion.mType = convertion.mType;

		found = true;
		break;
	    }
	}

	if(!found){
	    ALOGE("Error: can not find supported src->dst convertion for src:%d, dst:%d", mConvertion.mSrcFmt, mConvertion.mDstFmt);
	    return false;
	}
    }

    if(mConvertion.mType == HARD_IPU_CONVERTER){
	if(mIPUConverter == NULL)
	    mIPUConverter = new IPUConverter(mConvertion.mSrcFmt, mConvertion.mDstFmt);
    }else if(mConvertion.mType == SOFT_MXU_CONVERTER){
	if(mMXUConverter == NULL)
	    mMXUConverter = new MXUConverter(mConvertion.mSrcFmt, mConvertion.mDstFmt);
    }

    mStarted = true;

    return true;
}

bool HWColorConverter::stop() {
    if(!mStarted)
	return true;

    if(mIPUConverter != NULL){
	delete mIPUConverter;
	mIPUConverter = NULL;
    }
    if(mMXUConverter != NULL){
	delete mMXUConverter;
	mMXUConverter = NULL;
    }

    mStarted = false;

    return true;
}

bool HWColorConverter::convert(
        const void *srcBits, size_t srcStride,
        size_t srcWidth, size_t srcHeight,
        size_t srcCropLeft, size_t srcCropTop,
        size_t srcCropRight, size_t srcCropBottom,
        void *dstBits, size_t dstStride,
        size_t dstWidth, size_t dstHeight,
        size_t dstCropLeft, size_t dstCropTop,
        size_t dstCropRight, size_t dstCropBottom) {
    if(!mStarted)
	return false;

    void* finalDst = dstBits;

    GraphicBufferMapper &mapper = GraphicBufferMapper::get();
    buffer_handle_t bufferHandle = (buffer_handle_t)dstBits;
    if(mUseDstGraphicBuffer){
	Rect bounds(dstWidth, dstHeight);
	if(0 != mapper.lock(bufferHandle,
			    GRALLOC_USAGE_HW_TEXTURE |
			    GRALLOC_USAGE_HW_COMPOSER |
			    GRALLOC_USAGE_EXTERNAL_DISP,
			    bounds, &finalDst)){
	    ALOGE("Error: fail to mapper lock buffer for HWConverter!!!");
	    return false;
	}

	// only works for 4780..
	// const IMG_native_handle_t* gpuBufHandle = (IMG_native_handle_t*)bufferHandle;
	// dstStride = gpuBufHandle->iStride;

        //this is the correct way to get the stride in m200.
	gc_native_handle_t *gpuBufHandle = gc_native_handle_get(bufferHandle);
	dstStride = gpuBufHandle->stride;

	// stride is invalid for now, use dstWidth
	//dstStride = dstWidth;
    }

    BitmapParams dst(
	    finalDst, dstStride,
            dstWidth, dstHeight,
            dstCropLeft, dstCropTop, dstCropRight, dstCropBottom);

    BitmapParams src(
	    const_cast<void *>(srcBits), srcStride, //stride not working for now.
            srcWidth, srcHeight,
            srcCropLeft, srcCropTop, srcCropRight, srcCropBottom);

    bool res = false;

    switch(mConvertion.mType){
    case SOFT_MXU_CONVERTER:
    	if(mMXUConverter != NULL)
    	    res = mMXUConverter->convert(src, dst, dstStride);
    	break;
    case HARD_IPU_CONVERTER:
    	if(mIPUConverter != NULL)
    	    res = mIPUConverter->convert(src, dst);
    	break;
    case SOFT_420TILE_420PLANAR_CONVERTER:
	res = convertFromYUV420TileTo420Planar_select(src, dst);
	break;
    case SOFT_420TILE_RGB565_CONVERTER:
	res = convertFromYUV420TileToRGB565(src, dst);
	break;
    case SOFT_420ARRAY_420PLANAR_CONVERTER:
	res = convertFromYUV420ArrayTo420Planar_select(src, dst);
	break;
    case PASS_THROUGH_CONVERTER:
        memcpy(dstBits, srcBits, sizeof(INGENIC_OMX_YUV_FORMAT));
	break;
    default:
	res = false;
	break;
    }

    if(mUseDstGraphicBuffer){
	mapper.unlock(bufferHandle);
    }

    return res;
}

bool HWColorConverter::convert(Params& in, Params& out) {
    uint32_t srcStride, dstStride;
    uint32_t srcWidth, srcHeight;
    uint32_t dstWidth, dstHeight;
    uint32_t srcCropLeft, srcCropTop, srcCropRight, srcCropBottom;
    uint32_t dstCropLeft, dstCropTop, dstCropRight, dstCropBottom;

    srcStride = in.mIsRangeSet?in.mRange.mStride:mSrcRange.mStride;
    srcWidth = in.mIsRangeSet?in.mRange.mWidth:mSrcRange.mWidth;
    srcHeight = in.mIsRangeSet?in.mRange.mHeight:mSrcRange.mHeight;
    srcCropLeft = in.mIsRangeSet?in.mRange.mCropLeft:mSrcRange.mCropLeft;
    srcCropTop = in.mIsRangeSet?in.mRange.mCropTop:mSrcRange.mCropTop;
    srcCropRight = in.mIsRangeSet?in.mRange.mCropRight:mSrcRange.mCropRight;
    srcCropBottom = in.mIsRangeSet?in.mRange.mCropBottom:mSrcRange.mCropBottom;

    dstStride = out.mIsRangeSet?out.mRange.mStride:mDstRange.mStride;
    dstWidth = out.mIsRangeSet?out.mRange.mWidth:mDstRange.mWidth;
    dstHeight = out.mIsRangeSet?out.mRange.mHeight:mDstRange.mHeight;
    dstCropLeft = out.mIsRangeSet?out.mRange.mCropLeft:mDstRange.mCropLeft;
    dstCropTop = out.mIsRangeSet?out.mRange.mCropTop:mDstRange.mCropTop;
    dstCropRight = out.mIsRangeSet?out.mRange.mCropRight:mDstRange.mCropRight;
    dstCropBottom = out.mIsRangeSet?out.mRange.mCropBottom:mDstRange.mCropBottom;

    switch((int)mConvertion.mDstFmt){
    case OMX_COLOR_Format32bitRGBA8888:
	out.mLength = dstWidth * dstHeight * 4;
	break;
    case OMX_COLOR_Format16bitRGB565:
	out.mLength = dstWidth * dstHeight * 2;
	break;
    case OMX_COLOR_FormatYUV420Planar:
	out.mLength = dstWidth * dstHeight * 3 / 2;
	break;
    default:
	out.mLength = 0;
	break;
    }

    return convert(in.mData, srcStride,
		   srcWidth, srcHeight,
		   srcCropLeft, srcCropTop,
		   srcCropRight, srcCropBottom,
		   out.mData, dstStride,
		   dstWidth, dstHeight,
		   dstCropLeft, dstCropTop,
		   dstCropRight, dstCropBottom);
}

#ifdef MXU_CONVERT

bool HWColorConverter::convertFromYUV420TileTo420Planar_select(const BitmapParams& src,
							       const BitmapParams& dst){
    int src_width = src.mWidth;
    int dst_width = dst.mWidth;

    int scale = -1;
    scale = src_width / dst_width;

    if( scale == 1 ) {
	convertFromYUV420TileTo420Planar(src, dst);
    }else if( scale == 2 ) {
	convertFromYUV420TileTo420Planar_half(src, dst);
    }else if( scale == 4 ) {
	convertFromYUV420TileTo420Planar_quarter(src, dst);
    }else {
	LOGE("[ convertFromYUV420TileTo420Planar ] Invalid scale value : %d\n", scale);
	return false;
    }

    return true;
}

int HWColorConverter::convertFromYUV420TileTo420Planar(const BitmapParams &src,
						       const BitmapParams &dst){
    int8_t *dst_ptr = (int8_t*)dst.mBits + dst.mCropTop * dst.mWidth + dst.mCropLeft;
    int8_t *dst_start = dst_ptr;

    INGENIC_OMX_YUV_FORMAT* p = (INGENIC_OMX_YUV_FORMAT*)src.mBits;
    int8_t *src_y = (int8_t *)p->nPlanar[0];
    int8_t *src_c = (int8_t *)p->nPlanar[1];
    int stride_y = p->nStride[0];
    int stride_c = p->nStride[1];

    if ( (dst.mWidth > src.mWidth) || (dst.mHeight > src.mHeight) ) {
	ALOGE("convertFromYUV420TileTo420Planar Dest buffer is too big, check it !!!");
	return true;
    }

    int src_width = src.mWidth;
    int src_height = src.mHeight;

    int8_t *dst_y = dst_ptr;
    int8_t *dst_u = dst_y + dst.mWidth * dst.mHeight;
    int8_t *dst_v = dst_y + dst.mWidth * dst.mHeight + dst.mWidth * dst.mHeight / 4;

    int n_width = src_width / 16;
    int n_height = src_height / 16;

    int8_t *src_yh = src_y;
    int8_t *src_ch = src_c;

    int8_t *dst_yh = dst_y;
    int8_t *dst_uh = dst_u;
    int8_t *dst_vh = dst_v;

    int dst_y_width_1 = dst.mWidth;
    int dst_y_width_2 = dst.mWidth * 2;
    int dst_y_stride = dst.mWidth * 16;

    int dst_uv_width_1 = dst.mWidth / 2;
    int dst_uv_stride = (dst.mWidth / 2) * 8;

    for (int mbrow = 0; mbrow < n_height; ++mbrow) {
	int8_t *src_yhw = src_yh;
	int8_t *src_chw = src_ch;
	int8_t *dst_yh_mbcol = dst_yh;
	int8_t *dst_uh_mbcol = dst_uh;
	int8_t *dst_vh_mbcol = dst_vh;

	for(int mbcol = 0; mbcol < n_width; ++mbcol){
	    int8_t *src_yhw_mb32 = src_yhw - 32;
	    int8_t *src_chw_mb16 = src_chw - 16;

	    int8_t *dst_yhw1 = dst_yh_mbcol - dst_y_width_2;
	    int8_t *dst_yhw2 = dst_yh_mbcol - dst_y_width_1;

	    int8_t *dst_uhw = dst_uh_mbcol - dst_uv_width_1;
	    int8_t *dst_vhw = dst_vh_mbcol - dst_uv_width_1;

	    int8_t *dst_y1_pref = dst_yh_mbcol;
	    int8_t *dst_y2_pref = dst_yh_mbcol + dst_y_width_1;
	    int8_t *dst_u_pref = dst_uh_mbcol;
	    int8_t *dst_v_pref = dst_vh_mbcol;

	    i_pref(1, src_yhw, 0);
	    i_pref(1, src_chw, 0);

	    for (int i = 0; i < 8; ++i) {          //row
		//y
		i_pref(30, dst_y1_pref, 0);
		i_pref(30, dst_y2_pref, 0);
		i_pref(30, dst_u_pref, 0);
		i_pref(30, dst_v_pref, 0);

		S32LDI(xr1, src_yhw_mb32, 32);
		S32LDD(xr2, src_yhw_mb32, 4);
		S32LDD(xr3, src_yhw_mb32, 8);
		S32LDD(xr4, src_yhw_mb32, 12);

		S32SDIV(xr1, dst_yhw1, dst_y_width_2, 0);
		S32STD(xr2, dst_yhw1, 4);
		S32STD(xr3, dst_yhw1, 8);
		S32STD(xr4, dst_yhw1, 12);

		S32LDD(xr1, src_yhw_mb32, 16);
		S32LDD(xr2, src_yhw_mb32, 20);
		S32LDD(xr3, src_yhw_mb32, 24);
		S32LDD(xr4, src_yhw_mb32, 28);

		S32SDIV(xr1, dst_yhw2, dst_y_width_2, 0);
		S32STD(xr2, dst_yhw2, 4);
		S32STD(xr3, dst_yhw2, 8);
		S32STD(xr4, dst_yhw2, 12);

		//uv
		S32LDI(xr1, src_chw_mb16, 16);
		S32LDD(xr2, src_chw_mb16, 4);
		S32LDD(xr3, src_chw_mb16, 8);
		S32LDD(xr4, src_chw_mb16, 12);

		S32SDIV(xr1, dst_uhw, dst_uv_width_1, 0);
		S32STD(xr2, dst_uhw, 4);

		S32SDIV(xr3, dst_vhw, dst_uv_width_1, 0);
		S32STD(xr4, dst_vhw, 4);

		dst_y1_pref += dst_y_width_2;
		dst_y2_pref += dst_y_width_2;
		dst_u_pref += dst_uv_width_1;
		dst_v_pref += dst_uv_width_1;
	    }
	    src_yhw += 256;
	    src_chw += 128;
	    dst_yh_mbcol += 16;
	    dst_uh_mbcol += 8;
	    dst_vh_mbcol += 8;
	}
	src_yh += stride_y;
	src_ch += stride_c;
	dst_yh += dst_y_stride;
	dst_uh += dst_uv_stride;
	dst_vh += dst_uv_stride;
    }

    return 0;
}

int HWColorConverter::convertFromYUV420TileTo420Planar_half(const BitmapParams &src,
							     const BitmapParams &dst){
    int8_t *dst_ptr = (int8_t*)dst.mBits + dst.mCropTop * dst.mWidth + dst.mCropLeft;
    int8_t *dst_start = dst_ptr;

    INGENIC_OMX_YUV_FORMAT* p = (INGENIC_OMX_YUV_FORMAT*)src.mBits;
    int8_t *src_y = (int8_t *)p->nPlanar[0];
    int8_t *src_c = (int8_t *)p->nPlanar[1];
    int stride_y = p->nStride[0];
    int stride_c = p->nStride[1];

    if ( (dst.mWidth > src.mWidth) || (dst.mHeight > src.mHeight) ) {
	ALOGE("convertFromYUV420TileTo420Planar_half Dest buffer is too big, check it !!!");
	return true;
    }

    int src_width = src.mWidth;
    int src_height = src.mHeight;

    int8_t *dst_y = dst_ptr;
    int8_t *dst_u = dst_y + dst.mWidth * dst.mHeight;
    int8_t *dst_v = dst_y + dst.mWidth * dst.mHeight + dst.mWidth * dst.mHeight / 4;

    int n_width = src_width / 16;
    int n_height = src_height / 16;

    int8_t *src_yh = src_y;
    int8_t *src_ch = src_c;

    int8_t *dst_yh = dst_y;
    int8_t *dst_uh = dst_u;
    int8_t *dst_vh = dst_v;

    int dst_y_width_1 = dst.mWidth;
    int dst_y_width_2 = dst.mWidth * 2;
    int dst_y_stride = dst.mWidth * (16 / 2);

    int dst_uv_width_1 = dst.mWidth / 2;
    int dst_uv_stride = (dst.mWidth / 2) * (8 / 2);

    for (int mbrow = 0; mbrow < n_height; ++mbrow) {
	int8_t *src_yhw = src_yh;
	int8_t *src_chw = src_ch;
	int8_t *dst_yh_mbcol = dst_yh;
	int8_t *dst_uh_mbcol = dst_uh;
	int8_t *dst_vh_mbcol = dst_vh;

	for(int mbcol = 0; mbcol < n_width; ++mbcol){
	    int8_t *src_yhw_mb32 = src_yhw - 32 * 2;
	    int8_t *src_chw_mb16 = src_chw - 16 * 2;

	    int8_t *dst_yhw1 = dst_yh_mbcol - dst_y_width_2;
	    int8_t *dst_yhw2 = dst_yh_mbcol - dst_y_width_1;

	    int8_t *dst_uhw = dst_uh_mbcol - dst_uv_width_1;
	    int8_t *dst_vhw = dst_vh_mbcol - dst_uv_width_1;

	    int8_t *dst_y1_pref = dst_yh_mbcol;
	    int8_t *dst_y2_pref = dst_yh_mbcol + dst_y_width_1;
	    int8_t *dst_u_pref = dst_uh_mbcol;
	    int8_t *dst_v_pref = dst_vh_mbcol;

	    i_pref(1, src_yhw, 0);
	    i_pref(1, src_chw, 0);

	    for (int i = 0; i < 4; ++i){          //row
		//y
		i_pref(30, dst_y1_pref, 0);
		i_pref(30, dst_y2_pref, 0);
		i_pref(30, dst_u_pref, 0);
		i_pref(30, dst_v_pref, 0);

		S32LDI(xr1, src_yhw_mb32, 32 * 2);
		S32LDD(xr2, src_yhw_mb32, 4);
		S32LDD(xr4, src_yhw_mb32, 8);
		S32LDD(xr5, src_yhw_mb32, 12);

		S32SFL(xr0, xr2, xr1, xr3 , ptn3);
		S32SFL(xr0, xr5, xr4, xr6 , ptn3);

		S32SDIV(xr3, dst_yhw1, dst_y_width_2, 0);
		S32STD(xr6, dst_yhw1, 4);

		S32LDD(xr1, src_yhw_mb32, 16);
		S32LDD(xr2, src_yhw_mb32, 20);
		S32LDD(xr4, src_yhw_mb32, 24);
		S32LDD(xr5, src_yhw_mb32, 28);

		S32SFL(xr0, xr2, xr1, xr3 , ptn3);
		S32SFL(xr0, xr5, xr4, xr6 , ptn3);

		S32SDIV(xr3, dst_yhw2, dst_y_width_2, 0);
		S32STD(xr6, dst_yhw2, 4);

		//uv
		S32LDI(xr1, src_chw_mb16, 16 * 2);
		S32LDD(xr2, src_chw_mb16, 4);
		S32LDD(xr4, src_chw_mb16, 8);
		S32LDD(xr5, src_chw_mb16, 12);

		S32SFL(xr0, xr2, xr1, xr3 , ptn1);
		S32SFL(xr0, xr5, xr4, xr6 , ptn1);

		S32SDIV(xr3, dst_uhw, dst_uv_width_1, 0);
		S32SDIV(xr6, dst_vhw, dst_uv_width_1, 0);

		dst_y1_pref += dst_y_width_2;
		dst_y2_pref += dst_y_width_2;
		dst_u_pref += dst_uv_width_1;
		dst_v_pref += dst_uv_width_1;
	    }
	    src_yhw += 256;
	    src_chw += 128;
	    dst_yh_mbcol += 8;
	    dst_uh_mbcol += 4;
	    dst_vh_mbcol += 4;
	}
	src_yh += stride_y;
	src_ch += stride_c;
	dst_yh += dst_y_stride;
	dst_uh += dst_uv_stride;
	dst_vh += dst_uv_stride;
    }

    return 0;
}

int HWColorConverter::convertFromYUV420TileTo420Planar_quarter(const BitmapParams &src,
								 const BitmapParams &dst){
    int8_t *dst_ptr = (int8_t*)dst.mBits + dst.mCropTop * dst.mWidth + dst.mCropLeft;
    int8_t *dst_start = dst_ptr;

    INGENIC_OMX_YUV_FORMAT* p = (INGENIC_OMX_YUV_FORMAT*)src.mBits;
    int8_t *src_y = (int8_t *)p->nPlanar[0];
    int8_t *src_c = (int8_t *)p->nPlanar[1];
    int stride_y = p->nStride[0];
    int stride_c = p->nStride[1];

    if ( (dst.mWidth > src.mWidth) || (dst.mHeight > src.mHeight) ) {
	ALOGE("convertFromYUV420TileTo420Planar_quarter Dest buffer is too big, check it !!!");
	return true;
    }

    int src_width = src.mWidth;
    int src_height = src.mHeight;

    int8_t *dst_y = dst_ptr;
    int8_t *dst_u = dst_y + dst.mWidth * dst.mHeight;
    int8_t *dst_v = dst_y + dst.mWidth * dst.mHeight + dst.mWidth * dst.mHeight / 4;

    int n_width = src_width / 16;
    int n_height = src_height / 16;

    int8_t *src_yh = src_y;
    int8_t *src_ch = src_c;

    int8_t *dst_yh = dst_y;
    int8_t *dst_uh = dst_u;
    int8_t *dst_vh = dst_v;

    int dst_y_width_1 = dst.mWidth;
    int dst_y_width_2 = dst.mWidth * 2;
    int dst_y_stride = dst.mWidth * (16 / 4);

    int dst_uv_width_1 = dst.mWidth / 2;
    int dst_uv_stride = (dst.mWidth / 2) * (8 / 4);

    for (int mbrow = 0; mbrow < n_height; ++mbrow) {
	int8_t *src_yhw = src_yh;
	int8_t *src_chw = src_ch;
	int8_t *dst_yh_mbcol = dst_yh;
	int8_t *dst_uh_mbcol = dst_uh;
	int8_t *dst_vh_mbcol = dst_vh;

	for(int mbcol = 0; mbcol < n_width; ++mbcol){
	    int8_t *src_yhw_mb32 = src_yhw - 32 * 4;
	    int8_t *src_chw_mb16 = src_chw - 16 * 4;

	    int8_t *dst_yhw1 = dst_yh_mbcol - dst_y_width_2;
	    int8_t *dst_yhw2 = dst_yh_mbcol - dst_y_width_1;

	    int8_t *dst_uhw = dst_uh_mbcol;
	    int8_t *dst_vhw = dst_vh_mbcol;

	    int8_t *dst_y1_pref = dst_yh_mbcol;
	    int8_t *dst_y2_pref = dst_yh_mbcol + dst_y_width_1;
	    int8_t *dst_u_pref = dst_uh_mbcol;
	    int8_t *dst_v_pref = dst_vh_mbcol;

	    i_pref(1, src_yhw, 0);
	    i_pref(1, src_chw, 0);

	    for (int i = 0; i < 2; ++i){          //row
		//y
		i_pref(30, dst_y1_pref, 0);
		i_pref(30, dst_y2_pref, 0);
		i_pref(30, dst_u_pref, 0);
		i_pref(30, dst_v_pref, 0);

		S32LDI(xr1, src_yhw_mb32, 32 * 4);
		S32LDD(xr2, src_yhw_mb32, 8);
		S32LDD(xr4, src_yhw_mb32, 16);
		S32LDD(xr5, src_yhw_mb32, 24);

		S32SFL(xr0, xr2, xr1, xr3 , ptn3);
		S32SFL(xr0, xr5, xr4, xr6 , ptn3);

		S32SDIV(xr3, dst_yhw1, dst_y_width_2, 0);
		S32SDIV(xr6, dst_yhw2, dst_y_width_2, 0);

		//uv
		S32LDI(xr1, src_chw_mb16, 16 * 4);
		S32LDD(xr2, src_chw_mb16, 4);
		S32LDD(xr4, src_chw_mb16, 8);
		S32LDD(xr5, src_chw_mb16, 12);

		S32SFL(xr0, xr2, xr1, xr3 , ptn0);
		S32SFL(xr0, xr5, xr4, xr6 , ptn0);

		S16STD(xr3, dst_uhw, 0, ptn0);
		S16STD(xr6, dst_vhw, 0, ptn0);

		dst_uhw += dst_uv_width_1;
		dst_vhw += dst_uv_width_1;

		dst_y1_pref += dst_y_width_2;
		dst_y2_pref += dst_y_width_2;
		dst_u_pref += dst_uv_width_1;
		dst_v_pref += dst_uv_width_1;
	    }
	    src_yhw += 256;
	    src_chw += 128;
	    dst_yh_mbcol += 4;
	    dst_uh_mbcol += 2;
	    dst_vh_mbcol += 2;
	}
	src_yh += stride_y;
	src_ch += stride_c;
	dst_yh += dst_y_stride;
	dst_uh += dst_uv_stride;
	dst_vh += dst_uv_stride;
    }

    return 0;
}

#else

bool HWColorConverter::convertFromYUV420TileTo420Planar(const BitmapParams &src, const BitmapParams &dst)
{
    int8_t *dst_ptr = (int8_t*)dst.mBits + dst.mCropTop * dst.mWidth + dst.mCropLeft;
    int8_t *dst_start = dst_ptr;

    INGENIC_OMX_YUV_FORMAT* p = (INGENIC_OMX_YUV_FORMAT*)src.mBits;
    int8_t *src_y = (int8_t *)p->nPlanar[0];
    int8_t *src_u = (int8_t *)p->nPlanar[1];
    int8_t *src_v = src_u + 8;

    int8_t *dst_y = dst_ptr;
    int8_t *dst_u = dst_y + dst.mWidth * dst.mHeight;
    int8_t *dst_v = dst_y + dst.mWidth * dst.mHeight + dst.mWidth * dst.mHeight / 4;

    int iLoop = 0, jLoop = 0, kLoop = 0;
    int dxy;
    int stride_y = p->nStride[0];
    int stride_c = p->nStride[1];

    size_t dst_y_index = 0, dst_uv_index = 0;

    if ( (dst.mWidth > src.mWidth) || (dst.mHeight > src.mHeight) ) {
	ALOGE("convertFromYUV420ArrayTo420Planar Dest buffer is too big, check it !!!");
	return true;
    }

    for (size_t y = 0; y < src.cropHeight(); ++y) {
        for (size_t x = 0; x < src.cropWidth(); x += 2) {
	    if (iLoop>0 && iLoop%8==0){
		jLoop++;
		iLoop = 0;
		dxy = iLoop*2 + jLoop * 256;
		iLoop++;
	    }else{
		dxy = iLoop*2 + jLoop * 256;
		iLoop++;
	    }

            signed y1 = (signed)src_y[dxy];
            signed y2 = (signed)src_y[dxy + 1];

            signed u = (signed)src_u[dxy / 2];
            signed v = (signed)src_v[dxy / 2];

	    dst_y[dst_y_index++] = y1;
	    dst_y[dst_y_index++] = y2;

	    if(y%2 == 0){
		dst_u[dst_uv_index] = u;
		dst_v[dst_uv_index] = v;
		++dst_uv_index;
	    }
        }

	if (kLoop > 0 && kLoop % 15 == 0){
	    src_y += stride_y + 16 - 256;
	    src_u += stride_c + 16 - 128;
	    src_v = src_u + 8;
	    iLoop = 0;jLoop = 0;kLoop = 0;
	}else if (kLoop & 1){
	    src_y += 16;
	    src_u += 16;
	    src_v = src_u + 8;
	    iLoop = 0;jLoop = 0;kLoop++;
	}else{
	    src_y += 16;
	    iLoop = 0;jLoop = 0;kLoop++;
	}
    }

#if 0
    static int frameCount = 0;
    char filename[20] = {0};
    sprintf(filename, "/data/rgb/yuv%d.raw", ++frameCount);
    FILE* sfd = fopen(filename, "wb");
    if(sfd){
	ALOGE("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!fwrite dst_start:%p, wh:w * h:%d*%d", dst_start, dst.mWidth, dst.mHeight);
	fwrite(dst_start, dst.mWidth * dst.mHeight * 3 / 2, 1, sfd);
	fclose(sfd);
	sfd = NULL;
    }else
	ALOGE("fail to open nals.txt:%s", strerror(errno));
#endif

    return true;
}
#endif

bool HWColorConverter::convertFromYUV420ArrayTo420Planar_select(const BitmapParams &src,
								const BitmapParams &dst){
    int src_width = src.mWidth;
    int dst_width = dst.mWidth;

    int scale = -1;
    scale = src_width / dst_width;

    if( scale == 1 ) {
	convertFromYUV420ArrayTo420Planar(src, dst);
    }else if( scale == 2 ) {
	convertFromYUV420ArrayTo420Planar_half(src, dst);
    }else if( scale == 4 ) {
	convertFromYUV420ArrayTo420Planar_quarter(src, dst);
    }else {
	LOGE("[ convertFromYUV420ArrayTo420Planar ] Invalid scale value : %d\n", scale);
	return false;
    }

    return true;

}

bool HWColorConverter::convertFromYUV420ArrayTo420Planar(const BitmapParams &src, const BitmapParams &dst){
    uint8_t *dst_ptr = (uint8_t*)dst.mBits + dst.mCropTop * dst.mWidth + dst.mCropLeft;
    uint8_t *dst_start = dst_ptr;

    INGENIC_OMX_YUV_FORMAT* p = (INGENIC_OMX_YUV_FORMAT*)src.mBits;
    uint8_t *src_y = (uint8_t *)p->nPlanar[0];
    uint8_t *src_u = (uint8_t *)p->nPlanar[1];
    uint8_t *src_v = (uint8_t *)p->nPlanar[2];

    uint8_t *dst_y = dst_ptr;
    uint8_t *dst_u = dst_y + dst.mWidth * dst.mHeight;
    uint8_t *dst_v = dst_y + dst.mWidth * dst.mHeight + dst.mWidth * dst.mHeight / 4;

    if ( (dst.mWidth > src.mWidth) || (dst.mHeight > src.mHeight) ) {
	ALOGE("convertFromYUV420ArrayTo420Planar Dest buffer is too big, check it !!!");
	return true;
    }

    memcpy(dst_y, src_y, dst.mWidth * dst.mHeight);
    memcpy(dst_u, src_u, dst.mWidth * dst.mHeight / 4);
    memcpy(dst_v, src_v, dst.mWidth * dst.mHeight / 4);

    return true;
}

bool HWColorConverter::convertFromYUV420ArrayTo420Planar_half(const BitmapParams &src,
							      const BitmapParams &dst){
    uint8_t *dst_ptr = (uint8_t*)dst.mBits + dst.mCropTop * dst.mWidth + dst.mCropLeft;
    uint8_t *dst_start = dst_ptr;

    INGENIC_OMX_YUV_FORMAT* p = (INGENIC_OMX_YUV_FORMAT*)src.mBits;
    uint8_t *src_y = (uint8_t *)p->nPlanar[0];
    uint8_t *src_u = (uint8_t *)p->nPlanar[1];
    uint8_t *src_v = (uint8_t *)p->nPlanar[2];

    uint8_t *dst_y = dst_ptr;
    uint8_t *dst_u = dst_y + dst.mWidth * dst.mHeight;
    uint8_t *dst_v = dst_y + dst.mWidth * dst.mHeight + dst.mWidth * dst.mHeight / 4;

    if ( (dst.mWidth > src.mWidth) || (dst.mHeight > src.mHeight) ) {
	ALOGE("convertFromYUV420ArrayTo420Planar_half Dest buffer is too big, check it !!!");
	return true;
    }

    int dst_y_number = dst.mWidth * dst.mHeight;
    int dst_u_number = dst.mWidth/2 * dst.mHeight/2;
    int dst_v_number = dst.mWidth/2 * dst.mHeight/2;

    uint8_t *src_y_jump = src_y;
    uint8_t *src_u_jump = src_u;
    uint8_t *src_v_jump = src_v;

    uint8_t *dst_y_jump = dst_y;
    uint8_t *dst_u_jump = dst_u;
    uint8_t *dst_v_jump = dst_v;

    for( int i = 0; i < dst_y_number/2; i++ ) {
	memcpy(dst_y_jump, src_y_jump, 2);
	dst_y_jump += 2;
	src_y_jump += 4;
    }

    for( int i = 0; i < dst_u_number; i++ ) {
	memcpy(dst_u_jump, src_u_jump, 1);
	dst_u_jump++;
	src_u_jump += 2;
    }

    for( int i = 0; i < dst_v_number; i++ ) {
	memcpy(dst_v_jump, src_v_jump, 1);
	dst_v_jump++;
	src_v_jump += 2;
    }

    return true;
}

bool HWColorConverter::convertFromYUV420ArrayTo420Planar_quarter(const BitmapParams &src,
								 const BitmapParams &dst){
    uint8_t *dst_ptr = (uint8_t*)dst.mBits + dst.mCropTop * dst.mWidth + dst.mCropLeft;
    uint8_t *dst_start = dst_ptr;

    INGENIC_OMX_YUV_FORMAT* p = (INGENIC_OMX_YUV_FORMAT*)src.mBits;
    uint8_t *src_y = (uint8_t *)p->nPlanar[0];
    uint8_t *src_u = (uint8_t *)p->nPlanar[1];
    uint8_t *src_v = (uint8_t *)p->nPlanar[2];

    uint8_t *dst_y = dst_ptr;
    uint8_t *dst_u = dst_y + dst.mWidth * dst.mHeight;
    uint8_t *dst_v = dst_y + dst.mWidth * dst.mHeight + dst.mWidth * dst.mHeight / 4;

    if ( (dst.mWidth > src.mWidth) || (dst.mHeight > src.mHeight) ) {
	ALOGE("convertFromYUV420ArrayTo420Planar_quarter Dest buffer is too big, check it !!!");
	return true;
    }

    int dst_y_number = dst.mWidth * dst.mHeight;
    int dst_u_number = dst.mWidth/2 * dst.mHeight/2;
    int dst_v_number = dst.mWidth/2 * dst.mHeight/2;

    uint8_t *src_y_jump = src_y;
    uint8_t *src_u_jump = src_u;
    uint8_t *src_v_jump = src_v;

    uint8_t *dst_y_jump = dst_y;
    uint8_t *dst_u_jump = dst_u;
    uint8_t *dst_v_jump = dst_v;

    for( int i = 0; i < dst_y_number/2; i++ ) {
	memcpy(dst_y_jump, src_y_jump, 2);
	dst_y_jump += 2;
	src_y_jump += 8;
    }

    for( int i = 0; i < dst_u_number; i++ ) {
	memcpy(dst_u_jump, src_u_jump, 1);
	dst_u_jump++;
	src_u_jump += 4;
    }

    for( int i = 0; i < dst_v_number; i++ ) {
	memcpy(dst_v_jump, src_v_jump, 1);
	dst_v_jump++;
	src_v_jump += 4;
    }

    return true;
}

bool HWColorConverter::convertFromYUV420TileToRGB565(const BitmapParams &src, const BitmapParams &dst){
    uint8_t *kAdjustedClip = initClip();

    uint16_t *dst_ptr = (uint16_t *)dst.mBits + dst.mCropTop * dst.mWidth + dst.mCropLeft;
    uint16_t *dst_start = dst_ptr;

    INGENIC_OMX_YUV_FORMAT* p = (INGENIC_OMX_YUV_FORMAT*)src.mBits;
    uint8_t *src_y = (uint8_t *)p->nPlanar[0];
    uint8_t *src_u = (uint8_t *)p->nPlanar[1];
    uint8_t *src_v = src_u + 8;

    int iLoop = 0, jLoop = 0, kLoop = 0;
    int dxy;
    int stride_y = p->nStride[0];
    int stride_c = p->nStride[1];

    if ( (dst.mWidth < src.mWidth) || (dst.mHeight < src.mHeight) ) {
	ALOGE("Dest buffer is too small, check it !!!");
	return true;
    }

    for (size_t y = 0; y < src.cropHeight(); ++y) {
        for (size_t x = 0; x < src.cropWidth(); x += 2) {
	    if (iLoop>0 && iLoop%8==0){
		jLoop++;
		iLoop = 0;
		dxy = iLoop*2 + jLoop * 256;
		iLoop++;
	    }else{
		dxy = iLoop*2 + jLoop * 256;
		iLoop++;
	    }

	    ALOGE("dxy:%d, x:%d, y:%d", dxy, x, y);

            signed y1 = (signed)src_y[dxy] - 16;
            signed y2 = (signed)src_y[dxy + 1] - 16;

            signed u = (signed)src_u[dxy / 2] - 128;
            signed v = (signed)src_v[dxy / 2] - 128;

            signed u_b = u * 517;
            signed u_g = -u * 100;
            signed v_g = -v * 208;
            signed v_r = v * 409;

            signed tmp1 = y1 * 298;
            signed b1 = (tmp1 + u_b) / 256;
            signed g1 = (tmp1 + v_g + u_g) / 256;
            signed r1 = (tmp1 + v_r) / 256;

            signed tmp2 = y2 * 298;
            signed b2 = (tmp2 + u_b) / 256;
            signed g2 = (tmp2 + v_g + u_g) / 256;
            signed r2 = (tmp2 + v_r) / 256;

            uint32_t rgb1 =
                ((kAdjustedClip[r1] >> 3) << 11)
                | ((kAdjustedClip[g1] >> 2) << 5)
                | (kAdjustedClip[b1] >> 3);

            uint32_t rgb2 =
                ((kAdjustedClip[r2] >> 3) << 11)
                | ((kAdjustedClip[g2] >> 2) << 5)
                | (kAdjustedClip[b2] >> 3);

            if (x + 1 < src.cropWidth()) {
	      //if (y < 4)
	      //LOGE("1 :: dst %d = %d", x, ((rgb2 << 16) | rgb1));
                *(uint32_t *)(&dst_ptr[x]) = (rgb2 << 16) | rgb1;
            } else {
	      //if (y < 4)
	      //LOGE("2 :: dst %d = %d", x, rgb1);
                dst_ptr[x] = rgb1;
            }
        }

	if (kLoop > 0 && kLoop % 15 == 0){
	    src_y += stride_y + 16 - 256;
	    src_u += stride_c + 16 - 128;
	    src_v = src_u + 8;
	    iLoop = 0;jLoop = 0;kLoop = 0;
	}else if (kLoop & 1){
	    src_y += 16;
	    src_u += 16;
	    src_v = src_u + 8;
	    iLoop = 0;jLoop = 0;kLoop++;
	}else{
	    src_y += 16;
	    iLoop = 0;jLoop = 0;kLoop++;
	}

        dst_ptr += dst.mWidth;
    }

#if 0
    static int frameCount = 0;
    char filename[20] = {0};
    sprintf(filename, "/data/rgb/rgb%d.raw", ++frameCount);
    FILE* sfd = fopen(filename, "wb");
    if(sfd){
	LOGE("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!fwrite dst_start:%p, wh:w * h", dst_start, dst.mWidth, dst.mHeight);
	fwrite(dst_start, dst.mWidth * dst.mHeight * 2, 1, sfd);
	fclose(sfd);
	sfd = NULL;
    }else
	LOGE("fail to open nals.txt:%s", strerror(errno));
#endif

    return true;
}

uint8_t *HWColorConverter::initClip() {
    static const signed kClipMin = -278;
    static const signed kClipMax = 535;

    if (mClip == NULL) {
        mClip = new uint8_t[kClipMax - kClipMin + 1];

        for (signed i = kClipMin; i <= kClipMax; ++i) {
            mClip[i - kClipMin] = (i < 0) ? 0 : (i > 255) ? 255 : (uint8_t)i;
        }
    }

    return &mClip[-kClipMin];
}

}  // namespace android
