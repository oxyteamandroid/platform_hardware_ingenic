/*
 * Copyright (C) 2009 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define LOG_TAG "MXUConverter"
#include <utils/Log.h>

#include <ui/GraphicBufferMapper.h>
#include "MXUConverter.h"
#include "jzasm.h"
#include "jzmedia.h"

#define LOGE ALOGE
#define MXU_OPT

#ifdef MXU_OPT
#define i_pref(hint,base,offset)					\
    ({ __asm__ __volatile__("pref %0,%2(%1)"::"i"(hint),"r"(base),"i"(offset):"memory");})
#endif

namespace android {

    MXUConverter::MXUConverter(OMX_COLOR_FORMATTYPE from, OMX_COLOR_FORMATTYPE to)
	:mSrcFormat(from),
	 mDstFormat(to) {
    }

    bool MXUConverter::isValid() const {
	if(mSrcFormat != (OMX_COLOR_FORMATTYPE)OMX_COLOR_FormatYUV420Tile) {
	    ALOGE("Error: unsupported src format for mxu converter!!!");
	    return false;
	}
    
	if(mDstFormat != (OMX_COLOR_FORMATTYPE)OMX_COLOR_Format32bitRGBA8888) {
	    ALOGE("Error: unsupported dst format for mxu converter!!!");
	    return false;
	}

	return true;
    }
  
    MXUConverter::~MXUConverter() {  
    }
#ifdef MXU_OPT
    // Parameter :
    // data   : source buffer from VPU, which format is tile
    // data1  : dest buffer
    // width  : picture width
    // height : picture height    
    int tile2RGB(void *data, void *data1, size_t src_width, size_t src_height, size_t dstStride)
    {
	INGENIC_OMX_YUV_FORMAT* pimg = (INGENIC_OMX_YUV_FORMAT*)data;
	unsigned char *src_y = reinterpret_cast<unsigned char*>(pimg->nPlanar[0]);
	unsigned char *src_c = reinterpret_cast<unsigned char*>(pimg->nPlanar[1]);
	unsigned int y_stride = pimg->nStride[0];
	unsigned int c_stride = pimg->nStride[1];
	unsigned char *dst = (unsigned char *)data1;

	// B = 1.164 * (Y - 16) + 2.018 * (U - 128)
	// G = 1.164 * (Y - 16) - 0.813 * (V - 128) - 0.391 * (U - 128)
	// R = 1.164 * (Y - 16) + 1.596 * (V - 128)

	// B = 298/256 * (Y - 16) + 517/256 * (U - 128)
	// G = .................. - 208/256 * (V - 128) - 100/256 * (U - 128)
	// R = .................. + 409/256 * (V - 128)
	size_t n_width = src_width / 16;
	size_t n_height = src_height / 16;

	size_t width = dstStride;

	unsigned char *src_yh = src_y;
	unsigned char *src_ch = src_c;
	unsigned char *dst_h = dst;
	unsigned int dst_stride = width * 16 * 4;
	size_t dst_width_2 = width * 4 * 2;
	size_t dst_width_1 = width * 4;

	S32I2M(xr15, 0x4a4a4a4a);              // xr15 : 74, 74, 74, 74
	S32I2M(xr14, 0x66668181);              // xr14 : 102,102,129,129 , Calc: b , r
	S32I2M(xr13, 0x34341919);              // xr13 : 52, 52, 25, 25 ,  Calc: g

	for (size_t mbrow = 0; mbrow < n_height; ++mbrow) {
	    unsigned char *src_yhw = src_yh;
	    unsigned char *src_chw = src_ch;
	    unsigned char *dst_h_mbcol = dst_h;

	    for (size_t mbcol = 0; mbcol < n_width ; ++mbcol) {	    
		unsigned char *src_yhw_mb32 = src_yhw - 32;
		unsigned char *src_chw_mb16 = src_chw - 16;

		unsigned char *dst_h_mb1 = dst_h_mbcol - dst_width_2;
		unsigned char *dst_h_mb2 = dst_h_mbcol - dst_width_1;

		unsigned char *dst_pref = dst_h_mbcol;

		i_pref(1, src_yhw, 0);
		i_pref(1, src_chw, 0);

		for (size_t i = 0; i < 8; ++i){          //row

		    i_pref(30,dst_pref,0);
		    i_pref(30,dst_pref,32);

		    i_pref(30,dst_pref+dst_width_1,0);	    
		    i_pref(30,dst_pref+dst_width_1,32);
	
		    // Calc : b3 - b0
		    S32LDI(xr1, src_yhw_mb32, 32);          // xr1  : y3, y2, y1, y0
		    S32I2M(xr12, 0x45204520);               // xr12 :17696,17696 Calc:b 
		    
		    Q8MUL(xr2, xr1, xr15, xr3);             // xr2 : y3*74, y2*74
		                                            // xr3 : y1*74, y0*74
		    S16LDI(xr1, src_chw_mb16, 16, 0);
		    S16LDD(xr1, src_chw_mb16, 8, 1);        // xr1 : v1, v0, u1, u0

		    S32SFL(xr6, xr2, xr3, xr7, ptn3);       // xr6 : y3*74  y1*74
		                                            // xr7 : y2*74  y0*74
		    S32SFL(xr8, xr2, xr3, xr9, ptn3);       //xr8 : y3*74  y1*74
		                                            //xr9 : y2*74  y0*74
		    Q8MUL(xr4, xr1, xr14, xr5);             // xr4 : v1*102, v0*102
		                                            // xr5 : u1*129, u0*129
		    S32I2M(xr11, 0x37a037a0);               // xr11 :14240,14240,Calc:r
		    S32I2M(xr10, 0x21e021e0);               // xr10 : 8672, 8672,Calc:g

		    Q16ACC_SS(xr6, xr5, xr12, xr0);         // xr6 : u1*129-17696+y3*74, u0*129-17696+y1*74
		    Q16ACC_SS(xr7, xr5, xr12, xr0);         // xr7 : u1*129-17696+y2*74, u0*129-17696+y0*74

		    Q16ACC_SS(xr8, xr4, xr11, xr0);         // xr8 : v1*102-14240+y3*74, v0*102-14240+y1*74
		    Q16ACC_SS(xr9, xr4, xr11, xr0);         // xr9 : v1*102-14240+y2*74, v0*102-14240+y0*74

		    Q16SAR(xr6, xr6, xr7, xr7, 6);        //xr6:(u1*129+y3*74-17696)/64,(u0*129-17696+y1*74)/64
     		                                          //xr7:(u1*129+y2*74-17696)/64,(u0*129-17696+y0*74)/64
		    Q16SAT(xr12, xr6, xr7);     // xr12 : b3 b1 b2 b0

		    Q16SAR(xr8, xr8, xr9, xr9, 6);         //xr8:(v1*102+y3*74-14240)/64,(v0*102-14240+y1*74)/64
  		                                           //xr9:(v1*102+y2*74-14240)/64,(v0*102-14240+y0*74)/64
		    Q16SAT(xr11, xr8, xr9);      // xr11 : r3 r1 r2 r0

		    //Calc : g3 g2 g1 g0
		    Q8MUL(xr6, xr1, xr13, xr7);             // xr6 : v1*52, v0*52
		                                            // xr7 : u1*25, u0*25
		    S32SFL(xr8, xr2, xr3, xr9, ptn3);       // xr8 : y3*74  y1*74
		                                            // xr9 : y2*74  y0*74
		    S32LDD(xr1, src_yhw_mb32, 16);          // xr1  : y19 - y16

		    Q16ACC_SS(xr8, xr10, xr6, xr0);         // xr8 :y3*74+8672-v1*52,y1*74+8672-v1*52
		    Q16ACC_SS(xr9, xr10, xr6, xr0);         // xr9 :y2*74+8672-v1*52,y0*74+8672-v1*52

		    Q8MUL(xr2, xr1, xr15, xr3);           // xr2 : y19*74, y18*74
		                                          // xr3 : y17*74, y16*74
		    Q16ADD_SS_WW(xr8, xr8, xr7, xr0);     // xr8 : y3*74+8672-v1*52-u1*25,y1*74+8672-v1*52-u1*25
		    Q16ADD_SS_WW(xr9, xr9, xr7, xr0);     // xr9 : y2*74+8672-v1*52-u1*25,y0*74+8672-v1*52-u1*25

		    Q16SAR(xr8, xr8, xr9, xr9, 6);          // xr8 : (y3*74+8672-v1*52-u1*25)/64,  y1...
		                                            // xr9 : y2..., y0...
		    Q16SAT(xr8, xr8, xr9);                  // xr8 : g3, g1 , g2 , g0 

		    S32SFL(xr8, xr8, xr11, xr9, ptn2);      // xr8 : g3 r3 g2 r2  xr9 : g1 r1 g0 r0

		    S32SFL(xr9, xr12, xr9, xr10, ptn3);      //xr9 : b3 b1 g1 r1  xr10 : b2 b0 g0 r0

		    S32SDIV(xr10, dst_h_mb1, dst_width_2, 0);  
		    S32STD(xr9, dst_h_mb1, 4);

		    S32SFL(xr9, xr9, xr10, xr0, ptn1);      // xr9 :b3, X, b2, X
		    D32SLR(xr9, xr9, xr0, xr0, 8);          // xr9 : xr2 >> 8 ---> xr8 : X, b3, X, b1
		    S32SFL(xr8, xr9, xr8, xr9, ptn3);       // xr8 : X b3 g3 r3   xr9:X b2 g2 r2 

		    S32STD(xr9, dst_h_mb1, 8);
		    S32STD(xr8, dst_h_mb1, 12);

		    // Calc : b19 - b16
		    S32I2M(xr12, 0x45204520);            // xr12 :17696, 17696,Calc : b
		    S32I2M(xr11, 0x37a037a0);            // xr11 :14240, 14240,Calc : r

		    S32SFL(xr1, xr2, xr3, xr8, ptn3);    // xr1 : y19*74  y17*74
		                                         // xr8 : y18*74  y16*74
		    S32SFL(xr9, xr2, xr3, xr10, ptn3);   // xr9 : y19*74  y17*74
		                                         // xr10 : y18*74  y16*74
		    S32SFL(xr2, xr2, xr3, xr3, ptn3);    // xr2 : y19*74  y17*74
		                                         // xr3 : y18*74  y16*74
		    Q16ACC_SS(xr1, xr5, xr12, xr0);      // xr1 : u1*129-17696+y3*74, u0*129-17696+y1*74
		    Q16ACC_SS(xr8, xr5, xr12, xr0);      // xr8 : u1*129-17696+y2*74, u0*129-17696+y0*74

		    Q16ACC_SS(xr9, xr4, xr11, xr0);       // xr9  : v1*102-14240+y19*74, v0*102-14240+y17*74
		    Q16ACC_SS(xr10, xr4, xr11, xr0);      // xr10 : v1*102-14240+y18*74, v0*102-14240+y16*74

		    Q16SAR(xr11, xr1, xr8, xr12, 6);    //xr11:(u1*129+y19*74-17696)/64,(u0*129-17696+y17*74)/64
		                                        //xr12:(u1*129+y18*74-17696)/64,(u0*129-17696+y16*74)/64
		    Q16SAT(xr12, xr11, xr12);   // xr12 : b19, b17, b18, b16

		    Q16SAR(xr4, xr9, xr10, xr5, 6);     //xr9 :(v1*102+y19*74-14240)/64,(v0*102-14240+y18*74)/64
		                                        //xr10:(v1*102+y17*74-14240)/64,(v0*102-14240+y16*74)/64
		    S32I2M(xr10, 0x21e021e0);              // xr10 : 8672, 8672     , Calc : g
		    Q16ACC_SS(xr2, xr10, xr6, xr0);        // xr2:y19*74+8672-v1*52, y17...
		    Q16ACC_SS(xr3, xr10, xr6, xr0);        // xr3:y18*74+8672-v1*52, y16...

		    Q16SAT(xr11, xr4, xr5);    //xr11: r19 r17 r18 r16

		    //Calc: g19 - g16
		    Q16ADD_SS_WW(xr2, xr2, xr7, xr0);      //xr2:y19*74+8672-v1*52-u1*25, y17...
		    Q16ADD_SS_WW(xr3, xr3, xr7, xr0);      //xr3:y18..., y16...

		    Q16SAR(xr8, xr2, xr3, xr9, 6);

		    Q16SAT(xr2, xr8, xr9);                 // xr2 : g19, g17, g18, g16

		    S32SFL(xr1, xr2, xr11, xr3, ptn2);     // xr1 : g19, r19, g18, r18  
		                                           // xr3 : g17, r17, g16, r16
		    S32SFL(xr8, xr12, xr3, xr9, ptn3);     // xr8 : b19, b17, g17, r17,
		                                           // xr9 : b18 b16, g16, r16
		    S32SDIV(xr9, dst_h_mb2, dst_width_2, 0);
		    S32STD(xr8, dst_h_mb2, 4);

		    S32SFL(xr2, xr8, xr9, xr0, ptn1);     // xr2 :b19, X, b18, X
		    D32SLR(xr8, xr2, xr0, xr0, 8);        // xr8 : xr2 >> 8 ---> xr8 : X, b19, X, b18
		    S32SFL(xr2, xr8, xr1,xr3, ptn3);      //xr2 : X, b19, g19, r19
		                                          //xr3 : X, b18, g18, r18
		    S32STD(xr3, dst_h_mb2, 8);
		    S32STD(xr2, dst_h_mb2, 12);

		    //Calc : 4 5 6 7
		    S32LDD(xr1, src_yhw_mb32, 4);
		    S32I2M(xr12, 0x45204520);

		    Q8MUL(xr2, xr1, xr15, xr3);

		    S16LDD(xr1, src_chw_mb16, 2, 0);
		    S16LDD(xr1, src_chw_mb16, 10, 1);

		    S32SFL(xr6, xr2, xr3, xr7, ptn3);
		    S32SFL(xr8, xr2, xr3, xr9, ptn3);

		    Q8MUL(xr4, xr1, xr14, xr5);

		    S32I2M(xr11, 0x37a037a0);
		    S32I2M(xr10, 0x21e021e0);

		    Q16ACC_SS(xr6, xr5, xr12, xr0);
		    Q16ACC_SS(xr7, xr5, xr12, xr0);

		    Q16ACC_SS(xr8, xr4, xr11, xr0);
		    Q16ACC_SS(xr9, xr4, xr11, xr0);

		    Q16SAR(xr6, xr6, xr7, xr7, 6);
		    Q16SAT(xr12, xr6, xr7);

		    Q16SAR(xr8, xr8, xr9, xr9, 6);
		    Q16SAT(xr11, xr8, xr9);

		    //Calc : g7 - g4
		    Q8MUL(xr6, xr1, xr13, xr7);

		    S32SFL(xr8, xr2, xr3, xr9, ptn3);

		    S32LDD(xr1, src_yhw_mb32, 20);

		    Q16ACC_SS(xr8, xr10, xr6, xr0);
		    Q16ACC_SS(xr9, xr10, xr6, xr0);

		    Q8MUL(xr2, xr1, xr15, xr3);

		    Q16ADD_SS_WW(xr8, xr8, xr7, xr0);
		    Q16ADD_SS_WW(xr9, xr9, xr7, xr0);
		    Q16SAR(xr8, xr8, xr9, xr9, 6);
		    Q16SAT(xr8, xr8, xr9);

		    S32SFL(xr8, xr8, xr11, xr9, ptn2);
		    S32SFL(xr9, xr12, xr9, xr10, ptn3);
		    S32STD(xr10, dst_h_mb1, 16);  
		    S32STD(xr9, dst_h_mb1, 20);

		    S32SFL(xr9, xr9, xr10, xr0, ptn1);
		    D32SLR(xr9, xr9, xr0, xr0, 8);
		    S32SFL(xr8, xr9, xr8, xr9, ptn3);		    
		    S32STD(xr9, dst_h_mb1, 24);
		    S32STD(xr8, dst_h_mb1, 28);

		    // Calc : b23 - b20
		    S32I2M(xr12, 0x45204520);
		    S32I2M(xr11, 0x37a037a0);

		    S32SFL(xr1, xr2, xr3, xr8, ptn3);
		    S32SFL(xr9, xr2, xr3, xr10, ptn3);
		    S32SFL(xr2, xr2, xr3, xr3, ptn3);

		    Q16ACC_SS(xr1, xr5, xr12, xr0);
		    Q16ACC_SS(xr8, xr5, xr12, xr0);

		    Q16ACC_SS(xr9, xr4, xr11, xr0);
		    Q16ACC_SS(xr10, xr4, xr11, xr0);

		    Q16SAR(xr11, xr1, xr8, xr12, 6);
		    Q16SAT(xr12, xr11, xr12);

		    Q16SAR(xr4, xr9, xr10, xr5, 6);

		    S32I2M(xr10, 0x21e021e0);
		    Q16ACC_SS(xr2, xr10, xr6, xr0);
		    Q16ACC_SS(xr3, xr10, xr6, xr0);

		    Q16SAT(xr11, xr4, xr5);

		    //Calc: g23 - g20
		    Q16ADD_SS_WW(xr2, xr2, xr7, xr0);
		    Q16ADD_SS_WW(xr3, xr3, xr7, xr0);

		    Q16SAR(xr8, xr2, xr3, xr9, 6);
		    Q16SAT(xr2, xr8, xr9);

		    S32SFL(xr1, xr2, xr11, xr3, ptn2);
		    S32SFL(xr8, xr12, xr3, xr9, ptn3);
		    S32STD(xr9, dst_h_mb2, 16);
		    S32STD(xr8, dst_h_mb2, 20);

		    S32SFL(xr2, xr8, xr9, xr0, ptn1);
		    D32SLR(xr8, xr2, xr0, xr0, 8);
		    S32SFL(xr2, xr8, xr1,xr3, ptn3);
		    S32STD(xr3, dst_h_mb2, 24);
		    S32STD(xr2, dst_h_mb2, 28);

		    //Calc : 8 9 10 11
		    S32LDD(xr1, src_yhw_mb32, 8);
		    S32I2M(xr12, 0x45204520);

		    Q8MUL(xr2, xr1, xr15, xr3);

		    S16LDD(xr1, src_chw_mb16, 4, 0);
		    S16LDD(xr1, src_chw_mb16, 12, 1);

		    S32SFL(xr6, xr2, xr3, xr7, ptn3);
		    S32SFL(xr8, xr2, xr3, xr9, ptn3);

		    Q8MUL(xr4, xr1, xr14, xr5);

		    S32I2M(xr11, 0x37a037a0);
		    S32I2M(xr10, 0x21e021e0);

		    Q16ACC_SS(xr6, xr5, xr12, xr0);
		    Q16ACC_SS(xr7, xr5, xr12, xr0);

		    Q16ACC_SS(xr8, xr4, xr11, xr0);
		    Q16ACC_SS(xr9, xr4, xr11, xr0);

		    Q16SAR(xr6, xr6, xr7, xr7, 6);
		    Q16SAT(xr12, xr6, xr7);

		    Q16SAR(xr8, xr8, xr9, xr9, 6);
		    Q16SAT(xr11, xr8, xr9);

		    //Calc : g11 - g8
		    Q8MUL(xr6, xr1, xr13, xr7);

		    S32SFL(xr8, xr2, xr3, xr9, ptn3);

		    S32LDD(xr1, src_yhw_mb32, 24);

		    Q16ACC_SS(xr8, xr10, xr6, xr0);
		    Q16ACC_SS(xr9, xr10, xr6, xr0);

		    Q8MUL(xr2, xr1, xr15, xr3);

		    Q16ADD_SS_WW(xr8, xr8, xr7, xr0);
		    Q16ADD_SS_WW(xr9, xr9, xr7, xr0);
		    Q16SAR(xr8, xr8, xr9, xr9, 6);
		    Q16SAT(xr8, xr8, xr9);

		    S32SFL(xr8, xr8, xr11, xr9, ptn2);
		    S32SFL(xr9, xr12, xr9, xr10, ptn3);
		    S32STD(xr10, dst_h_mb1, 32);  
		    S32STD(xr9, dst_h_mb1, 36);

		    S32SFL(xr9, xr9, xr10, xr0, ptn1);
		    D32SLR(xr9, xr9, xr0, xr0, 8);
		    S32SFL(xr8, xr9, xr8, xr9, ptn3);		    
		    S32STD(xr9, dst_h_mb1, 40);
		    S32STD(xr8, dst_h_mb1, 44);

		    // Calc : b27 - b24
		    S32I2M(xr12, 0x45204520);
		    S32I2M(xr11, 0x37a037a0);

		    S32SFL(xr1, xr2, xr3, xr8, ptn3);
		    S32SFL(xr9, xr2, xr3, xr10, ptn3);
		    S32SFL(xr2, xr2, xr3, xr3, ptn3);

		    Q16ACC_SS(xr1, xr5, xr12, xr0);
		    Q16ACC_SS(xr8, xr5, xr12, xr0);

		    Q16ACC_SS(xr9, xr4, xr11, xr0);
		    Q16ACC_SS(xr10, xr4, xr11, xr0);

		    Q16SAR(xr11, xr1, xr8, xr12, 6);
		    Q16SAT(xr12, xr11, xr12);

		    Q16SAR(xr4, xr9, xr10, xr5, 6);

		    S32I2M(xr10, 0x21e021e0);
		    Q16ACC_SS(xr2, xr10, xr6, xr0);
		    Q16ACC_SS(xr3, xr10, xr6, xr0);

		    Q16SAT(xr11, xr4, xr5);

		    //Calc: g27 - g24
		    Q16ADD_SS_WW(xr2, xr2, xr7, xr0);
		    Q16ADD_SS_WW(xr3, xr3, xr7, xr0);

		    Q16SAR(xr8, xr2, xr3, xr9, 6);
		    Q16SAT(xr2, xr8, xr9);

		    S32SFL(xr1, xr2, xr11, xr3, ptn2);
		    S32SFL(xr8, xr12, xr3, xr9, ptn3);
		    S32STD(xr9, dst_h_mb2, 32);
		    S32STD(xr8, dst_h_mb2, 36);

		    S32SFL(xr2, xr8, xr9, xr0, ptn1);
		    D32SLR(xr8, xr2, xr0, xr0, 8);
		    S32SFL(xr2, xr8, xr1,xr3, ptn3);
		    S32STD(xr3, dst_h_mb2, 40);
		    S32STD(xr2, dst_h_mb2, 44);

		    //Calc : 12 13 14 15
		    S32LDD(xr1, src_yhw_mb32, 12);
		    S32I2M(xr12, 0x45204520);

		    Q8MUL(xr2, xr1, xr15, xr3);

		    S16LDD(xr1, src_chw_mb16, 6, 0);
		    S16LDD(xr1, src_chw_mb16, 14, 1);

		    S32SFL(xr6, xr2, xr3, xr7, ptn3);
		    S32SFL(xr8, xr2, xr3, xr9, ptn3);

		    Q8MUL(xr4, xr1, xr14, xr5);

		    S32I2M(xr11, 0x37a037a0);
		    S32I2M(xr10, 0x21e021e0);

		    Q16ACC_SS(xr6, xr5, xr12, xr0);
		    Q16ACC_SS(xr7, xr5, xr12, xr0);

		    Q16ACC_SS(xr8, xr4, xr11, xr0);
		    Q16ACC_SS(xr9, xr4, xr11, xr0);

		    Q16SAR(xr6, xr6, xr7, xr7, 6);
		    Q16SAT(xr12, xr6, xr7);

		    Q16SAR(xr8, xr8, xr9, xr9, 6);
		    Q16SAT(xr11, xr8, xr9);

		    //Calc : g15 - g12
		    Q8MUL(xr6, xr1, xr13, xr7);

		    S32SFL(xr8, xr2, xr3, xr9, ptn3);

		    S32LDD(xr1, src_yhw_mb32, 28);

		    Q16ACC_SS(xr8, xr10, xr6, xr0);
		    Q16ACC_SS(xr9, xr10, xr6, xr0);

		    Q8MUL(xr2, xr1, xr15, xr3);

		    Q16ADD_SS_WW(xr8, xr8, xr7, xr0);
		    Q16ADD_SS_WW(xr9, xr9, xr7, xr0);
		    Q16SAR(xr8, xr8, xr9, xr9, 6);
		    Q16SAT(xr8, xr8, xr9);

		    S32SFL(xr8, xr8, xr11, xr9, ptn2);
		    S32SFL(xr9, xr12, xr9, xr10, ptn3);
		    S32STD(xr10, dst_h_mb1, 48);
		    S32STD(xr9, dst_h_mb1, 52);

		    S32SFL(xr9, xr9, xr10, xr0, ptn1);
		    D32SLR(xr9, xr9, xr0, xr0, 8);
		    S32SFL(xr8, xr9, xr8, xr9, ptn3);		    
		    S32STD(xr9, dst_h_mb1, 56);
		    S32STD(xr8, dst_h_mb1, 60);

		    // Calc : b31 - b28
		    S32I2M(xr12, 0x45204520);
		    S32I2M(xr11, 0x37a037a0);

		    S32SFL(xr1, xr2, xr3, xr8, ptn3);
		    S32SFL(xr9, xr2, xr3, xr10, ptn3);
		    S32SFL(xr2, xr2, xr3, xr3, ptn3);

		    Q16ACC_SS(xr1, xr5, xr12, xr0);
		    Q16ACC_SS(xr8, xr5, xr12, xr0);

		    Q16ACC_SS(xr9, xr4, xr11, xr0);
		    Q16ACC_SS(xr10, xr4, xr11, xr0);

		    Q16SAR(xr11, xr1, xr8, xr12, 6);
		    Q16SAT(xr12, xr11, xr12);

		    Q16SAR(xr4, xr9, xr10, xr5, 6);

		    S32I2M(xr10, 0x21e021e0);
		    Q16ACC_SS(xr2, xr10, xr6, xr0);
		    Q16ACC_SS(xr3, xr10, xr6, xr0);

		    Q16SAT(xr11, xr4, xr5);

		    //Calc: g31 - g28
		    Q16ADD_SS_WW(xr2, xr2, xr7, xr0);
		    Q16ADD_SS_WW(xr3, xr3, xr7, xr0);

		    Q16SAR(xr8, xr2, xr3, xr9, 6);
		    Q16SAT(xr2, xr8, xr9);

		    S32SFL(xr1, xr2, xr11, xr3, ptn2);
		    S32SFL(xr8, xr12, xr3, xr9, ptn3);
		    S32STD(xr9, dst_h_mb2, 48);
		    S32STD(xr8, dst_h_mb2, 52);

		    S32SFL(xr2, xr8, xr9, xr0, ptn1);
		    D32SLR(xr8, xr2, xr0, xr0, 8);
		    S32SFL(xr2, xr8, xr1,xr3, ptn3);
		    S32STD(xr3, dst_h_mb2, 56);
		    S32STD(xr2, dst_h_mb2, 60);
		    dst_pref+=dst_width_2;
		}	    
		src_yhw += 256;
		src_chw += 128;
		dst_h_mbcol+=64;
	    }
	    src_yh += y_stride;
	    src_ch += c_stride;
	    dst_h  += dst_stride;
	}

	return 0;
    }

    int tile2RGB_half(void *data, void *data1, size_t src_width, size_t src_height, size_t dstStride)
    {
	INGENIC_OMX_YUV_FORMAT* pimg = (INGENIC_OMX_YUV_FORMAT*)data;
	unsigned char *src_y = reinterpret_cast<unsigned char*>(pimg->nPlanar[0]);
	unsigned char *src_c = reinterpret_cast<unsigned char*>(pimg->nPlanar[1]);
	unsigned int y_stride = pimg->nStride[0];
	unsigned int c_stride = pimg->nStride[1];
	unsigned char *dst = (unsigned char *)data1;

	size_t n_width = src_width / 16;
	size_t n_height = src_height / 16;

	size_t width = dstStride * 2;

	unsigned char *src_yh = src_y;
	unsigned char *src_ch = src_c;
	unsigned char *dst_h  = dst;
	size_t dst_stride = (width/2) * (16/2) * 4;
	size_t dst_width = (width/2) * 4;

	S32I2M(xr15, 0x4a4a4a4a);              // xr15 : 74, 74, 74, 74
	S32I2M(xr14, 0x81818181);              // xr14 : 129,129,129,129 , Calc: b
	S32I2M(xr13, 0x66666666);              // xr13 : 102,102,102,102 , Calc: r
	S32I2M(xr12, 0x19191919);              // xr13 : 25, 25 , 25, 25 ,  Calc: g
	S32I2M(xr11, 0x34343434);              // xr13 : 52, 52, 52, 52, ,  Calc: g

	for (size_t mbrow = 0; mbrow < n_height; ++mbrow ) {
	    unsigned char *src_yhw = src_yh;
	    unsigned char *src_chw = src_ch;
	    unsigned char *dst_hw = dst_h;

	    for (size_t mbcol = 0; mbcol < n_width ; ++mbcol) {	    
		unsigned char *src_yhw_mb32 = src_yhw - 32;
		unsigned char *src_chw_mb16 = src_chw - 16;
		unsigned char *dst_h_mb = dst_hw - dst_width;
		unsigned char *dst_pref = dst_hw;
		i_pref(1, src_yhw, 0);
		i_pref(1, src_chw, 0);

		for (size_t i = 0; i < 8; ++i ){          //row

		    i_pref(30, dst_pref, 0);

		    S32LDI(xr1, src_yhw_mb32, 32);       // xr1 : y3, y2, y1, y0
		    S32LDD(xr2, src_yhw_mb32, 4);        // xr2 : y7, y6, y5, y4
		    S32I2M(xr10, 0x45204520);            // xr12 : 17696, 17696  , Calc : b
		    S32SFL(xr0, xr2, xr1, xr3 , ptn1);   // xr3 : y6 y4 y2 y0

		    Q8MUL(xr1, xr3, xr15, xr2);          // xr1 : y6*74, y4*74
		                                         // xr2 : y2*74, y0*74
		    S32LDI(xr3, src_chw_mb16, 16);       // xr3 : u3,u2,u1,u0
		    S32LDD(xr4, src_chw_mb16, 8);        // xr4 : v3,v2,v1,u0

		    Q8MUL(xr5, xr3, xr14, xr6);          // xr5 : 129*u3, 129*u2
		                                         // xr6 : 129*u1, 129*u0
		    S32I2M(xr9, 0x37a037a0);             // xr9 : 14240, 14240  , Calc : r
		    Q8MUL(xr7, xr4, xr13, xr8);          // xr7 : 102*v3, 102*v2
		                                         // xr8 : 102*v1, 102*v0
		    Q16ACC_SS(xr5, xr1, xr10, xr0);      // xr5 : 129*u3+74*y6-17696  129*u2+74*y4-17696  
		    Q16ACC_SS(xr6, xr2, xr10, xr0);      // xr6 : 129*u1+74*y6-17696  129*u0+74*y4-17696  
		    Q16ACC_SS(xr7, xr1, xr9, xr0);       // xr7 : y6*74+102*v3-14240,y4*74+102*v2-14240
		    Q16ACC_SS(xr8, xr2, xr9, xr0);       // xr8 : y2*74+102*v1-14240,y0*74+102*v0-14240

		    Q16SAR(xr5, xr5, xr6, xr6, 6);       //xr5:(y6*74+129*u3-17696)/64,(y4*74+129*u3-17696)/64
 		                                         //xr6:(y2*74+129*u3-17696)/64,(y0*74+129*u3-17696)/64
		    Q16SAT(xr10, xr5, xr6);              //xr10 : b6 b4 b2 b0

		    Q16SAR(xr7, xr7, xr8, xr8, 6);        //xr7:(y6*74+102*v3-14240)/64,(y4*74+102*v3-14240)/64
		                                          //xr8:(y2*74+102*v1-14240)/64,(y0*74+102*v0-14240)/64
		    Q16SAT(xr9, xr7, xr8);                //xr9 : r6 r4 r2 r0

		    //Calc : g6 g4 g2 g0
		    Q8MUL(xr5, xr3, xr12, xr6);           // xr5 : 25*u3, 25*u2
		                                          // xr6 : 25*u1, 25*u0
		    Q8MUL(xr3, xr4, xr11, xr4);           // xr3 : 52*v3, 52*v2
		                                          // xr4 : 52*v1, 52*v0
		    S32I2M(xr8, 0x21e021e0);              // xr10 : 8672, 8672    , Calc : g
		    Q16ACC_SS(xr1, xr8, xr5, xr0);        // xr1 : 74*y6+8672-25*u3 , 74*y4+8672-25*u2
		    Q16ACC_SS(xr2, xr8, xr6, xr0);        // xr2 : 74*y2+8672-25*u1,  74*y0+8672-25*u0

		    S32LDD(xr5, src_yhw_mb32, 8);
		    S32LDD(xr6, src_yhw_mb32, 12);

		    Q16ADD_SS_WW(xr3, xr1, xr3, xr0);     // xr3: 74*y6+8672-25*u3-52*v3, 74*y4+8672-25*u2-52*v3
		    Q16ADD_SS_WW(xr4, xr2, xr4, xr0);     // xr4: 74*y2+8672-25*u1-52*v1, 74*y0+8672-25*u0-52*v0

		    Q16SAR(xr3, xr3, xr4, xr4, 6);
		    Q16SAT(xr1, xr3, xr4);                // xr1 : g6 g4 g2 g0

		    S32SFL(xr2, xr1, xr9, xr3, ptn2);     // xr2 : g6 r6 g2 r2   xr3 : g4 r4 g0 r0 

		    S32SFL(xr3, xr10, xr3, xr4, ptn3);    //xr3 : b6 b4 g4 r4  xr4 : b2 b0 g0 r0

		    S32SDIV(xr4, dst_h_mb, dst_width, 0);
		    S32STD(xr3, dst_h_mb, 8);

		    S32SFL(xr3, xr3, xr4, xr0, ptn1);     // xr1 :b6, X, b2, X
		    D32SLR(xr3, xr3, xr0, xr0, 8);        // xr1 : xr1 >> 8 ---> xr3 : X, b6, X, b2
		    S32SFL(xr2, xr3, xr2,xr3, ptn3);      //xr4 : X b6 g6 r6   xr5:X b2 g2 r2 

		    S32STD(xr3, dst_h_mb, 4);
		    S32STD(xr2, dst_h_mb, 12);

		    // Calc : 8 10 12 14
		    // Calc : b14, b12, b10, b8
		    S32I2M(xr10, 0x45204520);
		    S32SFL(xr0, xr6, xr5, xr3 , ptn1);

		    Q8MUL(xr1, xr3, xr15, xr2);

		    S32LDD(xr3, src_chw_mb16, 4);
		    S32LDD(xr4, src_chw_mb16, 12);

		    Q8MUL(xr5, xr3, xr14, xr6);
		    Q8MUL(xr7, xr4, xr13, xr8);
		    S32I2M(xr9, 0x37a037a0);

		    Q16ACC_SS(xr5, xr1, xr10, xr0);
		    Q16ACC_SS(xr6, xr2, xr10, xr0);
		    Q16ACC_SS(xr7, xr1, xr9, xr0);
		    Q16ACC_SS(xr8, xr2, xr9, xr0);

		    Q16SAR(xr7, xr7, xr8, xr8, 6); 
		    Q16SAT(xr9, xr7, xr8);

		    Q8MUL(xr7, xr3, xr12, xr10);
		    Q8MUL(xr3, xr4, xr11, xr4);
		    S32I2M(xr8, 0x21e021e0);
		    Q16ACC_SS(xr1, xr8, xr7, xr0);
		    Q16ACC_SS(xr2, xr8, xr10, xr0);

		    Q16SAR(xr5, xr5, xr6, xr6, 6);     
		    Q16SAT(xr10, xr5, xr6);

		    //Calc : g14, g12, g10, g8
		    Q16ADD_SS_WW(xr3, xr1, xr3, xr0);
		    Q16ADD_SS_WW(xr4, xr2, xr4, xr0);

		    Q16SAR(xr5, xr3, xr4, xr6, 6);
		    Q16SAT(xr1, xr5, xr6);

		    S32SFL(xr2, xr1, xr9, xr3, ptn2);

		    S32SFL(xr4, xr10, xr3, xr5, ptn3);

		    S32STD(xr5, dst_h_mb, 16);
		    S32STD(xr4, dst_h_mb, 24);

		    S32SFL(xr1, xr4, xr5, xr0, ptn1);
		    D32SLR(xr3, xr1, xr0, xr0, 8);
		    S32SFL(xr4, xr3, xr2,xr5, ptn3);

		    S32STD(xr5, dst_h_mb, 20);
		    S32STD(xr4, dst_h_mb, 28);

		    dst_pref+=dst_width;
		}
		src_yhw += 256;
		src_chw += 128;
		dst_hw += 32;
	    }
	    src_yh += y_stride;
	    src_ch += c_stride;
	    dst_h += dst_stride;
	}

	return 0;
    }

    int tile2RGB_quarter(void *data, void *data1, size_t src_width, size_t src_height, size_t dstStride)
    {
	INGENIC_OMX_YUV_FORMAT* pimg = (INGENIC_OMX_YUV_FORMAT*)data;
	unsigned char *src_y = reinterpret_cast<unsigned char*>(pimg->nPlanar[0]);
	unsigned char *src_c = reinterpret_cast<unsigned char*>(pimg->nPlanar[1]);
	unsigned int y_stride = pimg->nStride[0];
	unsigned int c_stride = pimg->nStride[1];
	unsigned char *dst = (unsigned char *)data1;

	size_t n_width = src_width / 16;
	size_t n_height = src_height / 16;

	size_t width = dstStride * 4;

	unsigned char *src_yh = src_y;
	unsigned char *src_ch = src_c;
	unsigned char *dst_h  = dst;
	size_t dst_stride = (width/4) * (16/4) * 4;
	size_t dst_width = (width/4) * 4;

	S32I2M(xr15, 0x4a4a4a4a);              // xr15 : 74, 74, 74, 74
	S32I2M(xr14, 0x81818181);              // xr14 : 129,129,129,129 , Calc: b
	S32I2M(xr13, 0x66666666);              // xr13 : 102,102,102,102 , Calc: r
	S32I2M(xr12, 0x19191919);              // xr13 : 25, 25 , 25, 25 ,  Calc: g
	S32I2M(xr11, 0x34343434);              // xr13 : 52, 52, 52, 52, ,  Calc: g

	for (size_t mbrow = 0; mbrow < n_height; ++mbrow ) {
	    unsigned char *src_yhw = src_yh;
	    unsigned char *src_chw = src_ch;
	    unsigned char *dst_hw = dst_h;

	    for (size_t mbcol = 0; mbcol < n_width; ++mbcol) {	    
		unsigned char *src_yhw_mb32 = src_yhw - 64;
		unsigned char *src_chw_mb16 = src_chw - 32;
		unsigned char *dst_h_mb = dst_hw - dst_width;
		unsigned char *dst_pref1 = dst_hw;
		unsigned char *dst_pref2 = dst_pref1 + dst_width;
		unsigned char *dst_pref3 = dst_pref2 + dst_width;
		unsigned char *dst_pref4 = dst_pref3 + dst_width;

		i_pref(1, src_yhw, 0);
		i_pref(1, src_chw, 0);

		i_pref(30, dst_pref1, 0);
		i_pref(30, dst_pref2, 0);
		i_pref(30, dst_pref3, 0);
		i_pref(30, dst_pref4, 0);

		//Calc : b11, b8, b3, b0
		S32LDI(xr1, src_yhw_mb32, 64);                                // xr1 : y3, y2, y1, y0
		S32LDD(xr2, src_yhw_mb32, 8);                                 // xr2 : y11, y10, y9, y8

		S32LDI(xr5, src_chw_mb16, 32);                                // xr5 : u3,u2,u1,u0
		S32LDD(xr6, src_chw_mb16, 4);                                 // xr6 : u7,u6,u5,u4
		S32LDD(xr7, src_chw_mb16, 8);                                 // xr7 : v3,v2,v1,v0
		S32LDD(xr8, src_chw_mb16, 12);                                // xr8 : v7,v6,v5,v4

		S32SFL(xr2, xr2, xr1, xr1, ptn1);                        // xr2: y11, X, y3, X,xr1: X, y8, X, y0
		D32SLRV(xr2, xr0, 8);                                         // xr2 : X, y11, X, y3
		S32SFL(xr0, xr2, xr1, xr1, ptn2);                             // xr1 : y11, y8, y3, y0
		Q8MUL(xr1, xr1, xr15, xr2);           // xr1 : y11*74, y8*74
		                                      // xr2 : y3*74,  y0*74
		S32SFL(xr0, xr6, xr5, xr3, ptn3);    // xr3 : u5, u4, u1, u0
		S32SFL(xr0, xr8, xr7, xr4, ptn3);    // xr4 : v5, v4, v1, v0

		Q8MUL(xr5, xr3, xr14, xr6);          // xr5 : 129*u5, 129*u4
		                                     // xr6 : 129*u1, 129*u0
		Q8MUL(xr7, xr4, xr13, xr8);
		S32I2M(xr10, 0x45204520);                                     // xr12 : 17696, 17696  , Calc : b
		S32I2M(xr9, 0x37a037a0);

		Q16ACC_SS(xr5, xr1, xr10, xr0);      // xr5 : 129*u5+74*y11-17696  129*u4+74*y8-17696
		Q16ACC_SS(xr6, xr2, xr10, xr0);      // xr6 : 129*u1+74*y3-17696   129*u0+74*y0-17696

		Q16ACC_SS(xr7, xr1, xr9, xr0);        
		Q16ACC_SS(xr8, xr2, xr9, xr0);

		Q16SAR(xr5, xr5, xr6, xr6, 6);       //xr5:(129*u6+74*y12-17696)/64,(129*u4+74*y8-17696)/64
		                                     //xr6:(129*u2+74*y4-17696)/64,(129*u0+74*y0-17696)/64
		Q16SAT(xr10, xr5, xr6);              //xr10 : b11 b8 b3 b0
		Q8MUL(xr5, xr3, xr12, xr6);           // xr5 : 25*u6, 25*u4
		                                      // xr6 : 25*u2, 25*u0
		Q16SAR(xr7, xr7, xr8, xr8, 6);
		Q16SAT(xr9, xr7, xr8);                //xr9 : r11 r8 rb3 b0

		//g11 g8 gb3 b0
		S32I2M(xr8, 0x21e021e0);              // xr10 : 8672, 8672    , Calc : g
		Q16ACC_SS(xr1, xr8, xr5, xr0);        // xr1 : 74*y12+8672-25*u6 , 74*y8+8672-25*u4
		Q16ACC_SS(xr2, xr8, xr6, xr0);        // xr2 : 74*y4+8672-25*u2,  74*y0+8672-25*u0

		//insert xr7 , xr8
		S32LDI(xr7, src_yhw_mb32, 64);
		S32LDD(xr8, src_yhw_mb32, 8);

		Q8MUL(xr5, xr4, xr11, xr6);           // xr5 : 52*v6, 52*v4// xr6 : 52*v2, 52*v0

		S32SFL(xr8, xr8, xr7, xr7, ptn1);
		D32SLRV(xr8, xr0, 8);
		S32SFL(xr0, xr8, xr7, xr7, ptn2);

		Q16ADD_SS_WW(xr3, xr1, xr5, xr0);     // xr3 : 74*y12+8672-25*u6-52*v6, 74*y8+8672-25*u4-52*v4
		Q16ADD_SS_WW(xr4, xr2, xr6, xr0);     // xr4 : 74*y4+8672-25*u2-52*v2,  74*y0+8672-25*u0-52*v0

		Q16SAR(xr5, xr3, xr4, xr6, 6);
		Q16SAT(xr1, xr5, xr6);                // xr1 : g11 g8 gb3 b0

		S32SFL(xr2, xr1, xr9, xr3, ptn2);     // xr2 : g6 r6 g2 r2   xr3 : g4 r4 g0 r0 		 
		S32SFL(xr4, xr10, xr3, xr5, ptn3);    //xr4 : b6 b4 g4 r4  xr5 : b2 b0 g0 r0
		S32SDIV(xr5, dst_h_mb, dst_width, 0);
		S32STD(xr4, dst_h_mb, 8);

		S32SFL(xr1, xr4, xr5, xr0, ptn1);    // xr1 :b6, X, b2, X
		D32SLR(xr3, xr1, xr0, xr0, 8);       // xr1 : xr1 >> 8 ---> xr3 : X, b6, X, b2
		S32SFL(xr4, xr3, xr2,xr5, ptn3);     //xr4 : X g6 b6 r6   xr5:X g2 b2 r2 
		S32STD(xr5, dst_h_mb, 4);
		S32STD(xr4, dst_h_mb, 12);

		//Col 4
		Q8MUL(xr1, xr7, xr15, xr2);

		S32LDI(xr5, src_chw_mb16, 32);
		S32LDD(xr6, src_chw_mb16, 4);
		S32LDD(xr7, src_chw_mb16, 8);
		S32LDD(xr8, src_chw_mb16, 12);
		S32SFL(xr0, xr6, xr5, xr3, ptn3);
		S32SFL(xr0, xr8, xr7, xr4, ptn3);

		Q8MUL(xr5, xr3, xr14, xr6);
		Q8MUL(xr7, xr4, xr13, xr8);

		S32I2M(xr10, 0x45204520);
		Q16ACC_SS(xr5, xr1, xr10, xr0);
		Q16ACC_SS(xr6, xr2, xr10, xr0);
		S32I2M(xr9, 0x37a037a0);
		Q16ACC_SS(xr7, xr1, xr9, xr0);
		Q16ACC_SS(xr8, xr2, xr9, xr0);

		Q16SAR(xr5, xr5, xr6, xr6, 6);
		Q16SAT(xr10, xr5, xr6);

		Q8MUL(xr5, xr3, xr12, xr6);

		Q16SAR(xr7, xr7, xr8, xr8, 6);
		Q16SAT(xr9, xr7, xr8);

		//col 4 :g11 g8 gb3 b0
		S32I2M(xr8, 0x21e021e0);
		Q16ACC_SS(xr1, xr8, xr5, xr0);
		Q16ACC_SS(xr2, xr8, xr6, xr0);

		//insert : xr7, xr8
		S32LDI(xr7, src_yhw_mb32, 64);
		S32LDD(xr8, src_yhw_mb32, 8);

		Q8MUL(xr5, xr4, xr11, xr6);

		//insert : xr7, xr8
		S32SFL(xr8, xr8, xr7, xr7, ptn1);
		D32SLRV(xr8, xr0, 8);
		S32SFL(xr0, xr8, xr7, xr7, ptn2);

		Q16ADD_SS_WW(xr3, xr1, xr5, xr0);
		Q16ADD_SS_WW(xr4, xr2, xr6, xr0);

		Q16SAR(xr5, xr3, xr4, xr6, 6);
		Q16SAT(xr1, xr5, xr6);

		S32SFL(xr2, xr1, xr9, xr3, ptn2);

		S32SFL(xr4, xr10, xr3, xr5, ptn3);

		S32SDIV(xr5, dst_h_mb, dst_width, 0);
		S32STD(xr4, dst_h_mb, 8);

		S32SFL(xr1, xr4, xr5, xr0, ptn1);
		D32SLR(xr3, xr1, xr0, xr0, 8);
		S32SFL(xr4, xr3, xr2,xr5, ptn3);

		S32STD(xr5, dst_h_mb, 4);
		S32STD(xr4, dst_h_mb, 12);

		//Col 8
		Q8MUL(xr1, xr7, xr15, xr2);

		S32LDI(xr5, src_chw_mb16, 32);
		S32LDD(xr6, src_chw_mb16, 4);
		S32LDD(xr7, src_chw_mb16, 8);
		S32LDD(xr8, src_chw_mb16, 12);
		S32SFL(xr0, xr6, xr5, xr3, ptn3);
		S32SFL(xr0, xr8, xr7, xr4, ptn3);

		Q8MUL(xr5, xr3, xr14, xr6);
		Q8MUL(xr7, xr4, xr13, xr8);

		S32I2M(xr10, 0x45204520);
		Q16ACC_SS(xr5, xr1, xr10, xr0);
		Q16ACC_SS(xr6, xr2, xr10, xr0);
		S32I2M(xr9, 0x37a037a0);
		Q16ACC_SS(xr7, xr1, xr9, xr0);
		Q16ACC_SS(xr8, xr2, xr9, xr0);

		Q16SAR(xr5, xr5, xr6, xr6, 6);
		Q16SAT(xr10, xr5, xr6);

		Q8MUL(xr5, xr3, xr12, xr6);

		Q16SAR(xr7, xr7, xr8, xr8, 6);
		Q16SAT(xr9, xr7, xr8);

		//col 8 :g11 g8 gb3 b0
		S32I2M(xr8, 0x21e021e0);
		Q16ACC_SS(xr1, xr8, xr5, xr0);
		Q16ACC_SS(xr2, xr8, xr6, xr0);

		//insert : xr7, xr8
		S32LDI(xr7, src_yhw_mb32, 64);
		S32LDD(xr8, src_yhw_mb32, 8);

		Q8MUL(xr5, xr4, xr11, xr6);

		//insert : xr7, xr8
		S32SFL(xr8, xr8, xr7, xr7, ptn1);
		D32SLRV(xr8, xr0, 8);
		S32SFL(xr0, xr8, xr7, xr7, ptn2);

		Q16ADD_SS_WW(xr3, xr1, xr5, xr0);
		Q16ADD_SS_WW(xr4, xr2, xr6, xr0);

		Q16SAR(xr5, xr3, xr4, xr6, 6);
		Q16SAT(xr1, xr5, xr6);

		S32SFL(xr2, xr1, xr9, xr3, ptn2);

		S32SFL(xr4, xr10, xr3, xr5, ptn3);

		S32SDIV(xr5, dst_h_mb, dst_width, 0);
		S32STD(xr4, dst_h_mb, 8);

		S32SFL(xr1, xr4, xr5, xr0, ptn1);
		D32SLR(xr3, xr1, xr0, xr0, 8);
		S32SFL(xr4, xr3, xr2,xr5, ptn3);

		S32STD(xr5, dst_h_mb, 4);
		S32STD(xr4, dst_h_mb, 12);

		//Col : 12 
		Q8MUL(xr1, xr7, xr15, xr2);

		S32LDI(xr5, src_chw_mb16, 32);
		S32LDD(xr6, src_chw_mb16, 4);
		S32LDD(xr7, src_chw_mb16, 8);
		S32LDD(xr8, src_chw_mb16, 12);
		S32SFL(xr0, xr6, xr5, xr3, ptn3);
		S32SFL(xr0, xr8, xr7, xr4, ptn3);

		Q8MUL(xr5, xr3, xr14, xr6);
		Q8MUL(xr7, xr4, xr13, xr8);

		S32I2M(xr10, 0x45204520);
		Q16ACC_SS(xr5, xr1, xr10, xr0);
		Q16ACC_SS(xr6, xr2, xr10, xr0);
		S32I2M(xr9, 0x37a037a0);
		Q16ACC_SS(xr7, xr1, xr9, xr0);
		Q16ACC_SS(xr8, xr2, xr9, xr0);

		Q16SAR(xr5, xr5, xr6, xr6, 6);
		Q16SAT(xr10, xr5, xr6);

		Q8MUL(xr5, xr3, xr12, xr6);

		Q16SAR(xr7, xr7, xr8, xr8, 6);
		Q16SAT(xr9, xr7, xr8);

		//col 12 :g11 g8 gb3 b0
		S32I2M(xr8, 0x21e021e0);
		Q16ACC_SS(xr1, xr8, xr5, xr0);
		Q16ACC_SS(xr2, xr8, xr6, xr0);

		Q8MUL(xr5, xr4, xr11, xr6);

		Q16ADD_SS_WW(xr3, xr1, xr5, xr0);
		Q16ADD_SS_WW(xr4, xr2, xr6, xr0);

		Q16SAR(xr5, xr3, xr4, xr6, 6);
		Q16SAT(xr1, xr5, xr6);

		S32SFL(xr2, xr1, xr9, xr3, ptn2);

		S32SFL(xr4, xr10, xr3, xr5, ptn3);

		S32SDIV(xr5, dst_h_mb, dst_width, 0);
		S32STD(xr4, dst_h_mb, 8);

		S32SFL(xr1, xr4, xr5, xr0, ptn1);
		D32SLR(xr3, xr1, xr0, xr0, 8);
		S32SFL(xr4, xr3, xr2,xr5, ptn3);

		S32STD(xr5, dst_h_mb, 4);
		S32STD(xr4, dst_h_mb, 12);

		src_yhw += 256;
		src_chw += 128;
		dst_hw += 16;
	    }
	    src_yh += y_stride;
	    src_ch += c_stride;
	    dst_h += dst_stride;
	}
	return 0;
    }
#else

    uint8_t *mClip;
    uint8_t *initClip() 
    {
	static const signed kClipMin = -278;
	static const signed kClipMax = 535;

	if (mClip == NULL) {
	    mClip = new uint8_t[kClipMax - kClipMin + 1];

	    for (signed i = kClipMin; i <= kClipMax; ++i) {
		mClip[i - kClipMin] = (i < 0) ? 0 : (i > 255) ? 255 : (uint8_t)i;
	    }
	}

	return &mClip[-kClipMin];
    }

    int tile2RGB(void *data, void *data1, size_t src_width, size_t src_height, size_t dstStride)
    {
	INGENIC_OMX_YUV_FORMAT* pimg = (INGENIC_OMX_YUV_FORMAT*)data;
	unsigned char *src_y = reinterpret_cast<unsigned char*>(pimg->nPlanar[0]);
	unsigned char *src_c = reinterpret_cast<unsigned char*>(pimg->nPlanar[1]);
	unsigned int y_stride = pimg->nStride[0];
	unsigned int c_stride = pimg->nStride[1];
	unsigned char *dst = (unsigned char *)data1;

	uint8_t *kAdjustedClip = initClip();

	size_t n_width = src_width / 16;
	size_t n_height = src_height / 16;

	size_t width = dstStride;
    
	unsigned char *src_yh = src_y;
	unsigned char *src_ch = src_c;
	unsigned char *dst_h = dst;
	unsigned int dst_stride = width * 16 * 4;
	size_t dst_width_2 = width * 4 * 2;
	size_t dst_width_1 = width * 4;
    
	for (size_t mbrow = 0; mbrow < n_height; ++mbrow ) {
	    unsigned char *src_yhw = src_yh;
	    unsigned char *src_chw = src_ch;
	    unsigned char *dst_hw = dst_h;

	    for (size_t mbcol = 0; mbcol < n_width ; ++mbcol) {
		unsigned char *dst_h1_mbcol = dst_hw;
		unsigned char *dst_h2_mbcol = dst_hw + dst_width_1;

		size_t y_row = 0;
		size_t c_row = 0;

		for (size_t i = 0; (i < 8); ++i ){          //row
		    int y0  = (int)src_yhw[y_row + 0] - 16;
		    int y1  = (int)src_yhw[y_row + 1] - 16;
		    int y16 = (int)src_yhw[y_row + 16] - 16;
		    int y17 = (int)src_yhw[y_row + 17] - 16;

		    int u0 = (int)src_chw[c_row + 0] - 128;
		    int v0 = (int)src_chw[c_row + 8] - 128;

		    int u_b0 = u0 * 129;
		    int u_g0 = -u0 * 25;
		    int v_g0 = -v0 * 52;
		    int v_r0 = v0 * 102;

		    int tmp0 = y0 * 74;
		    *(dst_h1_mbcol + 4*0 + 2 ) = kAdjustedClip[((tmp0 + u_b0) / 64)];
		    *(dst_h1_mbcol + 4*0 + 1 ) = kAdjustedClip[((tmp0 + v_g0 + u_g0) / 64)];
		    *(dst_h1_mbcol + 4*0 + 0 ) = kAdjustedClip[((tmp0 + v_r0) / 64)];

		    int tmp1 = y1 * 74;
		    *(dst_h1_mbcol + 4*1 + 2 ) = kAdjustedClip[((tmp1 + u_b0) / 64)];
		    *(dst_h1_mbcol + 4*1 + 1 ) = kAdjustedClip[((tmp1 + v_g0 + u_g0) / 64)];
		    *(dst_h1_mbcol + 4*1 + 0 ) = kAdjustedClip[((tmp1 + v_r0) / 64)];
		
		    int tmp16 = y16 * 74;
		    *(dst_h2_mbcol + 4*0 + 2 ) = kAdjustedClip[((tmp16 + u_b0) / 64)];
		    *(dst_h2_mbcol + 4*0 + 1 ) = kAdjustedClip[((tmp16 + v_g0 + u_g0) / 64)];
		    *(dst_h2_mbcol + 4*0 + 0 ) = kAdjustedClip[((tmp16 + v_r0) / 64)];
		
		    int tmp17 = y17 * 74;
		    *(dst_h2_mbcol + 4*1 + 2 ) = kAdjustedClip[((tmp17 + u_b0) / 64)];
		    *(dst_h2_mbcol + 4*1 + 1 ) = kAdjustedClip[((tmp17 + v_g0 + u_g0) / 64)];
		    *(dst_h2_mbcol + 4*1 + 0 ) = kAdjustedClip[((tmp17 + v_r0) / 64)];

		    int y2  = (int)src_yhw[y_row + 2] - 16;
		    int y3  = (int)src_yhw[y_row + 3] - 16;
		    int y18 = (int)src_yhw[y_row + 18] - 16;
		    int y19 = (int)src_yhw[y_row + 19] - 16;

		    int u1 = (int)src_chw[c_row + 1] - 128;
		    int v1 = (int)src_chw[c_row + 9] - 128;

		    int u_b1 = u1 * 129;
		    int u_g1 = -u1 * 25;
		    int v_g1 = -v1 * 52;
		    int v_r1 = v1 * 102;

		    int tmp2 = y2 * 74;
		    *(dst_h1_mbcol + 4*2 + 2 ) = kAdjustedClip[((tmp2 + u_b1) / 64)];
		    *(dst_h1_mbcol + 4*2 + 1 ) = kAdjustedClip[((tmp2 + v_g1 + u_g1) / 64)];
		    *(dst_h1_mbcol + 4*2 + 0 ) = kAdjustedClip[((tmp2 + v_r1) / 64)];

		    int tmp3 = y3 * 74;
		    *(dst_h1_mbcol + 4*3 + 2 ) = kAdjustedClip[((tmp3 + u_b1) / 64)];
		    *(dst_h1_mbcol + 4*3 + 1 ) = kAdjustedClip[((tmp3 + v_g1 + u_g1) / 64)];
		    *(dst_h1_mbcol + 4*3 + 0 ) = kAdjustedClip[((tmp3 + v_r1) / 64)];
		
		    int tmp18 = y18 * 74;
		    *(dst_h2_mbcol + 4*2 + 2 ) = kAdjustedClip[((tmp18 + u_b1) / 64)];
		    *(dst_h2_mbcol + 4*2 + 1 ) = kAdjustedClip[((tmp18 + v_g1 + u_g1) / 64)];
		    *(dst_h2_mbcol + 4*2 + 0 ) = kAdjustedClip[((tmp18 + v_r1) / 64)];
		
		    int tmp19 = y19 * 74;
		    *(dst_h2_mbcol + 4*3 + 2 ) = kAdjustedClip[((tmp19 + u_b1) / 64)];
		    *(dst_h2_mbcol + 4*3 + 1 ) = kAdjustedClip[((tmp19 + v_g1 + u_g1) / 64)];
		    *(dst_h2_mbcol + 4*3 + 0 ) = kAdjustedClip[((tmp19 + v_r1) / 64)];
		
		    int y4  = (int)src_yhw[y_row + 4] - 16;
		    int y5  = (int)src_yhw[y_row + 5] - 16;
		    int y20 = (int)src_yhw[y_row + 20] - 16;
		    int y21 = (int)src_yhw[y_row + 21] - 16;

		    int u2 = (int)src_chw[c_row + 2] - 128;
		    int v2 = (int)src_chw[c_row + 10] - 128;

		    int u_b2 = u2 * 129;
		    int u_g2 = -u2 * 25;
		    int v_g2 = -v2 * 52;
		    int v_r2 = v2 * 102;

		    int tmp4 = y4 * 74;
		    *(dst_h1_mbcol + 4*4 + 2 ) = kAdjustedClip[((tmp4 + u_b2) / 64)];
		    *(dst_h1_mbcol + 4*4 + 1 ) = kAdjustedClip[((tmp4 + v_g2 + u_g2) / 64)];
		    *(dst_h1_mbcol + 4*4 + 0 ) = kAdjustedClip[((tmp4 + v_r2) / 64)];

		    int tmp5 = y5 * 74;
		    *(dst_h1_mbcol + 4*5 + 2 ) = kAdjustedClip[((tmp5 + u_b2) / 64)];
		    *(dst_h1_mbcol + 4*5 + 1 ) = kAdjustedClip[((tmp5 + v_g2 + u_g2) / 64)];
		    *(dst_h1_mbcol + 4*5 + 0 ) = kAdjustedClip[((tmp5 + v_r2) / 64)];
		
		    int tmp20 = y20 * 74;
		    *(dst_h2_mbcol + 4*4 + 2 ) = kAdjustedClip[((tmp20 + u_b2) / 64)];
		    *(dst_h2_mbcol + 4*4 + 1 ) = kAdjustedClip[((tmp20 + v_g2 + u_g2) / 64)];
		    *(dst_h2_mbcol + 4*4 + 0 ) = kAdjustedClip[((tmp20 + v_r2) / 64)];
		
		    int tmp21 = y21 * 74;
		    *(dst_h2_mbcol + 4*5 + 2 ) = kAdjustedClip[((tmp21 + u_b2) / 64)];
		    *(dst_h2_mbcol + 4*5 + 1 ) = kAdjustedClip[((tmp21 + v_g2 + u_g2) / 64)];
		    *(dst_h2_mbcol + 4*5 + 0 ) = kAdjustedClip[((tmp21 + v_r2) / 64)];


		    int y6  = (int)src_yhw[y_row + 6] - 16;
		    int y7  = (int)src_yhw[y_row + 7] - 16;
		    int y22 = (int)src_yhw[y_row + 22] - 16;
		    int y23 = (int)src_yhw[y_row + 23] - 16;

		    int u3 = (int)src_chw[c_row + 3] - 128;
		    int v3 = (int)src_chw[c_row + 11] - 128;

		    int u_b3 = u3 * 129;
		    int u_g3 = -u3 * 25;
		    int v_g3 = -v3 * 52;
		    int v_r3 = v3 * 102;

		    int tmp6 = y6 * 74;
		    *(dst_h1_mbcol + 4*6 + 2 ) = kAdjustedClip[((tmp6 + u_b3) / 64)];
		    *(dst_h1_mbcol + 4*6 + 1 ) = kAdjustedClip[((tmp6 + v_g3 + u_g3) / 64)];
		    *(dst_h1_mbcol + 4*6 + 0 ) = kAdjustedClip[((tmp6 + v_r3) / 64)];

		    int tmp7 = y7 * 74;
		    *(dst_h1_mbcol + 4*7 + 2 ) = kAdjustedClip[((tmp7 + u_b3) / 64)];
		    *(dst_h1_mbcol + 4*7 + 1 ) = kAdjustedClip[((tmp7 + v_g3 + u_g3) / 64)];
		    *(dst_h1_mbcol + 4*7 + 0 ) = kAdjustedClip[((tmp7 + v_r3) / 64)];
		
		    int tmp22 = y22 * 74;
		    *(dst_h2_mbcol + 4*6 + 2 ) = kAdjustedClip[((tmp22 + u_b3) / 64)];
		    *(dst_h2_mbcol + 4*6 + 1 ) = kAdjustedClip[((tmp22 + v_g3 + u_g3) / 64)];
		    *(dst_h2_mbcol + 4*6 + 0 ) = kAdjustedClip[((tmp22 + v_r3) / 64)];

		    int tmp23 = y23 * 74;
		    *(dst_h2_mbcol + 4*7 + 2 ) = kAdjustedClip[((tmp23 + u_b3) / 64)];
		    *(dst_h2_mbcol + 4*7 + 1 ) = kAdjustedClip[((tmp23 + v_g3 + u_g3) / 64)];
		    *(dst_h2_mbcol + 4*7 + 0 ) = kAdjustedClip[((tmp23 + v_r3) / 64)];

		    int y8  = (int)src_yhw[y_row + 8] - 16;
		    int y9  = (int)src_yhw[y_row + 9] - 16;
		    int y24 = (int)src_yhw[y_row + 24] - 16;
		    int y25 = (int)src_yhw[y_row + 25] - 16;

		    int u4 = (int)src_chw[c_row + 4] - 128;
		    int v4 = (int)src_chw[c_row + 12] - 128;

		    int u_b4 = u4 * 129;
		    int u_g4 = -u4 * 25;
		    int v_g4 = -v4 * 52;
		    int v_r4 = v4 * 102;

		    int tmp8 = y8 * 74;
		    *(dst_h1_mbcol + 4*8 + 2 ) = kAdjustedClip[((tmp8 + u_b4) / 64)];
		    *(dst_h1_mbcol + 4*8 + 1 ) = kAdjustedClip[((tmp8 + v_g4 + u_g4) / 64)];
		    *(dst_h1_mbcol + 4*8 + 0 ) = kAdjustedClip[((tmp8 + v_r4) / 64)];

		    int tmp9 = y9 * 74;
		    *(dst_h1_mbcol + 4*9 + 2 ) = kAdjustedClip[((tmp9 + u_b4) / 64)];
		    *(dst_h1_mbcol + 4*9 + 1 ) = kAdjustedClip[((tmp9 + v_g4 + u_g4) / 64)];
		    *(dst_h1_mbcol + 4*9 + 0 ) = kAdjustedClip[((tmp9 + v_r4) / 64)];

		    int tmp24 = y24 * 74;
		    *(dst_h2_mbcol + 4*8 + 2 ) = kAdjustedClip[((tmp24 + u_b4) / 64)];
		    *(dst_h2_mbcol + 4*8 + 1 ) = kAdjustedClip[((tmp24 + v_g4 + u_g4) / 64)];
		    *(dst_h2_mbcol + 4*8 + 0 ) = kAdjustedClip[((tmp24 + v_r4) / 64)];

		    int tmp25 = y25 * 74;
		    *(dst_h2_mbcol + 4*9 + 2 ) = kAdjustedClip[((tmp25 + u_b4) / 64)];
		    *(dst_h2_mbcol + 4*9 + 1 ) = kAdjustedClip[((tmp25 + v_g4 + u_g4) / 64)];
		    *(dst_h2_mbcol + 4*9 + 0 ) = kAdjustedClip[((tmp25 + v_r4) / 64)];


		    int y10 = (int)src_yhw[y_row + 10] - 16;
		    int y11 = (int)src_yhw[y_row + 11] - 16;
		    int y26 = (int)src_yhw[y_row + 26] - 16;
		    int y27 = (int)src_yhw[y_row + 27] - 16;

		    int u5 = (int)src_chw[c_row + 5] - 128;
		    int v5 = (int)src_chw[c_row + 13] - 128;

		    int u_b5 = u5 * 129;
		    int u_g5 = -u5 * 25;
		    int v_g5 = -v5 * 52;
		    int v_r5 = v5 * 102;

		    int tmp10 = y10 * 74;
		    *(dst_h1_mbcol + 4*10 + 2 ) = kAdjustedClip[((tmp10 + u_b5) / 64)];
		    *(dst_h1_mbcol + 4*10 + 1 ) = kAdjustedClip[((tmp10 + v_g5 + u_g5) / 64)];
		    *(dst_h1_mbcol + 4*10 + 0 ) = kAdjustedClip[((tmp10 + v_r5) / 64)];

		    int tmp11= y11* 74;
		    *(dst_h1_mbcol + 4*11 + 2 ) = kAdjustedClip[((tmp11 + u_b5) / 64)];
		    *(dst_h1_mbcol + 4*11 + 1 ) = kAdjustedClip[((tmp11 + v_g5 + u_g5) / 64)];
		    *(dst_h1_mbcol + 4*11 + 0 ) = kAdjustedClip[((tmp11 + v_r5) / 64)];

		    int tmp26 = y26 * 74;
		    *(dst_h2_mbcol + 4*10 + 2 ) = kAdjustedClip[((tmp26 + u_b5) / 64)];
		    *(dst_h2_mbcol + 4*10 + 1 ) = kAdjustedClip[((tmp26 + v_g5 + u_g5) / 64)];
		    *(dst_h2_mbcol + 4*10 + 0 ) = kAdjustedClip[((tmp26 + v_r5) / 64)];

		    int tmp27 = y27 * 74;
		    *(dst_h2_mbcol + 4*11 + 2 ) = kAdjustedClip[((tmp27 + u_b5) / 64)];
		    *(dst_h2_mbcol + 4*11 + 1 ) = kAdjustedClip[((tmp27 + v_g5 + u_g5) / 64)];
		    *(dst_h2_mbcol + 4*11 + 0 ) = kAdjustedClip[((tmp27 + v_r5) / 64)];

		    int y12 = (int)src_yhw[y_row + 12] - 16;
		    int y13 = (int)src_yhw[y_row + 13] - 16;
		    int y28 = (int)src_yhw[y_row + 28] - 16;
		    int y29 = (int)src_yhw[y_row + 29] - 16;

		    int u6 = (int)src_chw[c_row + 6] - 128;
		    int v6 = (int)src_chw[c_row + 14] - 128;

		    int u_b6 = u6 * 129;
		    int u_g6 = -u6 * 25;
		    int v_g6 = -v6 * 52;
		    int v_r6 = v6 * 102;

		    int tmp12 = y12 * 74;
		    *(dst_h1_mbcol + 4*12 + 2 ) = kAdjustedClip[((tmp12 + u_b6) / 64)];
		    *(dst_h1_mbcol + 4*12 + 1 ) = kAdjustedClip[((tmp12 + v_g6 + u_g6) / 64)];
		    *(dst_h1_mbcol + 4*12 + 0 ) = kAdjustedClip[((tmp12 + v_r6) / 64)];

		    int tmp13 = y13* 74;
		    *(dst_h1_mbcol + 4*13 + 2 ) = kAdjustedClip[((tmp13 + u_b6) / 64)];
		    *(dst_h1_mbcol + 4*13 + 1 ) = kAdjustedClip[((tmp13 + v_g6 + u_g6) / 64)];
		    *(dst_h1_mbcol + 4*13 + 0 ) = kAdjustedClip[((tmp13 + v_r6) / 64)];

		    int tmp28 = y28 * 74;
		    *(dst_h2_mbcol + 4*12 + 2 ) = kAdjustedClip[((tmp28 + u_b6) / 64)];
		    *(dst_h2_mbcol + 4*12 + 1 ) = kAdjustedClip[((tmp28 + v_g6 + u_g6) / 64)];
		    *(dst_h2_mbcol + 4*12 + 0 ) = kAdjustedClip[((tmp28 + v_r6) / 64)];

		    int tmp29 = y29 * 74;
		    *(dst_h2_mbcol + 4*13 + 2 ) = kAdjustedClip[((tmp29 + u_b6) / 64)];
		    *(dst_h2_mbcol + 4*13 + 1 ) = kAdjustedClip[((tmp29 + v_g6 + u_g6) / 64)];
		    *(dst_h2_mbcol + 4*13 + 0 ) = kAdjustedClip[((tmp29 + v_r6) / 64)];
		
					
		    int y14 = (int)src_yhw[y_row + 14] - 16;
		    int y15 = (int)src_yhw[y_row + 15] - 16;
		    int y30 = (int)src_yhw[y_row + 30] - 16;
		    int y31 = (int)src_yhw[y_row + 31] - 16;

		    int u7 = (int)src_chw[c_row + 7] - 128;
		    int v7 = (int)src_chw[c_row + 15] - 128;

		    int u_b7 = u7 * 129;
		    int u_g7 = -u7 * 25;
		    int v_g7 = -v7 * 52;
		    int v_r7 = v7 * 102;

		    int tmp14 = y14 * 74;
		    *(dst_h1_mbcol + 4*14 + 2 ) = kAdjustedClip[((tmp14 + u_b7) / 64)];
		    *(dst_h1_mbcol + 4*14 + 1 ) = kAdjustedClip[((tmp14 + v_g7 + u_g7) / 64)];
		    *(dst_h1_mbcol + 4*14 + 0 ) = kAdjustedClip[((tmp14 + v_r7) / 64)];

		    int tmp15 = y15 * 74;
		    *(dst_h1_mbcol + 4*15 + 2 ) = kAdjustedClip[((tmp15 + u_b7) / 64)];
		    *(dst_h1_mbcol + 4*15 + 1 ) = kAdjustedClip[((tmp15 + v_g7 + u_g7) / 64)];
		    *(dst_h1_mbcol + 4*15 + 0 ) = kAdjustedClip[((tmp15 + v_r7) / 64)];

		    int tmp30 = y30 * 74;
		    *(dst_h2_mbcol + 4*14 + 2 ) = kAdjustedClip[((tmp30 + u_b7) / 64)];
		    *(dst_h2_mbcol + 4*14 + 1 ) = kAdjustedClip[((tmp30 + v_g7 + u_g7) / 64)];
		    *(dst_h2_mbcol + 4*14 + 0 ) = kAdjustedClip[((tmp30 + v_r7) / 64)];

		    int tmp31 = y31 * 74;
		    *(dst_h2_mbcol + 4*15 + 2 ) = kAdjustedClip[((tmp31 + u_b7) / 64)];
		    *(dst_h2_mbcol + 4*15 + 1 ) = kAdjustedClip[((tmp31 + v_g7 + u_g7) / 64)];
		    *(dst_h2_mbcol + 4*15 + 0 ) = kAdjustedClip[((tmp31 + v_r7) / 64)];

		    y_row += 32;
		    c_row += 16;

		    dst_h1_mbcol+=dst_width_2;
		    dst_h2_mbcol+=dst_width_2;

		}
		src_yhw += 256;
		src_chw += 128;
		dst_hw+=64;
	    }
	    src_yh += y_stride;
	    src_ch += c_stride;
	    dst_h  += dst_stride;
	}
	return 0;
    }

    int tile2RGB_half(void *data, void *data1, size_t src_width, size_t src_height, size_t dstStride)
    {
	INGENIC_OMX_YUV_FORMAT* pimg = (INGENIC_OMX_YUV_FORMAT*)data;
	unsigned char *src_y = reinterpret_cast<unsigned char*>(pimg->nPlanar[0]);
	unsigned char *src_c = reinterpret_cast<unsigned char*>(pimg->nPlanar[1]);
	unsigned int y_stride = pimg->nStride[0];
	unsigned int c_stride = pimg->nStride[1];
	unsigned char *dst = (unsigned char *)data1;
    
	size_t n_width = src_width / 16;
	size_t n_height = src_height / 16;

	size_t width = dstStride * 2;
    
	uint8_t *kAdjustedClip = initClip();
    
	unsigned char *src_yh = src_y;
	unsigned char *src_ch = src_c;
	unsigned char *dst_h = dst;
	unsigned int dst_stride = (width/2) * 16/2 * 4;
	size_t dst_width_1 = width/2 * 4;
    
	for (size_t mbrow = 0; mbrow < n_height; ++mbrow ) {
	    unsigned char *src_yhw = src_yh;
	    unsigned char *src_chw = src_ch;
	    unsigned char *dst_hw = dst_h;

	    for (size_t mbcol = 0; mbcol < n_width ; ++mbcol) {
		unsigned char *dst_h_mbcol = dst_hw;

		size_t y_row = 0;
		size_t c_row = 0;

		for (size_t i = 0; (i < 8); ++i ){          //row
		    int y0  = (int)src_yhw[y_row] - 16;
		
		    int u0 = (int)src_chw[c_row] - 128;
		    int v0 = (int)src_chw[c_row + 8] - 128;

		    int u_b0 = u0 * 129;
		    int u_g0 = -u0 * 25;
		    int v_g0 = -v0 * 52;
		    int v_r0 = v0 * 102;

		    int tmp0 = y0 * 74;
		    *(dst_h_mbcol + 4*0 + 2 ) = kAdjustedClip[((tmp0 + u_b0) / 64)];
		    *(dst_h_mbcol + 4*0 + 1 ) = kAdjustedClip[((tmp0 + v_g0 + u_g0) / 64)];
		    *(dst_h_mbcol + 4*0 + 0 ) = kAdjustedClip[((tmp0 + v_r0) / 64)];
		
		    int y2  = (int)src_yhw[y_row + 2] - 16;
		
		    int u1 = (int)src_chw[c_row + 1] - 128;
		    int v1 = (int)src_chw[c_row + 9] - 128;

		    int u_b1 = u1 * 129;
		    int u_g1 = -u1 * 25;
		    int v_g1 = -v1 * 52;
		    int v_r1 = v1 * 102;

		    int tmp2 = y2 * 74;
		    *(dst_h_mbcol + 4*1 + 2 ) = kAdjustedClip[((tmp2 + u_b1) / 64)];
		    *(dst_h_mbcol + 4*1 + 1 ) = kAdjustedClip[((tmp2 + v_g1 + u_g1) / 64)];
		    *(dst_h_mbcol + 4*1 + 0 ) = kAdjustedClip[((tmp2 + v_r1) / 64)];
		
		    int y4  = (int)src_yhw[y_row + 4] - 16;

		    int u2 = (int)src_chw[c_row + 2] - 128;
		    int v2 = (int)src_chw[c_row + 10] - 128;

		    int u_b2 = u2 * 129;
		    int u_g2 = -u2 * 25;
		    int v_g2 = -v2 * 52;
		    int v_r2 = v2 * 102;

		    int tmp4 = y4 * 74;
		    *(dst_h_mbcol + 4*2 + 2 ) = kAdjustedClip[((tmp4 + u_b2) / 64)];
		    *(dst_h_mbcol + 4*2 + 1 ) = kAdjustedClip[((tmp4 + v_g2 + u_g2) / 64)];
		    *(dst_h_mbcol + 4*2 + 0 ) = kAdjustedClip[((tmp4 + v_r2) / 64)];
	
		    int y6  = (int)src_yhw[y_row + 6] - 16;

		    int u3 = (int)src_chw[c_row + 3] - 128;
		    int v3 = (int)src_chw[c_row + 11] - 128;

		    int u_b3 = u3 * 129;
		    int u_g3 = -u3 * 25;
		    int v_g3 = -v3 * 52;
		    int v_r3 = v3 * 102;

		    int tmp6 = y6 * 74;
		    *(dst_h_mbcol + 4*3 + 2 ) = kAdjustedClip[((tmp6 + u_b3) / 64)];
		    *(dst_h_mbcol + 4*3 + 1 ) = kAdjustedClip[((tmp6 + v_g3 + u_g3) / 64)];
		    *(dst_h_mbcol + 4*3 + 0 ) = kAdjustedClip[((tmp6 + v_r3) / 64)];

		    int y8  = (int)src_yhw[y_row + 8] - 16;

		    int u4 = (int)src_chw[c_row + 4] - 128;
		    int v4 = (int)src_chw[c_row + 12] - 128;

		    int u_b4 = u4 * 129;
		    int u_g4 = -u4 * 25;
		    int v_g4 = -v4 * 52;
		    int v_r4 = v4 * 102;

		    int tmp8 = y8 * 74;
		    *(dst_h_mbcol + 4*4 + 2 ) = kAdjustedClip[((tmp8 + u_b4) / 64)];
		    *(dst_h_mbcol + 4*4 + 1 ) = kAdjustedClip[((tmp8 + v_g4 + u_g4) / 64)];
		    *(dst_h_mbcol + 4*4 + 0 ) = kAdjustedClip[((tmp8 + v_r4) / 64)];

		    int y10 = (int)src_yhw[y_row + 10] - 16;
		
		    int u5 = (int)src_chw[c_row + 5] - 128;
		    int v5 = (int)src_chw[c_row + 13] - 128;

		    int u_b5 = u5 * 129;
		    int u_g5 = -u5 * 25;
		    int v_g5 = -v5 * 52;
		    int v_r5 = v5 * 102;

		    int tmp10 = y10 * 74;
		    *(dst_h_mbcol + 4*5 + 2 ) = kAdjustedClip[((tmp10 + u_b5) / 64)];
		    *(dst_h_mbcol + 4*5 + 1 ) = kAdjustedClip[((tmp10 + v_g5 + u_g5) / 64)];
		    *(dst_h_mbcol + 4*5 + 0 ) = kAdjustedClip[((tmp10 + v_r5) / 64)];
		
		    int y12 = (int)src_yhw[y_row + 12] - 16;
		
		    int u6 = (int)src_chw[c_row + 6] - 128;
		    int v6 = (int)src_chw[c_row + 14] - 128;

		    int u_b6 = u6 * 129;
		    int u_g6 = -u6 * 25;
		    int v_g6 = -v6 * 52;
		    int v_r6 = v6 * 102;

		    int tmp12 = y12 * 74;
		    *(dst_h_mbcol + 4*6 + 2 ) = kAdjustedClip[((tmp12 + u_b6) / 64)];
		    *(dst_h_mbcol + 4*6 + 1 ) = kAdjustedClip[((tmp12 + v_g6 + u_g6) / 64)];
		    *(dst_h_mbcol + 4*6 + 0 ) = kAdjustedClip[((tmp12 + v_r6) / 64)];
					
		    int y14 = (int)src_yhw[y_row + 14] - 16;

		    int u7 = (int)src_chw[c_row + 7] - 128;
		    int v7 = (int)src_chw[c_row + 15] - 128;

		    int u_b7 = u7 * 129;
		    int u_g7 = -u7 * 25;
		    int v_g7 = -v7 * 52;
		    int v_r7 = v7 * 102;

		    int tmp14 = y14 * 74;
		    *(dst_h_mbcol + 4*7 + 2 ) = kAdjustedClip[((tmp14 + u_b7) / 64)];
		    *(dst_h_mbcol + 4*7 + 1 ) = kAdjustedClip[((tmp14 + v_g7 + u_g7) / 64)];
		    *(dst_h_mbcol + 4*7 + 0 ) = kAdjustedClip[((tmp14 + v_r7) / 64)];
		
		    y_row += 32;
		    c_row += 16;

		    dst_h_mbcol+=dst_width_1;
		}
		src_yhw += 256;
		src_chw += 128;
		dst_hw+=32;
	    }
	    src_yh += y_stride;
	    src_ch += c_stride;
	    dst_h  += dst_stride;   
	}
	return 0;
    }

    int tile2RGB_quarter(void *data, void *data1, size_t src_width, size_t src_height, size_t dstStride)
    {
	INGENIC_OMX_YUV_FORMAT* pimg = (INGENIC_OMX_YUV_FORMAT*)data;
	unsigned char *src_y = reinterpret_cast<unsigned char*>(pimg->nPlanar[0]);
	unsigned char *src_c = reinterpret_cast<unsigned char*>(pimg->nPlanar[1]);
	unsigned int y_stride = pimg->nStride[0];
	unsigned int c_stride = pimg->nStride[1];
	unsigned char *dst = (unsigned char *)data1;
    
	uint8_t *kAdjustedClip = initClip();

	size_t n_width = src_width / 16;
	size_t n_height = src_height / 16;

	size_t width = dstStride * 4;

	unsigned char *src_yh = src_y;
	unsigned char *src_ch = src_c;
	unsigned char *dst_h = dst;
	unsigned int dst_stride = (width/4) * (16/4) * 4;
	size_t dst_width_1 = (width/4) * 4;
    
	for (size_t mbrow = 0; mbrow < n_height; ++mbrow ) {
	    unsigned char *src_yhw = src_yh;
	    unsigned char *src_chw = src_ch;
	    unsigned char *dst_hw = dst_h;

	    for (size_t mbcol = 0; mbcol < n_width ; ++mbcol) {
		unsigned char *dst_h_mbcol = dst_hw;
		size_t y_row = 0;
		size_t c_row = 0;

		for (size_t i = 0; i < 4; ++i ){          //row
		    int y0  = (int)src_yhw[y_row] - 16;
		
		    int u0 = (int)src_chw[c_row] - 128;
		    int v0 = (int)src_chw[c_row + 8] - 128;

		    int u_b0 = u0 * 129;
		    int u_g0 = -u0 * 25;
		    int v_g0 = -v0 * 52;
		    int v_r0 = v0 * 102;

		    int tmp0 = y0 * 74;
		    *(dst_h_mbcol + 4*0 + 2 ) = kAdjustedClip[((tmp0 + u_b0) / 64)];
		    *(dst_h_mbcol + 4*0 + 1 ) = kAdjustedClip[((tmp0 + v_g0 + u_g0) / 64)];
		    *(dst_h_mbcol + 4*0 + 0 ) = kAdjustedClip[((tmp0 + v_r0) / 64)];
		
		    int y4  = (int)src_yhw[y_row + 4] - 16;

		    int u2 = (int)src_chw[c_row + 2] - 128;
		    int v2 = (int)src_chw[c_row + 10] - 128;

		    int u_b2 = u2 * 129;
		    int u_g2 = -u2 * 25;
		    int v_g2 = -v2 * 52;
		    int v_r2 = v2 * 102;

		    int tmp4 = y4 * 74;
		    *(dst_h_mbcol + 4*1 + 2 ) = kAdjustedClip[((tmp4 + u_b2) / 64)];
		    *(dst_h_mbcol + 4*1 + 1 ) = kAdjustedClip[((tmp4 + v_g2 + u_g2) / 64)];
		    *(dst_h_mbcol + 4*1 + 0 ) = kAdjustedClip[((tmp4 + v_r2) / 64)];

		    int y8  = (int)src_yhw[y_row + 8] - 16;

		    int u4 = (int)src_chw[c_row + 4] - 128;
		    int v4 = (int)src_chw[c_row + 12] - 128;

		    int u_b4 = u4 * 129;
		    int u_g4 = -u4 * 25;
		    int v_g4 = -v4 * 52;
		    int v_r4 = v4 * 102;

		    int tmp8 = y8 * 74;
		    *(dst_h_mbcol + 4*2 + 2 ) = kAdjustedClip[((tmp8 + u_b4) / 64)];
		    *(dst_h_mbcol + 4*2 + 1 ) = kAdjustedClip[((tmp8 + v_g4 + u_g4) / 64)];
		    *(dst_h_mbcol + 4*2 + 0 ) = kAdjustedClip[((tmp8 + v_r4) / 64)];

		    int y12 = (int)src_yhw[y_row + 12] - 16;
		
		    int u6 = (int)src_chw[c_row + 6] - 128;
		    int v6 = (int)src_chw[c_row + 14] - 128;

		    int u_b6 = u6 * 129;
		    int u_g6 = -u6 * 25;
		    int v_g6 = -v6 * 52;
		    int v_r6 = v6 * 102;
		
		    int tmp12 = y12 * 74;
		    *(dst_h_mbcol + 4*3 + 2 ) = kAdjustedClip[((tmp12 + u_b6) / 64)];
		    *(dst_h_mbcol + 4*3 + 1 ) = kAdjustedClip[((tmp12 + v_g6 + u_g6) / 64)];
		    *(dst_h_mbcol + 4*3 + 0 ) = kAdjustedClip[((tmp12 + v_r6) / 64)];
					
		    y_row += 64;
		    c_row += 32;

		    dst_h_mbcol+=dst_width_1;
		}
		src_yhw += 256;
		src_chw += 128;
		dst_hw+=16;
	    }
	    src_yh += y_stride;
	    src_ch += c_stride;
	    dst_h  += dst_stride;   
	}
	return 0;
    }
#endif
    
    bool MXUConverter::convert(const BitmapParams& src, const BitmapParams& dst, size_t dstStride){

	int w = (src.mWidth / 16) * 16;
	int h = (src.mHeight / 16) * 16;

	int scale = 1;
	float scaleW = (float) src.mWidth / dst.mWidth;
	if (scaleW > 1 && scaleW < 3)
	    scale = 2;
	else if (scaleW >= 3)
	    scale = 4;

	if ((dst.mWidth != w / scale) || (dst.mHeight != h / scale))
	    scale = -1;

	if( scale == 1 ) {
	    tile2RGB(const_cast<void*>(src.mBits), dst.mBits, src.mWidth, src.mHeight, dstStride);
	    return true;
	} else if ( scale == 2 ){
	    tile2RGB_half(const_cast<void*>(src.mBits), dst.mBits, src.mWidth, src.mHeight, dstStride);
	    return true;
	} else if ( scale == 4 ){
	    tile2RGB_quarter(const_cast<void*>(src.mBits), dst.mBits, src.mWidth, src.mHeight, dstStride);
	    return true;
	} else {
	    LOGE("[ ColorConverter ] Invalid scale value : %d\n", scale);
	    return true;
	}    
    }
}  // namespace android
