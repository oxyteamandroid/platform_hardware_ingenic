/*
 * Copyright (C) 2009 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef BITMAP_PARAMS_H
#define BITMAP_PARAMS_H


namespace android {

struct BitmapParams {
    BitmapParams(void *bits, size_t stride,
		 size_t width, size_t height,
		 size_t cropLeft, size_t cropTop,
		 size_t cropRight, size_t cropBottom)
        :mBits(bits),
	 mStride(stride),
	 mWidth(width),
	 mHeight(height),
	 mCropLeft(cropLeft),
	 mCropTop(cropTop),
	 mCropRight(cropRight),
	 mCropBottom(cropBottom) { }
    
    size_t cropWidth() const { return mCropRight - mCropLeft + 1; }
    size_t cropHeight() const { return mCropBottom - mCropTop + 1; }
    
    void *mBits;
    size_t mStride;
    size_t mWidth, mHeight;
    size_t mCropLeft, mCropTop, mCropRight, mCropBottom;
};

}//namespace

#endif
