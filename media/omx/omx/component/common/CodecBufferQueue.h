/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CODEC_BUFFER_QUEUE_H
#define CODEC_BUFFER_QUEUE_H

#include <utils/List.h>
#include <utils/threads.h>
#include <OMX_Component.h>
#include "SimpleHardOMXComponent.h"


namespace android {

class CodecBufferQueue;

class CodecBuffer {
 public:
    uint64_t mPts;
    OMX_U32 mFlags;
    uint32_t mWidth;
    uint32_t mHeight;
    
    enum DATATYPE {
	UNKNOWN_MODE,
	PLANAR_IMAGE_MODE,
	OMX_BUFFERHEAD_MODE,
	HARD_OMX_BUFFERINFO_MODE,
    } mDataType;
    
    void* mData;//could be type of PlanarImage* | OMX_BUFFERHEADERTYPE* | SimpleHardOMXComponent::BufferInfo*, which depends on mMode.
    
 public:
    CodecBuffer(CodecBufferQueue*);
    ~CodecBuffer();
    
    void clear();
    bool allocateData();
    bool deallocateData();

 private:
    CodecBufferQueue* mOwner;

    bool mIsDataSelfAllocated;

    CodecBuffer(const CodecBuffer&);
    CodecBuffer& operator=(const CodecBuffer&);
};

class CodecBufferQueue {
 public:
    CodecBufferQueue(int capacity);
    ~CodecBufferQueue();

    bool acquireEnqableBuffer(CodecBuffer**);
    bool enq(CodecBuffer*);
    bool acquireDeqableBuffer(CodecBuffer**);
    bool deq(CodecBuffer*);

    void stop();
    void clear();

    bool empty();
    int size();
    
    void pauseAcquire();
    void resumeAcquire();

 private:
    friend class CodecBuffer;

    int mCapacity;
    List<CodecBuffer*> mOwnerList;
    List<CodecBuffer*> mBusyList;
    List<CodecBuffer*> mFreeList;

    Mutex mLock;
    Condition mCondition;
    bool mDone;
    bool mAcquireInPausing;
    
    bool foundInOwner(const CodecBuffer*);
    bool foundInBusy(const CodecBuffer*);
    bool foundInFree(const CodecBuffer*);
    bool foundInList(const CodecBuffer*, List<CodecBuffer*>&);
    
    CodecBufferQueue(const CodecBufferQueue&);
    CodecBufferQueue& operator=(const CodecBufferQueue&);
};

}//namespace

#endif
