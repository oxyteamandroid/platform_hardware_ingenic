# ifeq (true,false)
LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

LOCAL_SRC_FILES:= \
        HardOMXComponent.cpp \
	SimpleHardOMXComponent.cpp \
	colorconvert/IPUConverter.cpp \
	colorconvert/MXUConverter.cpp \
	colorconvert/HWColorConverter.cpp \
	CodecBufferQueue.cpp \

LOCAL_C_INCLUDES += \
	$(TOP)/frameworks/native/include/media/hardware \
        $(TOP)/frameworks/native/include/media/openmax \
	$(TOP)/hardware/ingenic/display/ipu/ipu \
	$(TOP)/hardware/ingenic/media/omx/omx/component/dec/lume/libjzcommon \
	$(TOP)/hardware/ingenic/display/hwcomposer/hwcomposer-gc1000 \
        $(TOP)/hardware/ingenic/media/omx/omx/core


LOCAL_SHARED_LIBRARIES :=               \
        libbinder                       \
        libmedia                        \
        libutils                        \
        libui                           \
        libcutils                       \
        libstagefright_foundation       \
        libdl				\
	libjzipu 			

LOCAL_MODULE:= libOMX_Basecomponent

include $(BUILD_STATIC_LIBRARY)

# endif
