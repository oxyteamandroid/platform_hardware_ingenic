/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "CodecBufferQueue.h"
#include <stdlib.h>
#include "Ingenic_OMX_Def.h"


namespace android {

CodecBuffer::CodecBuffer(CodecBufferQueue* owner)
    :mPts(0),
     mFlags(0),
     mWidth(0),
     mHeight(0),
     mDataType(UNKNOWN_MODE),
     mData(NULL),
     mOwner(owner),
     mIsDataSelfAllocated(false){
}

void CodecBuffer::clear() {
    mPts = 0;
    mFlags = 0;
    mWidth = mHeight = 0;

    if(!mIsDataSelfAllocated){
	mDataType = UNKNOWN_MODE;
	mData = NULL;
    }
}

bool CodecBuffer::allocateData() {
    if(mIsDataSelfAllocated)
	return true;

    int size = 0;

    //only support for planarimage for now.
    switch(mDataType){
    case PLANAR_IMAGE_MODE:
	size = sizeof(INGENIC_OMX_YUV_FORMAT);
	break;
    default:
	break;
    }

    if(size != 0){
	mData = malloc(size);
	mIsDataSelfAllocated = true;
	return true;
    }

    return false;
}

bool CodecBuffer::deallocateData() {
    if(mIsDataSelfAllocated){
	if(mData != NULL){
	    free(mData);
	    mData = NULL;
	}

	mIsDataSelfAllocated = false;
    }

    return true;
}

CodecBuffer::~CodecBuffer() {
    if(mIsDataSelfAllocated && mData != NULL){
	free(mData);
	mData = NULL;	
    }
}

CodecBufferQueue::CodecBufferQueue(int capacity)
    :mCapacity(capacity),
     mDone(false),
     mAcquireInPausing(false){
    for(int i = 0; i < mCapacity; ++i){
	CodecBuffer* buf = new CodecBuffer(this);
	mOwnerList.push_back(buf);
	mFreeList.push_back(buf);
    }
}

CodecBufferQueue::~CodecBufferQueue() {
    stop();

    for(List<CodecBuffer*>::iterator it = mOwnerList.begin(); it != mOwnerList.end(); ++it){
	CodecBuffer* buf = *it;
	delete buf;
    }
    mOwnerList.clear();
}

bool CodecBufferQueue::acquireEnqableBuffer(CodecBuffer** buf) {
    Mutex::Autolock ao(mLock);

    while(!mDone &&
	  !mAcquireInPausing &&
	  mFreeList.empty()){
	mCondition.waitRelative(mLock, 10000000);//10ms
    }

    if(mDone)
	return false;

    if(mAcquireInPausing)
        return false;

    *buf = *mFreeList.begin();

    return true;
}

bool CodecBufferQueue::enq(CodecBuffer* buf) {
    Mutex::Autolock ao(mLock);
    
    if(!foundInOwner(buf))
	return false;

    if(foundInBusy(buf))
	return false;
    
    if(buf != *mFreeList.begin())
	return false;

    mFreeList.erase(mFreeList.begin());

    mBusyList.push_back(buf);

    mCondition.signal();

    return true;
}

bool CodecBufferQueue::acquireDeqableBuffer(CodecBuffer** buf) {
    Mutex::Autolock ao(mLock);
    
    while(!mDone &&
	  !mAcquireInPausing &&
	  mBusyList.empty()){
	mCondition.waitRelative(mLock, 10000000);//10ms
    }
    
    if(mDone)
	return false;

    if(mAcquireInPausing)
        return false;
    
    *buf = *mBusyList.begin();

    return true;    
}

bool CodecBufferQueue::deq(CodecBuffer* buf) {
    Mutex::Autolock ao(mLock);

    if(!foundInOwner(buf))
	return false;
    
    if(foundInFree(buf))
	return false;

    if(buf != *mBusyList.begin())//can recover the pre foundInBusy().
	return false;
    
    mBusyList.erase(mBusyList.begin());

    mFreeList.push_back(buf);

    mCondition.signal();

    return true;
}

void CodecBufferQueue::stop() {
    Mutex::Autolock ao(mLock);
       
    mDone = true;
    mCondition.signal();
}

void CodecBufferQueue::clear() {
    Mutex::Autolock ao(mLock);

    List<CodecBuffer*>::iterator it = mBusyList.begin();
    while(it != mBusyList.end()){
	CodecBuffer* buf = *it;
	mFreeList.push_back(buf);
	it = mBusyList.erase(it);
    }
}

bool CodecBufferQueue::empty() {
    Mutex::Autolock ao(mLock);

    return mBusyList.empty();
}

int CodecBufferQueue::size() {
    Mutex::Autolock ao(mLock);

    return mBusyList.size();
}

void CodecBufferQueue::pauseAcquire() {
    Mutex::Autolock ao(mLock);
       
    mAcquireInPausing = true;
    mCondition.signal();
}

void CodecBufferQueue::resumeAcquire() {
    Mutex::Autolock ao(mLock);
    
    mAcquireInPausing = false;
    mCondition.signal();    
}

bool CodecBufferQueue::foundInOwner(const CodecBuffer* buf) {
    return foundInList(buf, mOwnerList);
}

bool CodecBufferQueue::foundInBusy(const CodecBuffer* buf) {
    return foundInList(buf, mBusyList);
}

bool CodecBufferQueue::foundInFree(const CodecBuffer* buf) {
    return foundInList(buf, mFreeList);
}

bool CodecBufferQueue::foundInList(const CodecBuffer* buf, List<CodecBuffer*>& list) {
    bool found = false;
    for(List<CodecBuffer*>::iterator it = list.begin(); it != list.end(); ++it){
	CodecBuffer* dst = *it;
	if(dst == buf){
	    found = true;
	    break;
	}
    }

    return found;    
}

}//namespace
