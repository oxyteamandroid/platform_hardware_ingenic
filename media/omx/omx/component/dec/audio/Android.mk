LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

LOCAL_ARM_MODE := arm

LOCAL_SRC_FILES := \
	HWAudioDec.cpp

LOCAL_SHARED_LIBRARIES := \
	libstagefright \
	libstagefright_omx \
	libstagefright_foundation \
	libcutils	\
	libutils \
	libbinder	\
	libdl \
	libui

LOCAL_STATIC_LIBRARIES := libstagefright_alume_codec

LOCAL_STATIC_LIBRARIES += \
	libstagefright_mad \
	libstagefright_faad2 \
	libFraunhoferAAC \
	libstagefright_alumedecoder \
	libstagefright_ffmpcommon \
	libstagefright_ffavutil  \
	libstagefright_ffavcore  \
	libOMX_Basecomponent \
	libstagefright_mphwapp\


LOCAL_MODULE := libstagefright_hard_alume

LOCAL_C_INCLUDES := \
	$(LOCAL_PATH)/./lume_audio/include \
	frameworks/av/media/libstagefright/include \
	frameworks/native/include/utils \
	frameworks/native/include/media/openmax \
	frameworks/native/include/media/hardware   \
	external/aac/libAACdec/include \
	external/aac/libPCMutils/include \
	external/aac/libFDK/include \
	external/aac/libMpegTPDec/include \
	external/aac/libSBRdec/include \
	external/aac/libSYS/include \
	hardware/ingenic/media/demux/demux/lume/stream \
	hardware/ingenic/media/demux/demux/lume/libmpdemux \
	hardware/ingenic/media/omx/omx/component/common \
	hardware/ingenic/media/omx/omx/component/dec/lume/libaf \
        hardware/ingenic/media/omx/omx/component/dec/lume/libavutil \
        hardware/ingenic/media/omx/omx/component/dec/lume/libavcodec \
        hardware/ingenic/media/omx/omx/component/dec/lume/libmpcodecs \
        hardware/ingenic/media/omx/omx/component/dec/lume/libjzcommon \
        hardware/ingenic/media/omx/omx/component/dec/lume/ \
        hardware/ingenic/media/demux/demux/include

LOCAL_MODULE_TAGS := optional
include $(BUILD_SHARED_LIBRARY)
include $(call all-makefiles-under,$(LOCAL_PATH))
