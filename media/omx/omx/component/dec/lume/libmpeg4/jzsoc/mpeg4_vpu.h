#ifndef __MPEG4_VPU_H__
#define __MPEG4_VPU_H__
#define write_vpu_reg(off, value) (*((volatile unsigned int *)(off)) = (value))
#define read_vpu_reg(off, ofst)   (*((volatile unsigned int *)(off + ofst)))

#define REG_SCH_GLBC         0x00000
#define REG_SCH_TLBA         0x00030
#define SCH_GLBC_TLBE        (0x1<<30)
#define SCH_GLBC_TLBINV      (0x1<<29)

# define SCH_TLBE_JPGC       (0x1<<26)
# define SCH_TLBE_DBLK       (0x1<<25)
# define SCH_TLBE_SDE        (0x1<<24)
# define SCH_TLBE_EFE        (0x1<<23)
# define SCH_TLBE_VDMA       (0x1<<22)
# define SCH_TLBE_MCE        (0x1<<21)

#define SCH_INTE_ACFGERR     (0x1<<20)
#define SCH_INTE_TLBERR      (0x1<<18)
#define SCH_INTE_BSERR       (0x1<<17)
#define SCH_INTE_ENDF        (0x1<<16)
#define SCH_GLBC_HIAXI       (0x1<<9)

# define REG_SCH_TLBC        0x00050
# define SCH_TLBC_VPN        (0xFFFFF000)
# define SCH_TLBC_RIDX(idx)  (((idx) & 0xFF)<<4)
# define SCH_TLBC_INVLD      (0x1<<1)
# define SCH_TLBC_RETRY      (0x1<<0)

# define REG_SCH_TLBV        0x00054
# define SCH_TLBV_CNM(cnm)   (((cnm) & 0xFFF)<<16)
# define SCH_TLBV_GCN(gcn)   (((gcn) & 0xFFF)<<0)
#endif
