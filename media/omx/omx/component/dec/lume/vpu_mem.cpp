#include <utils/Log.h>
#include <utils/threads.h>
#include "vpu_mem.h"

namespace android {

	Mutex DmmuLock;

	extern "C" {

		unsigned int get_phy_addr (unsigned int vaddr){
			ALOGE("FIXME : get_phy_addr");
			*(volatile int *)0x80000001 = 0;
			return 0;
		}

		void *jz4740_alloc_frame (int *VpuMem_ptr, int align, int size){
			VpuMem * vmem=(VpuMem*)VpuMem_ptr;
			int vaddr=(int)(vmem->vpu_mem_alloc(align+size));
			vaddr=(vaddr+align-1)&(~(align-1));
			return (void*)vaddr;
		}

		void jz4740_free_frame(int *VpuMem_ptr, void *addr){
			VpuMem * vmem=(VpuMem*)VpuMem_ptr;
			vmem->vpu_mem_dealloc(addr);
		}

		void jz4740_free_devmem ()
		{
		}

		unsigned int get_vaddr (unsigned int vaddr) {
			return 0;
		}

	}

	extern "C" int jz_vpu_mmap(unsigned int vaddr, unsigned int size);
	extern "C" int jz_vpu_munmap(unsigned int vaddr, unsigned int size);

	VpuMem::VpuMem() {
	}

	void VpuMem::vpu_mem_dealloc(void* ptr) {
		if (NULL == ptr)
			return;

		for(unsigned int i = 0; i < mDevBuffers.size(); ++i){
			MemoryHeapBase* heap = mDevBuffers.editItemAt(i);
			if(heap->getBase() == ptr){
				ALOGE("VpuMem::vpu_mem_dealloc heapbase:%p, vaddr:%p", heap, ptr);
				jz_vpu_munmap((unsigned int)heap->getBase(), heap->getSize());
				delete heap;
				heap = NULL;
				mDevBuffers.removeItemsAt(i);
				break;
			}
		}
	}

	void* VpuMem::vpu_mem_alloc(int size){
		mDevBuffers.push();
		MemoryHeapBase** devbuf = &mDevBuffers.editItemAt(mDevBuffers.size() - 1);
		*devbuf = new MemoryHeapBase(size);
		void* vaddr = (*devbuf)->getBase();
		int ret_size = (*devbuf)->getSize();
    		
		if (jz_vpu_mmap((unsigned int)vaddr, ret_size) == 0) {
			ALOGE("dmmu map error, media will crash");
			*(volatile int *)0x80000001 = 0;    
		}

		return vaddr;
	}

	VpuMem::~VpuMem(){
		for (unsigned int i = 0; i < mDevBuffers.size(); i++) {
			MemoryHeapBase* mptr = mDevBuffers.editItemAt(i);
			jz_vpu_munmap((unsigned int)mptr->getBase(), mptr->getSize());
			delete mptr;
			mptr = NULL;
			mDevBuffers.editItemAt(i) = NULL;
		}
	}

}
