#ifndef __VPU_MEM_H__
#define __VPU_MEM_H__

#include <utils/Vector.h>
#include "binder/MemoryHeapBase.h"

namespace android {

class VpuMem{
 public:
    VpuMem();
    ~VpuMem();

    void* vpu_mem_alloc(int size);
    void vpu_mem_dealloc(void* ptr);

 private:
    Vector<MemoryHeapBase * > mDevBuffers;
};

}

#endif
