LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

LOCAL_ARM_MODE := arm

LOCAL_SRC_FILES := \
	thread_impl/HWVideoDecoder.cpp \
	thread_impl/multi_thread/HWVideoDecoderThread.cpp \
	thread_impl/multi_thread/HWColorConverterThread.cpp \
	thread_impl/multi_thread/HWThread.cpp \
	thread_impl/multi_thread/HWMultiThreadImpl.cpp \
	thread_impl/BaseImpl.cpp \
	thread_impl/single_thread/HWSingleThreadImpl.cpp \
	thread_impl/single_thread/HWVideoDecConvThread.cpp \
	thread_impl/no_thread/HWNoThreadImpl.cpp \
	HWDec.cpp \


LOCAL_SHARED_LIBRARIES := \
	libstagefright \
	libstagefright_omx \
	libstagefright_foundation \
	libcutils	\
	libutils \
	libbinder	\
	libdl \
	libui \
	libgui \
	libjzipu \

LOCAL_STATIC_LIBRARIES := \
	libstagefright_vlume_codec

LOCAL_STATIC_LIBRARIES += \
		libstagefright_mpeg2 \
		libstagefright_vlumedecoder \
		libstagefright_vlumevc1  \
		libstagefright_realvideo  \
		libstagefright_mpeg4  \
		libstagefright_vlumeh264  \
		libstagefright_ffmpcommon \
		libstagefright_ffavutil  \
		libstagefright_ffavcore  \
		libOMX_Basecomponent \
		libstagefright_mphwapp\
	        libstagefright_jzmpeg2


LOCAL_MODULE := libstagefright_hard_vlume

LOCAL_C_INCLUDES := $(LOCAL_PATH)/./include \
	frameworks/av/media/libstagefright/include \
	frameworks/native/include/utils \
	frameworks/native/include/media/openmax \
	frameworks/native/include/media/hardware   \
	hardware/ingenic/display/ipu/ipu \
        hardware/ingenic/media/demux/demux/lume/stream \
        hardware/ingenic/media/demux/demux/lume/libmpdemux \
	hardware/ingenic/media/omx/omx/component/common \
	hardware/ingenic/media/omx/omx/component/common/colorconvert \
	hardware/ingenic/media/omx/omx/component/common/codeccontroller \
        hardware/ingenic/media/omx/omx/component/dec/video/lume_video/include \
	hardware/ingenic/media/omx/omx/component/dec/video/deccontroller \
	hardware/ingenic/media/omx/omx/component/dec/video/thread_impl \
	hardware/ingenic/media/omx/omx/component/dec/video/thread_impl/multi_thread \
	hardware/ingenic/media/omx/omx/component/dec/video/thread_impl/single_thread \
	hardware/ingenic/media/omx/omx/component/dec/video/thread_impl/no_thread \
        hardware/ingenic/media/omx/omx/component/dec/lume/libavutil \
        hardware/ingenic/media/omx/omx/component/dec/lume/libavcodec \
        hardware/ingenic/media/omx/omx/component/dec/lume/libmpcodecs \
        hardware/ingenic/media/omx/omx/component/dec/lume/libjzcommon \
        hardware/ingenic/media/omx/omx/component/dec/lume/ \
	hardware/ingenic/media/demux/demux/include

LOCAL_MODULE_TAGS := optional
include $(BUILD_SHARED_LIBRARY)
include $(call all-makefiles-under,$(LOCAL_PATH))
