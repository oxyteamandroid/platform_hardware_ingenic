/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

//#define LOG_NDEBUG 0
#define LOG_TAG "HWDec"
#include <utils/Log.h>

#include "HWDec.h"

#include <media/stagefright/foundation/ADebug.h>
#include <media/stagefright/MediaDefs.h>
#include <media/stagefright/MediaErrors.h>
#include <media/IOMX.h>
#include <dlfcn.h>
#include "lume_dec.h"

#include <LUMEDefs.h>
#include <HardwareAPI.h>
#include <ui/GraphicBufferMapper.h>

#include "Ingenic_OMX_Def.h"
#include <gui/ISurfaceComposer.h>
#include <gui/SurfaceComposerClient.h>
#include <ui/DisplayInfo.h>

extern "C"{
#include "stream.h"
#include "demuxer.h"
#include "stheader.h"
}


namespace android {

static const CodecProfileLevel kProfileLevels[] = {
    { OMX_VIDEO_AVCProfileBaseline, OMX_VIDEO_AVCLevel1  },
    { OMX_VIDEO_AVCProfileBaseline, OMX_VIDEO_AVCLevel1b },
    { OMX_VIDEO_AVCProfileBaseline, OMX_VIDEO_AVCLevel11 },
    { OMX_VIDEO_AVCProfileBaseline, OMX_VIDEO_AVCLevel12 },
    { OMX_VIDEO_AVCProfileBaseline, OMX_VIDEO_AVCLevel13 },
    { OMX_VIDEO_AVCProfileBaseline, OMX_VIDEO_AVCLevel2  },
    { OMX_VIDEO_AVCProfileBaseline, OMX_VIDEO_AVCLevel21 },
    { OMX_VIDEO_AVCProfileBaseline, OMX_VIDEO_AVCLevel22 },
    { OMX_VIDEO_AVCProfileBaseline, OMX_VIDEO_AVCLevel3  },
    { OMX_VIDEO_AVCProfileBaseline, OMX_VIDEO_AVCLevel31 },
    { OMX_VIDEO_AVCProfileBaseline, OMX_VIDEO_AVCLevel32 },
    { OMX_VIDEO_AVCProfileBaseline, OMX_VIDEO_AVCLevel4  },
    { OMX_VIDEO_AVCProfileBaseline, OMX_VIDEO_AVCLevel41 },
    { OMX_VIDEO_AVCProfileBaseline, OMX_VIDEO_AVCLevel42 },
    { OMX_VIDEO_AVCProfileBaseline, OMX_VIDEO_AVCLevel5  },
    { OMX_VIDEO_AVCProfileBaseline, OMX_VIDEO_AVCLevel51 },
};

template<class T>
static void InitOMXParams(T *params) {
    params->nSize = sizeof(T);
    params->nVersion.s.nVersionMajor = 1;
    params->nVersion.s.nVersionMinor = 0;
    params->nVersion.s.nRevision = 0;
    params->nVersion.s.nStep = 0;
}

HWDec::HWDec(
        const char *name,
        const OMX_CALLBACKTYPE *callbacks,
        OMX_PTR appData,
        OMX_COMPONENTTYPE **component)
    : SimpleHardOMXComponent(name, callbacks, appData, component),
      mWidth(320),
      mHeight(240),
      mDisplayWidth(mWidth),
      mDisplayHeight(mHeight),
      mCropLeft(0),
      mCropTop(0),
      mCropWidth(mDisplayWidth),
      mCropHeight(mDisplayHeight),
      mEOSStatus(INPUT_DATA_AVAILABLE),
      mOutputPortSettingsChange(NONE),
      mVideoFormat(OMX_VIDEO_CodingAVC),
      mConvertSrcFormat((OMX_COLOR_FORMATTYPE)OMX_COLOR_FormatYUV420Tile),
      mConvertDstFormat(OMX_COLOR_FormatYUV420Planar),
      mUseGraphicBuffer(false),
      mImplAPIs(NULL),
      mImplHandle(NULL),
      mImplStarted(false),
      mInputPortSettingsChangeWidth(mDisplayWidth),
      mInputPortSettingsChangeHeight(mDisplayHeight),
      mOutputPortSettingsChangeWidth(mDisplayWidth),
      mOutputPortSettingsChangeHeight(mDisplayHeight),
      mInputPortSettingsChangeWaitingForOutFull(false),
      mOutputPortSettingsChangeWaitingForOutFull(false),
      mBufCountInDecOutQueueForSizeChanged(0){

    initPorts();

    INGENIC_VIDEO_DEC_IMPL_INIT_CONFIG config;
    config.nInQueueCapacity = 10;
    config.nOutQueueCapacity = 10;
    config.eImplType = INGENIC_VIDEO_DEC_IMPL_INIT_CONFIG::IMPLE_TYPE_MULTI_THREAD_0;//IMPLE_TYPE_SINGLE_THREAD;//IMPLE_TYPE_NO_THREAD;//
    mImplAPIs = getIngenicVideoDecImplAPIs();
    mImplAPIs->Create(&mImplHandle, &config);

    // get screen size
    sp<IBinder> display(SurfaceComposerClient::getBuiltInDisplay(ISurfaceComposer::eDisplayIdMain));
    DisplayInfo info;
    SurfaceComposerClient::getDisplayInfo(display, &info);
    mScreenWidth = info.w > info.h ? info.w : info.h;
    mScreenHeight = info.w < info.h ? info.w : info.h;
    ALOGV("&&&&&&&&&&&&&&&&&&&&&&&&&& screen is %ld x %ld\n", mScreenWidth, mScreenHeight);
    checkDisplaySize();
}

HWDec::~HWDec() {
    ALOGV("~HWDec ");

    List<BufferInfo *> &outQueue = getPortQueue(kOutputPortIndex);
    List<BufferInfo *> &inQueue = getPortQueue(kInputPortIndex);
    CHECK(outQueue.empty());
    CHECK(inQueue.empty());

    mImplAPIs->Destroy(mImplHandle);
    ALOGV("~HWDec out");
}

void HWDec::initPorts() {
    OMX_PARAM_PORTDEFINITIONTYPE def;
    InitOMXParams(&def);

    def.nPortIndex = kInputPortIndex;
    def.eDir = OMX_DirInput;
    def.nBufferCountMin = kNumInputBuffers;
    def.nBufferCountActual = def.nBufferCountMin;
    def.nBufferSize = MAX_VIDEO_EXTRACTOR_BUFFER_RANGE;
    def.bEnabled = OMX_TRUE;
    def.bPopulated = OMX_FALSE;
    def.eDomain = OMX_PortDomainVideo;
    def.bBuffersContiguous = OMX_FALSE;
    def.nBufferAlignment = 1;

    def.format.video.pNativeRender = NULL;
    def.format.video.nFrameWidth = mWidth;
    def.format.video.nFrameHeight = mHeight;
    def.format.video.nStride = def.format.video.nFrameWidth;
    def.format.video.nSliceHeight = def.format.video.nFrameHeight;
    def.format.video.nBitrate = 0;
    def.format.video.xFramerate = 0;
    def.format.video.bFlagErrorConcealment = OMX_FALSE;
    def.format.video.eColorFormat = OMX_COLOR_FormatUnused;
    def.format.video.pNativeWindow = NULL;

    addPort(def);

    def.nPortIndex = kOutputPortIndex;
    def.eDir = OMX_DirOutput;
    def.nBufferCountMin = kNumOutputBuffers;
    def.nBufferCountActual = def.nBufferCountMin;
    def.bEnabled = OMX_TRUE;
    def.bPopulated = OMX_FALSE;
    def.eDomain = OMX_PortDomainVideo;
    def.bBuffersContiguous = OMX_FALSE;
    def.nBufferAlignment = 2;

    def.format.video.cMIMEType = const_cast<char *>(MEDIA_MIMETYPE_VIDEO_RAW);
    def.format.video.pNativeRender = NULL;
    def.format.video.nFrameWidth = mDisplayWidth;
    def.format.video.nFrameHeight = mDisplayHeight;
    def.format.video.nStride = def.format.video.nFrameWidth;
    def.format.video.nSliceHeight = def.format.video.nFrameHeight;
    def.format.video.nBitrate = 0;
    def.format.video.xFramerate = 0;
    def.format.video.bFlagErrorConcealment = OMX_FALSE;
    def.format.video.eCompressionFormat = OMX_VIDEO_CodingUnused;
    def.format.video.eColorFormat = OMX_COLOR_FormatYUV420Planar;//as default
    def.format.video.pNativeWindow = NULL;

    def.nBufferSize =
        (def.format.video.nFrameWidth * def.format.video.nFrameHeight * 3) / 2;

    addPort(def);

    updateVideoFormat();
}

OMX_ERRORTYPE HWDec::internalGetParameter(
        OMX_INDEXTYPE index, OMX_PTR params) {
    ALOGV("internalGetParameter in index = 0x%x",index);
    switch ((int)index) {
        case OMX_IndexParamGetAndroidNativeBuffer:
	{
	    GetAndroidNativeBufferUsageParams *Usageparams = (GetAndroidNativeBufferUsageParams *)params;
	    Usageparams->nUsage |= (GRALLOC_USAGE_HW_TEXTURE | GRALLOC_USAGE_EXTERNAL_DISP);
	    return OMX_ErrorNone;
	}
        case OMX_IndexParamVideoPortFormat:
	{
	    OMX_VIDEO_PARAM_PORTFORMATTYPE *formatParams = (OMX_VIDEO_PARAM_PORTFORMATTYPE *)params;

	    if (formatParams->nPortIndex > kOutputPortIndex) {
		return OMX_ErrorUndefined;
	    }

	    if (formatParams->nIndex != 0) {
		return OMX_ErrorNoMore;
	    }

	    if (formatParams->nPortIndex == kInputPortIndex) {
		OMX_PARAM_PORTDEFINITIONTYPE *def = &editPortInfo(formatParams->nPortIndex)->mDef;
		formatParams->eCompressionFormat = def->format.video.eCompressionFormat;//OMX_VIDEO_CodingAVC

		formatParams->eColorFormat = OMX_COLOR_FormatUnused;
		formatParams->xFramerate = 0;
	    } else {
		CHECK(formatParams->nPortIndex == kOutputPortIndex);

		OMX_PARAM_PORTDEFINITIONTYPE *def = &editPortInfo(formatParams->nPortIndex)->mDef;
		if(mConvertDstFormat == (OMX_COLOR_FORMATTYPE)OMX_COLOR_FormatGeneralHW){
		    mImplAPIs->GetParam(mImplHandle, INGENIC_VIDEO_DEC_PARAM_INDEXTYPE_OUTCOLORFORMAT, &mConvertDstFormat);
		    formatParams->eColorFormat = def->format.video.eColorFormat = mConvertDstFormat;
		}else
		    formatParams->eColorFormat = def->format.video.eColorFormat;//OMX_COLOR_FormatYUV420Planar;
		formatParams->eCompressionFormat = OMX_VIDEO_CodingUnused;
		formatParams->xFramerate = 0;
	    }
	    return OMX_ErrorNone;
	}

        case OMX_IndexParamVideoProfileLevelQuerySupported:
	{
	    OMX_VIDEO_PARAM_PROFILELEVELTYPE *profileLevel = (OMX_VIDEO_PARAM_PROFILELEVELTYPE *) params;

	    if (profileLevel->nPortIndex != kInputPortIndex) {
		ALOGE("Invalid port index: %ld", profileLevel->nPortIndex);
		return OMX_ErrorUnsupportedIndex;
	    }

	    size_t index = profileLevel->nProfileIndex;
	    size_t nProfileLevels =
		sizeof(kProfileLevels) / sizeof(kProfileLevels[0]);
	    if (index >= nProfileLevels) {
		return OMX_ErrorNoMore;
	    }

	    profileLevel->eProfile = kProfileLevels[index].mProfile;
	    profileLevel->eLevel = kProfileLevels[index].mLevel;
	    return OMX_ErrorNone;
	}

        default:
	    return SimpleHardOMXComponent::internalGetParameter(index, params);
    }
}

OMX_ERRORTYPE HWDec::internalSetParameter(
        OMX_INDEXTYPE index, const OMX_PTR params) {
    ALOGV("internalSetParameter in index = %x, %x ",index, OMX_IndexParamStandardComponentRole);
    switch ((int)index) {
        case OMX_IndexParamEnableAndroidBuffers:
	{
	    //OMX.google.android.index.enableAndroidNativeBuffers
	    EnableAndroidNativeBuffersParams *pANBParams = (EnableAndroidNativeBuffersParams *) params;
	    if(pANBParams->nPortIndex == kOutputPortIndex
	       && pANBParams->enable == OMX_TRUE) {
		OMX_PARAM_PORTDEFINITIONTYPE *def = &editPortInfo(pANBParams->nPortIndex)->mDef;
		def->format.video.eColorFormat = (OMX_COLOR_FORMATTYPE) HAL_PIXEL_FORMAT_RGBX_8888;//value with incompatiable HAL only under Android ANB mode.
		mConvertDstFormat = (OMX_COLOR_FORMATTYPE) OMX_COLOR_Format32bitRGBA8888;//OMX_COLOR_Format16bitRGB565;//
		mUseGraphicBuffer = true;
		updatePortDefinitions(pANBParams->nPortIndex);
	    }

	    return OMX_ErrorNone;
	}

        case OMX_IndexParamSetShContext:
	{
	    ALOGE("set sh to decoder:%p", params);

	    mImplAPIs->SetParam(mImplHandle, INGENIC_VIDEO_DEC_PARAM_INDEXTYPE_SHVIDEO, (sh_video_t*)params);
	    return OMX_ErrorNone;
	}

        case OMX_IndexParamStandardComponentRole:
	{
	    const OMX_PARAM_COMPONENTROLETYPE *roleParams =
		(const OMX_PARAM_COMPONENTROLETYPE *)params;

	    ALOGV("roleParams->cRole =  %s", (const char *)roleParams->cRole);
	    if (strncmp((const char *)roleParams->cRole, INGENIC_OMX_ROLE_MPEG4, OMX_MAX_STRINGNAME_SIZE - 1) == 0){
		mVideoFormat = OMX_VIDEO_CodingMPEG4;
	    }else if (strncmp((const char *)roleParams->cRole, INGENIC_OMX_ROLE_H264, OMX_MAX_STRINGNAME_SIZE - 1) == 0){
		mVideoFormat = OMX_VIDEO_CodingAVC;
	    }else if (strncmp((const char *)roleParams->cRole, INGENIC_OMX_ROLE_WMV3, OMX_MAX_STRINGNAME_SIZE - 1) == 0){
		mVideoFormat = (OMX_VIDEO_CODINGTYPE)OMX_VIDEO_CodingWMV3;
	    }else if (strncmp((const char *)roleParams->cRole, INGENIC_OMX_ROLE_RV40, OMX_MAX_STRINGNAME_SIZE - 1) == 0){
		mVideoFormat = (OMX_VIDEO_CODINGTYPE)OMX_VIDEO_CodingRV40;
	    }else if (strncmp((const char *)roleParams->cRole, INGENIC_OMX_ROLE_VIDEO_LUME, OMX_MAX_STRINGNAME_SIZE - 1) == 0){
	        mVideoFormat = (OMX_VIDEO_CODINGTYPE)OMX_VIDEO_CodingLUME;
	    }else{//TODO. more format should be add to ingenic extended. not this. also need re-value src/dst format for colorconverter.
		//return OMX_ErrorUndefined;
		mVideoFormat = OMX_VIDEO_CodingAVC;
	    }

	    updateVideoFormat();

	    return OMX_ErrorNone;
	}

        case OMX_IndexParamVideoPortFormat:
	{
	    OMX_VIDEO_PARAM_PORTFORMATTYPE *formatParams =
		(OMX_VIDEO_PARAM_PORTFORMATTYPE *)params;

	    if (formatParams->nPortIndex > kOutputPortIndex) {
		return OMX_ErrorUndefined;
	    }

	    if (formatParams->nIndex != 0) {
		return OMX_ErrorNoMore;
	    }

	    if(formatParams->nPortIndex == kOutputPortIndex) {
		//do nothing when android ANB mode, because its colorformat has been settled already. further color setparam wont work anymore.
		if(mUseGraphicBuffer){
		    return OMX_ErrorNone;
		}

		//set out omx colorformat for standard omx component when non android ANB mode.
		OMX_PARAM_PORTDEFINITIONTYPE *def = &editPortInfo(formatParams->nPortIndex)->mDef;

		switch((int)formatParams->eColorFormat){
		case OMX_COLOR_FormatGeneralHW:
		case OMX_COLOR_FormatYUV420Tile:
		case OMX_COLOR_FormatYUV420ArrayPlanar:
		{
		    //this is only a general one for hwcolorconverter, because we dont know the exact format ingenic omx output actually. which is to be sure afer the first decoded frame.
		    def->format.video.eColorFormat = mConvertDstFormat = (OMX_COLOR_FORMATTYPE)OMX_COLOR_FormatGeneralHW;
		    break;
		}
		case OMX_COLOR_FormatYUV420Planar:
		case OMX_COLOR_Format16bitRGB565:
		case OMX_COLOR_Format32bitRGBA8888:
		{
		    def->format.video.eColorFormat = mConvertDstFormat = formatParams->eColorFormat;
		    break;
		}
		default:
		    //unknown output color format.
		    ALOGE("Error: does not support the out format:0x%0x", formatParams->eColorFormat);
		    return OMX_ErrorUnsupportedSetting;
		}
	    }

	    return OMX_ErrorNone;
	}

        /*get ipuconvert dst buffer size from OMXCodec*/
        case OMX_IndexConfigCommonOutputSize:
	{
	    OMX_FRAMESIZETYPE *def = (OMX_FRAMESIZETYPE *)params;
	    if (def->nPortIndex == kOutputPortIndex) {
		setConvertDstSize(def->nWidth, def->nHeight);
	    }
	    return OMX_ErrorNone;
	}

	/*add by gysun : get w*h from ACodec.cpp*/
        case OMX_IndexParamPortDefinition:
	{
	    /**** decoder should have no need to gain width/height from outside.
	      OMX_PARAM_PORTDEFINITIONTYPE *def = (OMX_PARAM_PORTDEFINITIONTYPE *)params;
	      mWidth = def->format.video.nFrameWidth;
	      mHeight = def->format.video.nFrameHeight;
	      ALOGE("change w*h=(%d*%d)",mWidth,mHeight);
	    */
	}

        default:
	    return SimpleHardOMXComponent::internalSetParameter(index, params);
    }
}

OMX_ERRORTYPE HWDec::getConfig(
        OMX_INDEXTYPE index, OMX_PTR params) {
    switch (index) {
        case OMX_IndexConfigCommonOutputCrop:
        {
            OMX_CONFIG_RECTTYPE *rectParams = (OMX_CONFIG_RECTTYPE *)params;

            if (rectParams->nPortIndex != 1) {
                return OMX_ErrorUndefined;
            }

            rectParams->nLeft = mCropLeft;
            rectParams->nTop = mCropTop;
            rectParams->nWidth = mCropWidth;
            rectParams->nHeight = mCropHeight;

            return OMX_ErrorNone;
        }

        default:
            return OMX_ErrorUnsupportedIndex;
    }
}

void HWDec::onQueueFilled(OMX_U32 portIndex) {
    if(!startDecoding())
	return;

    if (mEOSStatus == OUTPUT_FRAMES_FLUSHED) {
	ALOGE("Warning: HWDec::onQueueFilled return for mEOSStatus!");
	return;
    }

    if(mOutputPortSettingsChange != NONE){
	ALOGE("Warning: HWDec::onQueueFilled return for mOutputPortSettingsChange!");
	return;
    }

    List<BufferInfo *> &inQueue = getPortQueue(kInputPortIndex);
    List<BufferInfo *> &outQueue = getPortQueue(kOutputPortIndex);

    if(mInputPortSettingsChangeWaitingForOutFull || mOutputPortSettingsChangeWaitingForOutFull){
	if(outQueue.size() < (kNumOutputBuffers - mBufCountInDecOutQueueForSizeChanged)){
	    ALOGE("Warning: portsettingschanged waiting for outqueue full filled!");
	    return;
	}else{
	    ALOGE("Warning: portsettingschanged got outqueue full filled!");
	    if (mInputPortSettingsChangeWaitingForOutFull)
		handlePortSettingsChangeEvent(kInputPortIndex);
	    if (mOutputPortSettingsChangeWaitingForOutFull)
		handlePortSettingsChangeEvent(kOutputPortIndex);
	    return;
	}
    }

    while(!inQueue.empty()){
	BufferInfo *inInfo = *inQueue.begin();
	inQueue.erase(inQueue.begin());

	mImplAPIs->Enqueue(mImplHandle, inInfo, INGENIC_VIDEO_DEC_DIR_IN);
    }

    while(!outQueue.empty()){
	BufferInfo *outInfo = *outQueue.begin();
	outQueue.erase(outQueue.begin());

	mImplAPIs->Enqueue(mImplHandle, outInfo, INGENIC_VIDEO_DEC_DIR_OUT);
    }
}

void HWDec::onBufferEmptied(BufferInfo* inInfo) {
    if(inInfo->mOwnedByUs == false){
	//dec/colorconvert trys to notify the buffer back when omx in flushing.
	ALOGE("Warning: onBufferEmptied ignore the flushed inbuffer:%p!", inInfo);
	return;
    }

    inInfo->mOwnedByUs = false;
    OMX_BUFFERHEADERTYPE* inHeader = inInfo->mHeader;
    notifyEmptyBufferDone(inHeader);
}

void HWDec::onBufferFilled(BufferInfo* outInfo) {
    if(outInfo->mOwnedByUs == false){
	ALOGE("Warning: onBufferEmptied ignore the flushed inbuffer:%p!", outInfo);
	return;
    }

    outInfo->mOwnedByUs = false;
    OMX_BUFFERHEADERTYPE* outHeader = outInfo->mHeader;
    notifyFillBufferDone(outHeader);
}

void HWDec::onSizeChanged(OMX_U32 portIndex, uint32_t width, uint32_t height) {
    ALOGE("Warning: HWDec::onSizeChanged portIndex:%u width:%u, height:%u!",(uint32_t)portIndex, width, height);

    INGENIC_VIDEO_DEC_IMPL_QUEUE_INFO info;
    mImplAPIs->GetParam(mImplHandle, INGENIC_VIDEO_DEC_PARAM_INDEXTYPE_OUTQUEUEINFO, &info);
    mBufCountInDecOutQueueForSizeChanged = info.nBufferInfoCount;
    //mImplAPIs->GetParam(mImplHandle, INGENIC_VIDEO_DEC_PARAM_INDEXTYPE_OUTBUFCOUNT, &mBufCountInDecOutQueueForSizeChanged);
    if (portIndex == kInputPortIndex) {
	mInputPortSettingsChangeWidth = width;
	mInputPortSettingsChangeHeight = height;
    } else {
	mOutputPortSettingsChangeWidth = width;
	mOutputPortSettingsChangeHeight = height;
	  //it is safe to flush dec/color thread without call pause, because the thread already in pause mode when size changed.
	mImplAPIs->Flush(mImplHandle, INGENIC_VIDEO_DEC_DIR_OUT);
    }

    //android OMXCodec requires that all out buffers should have been sent to its owner component before notified with portsettingschanged event.
    List<BufferInfo *> &outQueue = getPortQueue(kOutputPortIndex);
    if(outQueue.size() < kNumOutputBuffers - mBufCountInDecOutQueueForSizeChanged){
	ALOGE("Warning: onPortSettingsChanged won't trigger notify until the reset:%d buffer filled!!", kNumOutputBuffers - outQueue.size());
	if (portIndex == kInputPortIndex)
	    mInputPortSettingsChangeWaitingForOutFull = true;
	else
	    mOutputPortSettingsChangeWaitingForOutFull = true;
    }else
	handlePortSettingsChangeEvent(portIndex);
}

void HWDec::onEOS(BufferInfo* outInfo) {
    ALOGE("Warning: HWDec::onEOS!");

    List<BufferInfo *> &outQueue = getPortQueue(kOutputPortIndex);
    outQueue.clear();

    OMX_BUFFERHEADERTYPE *outHeader = outInfo->mHeader;
    outHeader->nTimeStamp = 0;
    outHeader->nFilledLen = 0;
    outHeader->nFlags = OMX_BUFFERFLAG_EOS;
    mEOSStatus = OUTPUT_FRAMES_FLUSHED;

    outInfo->mOwnedByUs = false;
    notifyFillBufferDone(outHeader);
}

void HWDec::onPortEnable(OMX_U32 portIndex, bool enable) {
    //TODO: no sure whether to flush dec/colorthread yet.
    SimpleHardOMXComponent::onPortEnable(portIndex, enable);
}

void HWDec::onPortFlush(OMX_U32 portIndex, bool sendFlushComplete) {
    ALOGE("Warning: HWDec::onPortFlush portIndex:%u!", (uint32_t)portIndex);

    INGENIC_VIDEO_DEC_IMPL_DIR dir;
    if(portIndex == 0)
	dir = INGENIC_VIDEO_DEC_DIR_IN;
    else
	dir = INGENIC_VIDEO_DEC_DIR_OUT;

    mImplAPIs->Pause(mImplHandle);

    mImplAPIs->Flush(mImplHandle, dir);

    SimpleHardOMXComponent::onPortFlush(portIndex, sendFlushComplete);
}

void HWDec::onPortFlushCompleted(OMX_U32 portIndex) {
    if (portIndex == kInputPortIndex) {
        mEOSStatus = INPUT_DATA_AVAILABLE;
    }

    mImplAPIs->Resume(mImplHandle);
}

void HWDec::onPortEnableCompleted(OMX_U32 portIndex, bool enabled) {
    switch (mOutputPortSettingsChange) {
    case NONE:
	break;
    case AWAITING_DISABLED:
	CHECK(!enabled);
	mOutputPortSettingsChange = AWAITING_ENABLED;
	break;
    default://enable
	CHECK_EQ((int)mOutputPortSettingsChange, (int)AWAITING_ENABLED);
	CHECK(enabled);
	mOutputPortSettingsChange = NONE;

	mImplAPIs->Resume(mImplHandle);

	break;
    }
}

bool HWDec::startDecoding() {
    if(mImplStarted)
	return true;

    INGENIC_VIDEO_DEC_IMPL_CALLBACKS callbacks;
    callbacks.BufferEmptied = SimpleHardOMXComponent::PostBufferEmptiedEvent;
    callbacks.BufferFilled = SimpleHardOMXComponent::PostBufferFilledEvent;
    callbacks.SizeChanged = SimpleHardOMXComponent::PostSizeChangedEvent;
    callbacks.EOSNotified = SimpleHardOMXComponent::PostEOSEvent;
    mImplAPIs->SetCallbacks(mImplHandle, &callbacks, this);

    mImplAPIs->SetParam(mImplHandle, INGENIC_VIDEO_DEC_PARAM_INDEXTYPE_CODINGTYPE, &mVideoFormat);

    INGENIC_VIDEO_DEC_PARAM_SIZE dec_size;
    dec_size.nWidth = mWidth;
    dec_size.nHeight = mHeight;
    mImplAPIs->SetParam(mImplHandle, INGENIC_VIDEO_DEC_PARAM_INDEXTYPE_DECODE_SIZE, &dec_size);

    setConvertDstSize(mDisplayWidth, mDisplayHeight);

    uint32_t useG = mUseGraphicBuffer?1:0;
    mImplAPIs->SetParam(mImplHandle, INGENIC_VIDEO_DEC_PARAM_INDEXTYPE_USEGRAPHICBUFFER, &useG);

    mImplAPIs->SetParam(mImplHandle, INGENIC_VIDEO_DEC_PARAM_INDEXTYPE_OUTCOLORFORMAT, &mConvertDstFormat);

    mImplAPIs->SetParam(mImplHandle, INGENIC_VIDEO_DEC_PARAM_INDEXTYPE_INCOLORFORMAT, &mConvertSrcFormat);

    mImplAPIs->Start(mImplHandle);

    mImplStarted = true;

    return true;
}

bool HWDec::stopDecoding() {
    if(!mImplStarted)
	return true;

    mImplAPIs->Stop(mImplHandle);

    mImplStarted = false;

    return true;
}

void HWDec::updatePortDefinitions(OMX_U32 portIndex) {
    OMX_PARAM_PORTDEFINITIONTYPE *def = &editPortInfo(portIndex)->mDef;
    if (portIndex == kInputPortIndex) {
	def->format.video.nFrameWidth = mWidth;
	def->format.video.nFrameHeight = mHeight;
	def->format.video.nStride = def->format.video.nFrameWidth;
	def->format.video.nSliceHeight = def->format.video.nFrameHeight;
    } else {
	def->format.video.nFrameWidth = mDisplayWidth;
	def->format.video.nFrameHeight = mDisplayHeight;
	def->format.video.nStride = def->format.video.nFrameWidth;
	def->format.video.nSliceHeight = def->format.video.nFrameHeight;

	switch((int)mConvertDstFormat){
            case OMX_COLOR_Format32bitRGBA8888:
		def->nBufferSize = (def->format.video.nFrameWidth * def->format.video.nFrameHeight) * 4;
		break;
            case OMX_COLOR_Format16bitRGB565:
		def->nBufferSize = (def->format.video.nFrameWidth * def->format.video.nFrameHeight) * 2;
		break;
            case OMX_COLOR_FormatYUV420Tile:
            case OMX_COLOR_FormatYUV420ArrayPlanar:
		def->nBufferSize = sizeof(INGENIC_OMX_YUV_FORMAT);
		break;
            case OMX_COLOR_FormatYUV420Planar:
		def->nBufferSize = (def->format.video.nFrameWidth * def->format.video.nFrameHeight * 3) / 2;
		break;
            default:
		def->nBufferSize = (def->format.video.nFrameWidth * def->format.video.nFrameHeight * 3) / 2;
		break;
	}
    }
}

void HWDec::handlePortSettingsChangeEvent(OMX_U32 portIndex) {
    if (portIndex == kInputPortIndex) {
	mWidth = mInputPortSettingsChangeWidth;
	mHeight = mInputPortSettingsChangeHeight;
	updatePortDefinitions(kInputPortIndex);
	mInputPortSettingsChangeWaitingForOutFull = false;
	checkDisplaySize();
    } else {
	mDisplayWidth = mOutputPortSettingsChangeWidth;
	mDisplayHeight = mOutputPortSettingsChangeHeight;
	updatePortDefinitions(kOutputPortIndex);

	notify(OMX_EventPortSettingsChanged, 1, 0, NULL);
	mOutputPortSettingsChange = AWAITING_DISABLED;

	handleCropRectEvent(0, 0, mDisplayWidth, mDisplayHeight);
	mOutputPortSettingsChangeWaitingForOutFull = false;
    }
}

void HWDec::handleCropRectEvent(uint32_t cropLeft, uint32_t cropTop, uint32_t cropWidth, uint32_t cropHeight) {
    mCropLeft = cropLeft;
    mCropTop = cropTop;
    mCropWidth = cropWidth;
    mCropHeight = cropHeight;

    notify(OMX_EventPortSettingsChanged, 1,
	   OMX_IndexConfigCommonOutputCrop, NULL);//to set nativewindow rect.
}

void HWDec::updateVideoFormat(){
    PortInfo* info = editPortInfo(0);
    if(info){
	OMX_PARAM_PORTDEFINITIONTYPE *def = &info->mDef;
	def->format.video.cMIMEType = const_cast<char *>(MEDIA_MIMETYPE_VIDEO_AVC);
	def->format.video.eCompressionFormat = OMX_VIDEO_CodingAVC;

	switch((int)mVideoFormat){
	    case OMX_VIDEO_CodingMPEG4:
		def->format.video.cMIMEType = const_cast<char *>(MEDIA_MIMETYPE_VIDEO_MPEG4);
		def->format.video.eCompressionFormat = OMX_VIDEO_CodingMPEG4;
		mConvertSrcFormat = (OMX_COLOR_FORMATTYPE)OMX_COLOR_FormatYUV420Tile;
		break;
	    case OMX_VIDEO_CodingAVC:
		def->format.video.cMIMEType = const_cast<char *>(MEDIA_MIMETYPE_VIDEO_AVC);
		def->format.video.eCompressionFormat = OMX_VIDEO_CodingAVC;
		mConvertSrcFormat = (OMX_COLOR_FORMATTYPE)OMX_COLOR_FormatYUV420Tile;
		break;
	    case OMX_VIDEO_CodingWMV3:
		def->format.video.cMIMEType = const_cast<char *>(MEDIA_MIMETYPE_VIDEO_WMV3);
		def->format.video.eCompressionFormat = OMX_VIDEO_CodingWMV;
		mConvertSrcFormat = (OMX_COLOR_FORMATTYPE)OMX_COLOR_FormatYUV420Tile;
		break;
	    case OMX_VIDEO_CodingRV40:
		def->format.video.cMIMEType = const_cast<char *>(MEDIA_MIMETYPE_VIDEO_RV40);
		def->format.video.eCompressionFormat = OMX_VIDEO_CodingRV;
		mConvertSrcFormat = (OMX_COLOR_FORMATTYPE)OMX_COLOR_FormatYUV420Tile;
		break;
	    case OMX_VIDEO_CodingLUME:
	        def->format.video.cMIMEType = const_cast<char *>(MEDIA_MIMETYPE_VIDEO_LUME);
	        def->format.video.eCompressionFormat = OMX_VIDEO_CodingAutoDetect;
	        mConvertSrcFormat = (OMX_COLOR_FORMATTYPE)OMX_COLOR_FormatYUV420Tile;
	        break;
	    default:
		mConvertSrcFormat = (OMX_COLOR_FORMATTYPE)OMX_COLOR_FormatYUV420ArrayPlanar;
		break;
	}
    }
}

/**
 * Check display size with screen size. There are three scales to set display size.
 * scaleW: the ratio of srcWidth and screenWidth.
 * scale:  1: if scaleW <= 1
 *         2: if 1 < scaleW < 3
 *         3: if scaleW >= 3
 */
void HWDec::checkDisplaySize() {
    float scaleW = (float) mWidth / mScreenWidth;
    ALOGV("checkDisplaySize scale=%f mWidth=%d mScreenWidth=%d",scaleW,mWidth,mScreenWidth);
    int scale = 1;
    if (scaleW > 1 && scaleW < 3)
	    scale = 2;
    else if (scaleW >= 3)
	    scale = 4;

    uint32_t n_width = mWidth / 16;
    uint32_t n_height = mHeight / 16;

    uint32_t width = n_width * 16;
    uint32_t height = n_height * 16;

    uint32_t dstWidth = width / scale;
    uint32_t dstHeight = height / scale;

    setConvertDstSize(dstWidth, dstHeight);

}

void HWDec::setConvertDstSize(uint32_t dstWidth, uint32_t dstHeight) {
    INGENIC_VIDEO_DEC_PARAM_SIZE conv_size;
    conv_size.nWidth = dstWidth;
    conv_size.nHeight = dstHeight;
    mImplAPIs->SetParam(mImplHandle, INGENIC_VIDEO_DEC_PARAM_INDEXTYPE_CONVERT_SIZE, &conv_size);
}

}  // namespace android

OMX_ERRORTYPE createHardOMXComponent(
        const char *name, const OMX_CALLBACKTYPE *callbacks,
        OMX_PTR appData, OMX_COMPONENTTYPE **component) {
    ALOGV("video createHardOMXComponent in");
    android::sp<android::HardOMXComponent> codec = new android::HWDec(name, callbacks, appData, component);
    if (codec == NULL) {
	return OMX_ErrorInsufficientResources;
    }

    OMX_ERRORTYPE err = codec->initCheck();
    if (err != OMX_ErrorNone)
	return err;

    codec->incStrong(NULL);

    return OMX_ErrorNone;
}
