/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, hardware
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef HARD_AVC_H_

#define HARD_AVC_H_

#include "SimpleHardOMXComponent.h"
#include <utils/KeyedVector.h>

#include "basetype.h"
#include "lume_dec.h"
#include "Ingenic_OMX_Def.h"

namespace android {

typedef struct{
    u32 cropLeftOffset;
    u32 cropOutWidth;
    u32 cropTopOffset;
    u32 cropOutHeight;
} CropParams;
  
struct HWDec : public SimpleHardOMXComponent {
    HWDec(const char *name,
	  const OMX_CALLBACKTYPE *callbacks,
	  OMX_PTR appData,
	  OMX_COMPONENTTYPE **component);

protected:
    virtual ~HWDec();

    virtual OMX_ERRORTYPE internalGetParameter(
            OMX_INDEXTYPE index, OMX_PTR params);

    virtual OMX_ERRORTYPE internalSetParameter(
            OMX_INDEXTYPE index, const OMX_PTR params);

    virtual OMX_ERRORTYPE getConfig(OMX_INDEXTYPE index, OMX_PTR params);

    virtual void onQueueFilled(OMX_U32 portIndex);
    virtual void onPortFlushCompleted(OMX_U32 portIndex);
    virtual void onPortEnableCompleted(OMX_U32 portIndex, bool enabled);

    virtual void onBufferEmptied(BufferInfo*);
    virtual void onBufferFilled(BufferInfo*);
    virtual void onSizeChanged(OMX_U32 portIndex, uint32_t width, uint32_t height);
    virtual void onEOS(BufferInfo*);

    virtual void onPortEnable(OMX_U32 portIndex, bool enable);
    virtual void onPortFlush(OMX_U32 portIndex, bool sendFlushComplete);

private:    
    enum {
        kInputPortIndex   = 0,
        kOutputPortIndex  = 1,
        kNumInputBuffers  = 8,
	kNumOutputBuffers = 8,
    };

    enum EOSStatus {
        INPUT_DATA_AVAILABLE,
        INPUT_EOS_SEEN,
        OUTPUT_FRAMES_FLUSHED,
    };

    enum OutputPortSettingChange {
        NONE,
        AWAITING_DISABLED,
        AWAITING_ENABLED
    };
    
    uint32_t mWidth, mHeight;
    uint32_t mDisplayWidth, mDisplayHeight;
    uint32_t mCropLeft, mCropTop;
    uint32_t mCropWidth, mCropHeight;
    uint32_t mScreenWidth, mScreenHeight;
    
    EOSStatus mEOSStatus;
    OutputPortSettingChange mOutputPortSettingsChange;

    OMX_VIDEO_CODINGTYPE mVideoFormat;
    OMX_COLOR_FORMATTYPE mConvertSrcFormat, mConvertDstFormat;
    bool mUseGraphicBuffer;

    const INGENIC_VIDEO_DEC_IMPL_APIS* mImplAPIs;
    INGENIC_VIDEO_DEC_IMPL_DECHANDLE mImplHandle;
    bool mImplStarted;

    uint32_t mInputPortSettingsChangeWidth, mInputPortSettingsChangeHeight;
    uint32_t mOutputPortSettingsChangeWidth, mOutputPortSettingsChangeHeight;
    bool mInputPortSettingsChangeWaitingForOutFull, mOutputPortSettingsChangeWaitingForOutFull;
    
    uint32_t mBufCountInDecOutQueueForSizeChanged;

    bool startDecoding();
    bool stopDecoding();

    void initPorts();
    void updatePortDefinitions(OMX_U32 portIndex);
    void updateVideoFormat();

    void handlePortSettingsChangeEvent(OMX_U32 portIndex);
    void handleCropRectEvent(uint32_t cropLeft, uint32_t cropTop, uint32_t cropWidth, uint32_t cropHeight);
        
    void checkDisplaySize();
    void setConvertDstSize(uint32_t dstWidth, uint32_t dstHeight);

    DISALLOW_EVIL_CONSTRUCTORS(HWDec);
};

}  // namespace android

#endif  // HARD_AVC_H_

