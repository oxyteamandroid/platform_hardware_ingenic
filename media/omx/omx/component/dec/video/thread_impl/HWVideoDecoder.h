/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef HW_VIDEO_DECODER_H
#define HW_VIDEO_DECODER_H

#include <OMX_Video.h>
#include "lume_dec.h"
#include "Ingenic_OMX_Def.h"

namespace android {

class HWVideoDecoder {
 public:
    HWVideoDecoder();
    ~HWVideoDecoder();
    
    bool setContext(sh_video_t*);
    bool setFormat(OMX_VIDEO_CODINGTYPE type);
    bool getFormat(OMX_VIDEO_CODINGTYPE* type);
    bool setSize(uint32_t width, uint32_t height);
    bool getJZflag(int32_t* useJzBuf);

    bool start();
    bool stop();

    enum DECODE_RESULT {
	SUCCESS_RESULT,
	SIZE_CHANGED_RESULT,
	SKIP_FRAME_RESULT,
	ERROR_RESULT,
    };

    struct Params {
	void* mData;
	uint32_t mLength;
	uint64_t mPts;
	OMX_U32 mFlags;
	uint32_t mWidth;
	uint32_t mHeight;
    };
    
    DECODE_RESULT decode(Params& in, Params& out);

 private:
    OMX_VIDEO_CODINGTYPE mFormat;
    uint32_t mWidth;
    uint32_t mHeight;
    bool mStarted;

    VideoDecoder* mVideoDecoder;
    sh_video_t* mContext;
    bool mContextSelfAllocated;

    OMX_PARAM_PORTDEFINITIONTYPE mPortParam;
    OMX_BOOL mDropFrame;
    OMX_S32 mFrameCount;

    HWVideoDecoder(const HWVideoDecoder&);
    HWVideoDecoder& operator=(const HWVideoDecoder&);
};

}//namespace

#endif
