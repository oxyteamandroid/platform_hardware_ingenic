/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "BaseImpl.h"
#include <media/stagefright/foundation/ADebug.h>
#include "HWMultiThreadImpl.h"
#include "HWSingleThreadImpl.h"
#include "HWNoThreadImpl.h"

namespace android {

OMX_ERRORTYPE BaseImpl::Create(OMX_IN INGENIC_VIDEO_DEC_IMPL_DECHANDLE* phDec,
				OMX_IN INGENIC_VIDEO_DEC_IMPL_INIT_CONFIG* config) {
    if(config->eImplType == INGENIC_VIDEO_DEC_IMPL_INIT_CONFIG::IMPLE_TYPE_MULTI_THREAD_0){
	HWMultiThreadImpl::Config hwConfig;
	hwConfig.mInQueueCapacity = config->nInQueueCapacity;
	hwConfig.mOutQueueCapacity = config->nOutQueueCapacity;
	hwConfig.mPlanarImageQueueCapacity = 3;//limited to lume decoder pre-allocated dmmu buffer.
	
	*phDec = new HWMultiThreadImpl(hwConfig);

	return OMX_ErrorNone;
    }else if(config->eImplType == INGENIC_VIDEO_DEC_IMPL_INIT_CONFIG::IMPLE_TYPE_SINGLE_THREAD){
	HWSingleThreadImpl::Config hwConfig;
	hwConfig.mInQueueCapacity = config->nInQueueCapacity;
	hwConfig.mOutQueueCapacity = config->nOutQueueCapacity;
	
	*phDec = new HWSingleThreadImpl(hwConfig);

	return OMX_ErrorNone;
    }else if(config->eImplType == INGENIC_VIDEO_DEC_IMPL_INIT_CONFIG::IMPLE_TYPE_NO_THREAD){
        HWNoThreadImpl::Config hwConfig;
	hwConfig.mInQueueCapacity = config->nInQueueCapacity;
	hwConfig.mOutQueueCapacity = config->nOutQueueCapacity;
	
	*phDec = new HWNoThreadImpl(hwConfig);

	return OMX_ErrorNone;
    }
    
    return OMX_ErrorUndefined;
}
    
OMX_ERRORTYPE BaseImpl::Destroy(OMX_IN INGENIC_VIDEO_DEC_IMPL_DECHANDLE hDec) {
    BaseImpl* impl = (BaseImpl*)hDec;
    delete impl;

    return OMX_ErrorNone;
}

OMX_ERRORTYPE BaseImpl::Start(OMX_IN INGENIC_VIDEO_DEC_IMPL_DECHANDLE hDec) {
    BaseImpl* impl = (BaseImpl*)hDec;
    return impl->start();
}

OMX_ERRORTYPE BaseImpl::Stop(OMX_IN INGENIC_VIDEO_DEC_IMPL_DECHANDLE hDec) {
    BaseImpl* impl = (BaseImpl*)hDec;
    return impl->stop();
}

OMX_ERRORTYPE BaseImpl::Pause(OMX_IN INGENIC_VIDEO_DEC_IMPL_DECHANDLE hDec) {
    BaseImpl* impl = (BaseImpl*)hDec;
    return impl->pause();
}

OMX_ERRORTYPE BaseImpl::Resume(OMX_IN INGENIC_VIDEO_DEC_IMPL_DECHANDLE hDec) {
    BaseImpl* impl = (BaseImpl*)hDec;
    return impl->resume();
}

OMX_ERRORTYPE BaseImpl::Enqueue(OMX_IN INGENIC_VIDEO_DEC_IMPL_DECHANDLE hDec,
				 OMX_IN INGENIC_VIDEO_DEC_IMPL_BUFFERINFO_PTR pBuffer,
				 OMX_IN INGENIC_VIDEO_DEC_IMPL_DIR eDir) {
    BaseImpl* impl = (BaseImpl*)hDec;
    return impl->enqueue(pBuffer, eDir);
}

OMX_ERRORTYPE BaseImpl::Flush(OMX_IN INGENIC_VIDEO_DEC_IMPL_DECHANDLE hDec,
			       OMX_IN INGENIC_VIDEO_DEC_IMPL_DIR eDir) {
    BaseImpl* impl = (BaseImpl*)hDec;
    return impl->flush(eDir);
}

OMX_ERRORTYPE BaseImpl::SetParam(OMX_IN INGENIC_VIDEO_DEC_IMPL_DECHANDLE hDec,
				  OMX_IN INGENIC_VIDEO_DEC_IMPL_PARAM_INDEXTYPE eIndex,
				  OMX_IN OMX_PTR pParams) {
    BaseImpl* impl = (BaseImpl*)hDec;
    return impl->setParam(eIndex, pParams);
}

OMX_ERRORTYPE BaseImpl::GetParam(OMX_IN INGENIC_VIDEO_DEC_IMPL_DECHANDLE hDec,
				  OMX_IN INGENIC_VIDEO_DEC_IMPL_PARAM_INDEXTYPE eIndex,
				  OMX_OUT OMX_PTR pParams) {
    BaseImpl* impl = (BaseImpl*)hDec;
    return impl->getParam(eIndex, pParams);
}

OMX_ERRORTYPE BaseImpl::SetCallbacks(OMX_IN INGENIC_VIDEO_DEC_IMPL_DECHANDLE hDec,
				      OMX_IN INGENIC_VIDEO_DEC_IMPL_CALLBACKS* pCallbacks,
				      OMX_IN OMX_PTR pCallbacksData) {
    BaseImpl* impl = (BaseImpl*)hDec;
    return impl->setCallbacks(pCallbacks, pCallbacksData);
}

OMX_ERRORTYPE BaseImpl::start() {
    CHECK(false);
    return OMX_ErrorUndefined;
}

OMX_ERRORTYPE BaseImpl::stop() {
    CHECK(false);
    return OMX_ErrorUndefined;    
}

OMX_ERRORTYPE BaseImpl::pause() {
    CHECK(false);
    return OMX_ErrorUndefined;
}

OMX_ERRORTYPE BaseImpl::resume() {
    CHECK(false);
    return OMX_ErrorUndefined;
}
    
OMX_ERRORTYPE BaseImpl::flush(INGENIC_VIDEO_DEC_IMPL_DIR eDir) {
    CHECK(false);
    return OMX_ErrorUndefined;
}

OMX_ERRORTYPE BaseImpl::setParam(INGENIC_VIDEO_DEC_IMPL_PARAM_INDEXTYPE eIndex,
				 OMX_PTR pParams) {
    CHECK(false);
    return OMX_ErrorUndefined;
}

OMX_ERRORTYPE BaseImpl::getParam(INGENIC_VIDEO_DEC_IMPL_PARAM_INDEXTYPE eIndex,
				 OMX_PTR pParams) {
    CHECK(false);
    return OMX_ErrorUndefined;
}

OMX_ERRORTYPE BaseImpl::setCallbacks(INGENIC_VIDEO_DEC_IMPL_CALLBACKS* pCallbacks,
				     OMX_PTR pCallbacksData) {
    mCallbacks.BufferEmptied = pCallbacks->BufferEmptied;
    mCallbacks.BufferFilled = pCallbacks->BufferFilled;
    mCallbacks.SizeChanged = pCallbacks->SizeChanged;
    mCallbacks.EOSNotified = pCallbacks->EOSNotified;

    mCallbacksData = pCallbacksData;

    return OMX_ErrorNone;
}

BaseImpl::BaseImpl(int inQueueCapacity, int outQueueCapacity)
    :mInQueueCapacity(inQueueCapacity),
     mOutQueueCapacity(outQueueCapacity),
     mCallbacksData(NULL){
    mInQueue = new CodecBufferQueue(mInQueueCapacity);
    mOutQueue = new CodecBufferQueue(mOutQueueCapacity);

    memset(&mCallbacks, 0, sizeof(INGENIC_VIDEO_DEC_IMPL_CALLBACKS));
}

BaseImpl::~BaseImpl() {
    if(mInQueue){
	delete mInQueue;
	mInQueue = NULL;
    }

    if(mOutQueue){
	delete mOutQueue;
	mOutQueue = NULL;
    }
}

bool BaseImpl::enqInBuffer(SimpleHardOMXComponent::BufferInfo* info) {
    CodecBuffer* buf = NULL;
    bool res = false;

    res = mInQueue->acquireEnqableBuffer(&buf);//blocking
    if(!res)
	return res;
    
    buf->mDataType = CodecBuffer::HARD_OMX_BUFFERINFO_MODE;
    buf->mData = info;
    
    res = mInQueue->enq(buf);
    if(!res)
	return res;

    return true;
}

bool BaseImpl::enqOutBuffer(SimpleHardOMXComponent::BufferInfo* info) {
    CodecBuffer* buf = NULL;
    bool res = false;

    res = mOutQueue->acquireEnqableBuffer(&buf);//blocking
    if(!res)
	return res;
    
    buf->mDataType = CodecBuffer::HARD_OMX_BUFFERINFO_MODE;
    buf->mData = info;
    
    res = mOutQueue->enq(buf);
    if(!res)
	return res;

    return true;
}

bool BaseImpl::deqInBuffer(SimpleHardOMXComponent::BufferInfo** info) {
    CodecBuffer* buf = NULL;
    bool res = false;

    res = mInQueue->empty();
    if(res)
	return false;

    res = mInQueue->acquireDeqableBuffer(&buf);
    if(!res)
	return res;
    
    res = mInQueue->deq(buf);
    if(!res)
	return res;
    
    CHECK(buf->mDataType == CodecBuffer::HARD_OMX_BUFFERINFO_MODE);
    *info = (SimpleHardOMXComponent::BufferInfo*)buf->mData;
    
    return true;
}

bool BaseImpl::deqOutBuffer(SimpleHardOMXComponent::BufferInfo** info) {
    CodecBuffer* buf = NULL;
    bool res = false;

    res = mOutQueue->empty();
    if(res)
	return false;

    res = mOutQueue->acquireDeqableBuffer(&buf);
    if(!res)
	return res;
    
    res = mOutQueue->deq(buf);
    if(!res)
	return res;
    
    CHECK(buf->mDataType == CodecBuffer::HARD_OMX_BUFFERINFO_MODE);
    *info = (SimpleHardOMXComponent::BufferInfo*)buf->mData;
    
    return true;
}
    
OMX_ERRORTYPE BaseImpl::enqueue(INGENIC_VIDEO_DEC_IMPL_BUFFERINFO_PTR pBuffer,
				INGENIC_VIDEO_DEC_IMPL_DIR eDir) {
    SimpleHardOMXComponent::BufferInfo* info = (SimpleHardOMXComponent::BufferInfo*)pBuffer;

    bool res = false;
    if(eDir == INGENIC_VIDEO_DEC_DIR_IN){
	res = enqInBuffer(info);
    }else if(eDir == INGENIC_VIDEO_DEC_DIR_OUT){
	res = enqOutBuffer(info);
    }else{
	ALOGE("Error: a omx buffer info can not be fill to two Dirs at the same time!!!!!!!");
    }
    
    return res?OMX_ErrorNone:OMX_ErrorUndefined;
}

}//namespace

const INGENIC_VIDEO_DEC_IMPL_APIS gIngenicVideoDecImplAPIs = {
    &android::BaseImpl::Create,
    &android::BaseImpl::Destroy,
    &android::BaseImpl::Start,
    &android::BaseImpl::Stop,
    &android::BaseImpl::Pause,
    &android::BaseImpl::Resume,
    &android::BaseImpl::Enqueue,
    &android::BaseImpl::Flush,
    &android::BaseImpl::SetParam,
    &android::BaseImpl::GetParam,
    &android::BaseImpl::SetCallbacks,
};

const INGENIC_VIDEO_DEC_IMPL_APIS* getIngenicVideoDecImplAPIs() {
    return &gIngenicVideoDecImplAPIs;
}
