/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "HWNoThreadImpl.h"
#include <media/stagefright/foundation/ADebug.h>

namespace android {

HWNoThreadImpl::HWNoThreadImpl(const Config& config)
    :BaseImpl(config.mInQueueCapacity, config.mOutQueueCapacity),
     mStarted(false){
    mDecoder = new HWVideoDecoder;
    mConverter = new HWColorConverter;
}

HWNoThreadImpl::~HWNoThreadImpl() {
    ALOGE("HWNoThreadImpl::~HWNoThreadImpl");
    
    if(mDecoder){
	delete mDecoder;
	mDecoder = NULL;
    }

    if(mConverter){
	delete mConverter;
	mConverter = NULL;
    }

    ALOGE("HWNoThreadImpl::~HWNoThreadImpl out");
}

HWVideoDecoder* HWNoThreadImpl::getDecoder() {
    return mDecoder;
}

HWColorConverter* HWNoThreadImpl::getConverter() {
    return mConverter;
}

OMX_ERRORTYPE HWNoThreadImpl::start() {
    if(mStarted)
	return OMX_ErrorNone;

    mDecoder->start();
    mConverter->start();

    mStarted = true;
    
    return OMX_ErrorNone;
}

OMX_ERRORTYPE HWNoThreadImpl::stop() {
    if(!mStarted)
	return OMX_ErrorNone;    

    mDecoder->stop();
    mConverter->stop();

    mStarted = false;

    return OMX_ErrorNone;
}

OMX_ERRORTYPE HWNoThreadImpl::pause() {

    return OMX_ErrorNone;
}

OMX_ERRORTYPE HWNoThreadImpl::resume() {

    return OMX_ErrorNone;
}

OMX_ERRORTYPE HWNoThreadImpl::enqueue(INGENIC_VIDEO_DEC_IMPL_BUFFERINFO_PTR pBuffer, INGENIC_VIDEO_DEC_IMPL_DIR eDir) {
    if(!mStarted)
	return OMX_ErrorUndefined;

    OMX_ERRORTYPE res = BaseImpl::enqueue(pBuffer, eDir);
    CHECK(res == OMX_ErrorNone);
    
    if(!mInQueue->empty() && !mOutQueue->empty()){
        bool res = false;
	CodecBuffer* inBuf = NULL;
	CodecBuffer* outBuf = NULL;
    
	res = mInQueue->acquireDeqableBuffer(&inBuf);
	CHECK(res);
		
	res = mOutQueue->acquireDeqableBuffer(&outBuf);
	CHECK(res);

	//input
	CHECK(inBuf->mDataType == CodecBuffer::HARD_OMX_BUFFERINFO_MODE);
	memset(&mDecInput, 0, sizeof(HWVideoDecoder::Params));
	SimpleHardOMXComponent::BufferInfo* inBufferInfo = (SimpleHardOMXComponent::BufferInfo*)inBuf->mData;
	OMX_BUFFERHEADERTYPE *inHeader = inBufferInfo->mHeader;
	mDecInput.mData = inHeader->pBuffer + inHeader->nOffset;
	mDecInput.mLength = inHeader->nFilledLen;
	mDecInput.mPts = inHeader->nTimeStamp;
	mDecInput.mFlags = inHeader->nFlags;
	
	//output
	memset(&mDecOutput, 0, sizeof(HWVideoDecoder::Params));
	mDecOutput.mData = &mPlanarImage;//type of PlanarImage.

	if(inHeader->nFlags & OMX_BUFFERFLAG_EOS) {
	    ALOGE("Warning: EOS for video decode thread!");

	    //return EOS Marked buffer back to omx.
	    inBuf->clear();
	    res = mInQueue->deq(inBuf);
	    CHECK(res);
	    mCallbacks.BufferEmptied(mCallbacksData, inBufferInfo);

	    SimpleHardOMXComponent::BufferInfo* outBufferInfo = (SimpleHardOMXComponent::BufferInfo*)outBuf->mData;
	    OMX_BUFFERHEADERTYPE *outHeader = outBufferInfo->mHeader;
	    outHeader->nTimeStamp = 0;
	    outHeader->nFilledLen = 0;
	    outHeader->nFlags = OMX_BUFFERFLAG_EOS;

	    outBuf->clear();
	    res = mOutQueue->deq(outBuf);
	    CHECK(res);

	    mCallbacks.EOSNotified(mCallbacksData, outBufferInfo);

	    return OMX_ErrorNone;
	}

	HWVideoDecoder::DECODE_RESULT decRes = mDecoder->decode(mDecInput, mDecOutput);

	if(decRes == HWVideoDecoder::SKIP_FRAME_RESULT) {
	    ALOGE("Warning: video Dec/Conv thread failed and return skip frame!!!");	    
	}else if(decRes == HWVideoDecoder::SIZE_CHANGED_RESULT){
	    ALOGE("Warning: video Dec/Conv thread detects portsettings changed!!");

	    mCallbacks.SizeChanged(mCallbacksData, 0, mDecOutput.mWidth, mDecOutput.mHeight);
	    mCallbacks.SizeChanged(mCallbacksData, 1, mDecOutput.mWidth, mDecOutput.mHeight);

	    OMX_COLOR_FORMATTYPE srcFmt, dstFmt;
	    mConverter->getFormat(&srcFmt, &dstFmt);
	    
	    HWColorConverter::Range srcRange(mDecOutput.mWidth, mDecOutput.mHeight);
	    HWColorConverter::Range dstRange(mDecOutput.mWidth, mDecOutput.mHeight);

	    bool useDstGraphicBuffer;
	    mConverter->getUseDstGraphicBuffer(&useDstGraphicBuffer);
	    
	    if(mConverter){
		delete mConverter;
		mConverter = NULL;
	    }
	    
	    mConverter = new HWColorConverter();
	    mConverter->setFormat(srcFmt, dstFmt);
	    mConverter->setRange(srcRange, dstRange);
	    mConverter->setUseDstGraphicBuffer(useDstGraphicBuffer);
	    
	    mConverter->start();
	}else{
	    //input
	    memset(&mColorInput, 0, sizeof(HWColorConverter::Params));
	    mColorInput.mData = mDecOutput.mData;//type of PlanarImage.
	    mColorInput.mIsRangeSet = false;

	    //output
	    memset(&mColorOutput, 0, sizeof(HWColorConverter::Params));
	    SimpleHardOMXComponent::BufferInfo* outBufferInfo = (SimpleHardOMXComponent::BufferInfo*)outBuf->mData;
	    OMX_BUFFERHEADERTYPE *outHeader = outBufferInfo->mHeader;
	    mColorOutput.mData = outHeader->pBuffer + outHeader->nOffset;
	    mColorOutput.mIsRangeSet = false;
	
	    res = mConverter->convert(mColorInput, mColorOutput);

	    outHeader->nFilledLen = mColorOutput.mLength;
	    outHeader->nFlags = mDecOutput.mFlags;
	    outHeader->nTimeStamp = mDecOutput.mPts;

	    outBuf->clear();
	    res = mOutQueue->deq(outBuf);
	    CHECK(res);

	    mCallbacks.BufferFilled(mCallbacksData, outBufferInfo);
	}

	inBuf->clear();
	res = mInQueue->deq(inBuf);
	CHECK(res);
	mCallbacks.BufferEmptied(mCallbacksData, inBufferInfo);
    }

    return OMX_ErrorNone;
}

OMX_ERRORTYPE HWNoThreadImpl::flush(INGENIC_VIDEO_DEC_IMPL_DIR eDir) {
    if(eDir == INGENIC_VIDEO_DEC_DIR_ALL){
	mInQueue->clear();
	mOutQueue->clear();
    }else if(eDir == INGENIC_VIDEO_DEC_DIR_IN){
	mInQueue->clear();
    }else if(eDir == INGENIC_VIDEO_DEC_DIR_OUT){
	mOutQueue->clear();
    }
    
    return OMX_ErrorNone;
}
    
OMX_ERRORTYPE HWNoThreadImpl::setParam(INGENIC_VIDEO_DEC_IMPL_PARAM_INDEXTYPE eIndex,
					     OMX_PTR pParams) {
    bool res = false;
    switch(eIndex){
    case INGENIC_VIDEO_DEC_PARAM_INDEXTYPE_SHVIDEO:
    {
	sh_video_t* sh = (sh_video_t*)pParams;
	res = mDecoder->setContext(sh);
	break;
    }
    case INGENIC_VIDEO_DEC_PARAM_INDEXTYPE_CODINGTYPE:
    {
	INGENIC_VIDEO_DEC_IMPL_PARAM_CODINGTYPE* coding = (INGENIC_VIDEO_DEC_IMPL_PARAM_CODINGTYPE*)pParams;
	res = mDecoder->setFormat(*coding);
	break;
    }
    case INGENIC_VIDEO_DEC_PARAM_INDEXTYPE_DECODE_SIZE:
    {
	INGENIC_VIDEO_DEC_PARAM_SIZE* size = (INGENIC_VIDEO_DEC_PARAM_SIZE*)pParams;
	res = mDecoder->setSize(size->nWidth, size->nHeight);
	if(!res)
	    break;

	HWColorConverter::Range srcRange(size->nWidth, size->nHeight);
	res = mConverter->setSrcRange(srcRange);
	break;
    }
    case INGENIC_VIDEO_DEC_PARAM_INDEXTYPE_CONVERT_SIZE:
    {
	INGENIC_VIDEO_DEC_PARAM_SIZE* size = (INGENIC_VIDEO_DEC_PARAM_SIZE*)pParams;
	HWColorConverter::Range dstRange(size->nWidth, size->nHeight);
	res = mConverter->setDstRange(dstRange);
	break;
    }
    case INGENIC_VIDEO_DEC_PARAM_INDEXTYPE_USEGRAPHICBUFFER:
    {
	INGENIC_VIDEO_DEC_PARAM_USEGRAPHICBUFFER* useG = (INGENIC_VIDEO_DEC_PARAM_USEGRAPHICBUFFER*)pParams;
	bool enable = (*useG == 0)?false:true;
	res = mConverter->setUseDstGraphicBuffer(enable);
	break;
    }
    case INGENIC_VIDEO_DEC_PARAM_INDEXTYPE_OUTCOLORFORMAT:
    {
	INGENIC_VIDEO_DEC_PARAM_OUTCOLORFORMAT* dstFmt = (INGENIC_VIDEO_DEC_PARAM_OUTCOLORFORMAT*)pParams;
	OMX_COLOR_FORMATTYPE src, dst;
	mConverter->getFormat(&src, &dst);
	res = mConverter->setFormat(src, *dstFmt);
	break;
    }
    default:
	break;
    }

    return res?OMX_ErrorNone:OMX_ErrorUndefined;
}

OMX_ERRORTYPE HWNoThreadImpl::getParam(INGENIC_VIDEO_DEC_IMPL_PARAM_INDEXTYPE eIndex,
					     OMX_PTR pParams) {
    bool res = false;
    switch(eIndex){
    case INGENIC_VIDEO_DEC_PARAM_INDEXTYPE_INQUEUEINFO:
    {
	INGENIC_VIDEO_DEC_IMPL_QUEUE_INFO* info = (INGENIC_VIDEO_DEC_IMPL_QUEUE_INFO*)pParams;
	CHECK(mInQueue->size() < INGENIC_VIDEO_DEC_IMPL_MAX_QUEUE_ITEMS_COUNT);
	info->nBufferInfoCount = mInQueue->size();
	res = true;
	break;
    }
    case INGENIC_VIDEO_DEC_PARAM_INDEXTYPE_OUTQUEUEINFO:
    {
	INGENIC_VIDEO_DEC_IMPL_QUEUE_INFO* info = (INGENIC_VIDEO_DEC_IMPL_QUEUE_INFO*)pParams;
	CHECK(mOutQueue->size() < INGENIC_VIDEO_DEC_IMPL_MAX_QUEUE_ITEMS_COUNT);
	info->nBufferInfoCount = mOutQueue->size();
	res = true;
	break;
    }
    case INGENIC_VIDEO_DEC_PARAM_INDEXTYPE_OUTCOLORFORMAT:
    {
	INGENIC_VIDEO_DEC_PARAM_OUTCOLORFORMAT* dstFmt = (INGENIC_VIDEO_DEC_PARAM_OUTCOLORFORMAT*)pParams;
	OMX_COLOR_FORMATTYPE src;
	res = mConverter->getFormat(&src, dstFmt);
	break;
    }
    default:
	break;
    }

    return res?OMX_ErrorNone:OMX_ErrorUndefined;
}


}//namespace 
