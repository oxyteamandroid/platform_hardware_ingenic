/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "HWMultiThreadImpl.h"
#include <media/stagefright/foundation/ADebug.h>

namespace android {

HWMultiThreadImpl::HWMultiThreadImpl(const Config& config)
    :BaseImpl(config.mInQueueCapacity, config.mOutQueueCapacity),
     mDecoderThread(NULL),
     mConverterThread(NULL),
     mStarted(false){
    mDecoder = new HWVideoDecoder;
    mConverter = new HWColorConverter;
    
    mPlanarImageQueue = new CodecBufferQueue(config.mPlanarImageQueueCapacity);
}

HWMultiThreadImpl::~HWMultiThreadImpl() {
    ALOGE("HWMultiThreadImpl::~HWMultiThreadImpl this:%p in", this);
    
    stopThread();
    
    if(mDecoder){
	delete mDecoder;
	mDecoder = NULL;
    }

    if(mConverter){
	delete mConverter;
	mConverter = NULL;
    }

    if(mPlanarImageQueue){
	delete mPlanarImageQueue;
	mPlanarImageQueue = NULL;
    }

    ALOGE("HWMultiThreadImpl::~HWMultiThreadImpl this:%p out", this);
}

HWVideoDecoder* HWMultiThreadImpl::getDecoder() {
    return mDecoder;
}

HWColorConverter* HWMultiThreadImpl::getConverter() {
    return mConverter;
}

void HWMultiThreadImpl::startThread() {
    mDecoder->start();
    mConverter->start();
    
    mDecoderThread = new HWVideoDecoderThread(mInQueue, mPlanarImageQueue, mDecoder, &mCallbacks, mCallbacksData);
    mDecoderThread->start();
    
    mConverterThread = new HWColorConverterThread(mPlanarImageQueue, mOutQueue, mConverter, &mCallbacks, mCallbacksData);
    mConverterThread->start();
}

void HWMultiThreadImpl::stopThread() {
    if(mDecoderThread){
	delete mDecoderThread;
	mDecoderThread = NULL;
    }
	
    if(mConverterThread){
	delete mConverterThread;
	mConverterThread = NULL;
    }
}

//still there can be several empty/fill buffers done called when pauseThread.
void HWMultiThreadImpl::pauseThread() {
    if(!mStarted)
	return;    
    
    mDecoderThread->pause();
    mConverterThread->pause();

    //the stop of all queues, will make the thread can be returned at any acquire calling position.
    mInQueue->pauseAcquire();
    mOutQueue->pauseAcquire();
    mPlanarImageQueue->pauseAcquire();
    
    mDecoderThread->waitForPaused();
    mConverterThread->waitForPaused();
}
    
void HWMultiThreadImpl::resumeThread() {
    if(!mStarted)
	return;

    mInQueue->resumeAcquire();
    mOutQueue->resumeAcquire();
    mPlanarImageQueue->resumeAcquire();

    mDecoderThread->resume();
    mConverterThread->resume();

    mDecoderThread->waitForResumed();
    mConverterThread->waitForResumed();    
}

void HWMultiThreadImpl::clearQueues() {
    if(!mStarted)
	return;

    mInQueue->clear();
    mPlanarImageQueue->clear();
    mOutQueue->clear();
}

OMX_ERRORTYPE HWMultiThreadImpl::start() {
    if(mStarted)
	return OMX_ErrorNone;

    startThread();

    mStarted = true;

    return OMX_ErrorNone;
}

OMX_ERRORTYPE HWMultiThreadImpl::stop() {
    if(!mStarted)
	return OMX_ErrorNone;

    stopThread();

    mStarted = false;

    return OMX_ErrorNone;
}

OMX_ERRORTYPE HWMultiThreadImpl::pause() {
    if(!mStarted)
	return OMX_ErrorUndefined;

    pauseThread();

    return OMX_ErrorNone;
}

OMX_ERRORTYPE HWMultiThreadImpl::resume() {
    if(!mStarted)
	return OMX_ErrorUndefined;

    resumeThread();

    return OMX_ErrorNone;
}

OMX_ERRORTYPE HWMultiThreadImpl::flush(INGENIC_VIDEO_DEC_IMPL_DIR eDir) {
    if(eDir == INGENIC_VIDEO_DEC_DIR_ALL){
	clearQueues();
    }else if(eDir == INGENIC_VIDEO_DEC_DIR_IN){
	mInQueue->clear();
	mPlanarImageQueue->clear();//if not flush, the pts when seek may mess up. because the buffer left in mPlanarImageQueue could be much ahead of playing/seeked pts, then awesomeplayer post time early video event.
    }else if(eDir == INGENIC_VIDEO_DEC_DIR_OUT){
	mOutQueue->clear();
    }
    
    return OMX_ErrorNone;
}
    
OMX_ERRORTYPE HWMultiThreadImpl::setParam(INGENIC_VIDEO_DEC_IMPL_PARAM_INDEXTYPE eIndex,
					     OMX_PTR pParams) {
    bool res = false;
    switch(eIndex){
    case INGENIC_VIDEO_DEC_PARAM_INDEXTYPE_SHVIDEO:
    {
	sh_video_t* sh = (sh_video_t*)pParams;
	res = mDecoder->setContext(sh);
	break;
    }
    case INGENIC_VIDEO_DEC_PARAM_INDEXTYPE_CODINGTYPE:
    {
	INGENIC_VIDEO_DEC_IMPL_PARAM_CODINGTYPE* coding = (INGENIC_VIDEO_DEC_IMPL_PARAM_CODINGTYPE*)pParams;
	res = mDecoder->setFormat(*coding);
	break;
    }
    case INGENIC_VIDEO_DEC_PARAM_INDEXTYPE_DECODE_SIZE:
    {
	INGENIC_VIDEO_DEC_PARAM_SIZE* size = (INGENIC_VIDEO_DEC_PARAM_SIZE*)pParams;
	res = mDecoder->setSize(size->nWidth, size->nHeight);
	if(!res)
	    break;

	HWColorConverter::Range srcRange(size->nWidth, size->nHeight);
	res = mConverter->setSrcRange(srcRange);
	break;
    }
    case INGENIC_VIDEO_DEC_PARAM_INDEXTYPE_CONVERT_SIZE:
    {
	INGENIC_VIDEO_DEC_PARAM_SIZE* size = (INGENIC_VIDEO_DEC_PARAM_SIZE*)pParams;
	if (mConverterThread != NULL)
	    mConverterThread->setDstSize(size->nWidth, size->nHeight);
	else {
	    HWColorConverter::Range dstRange(size->nWidth, size->nHeight);
	    res = mConverter->setDstRange(dstRange);
	}
	break;
    }
    case INGENIC_VIDEO_DEC_PARAM_INDEXTYPE_USEGRAPHICBUFFER:
    {
	INGENIC_VIDEO_DEC_PARAM_USEGRAPHICBUFFER* useG = (INGENIC_VIDEO_DEC_PARAM_USEGRAPHICBUFFER*)pParams;
	bool enable = (*useG == 0)?false:true;
	res = mConverter->setUseDstGraphicBuffer(enable);
	break;
    }
    case INGENIC_VIDEO_DEC_PARAM_INDEXTYPE_OUTCOLORFORMAT:
    {
	INGENIC_VIDEO_DEC_PARAM_OUTCOLORFORMAT* dstFmt = (INGENIC_VIDEO_DEC_PARAM_OUTCOLORFORMAT*)pParams;
	OMX_COLOR_FORMATTYPE src, dst;
	mConverter->getFormat(&src, &dst);
	res = mConverter->setFormat(src, *dstFmt);
	break;
    }
    case INGENIC_VIDEO_DEC_PARAM_INDEXTYPE_INCOLORFORMAT:
    {
	INGENIC_VIDEO_DEC_PARAM_OUTCOLORFORMAT* srcFmt = (INGENIC_VIDEO_DEC_PARAM_OUTCOLORFORMAT*)pParams;
	OMX_COLOR_FORMATTYPE src, dst;
	mConverter->getFormat(&src, &dst);
	res = mConverter->setFormat(*srcFmt, dst);
	break;
    }
    default:
	break;
    }

    return res?OMX_ErrorNone:OMX_ErrorUndefined;
}

OMX_ERRORTYPE HWMultiThreadImpl::getParam(INGENIC_VIDEO_DEC_IMPL_PARAM_INDEXTYPE eIndex,
					     OMX_PTR pParams) {
    bool res = false;
    switch(eIndex){
    case INGENIC_VIDEO_DEC_PARAM_INDEXTYPE_INQUEUEINFO:
    {
	INGENIC_VIDEO_DEC_IMPL_QUEUE_INFO* info = (INGENIC_VIDEO_DEC_IMPL_QUEUE_INFO*)pParams;
	CHECK(mInQueue->size() < INGENIC_VIDEO_DEC_IMPL_MAX_QUEUE_ITEMS_COUNT);
	info->nBufferInfoCount = mInQueue->size();
	res = true;
	break;
    }
    case INGENIC_VIDEO_DEC_PARAM_INDEXTYPE_OUTQUEUEINFO:
    {
	INGENIC_VIDEO_DEC_IMPL_QUEUE_INFO* info = (INGENIC_VIDEO_DEC_IMPL_QUEUE_INFO*)pParams;
	CHECK(mOutQueue->size() < INGENIC_VIDEO_DEC_IMPL_MAX_QUEUE_ITEMS_COUNT);
	info->nBufferInfoCount = mOutQueue->size();
	res = true;
	break;
    }
    case INGENIC_VIDEO_DEC_PARAM_INDEXTYPE_OUTCOLORFORMAT:
    {
	INGENIC_VIDEO_DEC_PARAM_OUTCOLORFORMAT* dstFmt = (INGENIC_VIDEO_DEC_PARAM_OUTCOLORFORMAT*)pParams;
	OMX_COLOR_FORMATTYPE src;
	mConverter->getFormat(&src, dstFmt);
	break;
    }
    default:
	break;
    }

    return res?OMX_ErrorNone:OMX_ErrorUndefined;
}


}//namespace 
