/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef HW_COLOR_CONVERTER_THREAD_H
#define HW_COLOR_CONVERTER_THREAD_H

#include "HWThread.h"
#include "HWColorConverter.h"

namespace android {

class HWColorConverterThread : public HWThread {
 public:
    HWColorConverterThread(CodecBufferQueue* in, CodecBufferQueue* out, 
			   HWColorConverter* converter, INGENIC_VIDEO_DEC_IMPL_CALLBACKS* callbacks, void* callbacksData);
    virtual ~HWColorConverterThread();

    virtual void setDstSize(uint32_t width, uint32_t height);
 protected:
    virtual void threadEntry();

 private:
    HWColorConverter* mConverter;

    HWColorConverter::Params mInput;
    HWColorConverter::Params mOutput;

    uint32_t mLastBufferWidth, mLastBufferHeight;
    uint32_t mDstWidth, mDstHeight;
    bool mDstSizeChanged;

    HWColorConverterThread(const HWColorConverterThread&);
    HWColorConverterThread& operator=(const HWColorConverterThread&);
};

}

#endif
