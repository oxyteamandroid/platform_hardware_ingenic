/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "HWThread.h"

namespace android {

HWThread::HWThread(CodecBufferQueue* in, CodecBufferQueue* out, INGENIC_VIDEO_DEC_IMPL_CALLBACKS* callbacks, void* callbacksData)
    :mInQueue(in),
     mOutQueue(out),
     mStarted(false),
     mThread(0),
     mDone(false),
     mPausing(false),
     mPaused(false),
     mCallbacks(callbacks),
     mCallbacksData(callbacksData){
}

HWThread::~HWThread() {
}

void HWThread::start() {
    if(mStarted)
	return;
    
    pthread_attr_t attr;
    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
    pthread_create(&mThread, &attr, threadWrapper, this);
    pthread_attr_destroy(&attr);
    
    mStarted = true;
}

void HWThread::stop() {
    if(!mStarted)
	return;

    mInQueue->stop();
    mOutQueue->stop();
    
    {
	Mutex::Autolock ao(mLock);
	mDone = true;
	mCondition.signal();
    }
    
    void *dummy;
    pthread_join(mThread, &dummy);

    mStarted = false;
}

void HWThread::pause() {
    Mutex::Autolock ao(mLock);
    mPausing = true;
}

void HWThread::waitForPaused() {
    Mutex::Autolock ao(mPauseLock);
    while(!mPaused){
	mPauseCondition.waitRelative(mPauseLock, 10000000);
    }
}

void HWThread::resume() {
    Mutex::Autolock ao(mLock);
    mPausing = false;
}

void HWThread::waitForResumed() {
    Mutex::Autolock ao(mPauseLock);
    while(mPaused){
	mPauseCondition.waitRelative(mPauseLock, 10000000);
    }
}

void* HWThread::threadWrapper(void* me) {
    HWThread* p = (HWThread*)me;
    p->threadEntry();
    
    return NULL;
}

}//namespace
