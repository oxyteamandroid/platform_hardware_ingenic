/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef HW_THREAD_H
#define HW_THREAD_H

#include <utils/threads.h>
#include "CodecBufferQueue.h"
#include "Ingenic_VideoDec_Impl_Def.h"

namespace android {

class HWThread {
 public:
    HWThread(CodecBufferQueue* in, CodecBufferQueue* out, INGENIC_VIDEO_DEC_IMPL_CALLBACKS* callbacks, void* callbacksData);
    virtual ~HWThread();

    void start();
    void stop();
    void pause();
    void resume();

    void waitForPaused();
    void waitForResumed();
    
 protected:
    CodecBufferQueue* mInQueue;
    CodecBufferQueue* mOutQueue;

    bool mStarted;
    pthread_t mThread;
    
    Mutex mLock;
    Condition mCondition;
    bool mDone;
    
    Mutex mPauseLock;
    Condition mPauseCondition;
    bool mPausing;
    bool mPaused;

    INGENIC_VIDEO_DEC_IMPL_CALLBACKS* mCallbacks;
    void* mCallbacksData;

    static void* threadWrapper(void* me);
    virtual void threadEntry() = 0;
        
 private:
    HWThread(const HWThread&);
    HWThread& operator=(const HWThread&);
};

}//namespace

#endif
