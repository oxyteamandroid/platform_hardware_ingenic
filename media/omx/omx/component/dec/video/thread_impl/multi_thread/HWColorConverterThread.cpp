/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "HWColorConverterThread.h"

namespace android {

HWColorConverterThread::HWColorConverterThread(CodecBufferQueue* in, CodecBufferQueue* out,
					       HWColorConverter* converter, INGENIC_VIDEO_DEC_IMPL_CALLBACKS* callbacks, void* callbacksData)
    :HWThread(in, out, callbacks, callbacksData),
     mConverter(converter),
     mDstSizeChanged(false) {
    memset(&mInput, 0, sizeof(HWColorConverter::Params));
    memset(&mOutput, 0, sizeof(HWColorConverter::Params));

    HWColorConverter::Range srcRange, dstRange;
    mConverter->getRange(&srcRange, &dstRange);

    mLastBufferWidth = srcRange.mWidth;
    mLastBufferHeight = srcRange.mHeight;

    mDstWidth = dstRange.mWidth;
    mDstHeight = dstRange.mHeight;
}

HWColorConverterThread::~HWColorConverterThread() {
    stop();
}

void HWColorConverterThread::threadEntry() {
    for(;;){
	{
	    Mutex::Autolock ao(mLock);

	    while(!mDone &&
		  mPausing){
		{
		    Mutex::Autolock ao(mPauseLock);
		    mPaused = true;
		    mPauseCondition.signal();
		}

		mCondition.waitRelative(mLock, 10000000);
	    }

	    if(mDone){
		return;
	    }
	}

	{
	    Mutex::Autolock ao(mPauseLock);
	    if(mPaused){
		mPaused = false;
		mPauseCondition.signal();
	    }
	}

	bool res = false;
	CodecBuffer* inBuf = NULL;
	CodecBuffer* outBuf = NULL;

	res = mInQueue->acquireDeqableBuffer(&inBuf);
	if(!res){
	    ALOGE("Warning: fail to acquireDeqableBuffer for colorconvert thread and continue!");
	    continue;
	}

	res = mOutQueue->acquireDeqableBuffer(&outBuf);
	if(!res){
	    ALOGE("Warning: fail to acquireEnqableBuffer for colorconvert thread and continue!");
	    continue;
	}

	//check eos.
	if(inBuf->mFlags & OMX_BUFFERFLAG_EOS){
	    ALOGE("Warning: EOS for color convert thread!");
	    inBuf->clear();
	    res = mInQueue->deq(inBuf);
	    CHECK(res);

	    SimpleHardOMXComponent::BufferInfo* outBufferInfo = (SimpleHardOMXComponent::BufferInfo*)outBuf->mData;
	    OMX_BUFFERHEADERTYPE *outHeader = outBufferInfo->mHeader;
	    outHeader->nTimeStamp = 0;
	    outHeader->nFilledLen = 0;
	    outHeader->nFlags = OMX_BUFFERFLAG_EOS;

	    outBuf->clear();
	    res = mOutQueue->deq(outBuf);
	    CHECK(res);

	    mCallbacks->EOSNotified(mCallbacksData, outBufferInfo);

	    mPausing = true;

	    continue;
	}

	//check input size changed.
	if(mLastBufferWidth != inBuf->mWidth || mLastBufferHeight != inBuf->mHeight){
	    ALOGE("Warning: color convert thread detects intput portsettings changed!!");

	    // only change src size, so no need to pause
	    //automaticaly enter pause state.
	    mPausing = true;
	    mCallbacks->SizeChanged(mCallbacksData, 0 /*inputport*/, inBuf->mWidth, inBuf->mHeight);

	    mConverter->stop();

	    HWColorConverter::Range srcRange(inBuf->mWidth, inBuf->mHeight);
	    mConverter->setSrcRange(srcRange);

	    mConverter->start();

	    mLastBufferWidth = inBuf->mWidth;
	    mLastBufferHeight = inBuf->mHeight;

	    continue;
	}
#if 0
	{
	    Mutex::Autolock ao(mLock);
	    //check output size changed.
	    if (mDstSizeChanged) {
		ALOGE("*****Warning: color convert thread detects output portsettings changed!!");

		  //automaticaly enter pause state.
		mPausing = true;
		mCallbacks->SizeChanged(mCallbacksData, 1 /*outputport*/, mDstWidth, mDstHeight);

		mConverter->stop();

		HWColorConverter::Range dstRange(mDstWidth, mDstHeight);
		mConverter->setDstRange(dstRange);

		mConverter->start();

		mDstSizeChanged = false;

		continue;
	    }
	}
#endif
	//check color changed.
	OMX_COLOR_FORMATTYPE srcFmt, dstFmt;
	mConverter->getFormat(&srcFmt, &dstFmt);
	INGENIC_OMX_YUV_FORMAT* yuv = (INGENIC_OMX_YUV_FORMAT*)inBuf->mData;
	if(srcFmt != yuv->eFormat){
	    ALOGE("Warning: color convert thread detects colorfmt changed!!");
	    mConverter->stop();
	    mConverter->setFormat(yuv->eFormat, dstFmt);
	    mConverter->start();
	}

	//input
	CHECK(inBuf->mDataType == CodecBuffer::PLANAR_IMAGE_MODE);
	memset(&mInput, 0, sizeof(HWColorConverter::Params));
	mInput.mData = inBuf->mData;//type of PlanarImage.
	mInput.mIsRangeSet = false;

	//output
	CHECK(outBuf->mDataType == CodecBuffer::HARD_OMX_BUFFERINFO_MODE);
	memset(&mOutput, 0, sizeof(HWColorConverter::Params));
	SimpleHardOMXComponent::BufferInfo* outBufferInfo = (SimpleHardOMXComponent::BufferInfo*)outBuf->mData;
	OMX_BUFFERHEADERTYPE *outHeader = outBufferInfo->mHeader;
	mOutput.mData = outHeader->pBuffer + outHeader->nOffset;
	mOutput.mIsRangeSet = false;

	res = mConverter->convert(mInput, mOutput);

	outHeader->nFilledLen = mOutput.mLength;
	outHeader->nFlags = inBuf->mFlags;
	outHeader->nTimeStamp = inBuf->mPts;

	inBuf->clear();
	res = mInQueue->deq(inBuf);
	CHECK(res);

	outBuf->clear();
	res = mOutQueue->deq(outBuf);
	CHECK(res);

	mCallbacks->BufferFilled(mCallbacksData, outBufferInfo);
    }
}

void HWColorConverterThread::setDstSize(uint32_t width, uint32_t height) {
    Mutex::Autolock ao(mLock);
    ALOGW("*****Warning: color convert thread detects output portsettings changed!!");
    ALOGD("ori:(%dx%d) new:(%dx%d)", mDstWidth, mDstHeight, width, height);

    mPausing = true;
    // need to notify omx size changed to resume the thread even if there is not change
    mCallbacks->SizeChanged(mCallbacksData, 1 /*outputport*/, width, height);

    if (width != mDstWidth || height != mDstHeight) {
	mDstWidth = width;
	mDstHeight = height;

	mConverter->stop();
	HWColorConverter::Range dstRange(mDstWidth, mDstHeight);
	mConverter->setDstRange(dstRange);
	mConverter->start();
    }
}

}//namespace
