/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef HW_VIDEO_DECODER_THREAD_H
#define HW_VIDEO_DECODER_THREAD_H

#include "HWThread.h"
#include "HWVideoDecoder.h"

namespace android {

class HWVideoDecoderThread : public HWThread {
 public:
    HWVideoDecoderThread(CodecBufferQueue* in, CodecBufferQueue* out, 
			 HWVideoDecoder* decoder, INGENIC_VIDEO_DEC_IMPL_CALLBACKS* callbacks, void* callbacksData);
    virtual ~HWVideoDecoderThread();
  
 protected:
    virtual void threadEntry();
    
 private:
    HWVideoDecoder* mDecoder;

    HWVideoDecoder::Params mInput;
    HWVideoDecoder::Params mOutput;
    
    HWVideoDecoderThread(const HWVideoDecoderThread&);
    HWVideoDecoderThread& operator=(const HWVideoDecoderThread&);
};

}//namespace

#endif
