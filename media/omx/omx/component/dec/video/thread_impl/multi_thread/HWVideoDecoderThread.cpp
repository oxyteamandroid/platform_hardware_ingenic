/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "HWVideoDecoderThread.h"

namespace android {

HWVideoDecoderThread::HWVideoDecoderThread(CodecBufferQueue* in, CodecBufferQueue* out, 
					   HWVideoDecoder* decoder, INGENIC_VIDEO_DEC_IMPL_CALLBACKS* callbacks, void* callbacksData)
    :HWThread(in, out, callbacks, callbacksData),
     mDecoder(decoder){
    memset(&mInput, 0, sizeof(HWVideoDecoder::Params));
    memset(&mOutput, 0, sizeof(HWVideoDecoder::Params));
}

HWVideoDecoderThread::~HWVideoDecoderThread(){
    stop();
}

void HWVideoDecoderThread::threadEntry(){
    for(;;){
	{
	    Mutex::Autolock ao(mLock);
	    
	    while(!mDone &&
		  mPausing){
		{
		    Mutex::Autolock ao(mPauseLock);
		    mPaused = true;
		    mPauseCondition.signal();
		}
		
		mCondition.waitRelative(mLock, 10000000);
	    }
		    
	    if(mDone){
		return;
	    }
	}
	
	{
	    Mutex::Autolock ao(mPauseLock);
	    if(mPaused){
		mPaused = false;
		mPauseCondition.signal();
	    }
	}

	bool res = false;
	CodecBuffer* inBuf = NULL;
	CodecBuffer* outBuf = NULL;
    
	res = mInQueue->acquireDeqableBuffer(&inBuf);
	if(!res){
	    ALOGE("Warning: fail to acquireDeqableBuffer for video decoder thread and continue!");
	    continue;
	}
		
	res = mOutQueue->acquireEnqableBuffer(&outBuf);
	if(!res){
	    ALOGE("Warning: fail to acquireEnqableBuffer for video decoder thread and continue!");
	    continue;
	}
		
	//input
	CHECK(inBuf->mDataType == CodecBuffer::HARD_OMX_BUFFERINFO_MODE);
	memset(&mInput, 0, sizeof(HWVideoDecoder::Params));
	SimpleHardOMXComponent::BufferInfo* inBufferInfo = (SimpleHardOMXComponent::BufferInfo*)inBuf->mData;
	OMX_BUFFERHEADERTYPE *inHeader = inBufferInfo->mHeader;
	mInput.mData = inHeader->pBuffer + inHeader->nOffset;
	mInput.mLength = inHeader->nFilledLen;
	mInput.mPts = inHeader->nTimeStamp;
	mInput.mFlags = inHeader->nFlags;
	
	//output
	memset(&mOutput, 0, sizeof(HWVideoDecoder::Params));
	outBuf->mDataType = CodecBuffer::PLANAR_IMAGE_MODE;
	outBuf->allocateData();
	mOutput.mData = outBuf->mData;//type of PlanarImage.

	if(inHeader->nFlags & OMX_BUFFERFLAG_EOS) {
	    ALOGE("Warning: EOS for video decode thread!");

	    //return EOS Marked buffer back to omx.
	    inBuf->clear();
	    res = mInQueue->deq(inBuf);
	    CHECK(res);
	    mCallbacks->BufferEmptied(mCallbacksData, inBufferInfo);

	    //pass EOS through mPlanarImage to colorconvert thread.
	    outBuf->mDataType = CodecBuffer::PLANAR_IMAGE_MODE;
	    outBuf->mPts = mInput.mPts;
	    outBuf->mFlags = mInput.mFlags;
	    outBuf->mWidth = mInput.mWidth;
	    outBuf->mHeight = mInput.mHeight;
	    res = mOutQueue->enq(outBuf);
	    CHECK(res);

	    mPausing = true;
	    
	    continue;
	}
	
	HWVideoDecoder::DECODE_RESULT decRes = mDecoder->decode(mInput, mOutput);

	//dequeue the inbuf anyway because it has been decoded. re-decode the same buffer wont do any good.
	inBuf->clear();
	res = mInQueue->deq(inBuf);
	CHECK(res);
	mCallbacks->BufferEmptied(mCallbacksData, inBufferInfo);
	
	if(decRes == HWVideoDecoder::SIZE_CHANGED_RESULT ||
	   decRes == HWVideoDecoder::SUCCESS_RESULT) {
	    outBuf->mDataType = CodecBuffer::PLANAR_IMAGE_MODE;
	    outBuf->mPts = mOutput.mPts;
	    outBuf->mFlags = mOutput.mFlags;
	    outBuf->mWidth = mOutput.mWidth;
	    outBuf->mHeight = mOutput.mHeight;
	    
	    res = mOutQueue->enq(outBuf);
	    CHECK(res);
	}else if(decRes == HWVideoDecoder::SKIP_FRAME_RESULT) {
	    ALOGE("Warning: video decoder failed and return skip frame!!!");

	    continue;
	}
    }
}

}//namespace
