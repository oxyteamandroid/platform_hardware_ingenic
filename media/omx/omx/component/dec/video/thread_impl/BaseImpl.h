/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef BASE_IMPL_H
#define BASE_IMPL_H

#include "Ingenic_VideoDec_Impl_Def.h"
#include "CodecBufferQueue.h"

namespace android {

class BaseImpl {
 public:
    static OMX_ERRORTYPE Create(OMX_IN INGENIC_VIDEO_DEC_IMPL_DECHANDLE* phDec,
				OMX_IN INGENIC_VIDEO_DEC_IMPL_INIT_CONFIG* config);
    
    static OMX_ERRORTYPE Destroy(OMX_IN INGENIC_VIDEO_DEC_IMPL_DECHANDLE hDec);

    static OMX_ERRORTYPE Start(OMX_IN INGENIC_VIDEO_DEC_IMPL_DECHANDLE hDec);

    static OMX_ERRORTYPE Stop(OMX_IN INGENIC_VIDEO_DEC_IMPL_DECHANDLE hDec);

    static OMX_ERRORTYPE Pause(OMX_IN INGENIC_VIDEO_DEC_IMPL_DECHANDLE hDec);

    static OMX_ERRORTYPE Resume(OMX_IN INGENIC_VIDEO_DEC_IMPL_DECHANDLE hDec);
    
    static OMX_ERRORTYPE Enqueue(OMX_IN INGENIC_VIDEO_DEC_IMPL_DECHANDLE hDec,
				 OMX_IN INGENIC_VIDEO_DEC_IMPL_BUFFERINFO_PTR pBuffer,
				 OMX_IN INGENIC_VIDEO_DEC_IMPL_DIR eDir);

    static OMX_ERRORTYPE Flush(OMX_IN INGENIC_VIDEO_DEC_IMPL_DECHANDLE hDec,
			       OMX_IN INGENIC_VIDEO_DEC_IMPL_DIR eDir);

    static OMX_ERRORTYPE SetParam(OMX_IN INGENIC_VIDEO_DEC_IMPL_DECHANDLE hDec,
				  OMX_IN INGENIC_VIDEO_DEC_IMPL_PARAM_INDEXTYPE eIndex,
				  OMX_IN OMX_PTR pParams);

    static OMX_ERRORTYPE GetParam(OMX_IN INGENIC_VIDEO_DEC_IMPL_DECHANDLE hDec,
				  OMX_IN INGENIC_VIDEO_DEC_IMPL_PARAM_INDEXTYPE eIndex,
				  OMX_OUT OMX_PTR pParams);

    static OMX_ERRORTYPE SetCallbacks(OMX_IN INGENIC_VIDEO_DEC_IMPL_DECHANDLE hDec,
				      OMX_IN INGENIC_VIDEO_DEC_IMPL_CALLBACKS* pCallbacks,
				      OMX_IN OMX_PTR pCallbacksData);

 protected:
    virtual OMX_ERRORTYPE start();

    virtual OMX_ERRORTYPE stop();

    virtual OMX_ERRORTYPE pause();

    virtual OMX_ERRORTYPE resume();

    virtual OMX_ERRORTYPE enqueue(INGENIC_VIDEO_DEC_IMPL_BUFFERINFO_PTR pBuffer,
				  INGENIC_VIDEO_DEC_IMPL_DIR eDir);

    virtual OMX_ERRORTYPE flush(INGENIC_VIDEO_DEC_IMPL_DIR eDir);
    
    virtual OMX_ERRORTYPE setParam(INGENIC_VIDEO_DEC_IMPL_PARAM_INDEXTYPE eIndex,
				   OMX_PTR pParams);
    
    virtual OMX_ERRORTYPE getParam(INGENIC_VIDEO_DEC_IMPL_PARAM_INDEXTYPE eIndex,
				   OMX_PTR pParams);

    virtual OMX_ERRORTYPE setCallbacks(INGENIC_VIDEO_DEC_IMPL_CALLBACKS* pCallbacks,
				       OMX_PTR pCallbacksData);

 public:
    BaseImpl(int inQueueCapacity, int outQueueCapacity);
    virtual ~BaseImpl();
    
    //enq/deq of InQueue is not thread safe.(also for OutQueue).
    //enq is a blocking call, because inqueue may have no enough free item to carry BufferInfo*, but it never happens here if the queue item num is set right.
    bool enqInBuffer(SimpleHardOMXComponent::BufferInfo* info);
    bool enqOutBuffer(SimpleHardOMXComponent::BufferInfo* info);
    
    //not like enq, deq is not a blocking one. return false when no more to deq.
    bool deqInBuffer(SimpleHardOMXComponent::BufferInfo** info);
    bool deqOutBuffer(SimpleHardOMXComponent::BufferInfo** info);

 protected:
    int mInQueueCapacity;
    int mOutQueueCapacity;

    CodecBufferQueue* mInQueue;
    CodecBufferQueue* mOutQueue;
           
    INGENIC_VIDEO_DEC_IMPL_CALLBACKS mCallbacks;
    OMX_PTR mCallbacksData;
 
 private:
    BaseImpl(const BaseImpl&);
    BaseImpl& operator=(const BaseImpl&);
};

}

#endif
