/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "HWVideoDecoder.h"
#include <OMX_Core.h>
#include "vd_lume_ctx.h"

namespace android {

HWVideoDecoder::HWVideoDecoder()
    :mFormat(OMX_VIDEO_CodingUnused),
     mWidth(320),
     mHeight(240),
     mStarted(false),
     mVideoDecoder(NULL),
     mContext(NULL),
     mContextSelfAllocated(false),
     mDropFrame(OMX_FALSE),
     mFrameCount(0) {
    ALOGE("HWVideoDecoder::HWVideoDecoder this:%p", this);
    memset(&mPortParam, 0, sizeof(OMX_PARAM_PORTDEFINITIONTYPE));
}

HWVideoDecoder::~HWVideoDecoder() {
    ALOGE("HWVideoDecoder::~HWVideoDecoder this:%p in", this);
    stop();
    ALOGE("HWVideoDecoder::~HWVideoDecoder this:%p out", this);
}

bool HWVideoDecoder::setContext(sh_video_t* sh) {
    if(mStarted)
	return false;

    if(mContextSelfAllocated && mContext != NULL){
	delete mContext;
	mContext = NULL;
    }

    mContext = sh;
    mContextSelfAllocated = false;
    
    return true;
}

bool HWVideoDecoder::setFormat(OMX_VIDEO_CODINGTYPE type) {
    if(mStarted)
	return false;

    mFormat = type;
    
    return true;
}

bool HWVideoDecoder::getFormat(OMX_VIDEO_CODINGTYPE* type) {
    *type = mFormat;
    
    return true;
}

bool HWVideoDecoder::setSize(uint32_t width, uint32_t height) {
    if(mStarted)
	return false;

    mWidth = width;
    mHeight = height;

    return true;
}

//getjzflag has to be called when videodecoder got started.
bool HWVideoDecoder::getJZflag(int32_t *useJzBuf) {
    if(!mStarted)
	return false;

    *useJzBuf = 0;
    if (mVideoDecoder == NULL || mVideoDecoder->shContext == NULL)
	return true;

    if (((sh_video_t*)(mVideoDecoder->shContext))->drvmpeg2){
	*useJzBuf = 1;
    }else{
	*useJzBuf = ((vd_lume_ctx *)(((sh_video_t*)(mVideoDecoder->shContext))->context))->avctx->use_jz_buf;
    }

    return true;
}

bool HWVideoDecoder::start() {
    if(mStarted)
	return true;

    mVideoDecoder = new VideoDecoder();

    if(mVideoDecoder->DecInit() != OMX_ErrorNone){
	ALOGE("Error: failed to DecInit for lume decoder !!!!!");
	return false;
    }
    
    if (!mContext) {
	mContext = new sh_video_t;
	memset(mContext,0x0,sizeof(sh_video_t));
	
	mContext->bih = new BITMAPINFOHEADER;//no extradata.
	memset(mContext->bih,0,sizeof(BITMAPINFOHEADER) + 0);
	mContext->bih->biSize = sizeof(BITMAPINFOHEADER)  + 0;
        
	switch((int)mFormat){
	case OMX_VIDEO_CodingMPEG4:
	    mContext->bih->biCompression = mmioFOURCC('F', 'M', 'P', '4');
	    mContext->format = mmioFOURCC('F', 'M', 'P', '4');
	    break;
	case OMX_VIDEO_CodingAVC:
	    mContext->bih->biCompression = mmioFOURCC('A','V','C','1');
	    mContext->format = mmioFOURCC('A', 'V', 'C', '1');
	    break;
	case OMX_VIDEO_CodingWMV3:
	    mContext->bih->biCompression = mmioFOURCC('V','C','-','1');
	    mContext->format = mmioFOURCC('V','C','-','1');
	    break;
	case OMX_VIDEO_CodingRV40:
	    mContext->bih->biCompression = mmioFOURCC('R','V','4','0');
	    mContext->format = mmioFOURCC('R','V','4','0');
	    break;
	default:
	    ALOGE("Error: unspported video format:0x%x!!!", mFormat);
	    mVideoDecoder->DecDeinit();
	    delete mContext->bih;
	    delete mContext;
	    delete mVideoDecoder;
	    
	    return false;
	}
		
	mContext->is_rtsp = 1; //no extradata

	mContext->disp_w = mContext->bih->biWidth = mWidth;
	mContext->disp_h = mContext->bih->biHeight = mHeight;
    
	mContext->bih->biBitCount = 16; //YUV
	mContext->bih->biSizeImage = mWidth * mHeight * mContext->bih->biBitCount/8;
	mContext->bih->biCompression = mContext->format;
	
	mContextSelfAllocated = true;
    }
    
    if(mVideoDecoder->VideoDecSetConext(mContext) == OMX_FALSE){
	ALOGE("Error: VideoDecSetConext failed!!!");
	return false;
    }

    mStarted = true;
    
    return true;
}

bool HWVideoDecoder::stop() {
    if(!mStarted)
	return true;

    if(mContext && mContextSelfAllocated){
	if(mContext->bih){
	    delete mContext->bih;
	    mContext->bih = NULL;
	}
	
	delete mContext;
	mContext = NULL;
    }
    
    if(mVideoDecoder){
	delete mVideoDecoder;
	mVideoDecoder = NULL;
    }
    
    mStarted = false;

    return true;    
}

HWVideoDecoder::DECODE_RESULT HWVideoDecoder::decode(Params& in, Params& out) {
    if(!mStarted)
	return ERROR_RESULT;

    //clean up out params.
    out.mLength = 0;
    out.mPts = 0;
    out.mWidth = 0;
    out.mHeight = 0;

    //incoming pts and seekflag for lume decoder
    mVideoDecoder->shContext->pts = ((double)in.mPts)/1000000.0;
    mVideoDecoder->shContext->seekFlag = (in.mFlags & OMX_BUFFERFLAG_SEEKFLAG)?1:0;

    mPortParam.format.video.nFrameWidth = mWidth;
    mPortParam.format.video.nFrameHeight = mHeight;

    //don't pass input to Decode directly because it will be changed.
    void* input = in.mData;
    int32_t inputLength = in.mLength;

    OMX_BOOL ret = mVideoDecoder->DecodeVideo((OMX_U8*)out.mData,
					      (OMX_U32*)&out.mLength,
					      (OMX_U8**)&input,
					      (OMX_U32*)&inputLength,
					      &mPortParam,
					      &mFrameCount,
					      (OMX_BOOL)1,
					      &mDropFrame);
    if(ret == OMX_TRUE){

	out.mWidth = (int)mPortParam.format.video.nFrameWidth;
	out.mHeight = (int)mPortParam.format.video.nFrameHeight;

	//decoded video size changed.
	if (out.mWidth != mWidth || out.mHeight != mHeight) {
	    ALOGE("Warning: lume video decoded size changed from (%d * %d) to (%d * %d)!!", mWidth, mHeight, out.mWidth, out.mHeight);
	    
	    mWidth = out.mWidth;
	    mHeight = out.mHeight;

	    //the first buffer of size changed still should be saved for display.
	    if(((sh_video_t*)(mVideoDecoder->shContext))->drvmpeg2 == 0 && strcmp(mVideoDecoder->shContext->codec->dll,"h264") == 0){
	      out.mPts = ((INGENIC_OMX_YUV_FORMAT*)out.mData)->nPts;
	    }else{
	      out.mPts = in.mPts;
	    }
	    out.mLength = sizeof(INGENIC_OMX_YUV_FORMAT);
	    out.mFlags = in.mFlags;
	    
	    return SIZE_CHANGED_RESULT;

	}else if(out.mLength > 0){
	    if(((sh_video_t*)(mVideoDecoder->shContext))->drvmpeg2 == 0 && strcmp(mVideoDecoder->shContext->codec->dll,"h264") == 0){
	      out.mPts = ((INGENIC_OMX_YUV_FORMAT*)out.mData)->nPts;
	    }else{
	      out.mPts = in.mPts;
	    }
	    out.mLength = sizeof(INGENIC_OMX_YUV_FORMAT);
	    out.mFlags = in.mFlags;
	    
	    return SUCCESS_RESULT;
	}
    }

    return SKIP_FRAME_RESULT;
}

}//namespace
