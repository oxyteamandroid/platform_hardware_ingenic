/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "HWSingleThreadImpl.h"
#include <media/stagefright/foundation/ADebug.h>

namespace android {

HWSingleThreadImpl::HWSingleThreadImpl(const Config& config)
    :BaseImpl(config.mInQueueCapacity, config.mOutQueueCapacity),
     mThread(NULL),
     mStarted(false){
    mDecoder = new HWVideoDecoder;
    mConverter = new HWColorConverter;
}

HWSingleThreadImpl::~HWSingleThreadImpl() {
    ALOGE("HWSingleThreadImpl::~HWSingleThreadImpl");

    stopThread();

    if(mDecoder){
	delete mDecoder;
	mDecoder = NULL;
    }

    if(mConverter){
	delete mConverter;
	mConverter = NULL;
    }

    ALOGE("HWSingleThreadImpl::~HWSingleThreadImpl out");
}

HWVideoDecoder* HWSingleThreadImpl::getDecoder() {
    return mDecoder;
}

HWColorConverter* HWSingleThreadImpl::getConverter() {
    return mConverter;
}

void HWSingleThreadImpl::startThread() {
    mDecoder->start();
    mConverter->start();

    mThread = new HWVideoDecConvThread(mInQueue, mOutQueue, mDecoder, mConverter, &mCallbacks, mCallbacksData);
    mThread->start();
}

void HWSingleThreadImpl::stopThread() {
    if(!mStarted)
	return;

    if(mThread){
	delete mThread;
	mThread = NULL;
    }

    mDecoder->stop();
    mConverter->stop();

    mStarted = false;
}

//still there can be several empty/fill buffers done called when pauseThread.
void HWSingleThreadImpl::pauseThread() {
    if(!mStarted)
	return;

    mThread->pause();

    //the stop of all queues, will make the thread can be returned at any acquire calling position.
    mInQueue->pauseAcquire();
    mOutQueue->pauseAcquire();

    mThread->waitForPaused();
}

void HWSingleThreadImpl::resumeThread() {
    if(!mStarted)
	return;

    mInQueue->resumeAcquire();
    mOutQueue->resumeAcquire();

    mThread->resume();

    mThread->waitForResumed();
}

void HWSingleThreadImpl::clearQueues() {
    if(!mStarted)
	return;

    mInQueue->clear();
    mOutQueue->clear();
}

OMX_ERRORTYPE HWSingleThreadImpl::start() {
    if(mStarted)
	return OMX_ErrorNone;

    startThread();

    mStarted = true;

    return OMX_ErrorNone;
}

OMX_ERRORTYPE HWSingleThreadImpl::stop() {
    if(!mStarted)
	return OMX_ErrorNone;

    stopThread();

    return OMX_ErrorNone;
}

OMX_ERRORTYPE HWSingleThreadImpl::pause() {
    if(!mStarted)
	return OMX_ErrorUndefined;

    pauseThread();

    return OMX_ErrorNone;
}

OMX_ERRORTYPE HWSingleThreadImpl::resume() {
    if(!mStarted)
	return OMX_ErrorUndefined;

    resumeThread();

    return OMX_ErrorNone;
}

OMX_ERRORTYPE HWSingleThreadImpl::flush(INGENIC_VIDEO_DEC_IMPL_DIR eDir) {
    if(eDir == INGENIC_VIDEO_DEC_DIR_ALL){
	clearQueues();
    }else if(eDir == INGENIC_VIDEO_DEC_DIR_IN){
	mInQueue->clear();
    }else if(eDir == INGENIC_VIDEO_DEC_DIR_OUT){
	mOutQueue->clear();
    }

    return OMX_ErrorNone;
}

OMX_ERRORTYPE HWSingleThreadImpl::setParam(INGENIC_VIDEO_DEC_IMPL_PARAM_INDEXTYPE eIndex,
					     OMX_PTR pParams) {
    bool res = false;
    switch(eIndex){
    case INGENIC_VIDEO_DEC_PARAM_INDEXTYPE_SHVIDEO:
    {
	sh_video_t* sh = (sh_video_t*)pParams;
	res = mDecoder->setContext(sh);
	break;
    }
    case INGENIC_VIDEO_DEC_PARAM_INDEXTYPE_CODINGTYPE:
    {
	INGENIC_VIDEO_DEC_IMPL_PARAM_CODINGTYPE* coding = (INGENIC_VIDEO_DEC_IMPL_PARAM_CODINGTYPE*)pParams;
	res = mDecoder->setFormat(*coding);
	break;
    }
    case INGENIC_VIDEO_DEC_PARAM_INDEXTYPE_DECODE_SIZE:
    {
	INGENIC_VIDEO_DEC_PARAM_SIZE* size = (INGENIC_VIDEO_DEC_PARAM_SIZE*)pParams;
	res = mDecoder->setSize(size->nWidth, size->nHeight);
	if(!res)
	    break;

	HWColorConverter::Range srcRange(size->nWidth, size->nHeight);
	res = mConverter->setSrcRange(srcRange);
	break;
    }
    case INGENIC_VIDEO_DEC_PARAM_INDEXTYPE_CONVERT_SIZE:
    {
	INGENIC_VIDEO_DEC_PARAM_SIZE* size = (INGENIC_VIDEO_DEC_PARAM_SIZE*)pParams;
	if (mThread != NULL)
	    mThread->setDstSize(size->nWidth, size->nHeight);
	else {
	    HWColorConverter::Range dstRange(size->nWidth, size->nHeight);
	    res = mConverter->setDstRange(dstRange);
	}
	break;
    }
    case INGENIC_VIDEO_DEC_PARAM_INDEXTYPE_USEGRAPHICBUFFER:
    {
	INGENIC_VIDEO_DEC_PARAM_USEGRAPHICBUFFER* useG = (INGENIC_VIDEO_DEC_PARAM_USEGRAPHICBUFFER*)pParams;
	bool enable = (*useG == 0)?false:true;
	res = mConverter->setUseDstGraphicBuffer(enable);
	break;
    }
    case INGENIC_VIDEO_DEC_PARAM_INDEXTYPE_OUTCOLORFORMAT:
    {
	INGENIC_VIDEO_DEC_PARAM_OUTCOLORFORMAT* dstFmt = (INGENIC_VIDEO_DEC_PARAM_OUTCOLORFORMAT*)pParams;
	OMX_COLOR_FORMATTYPE src, dst;
	mConverter->getFormat(&src, &dst);
	res = mConverter->setFormat(src, *dstFmt);
	break;
    }
    case INGENIC_VIDEO_DEC_PARAM_INDEXTYPE_INCOLORFORMAT:
    {
	INGENIC_VIDEO_DEC_PARAM_OUTCOLORFORMAT* srcFmt = (INGENIC_VIDEO_DEC_PARAM_OUTCOLORFORMAT*)pParams;
	OMX_COLOR_FORMATTYPE src, dst;
	mConverter->getFormat(&src, &dst);
	res = mConverter->setFormat(*srcFmt, dst);
	break;
    }

    default:
	break;
    }

    return res?OMX_ErrorNone:OMX_ErrorUndefined;
}

OMX_ERRORTYPE HWSingleThreadImpl::getParam(INGENIC_VIDEO_DEC_IMPL_PARAM_INDEXTYPE eIndex,
					     OMX_PTR pParams) {
    bool res = false;
    switch(eIndex){
    case INGENIC_VIDEO_DEC_PARAM_INDEXTYPE_INQUEUEINFO:
    {
	INGENIC_VIDEO_DEC_IMPL_QUEUE_INFO* info = (INGENIC_VIDEO_DEC_IMPL_QUEUE_INFO*)pParams;
	CHECK(mInQueue->size() < INGENIC_VIDEO_DEC_IMPL_MAX_QUEUE_ITEMS_COUNT);
	info->nBufferInfoCount = mInQueue->size();
	res = true;
	break;
    }
    case INGENIC_VIDEO_DEC_PARAM_INDEXTYPE_OUTQUEUEINFO:
    {
	INGENIC_VIDEO_DEC_IMPL_QUEUE_INFO* info = (INGENIC_VIDEO_DEC_IMPL_QUEUE_INFO*)pParams;
	CHECK(mOutQueue->size() < INGENIC_VIDEO_DEC_IMPL_MAX_QUEUE_ITEMS_COUNT);
	info->nBufferInfoCount = mOutQueue->size();
	res = true;
	break;
    }
    case INGENIC_VIDEO_DEC_PARAM_INDEXTYPE_OUTCOLORFORMAT:
    {
	INGENIC_VIDEO_DEC_PARAM_OUTCOLORFORMAT* dstFmt = (INGENIC_VIDEO_DEC_PARAM_OUTCOLORFORMAT*)pParams;
	OMX_COLOR_FORMATTYPE src;
	res = mConverter->getFormat(&src, dstFmt);
	break;
    }
    default:
	break;
    }

    return res?OMX_ErrorNone:OMX_ErrorUndefined;
}


}//namespace
