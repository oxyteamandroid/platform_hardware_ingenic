/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef HW_SINGLE_THREAD_IMPL_H
#define HW_SINGLE_THREAD_IMPL_H

#include "BaseImpl.h"
#include "HWVideoDecoder.h"
#include "HWColorConverter.h"
#include "HWVideoDecConvThread.h"

#include <utils/threads.h>


namespace android {

class HWSingleThreadImpl : public BaseImpl {
 public:
    struct Config {
	int mInQueueCapacity;
	int mOutQueueCapacity;
	int mPlanarImageQueueCapacity;
    };

    HWSingleThreadImpl(const Config&);
    ~HWSingleThreadImpl();
    
    HWVideoDecoder* getDecoder();
    HWColorConverter* getConverter();
    
    void startThread();
    void stopThread();
    
    void pauseThread();
    void resumeThread();

    void clearQueues();

 protected:
    virtual OMX_ERRORTYPE start();

    virtual OMX_ERRORTYPE stop();

    virtual OMX_ERRORTYPE pause();

    virtual OMX_ERRORTYPE resume();

    virtual OMX_ERRORTYPE flush(INGENIC_VIDEO_DEC_IMPL_DIR eDir);
    
    virtual OMX_ERRORTYPE setParam(INGENIC_VIDEO_DEC_IMPL_PARAM_INDEXTYPE eIndex,
				   OMX_PTR pParams);
    
    virtual OMX_ERRORTYPE getParam(INGENIC_VIDEO_DEC_IMPL_PARAM_INDEXTYPE eIndex,
				   OMX_PTR pParams);

 private:
    HWVideoDecoder* mDecoder;
    HWColorConverter* mConverter;
    
    HWVideoDecConvThread* mThread;
    
    bool mStarted;
    
    HWSingleThreadImpl(const HWSingleThreadImpl&);
    HWSingleThreadImpl& operator=(const HWSingleThreadImpl&);
};

}//namespace

#endif
