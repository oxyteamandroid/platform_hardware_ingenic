/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "HWVideoDecConvThread.h"

namespace android {

HWVideoDecConvThread::HWVideoDecConvThread(CodecBufferQueue* in, CodecBufferQueue* out,
					   HWVideoDecoder* decoder, HWColorConverter* converter,
					   INGENIC_VIDEO_DEC_IMPL_CALLBACKS* callbacks, void* callbacksData)
    :mInQueue(in),
     mOutQueue(out),
     mStarted(false),
     mThread(0),
     mDone(false),
     mPausing(false),
     mPaused(false),
     mCallbacks(callbacks),
     mCallbacksData(callbacksData),
     mDecoder(decoder),
     mConverter(converter),
     mLastPlanarImageSaved(false),
     mDstSizeChanged(false) {

    HWColorConverter::Range srcRange, dstRange;
    mConverter->getRange(&srcRange, &dstRange);
    mDstWidth = dstRange.mWidth;
    mDstHeight = dstRange.mHeight;
}

HWVideoDecConvThread::~HWVideoDecConvThread() {
     stop();
}

void HWVideoDecConvThread::start() {
    if(mStarted)
	return;

    pthread_attr_t attr;
    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
    pthread_create(&mThread, &attr, threadWrapper, this);
    pthread_attr_destroy(&attr);

    mStarted = true;
}

void HWVideoDecConvThread::stop() {
    if(!mStarted)
	return;

    mInQueue->stop();
    mOutQueue->stop();

    {
	Mutex::Autolock ao(mLock);
	mDone = true;
	mCondition.signal();
    }

    void *dummy;
    pthread_join(mThread, &dummy);

    mStarted = false;
}

void HWVideoDecConvThread::pause() {
    Mutex::Autolock ao(mLock);
    mPausing = true;
}

void HWVideoDecConvThread::waitForPaused() {
    Mutex::Autolock ao(mPauseLock);
    while(!mPaused){
	mPauseCondition.waitRelative(mPauseLock, 10000000);
    }
}

void HWVideoDecConvThread::resume() {
    Mutex::Autolock ao(mLock);
    mPausing = false;
}

void HWVideoDecConvThread::waitForResumed() {
    Mutex::Autolock ao(mPauseLock);
    while(mPaused){
	mPauseCondition.waitRelative(mPauseLock, 10000000);
    }
}

void* HWVideoDecConvThread::threadWrapper(void* me) {
    HWVideoDecConvThread* p = (HWVideoDecConvThread*)me;
    p->threadEntry();

    return NULL;
}

void HWVideoDecConvThread::threadEntry(){
    for(;;){
	{
	    Mutex::Autolock ao(mLock);

	    while(!mDone &&
		  mPausing){
		{
		    Mutex::Autolock ao(mPauseLock);
		    mPaused = true;
		    mPauseCondition.signal();
		}

		mCondition.waitRelative(mLock, 10000000);
	    }

	    if(mDone){
		return;
	    }
	}

	{
	    Mutex::Autolock ao(mPauseLock);
	    if(mPaused){
		mPaused = false;
		mPauseCondition.signal();
	    }
	}

	bool res = false;
	CodecBuffer* inBuf = NULL;
	CodecBuffer* outBuf = NULL;

	res = mInQueue->acquireDeqableBuffer(&inBuf);
	if(!res){
	    ALOGE("Warning: fail to acquireDeqableBuffer for video decoder thread and continue!");
	    continue;
	}

	res = mOutQueue->acquireDeqableBuffer(&outBuf);
	if(!res){
	    ALOGE("Warning: fail to acquireEnqableBuffer for video decoder thread and continue!");
	    continue;
	}

	//input
	CHECK(inBuf->mDataType == CodecBuffer::HARD_OMX_BUFFERINFO_MODE);
	memset(&mDecInput, 0, sizeof(HWVideoDecoder::Params));
	SimpleHardOMXComponent::BufferInfo* inBufferInfo = (SimpleHardOMXComponent::BufferInfo*)inBuf->mData;
	OMX_BUFFERHEADERTYPE *inHeader = inBufferInfo->mHeader;
	mDecInput.mData = inHeader->pBuffer + inHeader->nOffset;
	mDecInput.mLength = inHeader->nFilledLen;
	mDecInput.mPts = inHeader->nTimeStamp;
	mDecInput.mFlags = inHeader->nFlags;

	//output
	memset(&mDecOutput, 0, sizeof(HWVideoDecoder::Params));
	mDecOutput.mData = &mPlanarImage;//type of PlanarImage.

	if(inHeader->nFlags & OMX_BUFFERFLAG_EOS) {
	    ALOGE("Warning: EOS for video decode thread!");

	    //return EOS Marked buffer back to omx.
	    inBuf->clear();
	    res = mInQueue->deq(inBuf);
	    CHECK(res);
	    mCallbacks->BufferEmptied(mCallbacksData, inBufferInfo);

	    SimpleHardOMXComponent::BufferInfo* outBufferInfo = (SimpleHardOMXComponent::BufferInfo*)outBuf->mData;
	    OMX_BUFFERHEADERTYPE *outHeader = outBufferInfo->mHeader;
	    outHeader->nTimeStamp = 0;
	    outHeader->nFilledLen = 0;
	    outHeader->nFlags = OMX_BUFFERFLAG_EOS;

	    outBuf->clear();
	    res = mOutQueue->deq(outBuf);
	    CHECK(res);

	    mCallbacks->EOSNotified(mCallbacksData, outBufferInfo);

	    mPausing = true;
	    continue;
	}

	HWVideoDecoder::DECODE_RESULT decRes = mDecoder->decode(mDecInput, mDecOutput);

	if(decRes == HWVideoDecoder::SKIP_FRAME_RESULT) {
	    ALOGE("Warning: video Dec/Conv thread failed and return skip frame!!!");
	}else if(decRes == HWVideoDecoder::SIZE_CHANGED_RESULT){
	    ALOGE("Warning: video Dec/Conv thread detects input portsettings changed!!");

	    // only change src size, so no need to pause
	    // mPausing = true;
	    mCallbacks->SizeChanged(mCallbacksData, 0 /*inputport*/, mDecOutput.mWidth, mDecOutput.mHeight);

	    OMX_COLOR_FORMATTYPE srcFmt, dstFmt;
	    mConverter->getFormat(&srcFmt, &dstFmt);

	    HWColorConverter::Range srcRange(mDecOutput.mWidth, mDecOutput.mHeight);
	    HWColorConverter::Range dstRange(mDstWidth, mDstHeight);
	    ALOGE("[ DEBUG ] mDecOutput.mWidth is %u", mDecOutput.mWidth);

	    bool useDstGraphicBuffer;
	    mConverter->getUseDstGraphicBuffer(&useDstGraphicBuffer);

	    if(mConverter){
		delete mConverter;
		mConverter = NULL;
	    }

	    mConverter = new HWColorConverter();
	    mConverter->setFormat(srcFmt, dstFmt);
	    mConverter->setRange(srcRange, dstRange);
	    mConverter->setUseDstGraphicBuffer(useDstGraphicBuffer);

	    mConverter->start();
	}else{
	    {
		Mutex::Autolock ao(mLock);

		  //check output size changed.
		if (mDstSizeChanged) {
		    ALOGE("*****Warning: video Dec/Conv thread detects output portsettings changed!!");

		      //automaticaly enter pause state.
		    mPausing = true;
		    mCallbacks->SizeChanged(mCallbacksData, 1 /*outputport*/, mDstWidth, mDstHeight);

		    mConverter->stop();

		    HWColorConverter::Range dstRange(mDstWidth, mDstHeight);
		    mConverter->setDstRange(dstRange);

		    mConverter->start();

		    mDstSizeChanged = false;

		    continue;
		}

	    }
	    //input
	    memset(&mColorInput, 0, sizeof(HWColorConverter::Params));
	    mColorInput.mData = mDecOutput.mData;//type of PlanarImage.
	    mColorInput.mIsRangeSet = false;

	    //output
	    memset(&mColorOutput, 0, sizeof(HWColorConverter::Params));
	    SimpleHardOMXComponent::BufferInfo* outBufferInfo = (SimpleHardOMXComponent::BufferInfo*)outBuf->mData;
	    OMX_BUFFERHEADERTYPE *outHeader = outBufferInfo->mHeader;
	    mColorOutput.mData = outHeader->pBuffer + outHeader->nOffset;
	    mColorOutput.mIsRangeSet = false;

	    res = mConverter->convert(mColorInput, mColorOutput);

	    outHeader->nFilledLen = mColorOutput.mLength;
	    outHeader->nFlags = mDecOutput.mFlags;
	    outHeader->nTimeStamp = mDecOutput.mPts;

	    outBuf->clear();
	    res = mOutQueue->deq(outBuf);
	    CHECK(res);

	    mCallbacks->BufferFilled(mCallbacksData, outBufferInfo);
	}

	//dequeue the inbuf anyway because it has been decoded. re-decode the same buffer wont do any good.
	inBuf->clear();
	res = mInQueue->deq(inBuf);
	CHECK(res);
	mCallbacks->BufferEmptied(mCallbacksData, inBufferInfo);
    }
}

void HWVideoDecConvThread::setDstSize(uint32_t width, uint32_t height) {
    Mutex::Autolock ao(mLock);
    if (width != mDstWidth || height != mDstHeight) {
	mDstWidth = width;
	mDstHeight = height;
	mDstSizeChanged = true;
    }
}

}//namespace
