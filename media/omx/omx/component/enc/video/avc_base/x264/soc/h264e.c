/*******************************************************
 Slice level init
******************************************************/

#include "jzasm.h"

#define LOGE(x,y...) //{printf(x,##y);}
extern volatile unsigned char *vmau_base;
extern volatile unsigned char *mc_base;
extern volatile unsigned char *gp0_base;

void tile_stuff(uint8_t *tile_y, uint8_t *tile_c, 
		uint8_t *frame_y, uint8_t *frame_u, uint8_t *frame_v,
		int linesize, int uvlinesize, 
		int mb_height, int mb_width)
{
    volatile uint32_t *tile, *dest, *dest1;
    int tile_linesize = mb_width * 16; 
    int mb_i, mb_j, col;
  
    for(mb_j = 0; mb_j < mb_height; mb_j++){
	for(mb_i = 0; mb_i < mb_width; mb_i++){
	    tile = tile_y + mb_j*16*tile_linesize + mb_i*16*16;
	    dest = frame_y + mb_j*16*linesize + mb_i*16;
	    for(col = 0; col < 16; col++){
		*tile++ = dest[0];
		*tile++ = dest[1];
		*tile++ = dest[2];
		*tile++ = dest[3];
		dest += (linesize / 4);
	    }
	    tile = tile_c + mb_j*8*tile_linesize + mb_i*16*8;
	    dest = frame_u + mb_j*8*uvlinesize + mb_i*8;
	    dest1 = frame_v + mb_j*8*uvlinesize + mb_i*8;
	    for(col = 0; col < 8; col++){
		tile[0] = dest[0];
		tile[1] = dest[1];
		tile[2] = dest1[0];
		tile[3] = dest1[1];
		tile += 4;
		dest += (uvlinesize / 4);
		dest1 += (uvlinesize / 4);
	    }
	}
    }
}

void H264E_SliceInit(_H264E_SliceInfo *s)
{
    int i, j, tmp;

    /**************************************************
     Motion configuration
    *************************************************/
    SET_REG1_STAT(1,/*pfe*/
		  1,/*lke*/
		  1 /*tke*/);
    SET_REG2_STAT(1,/*pfe*/
		  1,/*lke*/
		  1 /*tke*/);

    SET_REG1_CTRL(0,/*esms*/
		  15,/*erms*/
		  0,/*earm*/
		  1,/*pmve*/
		  3,/*esa*/
		  1,/*emet*/
		  0,/*cae*/
		  0,/*csf*/
		  0,/*pgc*/
		  1,/*ch2en*/
		  2,/*pri*/
		  1,/*ckge*/
		  1,/*ofa*/
		  0,/*rote*/
		  0,/*rotdir*/
		  1,/*wm*/
		  1,/*ccf*/
		  0,/*irqe*/
		  0,/*rst*/
		  1 /*en*/);
    SET_REG1_ESTIC(0, 10, 0, 1);

    /* Interpolation LUT init */
    for(i=0; i<16; i++){
#if 0
	SET_TAB1_ILUT(i,/*idx*/
		      IntpFMT[H264_QPEL][i].intp[1],/*intp2*/
		      IntpFMT[H264_QPEL][i].intp_pkg[1],/*intp2_pkg*/
		      IntpFMT[H264_QPEL][i].hldgl,/*hldgl*/
		      IntpFMT[H264_QPEL][i].avsdgl,/*avsdgl*/
		      IntpFMT[H264_QPEL][i].intp_dir[1],/*intp2_dir*/
		      IntpFMT[H264_QPEL][i].intp_rnd[1],/*intp2_rnd*/
		      IntpFMT[H264_QPEL][i].intp_sft[1],/*intp2_sft*/
		      IntpFMT[H264_QPEL][i].intp_sintp[1],/*sintp2*/
		      IntpFMT[H264_QPEL][i].intp_srnd[1],/*sintp2_rnd*/
		      IntpFMT[H264_QPEL][i].intp_sbias[1],/*sintp2_bias*/
		      IntpFMT[H264_QPEL][i].intp[0],/*intp1*/
		      IntpFMT[H264_QPEL][i].tap,/*tap*/
		      IntpFMT[H264_QPEL][i].intp_pkg[0],/*intp1_pkg*/
		      IntpFMT[H264_QPEL][i].intp_dir[0],/*intp1_dir*/
		      IntpFMT[H264_QPEL][i].intp_rnd[0],/*intp1_rnd*/
		      IntpFMT[H264_QPEL][i].intp_sft[0],/*intp1_sft*/
		      IntpFMT[H264_QPEL][i].intp_sintp[0],/*sintp1*/
		      IntpFMT[H264_QPEL][i].intp_srnd[0],/*sintp1_rnd*/
		      IntpFMT[H264_QPEL][i].intp_sbias[0]/*sintp1_bias*/
		      );
	SET_TAB1_CLUT(i,/*idx*/
		      IntpFMT[H264_QPEL][i].intp_coef[0][7],/*coef8*/
		      IntpFMT[H264_QPEL][i].intp_coef[0][6],/*coef7*/
		      IntpFMT[H264_QPEL][i].intp_coef[0][5],/*coef6*/
		      IntpFMT[H264_QPEL][i].intp_coef[0][4],/*coef5*/
		      IntpFMT[H264_QPEL][i].intp_coef[0][3],/*coef4*/
		      IntpFMT[H264_QPEL][i].intp_coef[0][2],/*coef3*/
		      IntpFMT[H264_QPEL][i].intp_coef[0][1],/*coef2*/
		      IntpFMT[H264_QPEL][i].intp_coef[0][0] /*coef1*/
		      );
	SET_TAB1_CLUT(16+i,/*idx*/
		      IntpFMT[H264_QPEL][i].intp_coef[1][7],/*coef8*/
		      IntpFMT[H264_QPEL][i].intp_coef[1][6],/*coef7*/
		      IntpFMT[H264_QPEL][i].intp_coef[1][5],/*coef6*/
		      IntpFMT[H264_QPEL][i].intp_coef[1][4],/*coef5*/
		      IntpFMT[H264_QPEL][i].intp_coef[1][3],/*coef4*/
		      IntpFMT[H264_QPEL][i].intp_coef[1][2],/*coef3*/
		      IntpFMT[H264_QPEL][i].intp_coef[1][1],/*coef2*/
		      IntpFMT[H264_QPEL][i].intp_coef[1][0] /*coef1*/
		      );
#endif
	SET_TAB2_ILUT(i,/*idx*/
		      IntpFMT[H264_EPEL][i].intp[1],/*intp2*/
		      IntpFMT[H264_EPEL][i].intp_dir[1],/*intp2_dir*/
		      IntpFMT[H264_EPEL][i].intp_sft[1],/*intp2_sft*/
		      IntpFMT[H264_EPEL][i].intp_coef[1][0],/*intp2_lcoef*/
		      IntpFMT[H264_EPEL][i].intp_coef[1][1],/*intp2_rcoef*/
		      IntpFMT[H264_EPEL][i].intp_rnd[1],/*intp2_rnd*/
		      IntpFMT[H264_EPEL][i].intp[0],/*intp1*/
		      IntpFMT[H264_EPEL][i].intp_dir[0],/*intp1_dir*/
		      IntpFMT[H264_EPEL][i].intp_sft[0],/*intp1_sft*/
		      IntpFMT[H264_EPEL][i].intp_coef[0][0],/*intp1_lcoef*/
		      IntpFMT[H264_EPEL][i].intp_coef[0][1],/*intp1_rcoef*/
		      IntpFMT[H264_EPEL][i].intp_rnd[0]/*intp1_rnd*/
		      );
    }

    SET_REG1_BINFO(AryFMT[H264_QPEL],/*ary*/
		   0,/*doe*/
		   12,/*expdy*/
		   12,/*expdx*/
		   0,/*ilmd*/
		   SubPel[H264_QPEL]-1,/*pel*/
		   0,/*fld*/
		   0,/*fldsel*/
		   0,/*boy*/
		   0,/*box*/
		   3,/*bh*/
		   3,/*bw*/
		   0/*pos*/);
    SET_REG2_BINFO(0,/*ary*/
		   0,/*doe*/
		   0,/*expdy*/
		   0,/*expdx*/
		   1,/*ilmd*/
		   SubPel[H264_EPEL],/*pel*/
		   0,/*fld*/
		   0,/*fldsel*/
		   0,/*boy*/
		   0,/*box*/
		   3,/*bh*/
		   3,/*bw*/
		   0/*pos*/);

    SET_REG1_IINFO1(IntpFMT[H264_QPEL][H2V0].intp[0],/*intp*/
		    IntpFMT[H264_QPEL][H2V0].tap,/*tap*/
		    1,/*intp_pkg*/
		    IntpFMT[H264_QPEL][H2V0].intp_dir[0],/*intp_dir*/
		    IntpFMT[H264_QPEL][H2V0].intp_rnd[0],/*intp_rnd*/
		    IntpFMT[H264_QPEL][H2V0].intp_sft[0],/*intp_sft*/
		    0,/*sintp*/
		    0,/*sintp_rnd*/
		    0 /*sintp_bias*/);
    SET_REG1_IINFO2(IntpFMT[H264_QPEL][H0V2].intp[0],/*intp*/
		    0,/*intp_pkg*/
		    0,/*hldgl*/
		    0,/*avsdgl*/
		    IntpFMT[H264_QPEL][H0V2].intp_dir[0],/*intp_dir*/
		    IntpFMT[H264_QPEL][H0V2].intp_rnd[0],/*intp_rnd*/
		    IntpFMT[H264_QPEL][H0V2].intp_sft[0],/*intp_sft*/
		    0,/*sintp*/
		    0,/*sintp_rnd*/
		    0 /*sintp_bias*/);

    SET_REG1_TAP1(IntpFMT[H264_QPEL][H2V0].intp_coef[0][7],
		  IntpFMT[H264_QPEL][H2V0].intp_coef[0][6],
		  IntpFMT[H264_QPEL][H2V0].intp_coef[0][5],
		  IntpFMT[H264_QPEL][H2V0].intp_coef[0][4],
		  IntpFMT[H264_QPEL][H2V0].intp_coef[0][3],
		  IntpFMT[H264_QPEL][H2V0].intp_coef[0][2],
		  IntpFMT[H264_QPEL][H2V0].intp_coef[0][1],
		  IntpFMT[H264_QPEL][H2V0].intp_coef[0][0]);
    SET_REG1_TAP2(IntpFMT[H264_QPEL][H0V2].intp_coef[0][7],
		  IntpFMT[H264_QPEL][H0V2].intp_coef[0][6],
		  IntpFMT[H264_QPEL][H0V2].intp_coef[0][5],
		  IntpFMT[H264_QPEL][H0V2].intp_coef[0][4],
		  IntpFMT[H264_QPEL][H0V2].intp_coef[0][3],
		  IntpFMT[H264_QPEL][H0V2].intp_coef[0][2],
		  IntpFMT[H264_QPEL][H0V2].intp_coef[0][1],
		  IntpFMT[H264_QPEL][H0V2].intp_coef[0][0]);

    /******************* ROA setting ******************/
    SET_TAB1_RLUT(0, s->fb[1][0], 0, 0);
    SET_TAB2_RLUT(0, s->fb[1][1], 0, 0, 0, 0);

    SET_REG1_PINFO(0,/*rgr*/
		   0,/*its*/
		   0,/*its_sft*/
		   0,/*its_scale*/
		   0 /*its_rnd*/);
    SET_REG2_PINFO(0,/*rgr*/
		   0,/*its*/
		   0,/*its_sft*/
		   0,/*its_scale*/
		   0 /*its_rnd*/);

    SET_REG1_WINFO(0,/*wt*/
		   0,/*wtpd*/
		   0,/*wtmd*/
		   1,/*biavg_rnd*/
		   0,/*wt_denom*/
		   5,/*wt_sft*/
		   0,/*wt_lcoef*/
		   0 /*wt_rcoef*/);
    SET_REG1_WTRND(1<<5);

    SET_REG2_WINFO1(0,/*wt*/
		    0,/*wtpd*/
		    0,/*wtmd*/
		    1,/*biavg_rnd*/
		    0,/*wt_denom*/
		    5,/*wt_sft*/
		    0,/*wt_lcoef*/
		    0 /*wt_rcoef*/);
    SET_REG2_WINFO2(5,/*wt_sft*/
		    0,/*wt_lcoef*/
		    0 /*wt_rcoef*/);
    SET_REG2_WTRND(1<<5, 1<<5);

    SET_REG1_DSTA(TCSM_PADDR(VRAM_ME_YDSTA));
    SET_REG1_STRD(s->mb_width*16+16*2,16,16);
    SET_REG1_GEOM(s->mb_height*16,s->mb_width*16);
    SET_REG1_DSA(TCSM_PADDR(VRAM_ME_DSA));
    SET_REG1_MVPA(TCSM_PADDR(VRAM_ME_MVPA));
    SET_REG1_RAWA(VRAM_ME_YRAWA);
    
    SET_REG2_DSTA(TCSM_PADDR(VRAM_ME_CDSTA));
    SET_REG2_STRD(s->mb_width*16+16*2,0,8);
    SET_REG1_DDC(TCSM_PADDR(VRAM_ME_CHN_BASE));

    /**************************************************
   VMAU configuration
    *************************************************/
    SET_VMAU(VMAU_SLV_GBL_RUN, VMAU_RESET);
    SET_VMAU(VMAU_SLV_VIDEO_TYPE, ( (H264 & MAU_VIDEO_MSK)<<MAU_VIDEO_SFT | 
				    (6 & MAU_SFT_MSK)<<MAU_SFT_SFT |
				    MAU_ENC_MSK << MAU_ENC_SFT ) );
    SET_VMAU(VMAU_SLV_NCCHN_ADDR, TCSM_PADDR(VRAM_MAU_CHN_BASE));
    SET_VMAU(VMAU_SLV_ENC_DONE, TCSM_PADDR(VRAM_MAU_ENC_SYNA));
    SET_VMAU(VMAU_SLV_DEC_DONE, TCSM_PADDR(VRAM_MAU_DEC_SYNA));
    SET_VMAU(VMAU_SLV_DEC_YADDR, VRAM_MAU_RCVA);
    SET_VMAU(VMAU_SLV_DEC_UADDR, VRAM_MAU_RCVA + 16*16);
    SET_VMAU(VMAU_SLV_DEC_VADDR, VRAM_MAU_RCVA + 16*16 + 8);
    SET_VMAU(VMAU_SLV_DEC_STR, 0x100010);
    SET_VMAU(VMAU_SLV_Y_GS, s->mb_width*16);
    SET_VMAU(VMAU_SLV_GBL_CTR, 1 | VMAU_CTRL_TO_DBLK);

    tmp = (int)(VMAU_V_BASE + VMAU_QT_BASE);
    for(j=0; j<4; j++){
	for(i=0; i<4; i++){
	    write_reg(tmp, *(int *)(&s->scaling_list[j][i*4]) );
	    tmp += 4;
	}
    }
    for(j=0; j<4; j++){
	for (i=0; i<8; i++){
	    int tmp0 = (1ULL << 8) / s->scaling_list[j][2*i];
	    int tmp1 = (1ULL << 8) / s->scaling_list[j][2*i+1];
	    write_reg(tmp, ((tmp0&0xFFFF) | (tmp1 << 16)));
	    tmp += 4;
	}
    }

    /**************************************************
   DBLK configuration
    *************************************************/
    SET_DBLK(DEBLK_REG_TRIG, DBLK_RESET);
    SET_DBLK(DEBLK_REG_DHA, TCSM_PADDR(VRAM_DBLK_CHN_BASE));
    SET_DBLK(DEBLK_REG_GENDA, TCSM_PADDR(VRAM_DBLK_CHN_SYNA));
    SET_DBLK(DEBLK_REG_GSIZE, (s->mb_height<<16 | s->mb_width) );
    SET_DBLK(DEBLK_REG_GPOS, (s->init_mby<<16 | s->init_mbx) );
    SET_DBLK(DEBLK_REG_CTRL, (1<<4/*expand edge*/ | s->rotate<<1 | s->deblock) );
    SET_DBLK(DEBLK_REG_GPIC_YA, s->fb[0][0] - (s->mb_width+3)*256);
    SET_DBLK(DEBLK_REG_GPIC_CA, s->fb[0][1] - (s->mb_width+3)*128);
    SET_DBLK(DEBLK_REG_GP_ENDA, TCSM_PADDR(VRAM_DBLK_DOUT_SYNA));
    SET_DBLK(DEBLK_REG_GPIC_STR, (s->mb_width<<23 | s->mb_width<<8) );
    SET_DBLK(DEBLK_REG_VTR, ( (s->beta_offset & DEBLK_VTR_BETA_MSK)<<DEBLK_VTR_BETA_SFT |
			      (s->alpha_c0_offset & DEBLK_VTR_ALPHA_MSK)<<DEBLK_VTR_ALPHA_SFT |
			      s->frame_type<<3 | DEBLK_VTR_VIDEO_H264) );
    SET_DBLK(DEBLK_REG_TRIG, DBLK_SLICE_RUN);
    write_reg(TCSM_VCADDR(VRAM_DBLK_DOUT_SYNA), 0xFFFFFFFF);
    for(i=0; i<47; i++)
	write_reg(TCSM_VCADDR(VRAM_DBLK_CHN_BASE+i*4), 0);
}

int load_index = 0;
#if 1
void elab_raw_start(x264_t *h, int x, int y)
{
    int mb_xy = x + y * h->sps->i_mb_width;
    if( mb_xy > h->sh.i_last_mb )
	return;

    unsigned int src = (unsigned int)(VRAM_ME_YRAWA + (load_index % 3) * 512 );
    volatile unsigned int * chain = TCSM_VCADDR(VRAM_DMA_CHN_BASE);
    chain[0] = get_phy_addr(h->tile_enc_y + mb_xy * 256) | 0x1;
    chain[1] = src;
    //chain[3] = (0x1 << 31) | 64;
    chain[4] = get_phy_addr(h->tile_enc_c + mb_xy * 128) | 0x1;
    chain[5] = src + 256;
    //chain[7] = 32;
    *(volatile unsigned int *)(gp0_base + 0xC) = 0;
    *(volatile unsigned int *)(gp0_base + 0x8) = TCSM_PADDR(VRAM_DMA_CHN_BASE) | 0x3;
    //while( (*(volatile unsigned int *)(vdma_base + 0xC) & 0x2) == 0 );
}
void elab_raw_end()
{
    while( (*(volatile unsigned int *)(gp0_base + 0xC) & 0x2) == 0 );
}
#else
void elab_raw(x264_t *h, int x, int y)
{
    if( (x + y * h->sps->i_mb_width) > h->sh.i_last_mb )
	return;

    int i;
    int i_stride = h->fdec->i_stride[0];
    int i_stride2 = i_stride << h->mb.b_interlaced;
    int i_pix_offset =
	h->mb.b_interlaced ? 16 * (x + (y & ~1) * i_stride) + (y & 1) * i_stride : 16 * (x + y * i_stride);

    uint8_t *src = (uint8_t *)(VRAM_ME_YRAWA + (load_index % 3) * 512 );
    uint8_t *p_fenc = &h->fenc->plane[0][i_pix_offset];
    for(i = 0; i < 16; i++) {
	write_reg( SRAM_VCADDR(src + i*16 + 0 ), *(unsigned int *)(&p_fenc[0]));
	write_reg( SRAM_VCADDR(src + i*16 + 4 ), *(unsigned int *)(&p_fenc[4]));
	write_reg( SRAM_VCADDR(src + i*16 + 8 ), *(unsigned int *)(&p_fenc[8]));
	write_reg( SRAM_VCADDR(src + i*16 + 12 ), *(unsigned int *)(&p_fenc[12]));
	p_fenc += i_stride2;
    }

    i_stride = h->fdec->i_stride[1];
    i_stride2 = i_stride << h->mb.b_interlaced;
    i_pix_offset =
	h->mb.b_interlaced ? 8 * (x + (y & ~1) * i_stride) + (y & 1) * i_stride : 8 * (x + y * i_stride);

    src = (uint8_t *)(VRAM_ME_CRAWA + (load_index % 3) * 512);
    p_fenc = &h->fenc->plane[1][i_pix_offset];
    for(i = 0; i < 8; i++) {
	write_reg( SRAM_VCADDR(src + i*16 + 0 ), *(unsigned int *)(&p_fenc[0]));
	write_reg( SRAM_VCADDR(src + i*16 + 4 ), *(unsigned int *)(&p_fenc[4]));
	p_fenc += i_stride2;
    }

    p_fenc = &h->fenc->plane[2][i_pix_offset];
    for(i = 0; i < 8; i++) {
	write_reg( SRAM_VCADDR(src + i*16 + 8 ), *(unsigned int *)(&p_fenc[0]));
	write_reg( SRAM_VCADDR(src + i*16 + 12 ), *(unsigned int *)(&p_fenc[4]));
	p_fenc += i_stride2;
    }
}
#endif

// mv_min and mv_max must be global.
int mv_min[2] = {0, 0};
int mv_max[2] = {0, 0};
void motion_execute(x264_t *h, int i_mb_x, int i_mb_y, int16_t *mvp)
{
    int *tdd = (int *)TCSM_VCADDR(VRAM_ME_CHN_BASE);

    SET_REG1_RAWA( (VRAM_ME_YRAWA + (load_index % 3) * 512) );

    int i_fmv_range = 4 * h->param.analyse.i_mv_range;
#define CLIP_FMV(mv) x264_clip3( mv, -i_fmv_range, i_fmv_range-1 )
    mv_min[0] = 4*( -16*i_mb_x - 24 );
    mv_max[0] = 4*( 16*( h->sps->i_mb_width - i_mb_x - 1 ) + 24 );
    mv_min[0] = CLIP_FMV( mv_min[0] );
    mv_max[0] = CLIP_FMV( mv_max[0] );
    mv_min[0] = (mv_min[0]>>2) + 6;
    mv_max[0] = (mv_max[0]>>2) - 6;
    if( i_mb_x == 0 ) {
	int mb_y = i_mb_y >> h->sh.b_mbaff;
	int mb_height = h->sps->i_mb_height >> h->sh.b_mbaff;

	mv_min[1] = 4*( -16*mb_y - 24 );
	mv_max[1] = 4*( 16*( mb_height - mb_y - 1 ) + 24 );
	mv_min[1] = x264_clip3( mv_min[1], -i_fmv_range, i_fmv_range );
	mv_max[1] = CLIP_FMV( mv_max[1] );
	mv_max[1] = X264_MIN( mv_max[1], i_fmv_range*4 );
	mv_min[1] = (mv_min[1]>>2) + 6;
	mv_max[1] = (mv_max[1]>>2) - 6;
    }
#undef CLIP_FMV

    int mv_x_min = X264_MAX(mv_min[0], -28);
    int mv_y_min = X264_MAX(mv_min[1], -28);
    int mv_x_max = X264_MIN(mv_max[0],  28);
    int mv_y_max = X264_MIN(mv_max[1],  28);

    if(i_mb_x == 0)
	mv_x_min = -12;
    if(i_mb_x == h->sps->i_mb_width-1) 
	mv_x_max = 12;
    if(i_mb_y == 0)
	mv_y_min = -12;
    if(i_mb_y == h->sps->i_mb_height-1) 
	mv_y_max = 12;

    int bmx = mvp[0] + 2;
    int bmy = mvp[1] + 2;
    bmx = x264_clip3( bmx, mv_x_min*4, mv_x_max*4 );
    bmy = x264_clip3( bmy, mv_y_min*4, mv_y_max*4 );

    //printf("[MOTION] bmy = %d, bmx = %d.\n", bmy, bmx);

    write_reg( (&tdd[0]) , TDD_CFG(1, 1, REG1_MVINFO) ); 
    write_reg( (&tdd[1]) , (bmx & 0xFFFF) | ((bmy & 0xFFFF)<<16) );
    
    write_reg( (&tdd[2]) , TDD_ESTI(1,/*vld*/
				    1,/*lk*/
				    0,/*dmy*/
				    1,/*pmc*/
				    0,/*list*/
				    0,/*boy*/
				    0,/*box*/
				    3,/*bh*/
				    3,/*bw*/
				    i_mb_y,/*mby*/
				    i_mb_x/*mbx*/) );

    write_reg( (&tdd[3]) , TDD_ESTI(1,/*vld*/
				    1,/*lk*/
				    1,/*dmy*/
				    1,/*pmc*/
				    0,/*list*/
				    0,/*boy*/
				    0,/*box*/
				    3,/*bh*/
				    3,/*bw*/
				    i_mb_y,/*mby*/
				    i_mb_x/*mbx*/) );
  
    write_reg( (&tdd[4]) , TDD_SYNC(1,/*vld*/
				    0,/*lk*/
				    0,/*crst*/
				    0xFFFF/*id*/) );

    //SET_REG1_DDC( VRAM_ME_CHN_BASE | 0x1 );
    //while(read_reg(TCSM_VCADDR(VRAM_ME_DSA),0) != (0x80000000 | 0xFFFF) );
}

// 720P used 6300 us. Avg mb is 1.75 us
void vmau_execute( x264_t *h, int i_mb_x, int i_mb_y )
{
    int *chnp = (int *)TCSM_VCADDR(VRAM_MAU_CHN_BASE);
    int main_cbp = 0;
    int y_pred_mode = 0;
    int c_pred_mode = 0;
  
    if( (i_mb_x + i_mb_y * h->sps->i_mb_width) > h->sh.i_last_mb )
	return;

    if( h->sh.i_type == SLICE_TYPE_I ) {
	main_cbp |= (MAU_Y_PREDE_MSK << MAU_Y_PREDE_SFT );//enable luma pred
	main_cbp |= (M16x16  << MAU_MTX_SFT );//enable luma 8x8

	if(i_mb_y && i_mb_x){
	    // I_PRED_16x16_V is defined to 0, which means I_PRED_16x16_DC in hardware
	    y_pred_mode = I_PRED_16x16_V;
	    c_pred_mode = I_PRED_CHROMA_DC;
	} else if(i_mb_y){
	    y_pred_mode = I_PRED_16x16_DC_TOP;
	    c_pred_mode = I_PRED_CHROMA_DC_TOP;
	} else if(i_mb_x) {
	    y_pred_mode = I_PRED_16x16_DC_LEFT;
	    c_pred_mode = I_PRED_CHROMA_DC_LEFT;
	} else {
	    y_pred_mode = I_PRED_16x16_DC_128;
	    c_pred_mode = I_PRED_CHROMA_DC_128;
	}
    } else {
	main_cbp |= (MAU_Y_ERR_MSK << MAU_Y_ERR_SFT );
	main_cbp |= (MAU_C_ERR_MSK << MAU_C_ERR_SFT );
    }

    int luma_qp = x264_ratecontrol_qp( h );
    int chroma_qp = h->chroma_qp_table[luma_qp];
    int quant_para = ( (((int)(luma_qp/6) & H264_YQP_DIV6_MSK) << H264_YQP_DIV6_SFT)
		       | (((int)(luma_qp%6) & H264_YQP_MOD6_MSK) << H264_YQP_MOD6_SFT)
		       | (((int)(chroma_qp/6) & H264_CQP_DIV6_MSK) << H264_CQP_DIV6_SFT)
		       | (((int)(chroma_qp%6) & H264_CQP_MOD6_MSK) << H264_CQP_MOD6_SFT)
		       | (((int)(chroma_qp/6) & H264_C1QP_DIV6_MSK) << H264_C1QP_DIV6_SFT)
		       | (((int)(chroma_qp%6) & H264_C1QP_MOD6_MSK) << H264_C1QP_MOD6_SFT) );


    write_reg( (&chnp[0]), main_cbp); //main_cbp 
    write_reg( (&chnp[1]), quant_para); //quant_para
    write_reg( (&chnp[2]), (VRAM_ME_YRAWA + ((load_index - 1) % 3) * 512)); //main_addr
    write_reg( (&chnp[3]), TCSM_PADDR(VRAM_MAU_CHN_BASE)); //ncchn_addr
    write_reg( (&chnp[4]), 24*16); //main_len, id, dec_incr

    if( load_index & 0x1 )
	write_reg( (&chnp[5]), VRAM_MAU_RESA); //aux cbp
    else
	write_reg( (&chnp[5]), VRAM_MAU_RESA1); //aux cbp
	
    write_reg( (&chnp[6]), c_pred_mode);
    write_reg( (&chnp[7]), y_pred_mode);

    SET_VMAU(VMAU_SLV_NCCHN_ADDR, TCSM_PADDR(VRAM_MAU_CHN_BASE));
    write_reg(TCSM_VCADDR(VRAM_MAU_DEC_SYNA), -1);
    write_reg(TCSM_VCADDR(VRAM_MAU_ENC_SYNA), -1);
    SET_VMAU(VMAU_SLV_GBL_RUN, VMAU_RUN);
}

void vmau_wait()
{
    while( (read_reg(TCSM_VCADDR(VRAM_MAU_DEC_SYNA), 0) == 0xFFFFFFFF) ||
    	   (read_reg(TCSM_VCADDR(VRAM_MAU_ENC_SYNA), 0) == 0xFFFFFFFF) );
}

int wait_mb_x, wait_mb_y;
void dblk_execute( x264_t *h, int i_mb_x, int i_mb_y ) {

    if( (i_mb_x + i_mb_y * h->sps->i_mb_width) > h->sh.i_last_mb )
	return;

    int slice_end = ( i_mb_x == (h->sps->i_mb_width - 1) && i_mb_y == (h->sps->i_mb_height - 1) );
    int top_valid = (!!i_mb_y)<<30;
    int left_valid = (!!i_mb_x)<<31;
    write_reg(TCSM_VCADDR(VRAM_DBLK_CHN_BASE), (slice_end<<31 | H264_MB_ID_MSK<<H264_MB_ID_SFT |
						(TCSM_PADDR(VRAM_DBLK_CHN_BASE) & H264_MB_DHA_MSK) ) );

    if(!h->sh.i_disable_deblocking_filter_idc){
	int mb_xy = i_mb_y*h->mb.i_mb_stride + i_mb_x;
	int top_xy = (i_mb_y-1)*h->mb.i_mb_stride + i_mb_x;
    
	int cur_qp = h->mb.qp[mb_xy];
	int cur_uqp = h->chroma_qp_table[cur_qp];
	int cur_vqp = h->chroma_qp_table[cur_qp];

	int top_qp = h->mb.qp[top_xy];
	int top_uqp = h->chroma_qp_table[top_qp];
	int top_vqp = h->chroma_qp_table[top_qp];  

	int main_cbp = ( (!!h->mb.non_zero_count[mb_xy][0]) |
			 (!!h->mb.non_zero_count[mb_xy][1])<<1 |
			 (!!h->mb.non_zero_count[mb_xy][4])<<2 |
			 (!!h->mb.non_zero_count[mb_xy][5])<<3 |
			 
			 (!!h->mb.non_zero_count[mb_xy][2])<<4 |
			 (!!h->mb.non_zero_count[mb_xy][3])<<5 |
			 (!!h->mb.non_zero_count[mb_xy][6])<<6 |
			 (!!h->mb.non_zero_count[mb_xy][7])<<7 |
			 
			 (!!h->mb.non_zero_count[mb_xy][8])<<8 |
			 (!!h->mb.non_zero_count[mb_xy][9])<<9 |
			 (!!h->mb.non_zero_count[mb_xy][12])<<10 |
			 (!!h->mb.non_zero_count[mb_xy][13])<<11 |
		     
			 (!!h->mb.non_zero_count[mb_xy][10])<<12 |
			 (!!h->mb.non_zero_count[mb_xy][11])<<13 |
			 (!!h->mb.non_zero_count[mb_xy][14])<<14 |
			 (!!h->mb.non_zero_count[mb_xy][15])<<15 );

	write_reg(TCSM_VCADDR(VRAM_DBLK_CHN_BASE+8), /*qp0*/
		  ( (cur_qp & H264_QP_MSK )<<H264_YQP_CUR_SFT |
		    (cur_uqp & H264_QP_MSK)<<H264_UQP_CUR_SFT |
		    (top_qp & H264_QP_MSK )<<H264_YQP_UP_SFT  |
		    (top_uqp & H264_QP_MSK)<<H264_VQP_UP_SFT  |
		    (top_vqp & H264_QP_MSK)<<H264_UQP_UP_SFT) );

	write_reg(TCSM_VCADDR(VRAM_DBLK_CHN_BASE+12), /*nnz*/
		  ( (main_cbp & H264_NNZ_MSK) |
		    (cur_vqp & H264_QP_MSK) << H264_VQP_CUR_SFT |
		    ( ( (!!h->mb.non_zero_count[top_xy][12]) | 
			(!!h->mb.non_zero_count[top_xy][13])<<1 | 
			(!!h->mb.non_zero_count[top_xy][14])<<2 | 
			(!!h->mb.non_zero_count[top_xy][15])<<3 ) )<< H264_TNNZ_SFT) );
  
	if(h->sh.i_type == SLICE_TYPE_I){ /*FIXME*/
	    write_reg(TCSM_VCADDR(VRAM_DBLK_CHN_BASE+4), /*curr_mb_type top_mb_type*/ 
		      (left_valid | top_valid | H264_16x16<<16/*curr*/ | H264_16x16/*top*/) );
	} else {
	    const int s4x4 = 4 * h->mb.i_mb_stride;
	    const int mb_4x4 = 4 * s4x4 * i_mb_y + 4 * i_mb_x;
	    int mb_4x4_top = mb_4x4 - 4 * s4x4;

	    write_reg(TCSM_VCADDR(VRAM_DBLK_CHN_BASE+4), /*curr_mb_type top_mb_type*/ 
		      (left_valid | top_valid | (H264_INTER_MB | H264_16x16)<<16/*curr*/ | 
		       H264_INTER_MB | I_16x16/*top*/) );
	    write_reg(TCSM_VCADDR(VRAM_DBLK_CHN_BASE+16), /*mv_para_addr*/
		      (1<<26/*mv_len*/ | (TCSM_PADDR(VRAM_DBLK_CHN_BASE+20) & 0xFFFFF)>>2) );
	    write_reg(TCSM_VCADDR(VRAM_DBLK_CHN_BASE+20), /*cache_ref0*/0);
	    write_reg(TCSM_VCADDR(VRAM_DBLK_CHN_BASE+24), /*cache_ref1*/0);
	    write_reg(TCSM_VCADDR(VRAM_DBLK_CHN_BASE+28), /*top_mv0*/
		      (h->mb.mv[0][mb_4x4_top][1]<<16 | (h->mb.mv[0][mb_4x4_top][0] & 0xFFFF) ) );
	    write_reg(TCSM_VCADDR(VRAM_DBLK_CHN_BASE+32), /*top_mv0*/
		      (h->mb.mv[0][mb_4x4_top][1]<<16 | (h->mb.mv[0][mb_4x4_top][0] & 0xFFFF) ) );
	    write_reg(TCSM_VCADDR(VRAM_DBLK_CHN_BASE+36), /*top_mv0*/
		      (h->mb.mv[0][mb_4x4_top][1]<<16 | (h->mb.mv[0][mb_4x4_top][0] & 0xFFFF) ) );
	    write_reg(TCSM_VCADDR(VRAM_DBLK_CHN_BASE+40), /*top_mv0*/
		      (h->mb.mv[0][mb_4x4_top][1]<<16 | (h->mb.mv[0][mb_4x4_top][0] & 0xFFFF) ) );
	    //write_reg(VRAM_DBLK_CHN_BASE+44/48/52/56, top_mv1);
	    write_reg(TCSM_VCADDR(VRAM_DBLK_CHN_BASE+60), /*curr_mv*/
		      (h->mb.mv[0][mb_4x4][1]<<16 | (h->mb.mv[0][mb_4x4][0] & 0xFFFF) ) );
	    LOGE("curr MV: %4x, %4x\n", h->mb.mv[0][mb_4x4][1], h->mb.mv[0][mb_4x4][0]);
	}

	LOGE("HW %x \n ", read_reg(TCSM_VCADDR(VRAM_DBLK_CHN_BASE+4), 0));
	LOGE("top QPy:%d, QPu:%d, QPv:%d \n ", top_qp, top_uqp, top_vqp );
	LOGE("    QPy:%d, QPu:%d, QPv:%d \n ", cur_qp, cur_uqp, cur_vqp );
	LOGE("qp0 :%x , main_cbp :%x, nnz:%x \n ", read_reg(TCSM_VCADDR(VRAM_DBLK_CHN_BASE+8), 0),
	     main_cbp, read_reg(TCSM_VCADDR(VRAM_DBLK_CHN_BASE+12), 0) );

    }

    SET_DBLK(DEBLK_REG_TRIG, DBLK_RUN);
    wait_mb_x = i_mb_x;
    wait_mb_y = i_mb_y;
}

void dblk_wait( x264_t *h ) {
    if(!wait_mb_x){
	if(wait_mb_y)
	    while(read_reg(TCSM_VCADDR(VRAM_DBLK_DOUT_SYNA), 0) != 
		  ((wait_mb_y-1)<<16 | (h->sps->i_mb_width-1)) );
    } else
	while(read_reg(TCSM_VCADDR(VRAM_DBLK_DOUT_SYNA), 0) != (wait_mb_y<<16 | (wait_mb_x-1)) );
}
