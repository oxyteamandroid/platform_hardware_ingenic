#ifndef __X264_DCORE_H__
#define __X264_DCORE_H__

typedef signed char int8_t;
typedef short int16_t;

typedef struct frameinfo {
    int frame_type;
    int i_first_mb;
    int i_last_mb;
    int i_mb_x; // start x
    int i_mb_y; // start y
    int i_mb_stride;
    int i_mb_width;
    int i_mb_height;
    int i_mv_range;
    int b_interlaced;
    int luma_qp;
    int chroma_qp;
    int enable_dblk;

    unsigned int tile_enc_y;
    unsigned int tile_enc_c;
    unsigned int mau_rst; // OUTPUT DST
} frameinfo_t;

#endif
