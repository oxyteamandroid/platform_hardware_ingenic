#ifndef __T_VPU_H__
#define __T_VPU_H__

#include "jzasm.h"

extern volatile unsigned char *vpu_base;
#define VPU_V_BASE          vpu_base
#define VPU_P_BASE          0x13200000

#define		CFGC_HID_CFGC		0x0 //0x132
#define		CFGC_HID_GP0		0x1
#define		CFGC_HID_GP1		0x2
#define		CFGC_HID_GP2		0x3
#define		CFGC_HID_MCE		0x5
#define		CFGC_HID_DBLK		0x7
#define		CFGC_HID_VMAU		0x8
#define		CFGC_HID_SDE		0x9
#define		CFGC_HID_AUX		0xA
#define		CFGC_HID_TCSM0		0xB
#define		CFGC_HID_TCSM1		0xC
#define		CFGC_HID_DBLK2		0xD

#define write_reg(reg, val) \
({ i_sw(((unsigned int)(val)), ((unsigned int)(reg)), 0); \
})

#define read_reg(reg, off) \
({ i_lw(((unsigned int)(reg)+(unsigned int)(off)), (0));  \
})

#define VPU_GLBC            0x0
#define GLBC_TLBMD_SFT      30
#define GLBC_TLBMD_MSK      0x3
#define GLBC_TLBIV_SFT      29
#define GLBC_TLBIV_MSK      0x1
#define GLBC_ENGM_SFT       10
#define GLBC_ENGM_MSK       0x3
#define GLBC_EPRI_SFT       8
#define GLBC_EPRI_MSK       0x3
#define GLBC_DPRI_SFT       4
#define GLBC_DPRI_MSK       0x7
#define GLBC_CPRI_SFT       0
#define GLBC_CPRI_MSK       0x7

#define GLBC_INTE_ENDF      (0x1<<16)
#define GLBC_INTE_BSERR     (0x1<<17)
#define GLBC_INTE_TLBERR    (0x1<<18)
#define GLBC_INTE_TIMEOUT   (0x1<<19)
#define GLBC_INTE_ACFGERR   (0x1<<20)

#define SET_VPU_GLBC(tlbmd, inv, engm, epri, dpri, cpri) \
  ({ write_reg((VPU_V_BASE + VPU_GLBC) , 	         \
      ((tlbmd) & GLBC_TLBMD_MSK)<<GLBC_TLBMD_SFT |	 \
      ((inv) & GLBC_TLBIV_MSK)<<GLBC_TLBIV_SFT |	 \
      ((engm) & GLBC_ENGM_MSK)<<GLBC_ENGM_SFT |		 \
      ((epri) & GLBC_EPRI_MSK)<<GLBC_EPRI_SFT |		 \
      ((dpri) & GLBC_DPRI_MSK)<<GLBC_DPRI_SFT |		 \
      ((cpri) & GLBC_CPRI_MSK)<<GLBC_CPRI_SFT );         \
  })
#define GET_VPU_GLBC()      (read_reg(VPU_V_BASE , VPU_GLBC))

#define VPU_TLBA            0x30
#define SET_VPU_TLBA(tlba)                               \
  ({ write_reg((VPU_V_BASE + VPU_TLBA) , (tlba));        \
  })
#define GET_VPU_TLBA()      (read_reg(VPU_V_BASE , VPU_TLBA))

#define VPU_STAT            0x34
#define CLR_VPU_STAT()      (write_reg((VPU_V_BASE+VPU_STAT), 0))
#define GET_VPU_STAT()      (read_reg(VPU_V_BASE , VPU_STAT))

#define VPU_STAT_ENDF       (0x1<<0)
#define VPU_STAT_SLDHIT     (0x1<<31)

#define VPU_TETY0           0x40
#define VPU_TETY1           0x44
#define VPU_TETY2           0x48
#define VPU_TETY3           0x4C

#define VPU_TLB_SLD_VLD     (0x1<<0)
#define VPU_TLB_SLD_VA(a)   (((int)a) & 0xFFC00000)
#define VPU_TLB_SLD_PA(a)   ((((int)a)>>10) & 0x003FF000)
#define SET_VPU_SLD(i, v)   write_reg((VPU_V_BASE+VPU_TETY0+i*4), v)
#define GET_VPU_SLD(i)      read_reg(VPU_V_BASE, VPU_TETY0+i*4)

#define VPU_SCHC            0x60
#define SET_VPU_SCHC(sch4_act,sch4_gs,sch4_pe,sch4_pch,  \
                     sch3_act,sch3_gs,sch3_pe,sch3_pch,  \
                     sch2_act,sch2_gs,sch2_pe,sch2_pch,  \
                     sch1_act,sch1_gs,sch1_pe,sch1_pch)  \
  ({ write_reg((VPU_V_BASE+VPU_SCHC) ,                   \
      ((sch4_act) & 0x1)<<31 |	                         \
      ((sch4_gs ) & 0x1)<<27 |	                         \
      ((sch4_pe ) & 0x1)<<26 |	                         \
      ((sch4_pch) & 0x3)<<24 | 	                         \
      ((sch3_act) & 0x1)<<23 |	                         \
      ((sch3_gs ) & 0x1)<<19 |	                         \
      ((sch3_pe ) & 0x1)<<18 |	                         \
      ((sch3_pch) & 0x3)<<16 |	                         \
      ((sch2_act) & 0x1)<<15 |	                         \
      ((sch2_gs ) & 0x1)<<11 |	                         \
      ((sch2_pe ) & 0x1)<<10 |	                         \
      ((sch2_pch) & 0x3)<<8  |	                         \
      ((sch1_act) & 0x1)<<7  |	                         \
      ((sch1_gs ) & 0x1)<<3  |	                         \
      ((sch1_pe ) & 0x1)<<2  |	                         \
      ((sch1_pch) & 0x3)<<0  );	                         \
  })
#define GET_VPU_SCHC() (read_reg(VPU_V_BASE , VPU_SCHC))

#define VPU_SCHBND          0x64   /*SCH bounding*/
#define SCH_CS_MSK          0xF
#define SCH_DEPTH_SFT       8
#define SCH_DEPTH_MSK       0xF
#define SCH_FE_MSK          0x1
#define SCHG1_FE4_SFT       7
#define SCHG1_FE3_SFT       6
#define SCHG1_FE2_SFT       5
#define SCHG1_FE1_SFT       4
#define SCHG0_FE4_SFT       3
#define SCHG0_FE3_SFT       2
#define SCHG0_FE2_SFT       1
#define SCHG0_FE1_SFT       0
#define SET_VPU_SCHBND(cs4, cs3, cs2, cs1, depth,        \
                       g1fe4, g1fe3, g1fe2, g1fe1,       \
                       g0fe4, g0fe3, g0fe2, g0fe1)       \
  ({ write_reg((VPU_V_BASE+VPU_SCHBND) ,                 \
      ((cs4  ) & SCH_CS_MSK   )<<28  |                   \
      ((cs3  ) & SCH_CS_MSK   )<<24  |                   \
      ((cs2  ) & SCH_CS_MSK   )<<20  |                   \
      ((cs1  ) & SCH_CS_MSK   )<<16  |                   \
      ((depth) & SCH_DEPTH_MSK)<<SCH_DEPTH_SFT |         \
      ((g1fe4) & SCH_FE_MSK   )<<SCHG1_FE4_SFT |         \
      ((g1fe3) & SCH_FE_MSK   )<<SCHG1_FE3_SFT |         \
      ((g1fe2) & SCH_FE_MSK   )<<SCHG1_FE2_SFT |         \
      ((g1fe1) & SCH_FE_MSK   )<<SCHG1_FE1_SFT |         \
      ((g0fe4) & SCH_FE_MSK   )<<SCHG0_FE4_SFT |         \
      ((g0fe3) & SCH_FE_MSK   )<<SCHG0_FE3_SFT |         \
      ((g0fe2) & SCH_FE_MSK   )<<SCHG0_FE2_SFT |         \
      ((g0fe1) & SCH_FE_MSK   )<<SCHG0_FE1_SFT );        \
  })
#define GET_VPU_SCHBND()     (read_reg(VPU_V_BASE , VPU_SCHBND))

#define VPU_SCHG0           0x68
#define VPU_SCHG1           0x6C
#define VPU_SCHE1           0x70
#define VPU_SCHE2           0x74
#define VPU_SCHE3           0x78
#define VPU_SCHE4           0x7C
#define SCHE_SN_SFT         4
#define SCHE_SN_MSK         0x1
#define SCHE_ID_SFT         0
#define SCHE_ID_MSK         0xF
#define SET_VPU_SCHG0(sche_sn, sche_id)	                 \
  ({ write_reg((VPU_V_BASE+VPU_SCHG0) ,                  \
      ((sche_sn ) & SCHE_SN_MSK )<<SCHE_SN_SFT  |        \
      ((sche_id ) & SCHE_ID_MSK )<<SCHE_ID_SFT  );       \
  })
#define SET_VPU_SCHG1(sche_sn, sche_id)	                 \
  ({ write_reg((VPU_V_BASE+VPU_SCHG1) ,                  \
      ((sche_sn ) & SCHE_SN_MSK )<<SCHE_SN_SFT  |        \
      ((sche_id ) & SCHE_ID_MSK )<<SCHE_ID_SFT  );       \
  })
#define SET_VPU_SCHE1(sche_sn, sche_id)	                 \
  ({ write_reg((VPU_V_BASE+VPU_SCHE1) ,                  \
      ((sche_sn ) & SCHE_SN_MSK )<<SCHE_SN_SFT  |        \
      ((sche_id ) & SCHE_ID_MSK )<<SCHE_ID_SFT  );       \
  })
#define SET_VPU_SCHE2(sche_sn, sche_id)	                 \
  ({ write_reg((VPU_V_BASE+VPU_SCHE2) ,                  \
      ((sche_sn ) & SCHE_SN_MSK )<<SCHE_SN_SFT  |        \
      ((sche_id ) & SCHE_ID_MSK )<<SCHE_ID_SFT  );       \
  })
#define SET_VPU_SCHE3(sche_sn, sche_id)	                 \
  ({ write_reg((VPU_V_BASE+VPU_SCHE3) ,                  \
      ((sche_sn ) & SCHE_SN_MSK )<<SCHE_SN_SFT  |        \
      ((sche_id ) & SCHE_ID_MSK )<<SCHE_ID_SFT  );       \
  })
#define SET_VPU_SCHE4(sche_sn, sche_id)	                 \
  ({ write_reg((VPU_V_BASE+VPU_SCHE4) ,                  \
      ((sche_sn ) & SCHE_SN_MSK )<<SCHE_SN_SFT  |        \
      ((sche_id ) & SCHE_ID_MSK )<<SCHE_ID_SFT  );       \
  })

#define GET_VPU_SCHG0() (read_reg(VPU_V_BASE , VPU_SCHG0))
#define GET_VPU_SCHG1() (read_reg(VPU_V_BASE , VPU_SCHG1))
#define GET_VPU_SCHE1() (read_reg(VPU_V_BASE , VPU_SCHE1))
#define GET_VPU_SCHE2() (read_reg(VPU_V_BASE , VPU_SCHE2))
#define GET_VPU_SCHE3() (read_reg(VPU_V_BASE , VPU_SCHE3))
#define GET_VPU_SCHE4() (read_reg(VPU_V_BASE , VPU_SCHE4))

#define SCH1_DSA  0x13200070
#define SCH2_DSA  0x13200074
#define SCH3_DSA  0x13200078
#define SCH4_DSA  0x1320007C

#define VPU_VCB_ID_AUX       0x1
#define VPU_VCB_ID_MCE       0x2
#define VPU_VCB_ID_DBLK      0x3
#define VPU_VCB_ID_SDE       0x4
#define VPU_VCB_ID_VMAU      0x5
#define VPU_VCB_ID_EFE       0x6
#define VPU_VCB_ID_GP1       0x7
#define VPU_VCB_ID_JPGC      0x8
#define VPU_VCB_ID_VDMA      0x9
#define VPU_VCB_ID_CPU       0xA

#define VPU_VDB_ID_AUX       0x1
#define VPU_VDB_ID_MCE       0x2
#define VPU_VDB_ID_DBLK      0x3
#define VPU_VDB_ID_SDE       0x4
#define VPU_VDB_ID_VMAU      0x5
#define VPU_VDB_ID_EFE       0x6
#define VPU_VDB_ID_GP1       0x7
#define VPU_VDB_ID_JPGC      0x8
#define VPU_VDB_ID_VDMA      0x9
#define VPU_VDB_ID_CPU       0xA

#define VPU_EXT_ID_MCE       0x1
#define VPU_EXT_ID_SDE       0x2
#define VPU_EXT_ID_DBLK      0x3
#define VPU_EXT_ID_GP1       0x4
#define VPU_EXT_ID_EFE       0x5
#define VPU_EXT_ID_JPGC      0x6
#define VPU_EXT_ID_VDMA      0x7
#define VPU_EXT_ID_AUX       0x8

#define VPU_MONC             0x4
#define VPU_MONC_EN          (0x1<<31)
#define VPU_MONC_TRAN        (0x0<<30)
#define VPU_MONC_NRDY        (0x1<<30)
#define VPU_MONC_ID(a)       (((a) & 0xF)<<24)
#define VPU_MONC_SCORE()     (read_reg(VPU_V_BASE, VPU_MONC) & 0xFFFFFF)

#define VPU_DBGC             0x8
#define VPU_DBGC_EHIT        (0x1<<31)
#define VPU_DBGC_DHIT        (0x1<<30)
#define VPU_DBGC_CHIT        (0x1<<29)
#define VPU_DBGC_IRQE        (0x1<<28)
#define VPU_DBGC_EID(a)      (((a) & 0xF)<<20)
#define VPU_DBGC_EWM         (0x1<<19)
#define VPU_DBGC_EWO         (0x1<<18)
#define VPU_DBGC_EWD         (0x1<<17)
#define VPU_DBGC_EWA         (0x1<<16)
#define VPU_DBGC_DID(a)      (((a) & 0xF)<<12)
#define VPU_DBGC_DWM         (0x1<<11)
#define VPU_DBGC_DWO         (0x1<<10)
#define VPU_DBGC_DWD         (0x1<<9)
#define VPU_DBGC_DWA         (0x1<<8)
#define VPU_DBGC_CID(a)      (((a) & 0xF)<<4)
#define VPU_DBGC_CWM         (0x1<<3)
#define VPU_DBGC_CWO         (0x1<<2)
#define VPU_DBGC_CWD         (0x1<<1)
#define VPU_DBGC_CWA         (0x1<<0)

#define VPU_EWD              0xC
#define VPU_EWA              0x10
#define VPU_DWD              0x14
#define VPU_DWA              0x18
#define VPU_CWD              0x1C
#define VPU_CWA              0x20

#define MSCOPE_START(mbnum)  write_reg(VPU_V_BASE+0x24, mbnum)
#define MSCOPE_STOP()        write_reg(VPU_V_BASE+0x28, 0)

#endif /*__T_VPU_H__*/
