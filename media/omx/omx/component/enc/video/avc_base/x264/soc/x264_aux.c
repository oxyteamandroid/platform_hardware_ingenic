#include "x264_dcore.h"
#include "x264_vram.h"
#include "jzasm.h"

#include "x264_func.c"

#define __p1_text __attribute__ ((__section__ (".p1_text")))
#define __p1_main __attribute__ ((__section__ (".p1_main")))
#define __p1_data __attribute__ ((__section__ (".p1_data")))

__p1_main int main() {

    frameinfo_t *h = (frameinfo_t *)VRAM_FRAMEINFO;
    int16_t (*mv_top)[2] = VRAM_ME_TOP_MV;
    int16_t *mv_left = (int16_t *)VRAM_ME_LEFT_MV;
    int mb_x = h->i_mb_x;
    int mb_y = h->i_mb_y;
    int i_mb_x = h->i_mb_x;
    int i_mb_y = h->i_mb_y;
    int mb_xy = 0;
    volatile int *fifo_read = (int *)VRAM_READ_SYNA;
    volatile int *fifo_write = (int *)VRAM_WRITE_SYNA;
    unsigned int fifo_addr = h->mau_rst;
    //unsigned int fifo_end = h->mau_rst + 0x400 * 100;

    int *debug = (int *)VRAM_DEBUG_SPACE;
    debug[0] = debug[1] = debug[2] = debug[3] = debug[4] = debug[5] =
	debug[6] = debug[7] = debug[8] = debug[9] = -1;

    load_index = 0;
    elab_raw(mb_x, mb_y);
    if( h->frame_type != 2 /* SLICE_TYPE_I */ )
	x264_do_mc(mb_x, mb_y);

    load_index++;
    if( h->b_interlaced ) {
	mb_x += mb_y & 1;
	mb_y ^= mb_x < h->i_mb_width;
    } else
	mb_x++;
    if( mb_x == h->i_mb_width ) {
	mb_y++;
	mb_x = 0;
    }
    elab_raw(mb_x, mb_y);
    if( h->frame_type != 2 /* SLICE_TYPE_I */ ) {
	x264_wait_mc();
	x264_do_mc(mb_x, mb_y);
	if( mb_x ) {
	    mv_top[mb_x - 1][0] = mv_left[0];
	    mv_top[mb_x - 1][1] = mv_left[1];
	} else if( mb_y ) {
	    mv_top[h->i_mb_width - 1][0] = mv_left[0];
	    mv_top[h->i_mb_width - 1][1] = mv_left[1];
	}
    }
    vmau_execute(i_mb_x, i_mb_y);

    while( (mb_xy = i_mb_x + i_mb_y * h->i_mb_width) <= h->i_last_mb ) {

	load_index++;
	debug[0] = load_index;
	if( h->b_interlaced ) {
	    mb_x += mb_y & 1;
	    mb_y ^= mb_x < h->i_mb_width;
	} else
	    mb_x++;
	if( mb_x == h->i_mb_width ) {
	    mb_y++;
	    mb_x = 0;
	}

	debug[1] = load_index;
	elab_raw(mb_x, mb_y);
	debug[2] = load_index;
	if( h->frame_type != 2 /* SLICE_TYPE_I */ ) {
	    x264_wait_mc();
	    x264_do_mc(mb_x, mb_y);
	    if( mb_x ) {
		mv_top[mb_x - 1][0] = mv_left[0];
		mv_top[mb_x - 1][1] = mv_left[1];
	    } else if( mb_y ) {
		mv_top[h->i_mb_width - 1][0] = mv_left[0];
		mv_top[h->i_mb_width - 1][1] = mv_left[1];
	    }
	}

	vmau_wait();
	debug[3] = load_index;

	volatile unsigned int *chain = (volatile unsigned int *)VRAM_DMA_CHN1_BASE;
	chain[0] = (load_index & 0x1) ? VRAM_MAU_RESA1 : VRAM_MAU_RESA;
	chain[1] = (fifo_addr) | 0x1;
	chain[3] = 207; // (16 + 4 + 4) * 8 + 8 + 2 + 2 + 2  // (Y+U+V)*8 + YDC + UDC + VDC + CBP
	write_reg(0x1321000C, 0);

	while( (fifo_write[0] >= fifo_read[0] + 99) );
	debug[4] = load_index;

	write_reg(0x13210008, TCSM_PADDR(VRAM_DMA_CHN1_BASE) | 0x3); // run

	if( load_index > 2 )
	    dblk_wait();
	dblk_execute(i_mb_x, i_mb_y);

	// Start the next MB VMAU
	if( h->b_interlaced ) {
	    i_mb_x += i_mb_y & 1;
	    i_mb_y ^= i_mb_x < h->i_mb_width;
	} else
	    i_mb_x++;
	if( i_mb_x == h->i_mb_width ) {
	    i_mb_y++;
	    i_mb_x = 0;
	}

	vmau_execute(i_mb_x, i_mb_y);

	while( (read_reg(0x1321000C, 0) & 0x2) == 0 );
	*fifo_write = *fifo_write + 1;
	debug[5] = load_index;

	fifo_addr += 0x400;
	/* if( fifo_addr >= fifo_end ) */
	/*     fifo_addr = h->mau_rst; */
    }

    while( !(read_reg(0x13270070, 0) & 0x1) ); // wait slice end

    debug[6] = h->i_last_mb;
    debug[7] = i_mb_y;
    debug[8] = i_mb_x;
    debug[9] = h->i_mb_width;

    i_nop;
    i_nop;
    i_nop;
    i_nop;
    i_wait();

    return 0;
}
