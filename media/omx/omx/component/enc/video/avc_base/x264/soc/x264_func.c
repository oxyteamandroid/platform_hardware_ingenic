#include "t_motion.h"
#include "t_vmau.h"
#include "t_dblk.h"

int load_index = 0;

// VDMA
void elab_raw(int x, int y)
{
    frameinfo_t *h = (frameinfo_t *)VRAM_FRAMEINFO;
    int mb_xy = x + y * h->i_mb_width;
    if( mb_xy > h->i_last_mb )
	return;

    unsigned int src = (unsigned int)(VRAM_ME_YRAWA + (load_index % 3) * 512 );
    volatile unsigned int *chain = (volatile unsigned int *)VRAM_DMA_CHN_BASE;
    chain[0] = (h->tile_enc_y + mb_xy * 256) | 0x1;
    chain[1] = src;
    chain[3] = (0x1 << 31) | 64;
    chain[4] = (h->tile_enc_c + mb_xy * 128) | 0x1;
    chain[5] = src + 256;
    chain[7] = 32;
    write_reg(0x1321000C, 0);
    write_reg(0x13210008, TCSM_PADDR(VRAM_DMA_CHN_BASE) | 0x3);
    while( (read_reg(0x1321000C, 0) & 0x2) == 0 );
}
// MOTION
static inline int x264_median( int a, int b, int c )
{
    int t = (a-b)&((a-b)>>31);
    a -= t;
    b += t;
    b -= (b-c)&((b-c)>>31);
    b += (a-b)&((a-b)>>31);
    return b;
}

static inline void x264_median_mv( int16_t *dst, int16_t *a, int16_t *b, int16_t *c )
{
    dst[0] = x264_median( a[0], b[0], c[0] );
    dst[1] = x264_median( a[1], b[1], c[1] );
}

static void x264_analyse_mvp( frameinfo_t * h, int i_mb_x, int i_mb_y, int16_t mvp[2] )
{
    int i_mb_xy = i_mb_y * h->i_mb_stride + i_mb_x;
    int i_top_y = i_mb_y - 1;
    int i_top_xy = i_top_y * h->i_mb_stride + i_mb_x;
    int i_refa, i_refb, i_refc;
    int16_t mv_a[2], mv_b[2], mv_c[2];
    int16_t *mv_left = (int16_t *)VRAM_ME_LEFT_MV;
    int16_t (*mv_top)[2] = VRAM_ME_TOP_MV;

    if( i_mb_x > 0 && i_mb_xy > h->i_first_mb ) { // LEFT
	i_refa  = 0;
	mv_a[0] = mv_left[0];
	mv_a[1] = mv_left[1];
    } else {
	i_refa  = -2;
	mv_a[0] = mv_a[1] = 0;
    }

    if( i_top_xy >= h->i_first_mb ) {  // TOP
	i_refb  = 0;
	mv_b[0] = mv_top[i_mb_x][0];
	mv_b[1] = mv_top[i_mb_x][1];
    } else {
	i_refb  = -2;
	mv_b[0] = mv_b[1] = 0;
    }
    if( i_mb_x < h->i_mb_width - 1 && i_top_xy + 1 >= h->i_first_mb ) { // TOPRIGHT
	i_refc  = 0;
	mv_c[0] = mv_top[i_mb_x + 1][0];
	mv_c[1] = mv_top[i_mb_x + 1][1];
    } else if( i_mb_x > 0 && i_top_xy - 1 >= h->i_first_mb ) { // TOPLEFT
	i_refc  = 0;
	mv_c[0] = mv_top[i_mb_x - 1][0];
	mv_c[1] = mv_top[i_mb_x - 1][1];
    } else {
	i_refb  = -2;
	mv_c[0] = mv_c[1] = 0;
    }

    int i_count = 0;

    if( i_refa == 0 ) i_count++;
    if( i_refb == 0 ) i_count++;
    if( i_refc == 0 ) i_count++;

    if( i_count > 1 )
    {
median:
        x264_median_mv( mvp, mv_a, mv_b, mv_c );
    }
    else if( i_count == 1 )
    {
        if( i_refa == 0 ) {
	    mvp[0] = mv_a[0];
	    mvp[1] = mv_a[1];
	} else if( i_refb == 0 ) {
	    mvp[0] = mv_b[0];
	    mvp[1] = mv_b[1];
	} else {
	    mvp[0] = mv_c[0];
	    mvp[1] = mv_c[1];
	}
    }
    else if( i_refb == -2 && i_refc == -2 && i_refa != -2 ) {
	mvp[0] = mv_a[0];
	mvp[1] = mv_a[1];
    } else
        goto median;
}

#define X264_MIN(a,b) ( (a)<(b) ? (a) : (b) )
#define X264_MAX(a,b) ( (a)>(b) ? (a) : (b) )

static inline int x264_clip3( int v, int i_min, int i_max )
{
    return ( (v < i_min) ? i_min : (v > i_max) ? i_max : v );
}

#define CLIP_FMV(mv) x264_clip3( mv, -i_fmv_range, i_fmv_range-1 )

// mv_min and mv_max must be global.
int mv_min[2] = {0, 0};
int mv_max[2] = {0, 0};
void motion_execute(frameinfo_t *h, int i_mb_x, int i_mb_y, int16_t *mvp)
{
    int *tdd = (int *)VRAM_ME_CHN_BASE;

    write_reg(0x13250010, (VRAM_ME_YRAWA + (load_index % 3) * 512) );

    int i_fmv_range = 4 * h->i_mv_range;
#define CLIP_FMV(mv) x264_clip3( mv, -i_fmv_range, i_fmv_range-1 )
    mv_min[0] = 4 * ( -16 * i_mb_x - 24 );
    mv_max[0] = 4 * ( 16 * ( h->i_mb_width - i_mb_x - 1 ) + 24 );
    mv_min[0] = CLIP_FMV( mv_min[0] );
    mv_max[0] = CLIP_FMV( mv_max[0] );
    mv_min[0] = (mv_min[0]>>2) + 6;
    mv_max[0] = (mv_max[0]>>2) - 6;
    if( i_mb_x == 0 ) {
	int mb_y = i_mb_y >> h->b_interlaced;
	int mb_height = h->i_mb_height >> h->b_interlaced;

	mv_min[1] = 4 * ( -16 * mb_y - 24 );
	mv_max[1] = 4 * ( 16 * ( mb_height - mb_y - 1 ) + 24 );
	mv_min[1] = x264_clip3(mv_min[1], -i_fmv_range, i_fmv_range);
	mv_max[1] = CLIP_FMV( mv_max[1] );
	mv_max[1] = X264_MIN( mv_max[1], i_fmv_range*4 );
	mv_min[1] = (mv_min[1]>>2) + 6;
	mv_max[1] = (mv_max[1]>>2) - 6;
    }
#undef CLIP_FMV

    int mv_x_min = X264_MAX(mv_min[0], -28);
    int mv_y_min = X264_MAX(mv_min[1], -28);
    int mv_x_max = X264_MIN(mv_max[0],  28);
    int mv_y_max = X264_MIN(mv_max[1],  28);

    if(i_mb_x == 0)
	mv_x_min = -12;
    if(i_mb_x == h->i_mb_width-1)
	mv_x_max = 12;
    if(i_mb_y == 0)
	mv_y_min = -12;
    if(i_mb_y == h->i_mb_height-1)
	mv_y_max = 12;

    int bmx = mvp[0] + 2;
    int bmy = mvp[1] + 2;
    bmx = x264_clip3( bmx, mv_x_min*4, mv_x_max*4 );
    bmy = x264_clip3( bmy, mv_y_min*4, mv_y_max*4 );

    tdd[0] = TDD_CFG(1, 1, 0x14); // REG1_MVINFO
    tdd[1] = (bmx & 0xFFFF) | ((bmy & 0xFFFF)<<16);

    tdd[2] = TDD_ESTI(1,/*vld*/
		      1,/*lk*/
		      0,/*dmy*/
		      1,/*pmc*/
		      0,/*list*/
		      0,/*boy*/
		      0,/*box*/
		      3,/*bh*/
		      3,/*bw*/
		      i_mb_y,/*mby*/
		      i_mb_x/*mbx*/);

    tdd[3] = TDD_ESTI(1,/*vld*/
		      1,/*lk*/
		      1,/*dmy*/
		      1,/*pmc*/
		      0,/*list*/
		      0,/*boy*/
		      0,/*box*/
		      3,/*bh*/
		      3,/*bw*/
		      i_mb_y,/*mby*/
		      i_mb_x/*mbx*/);

    tdd[4] = TDD_SYNC(1,/*vld*/
		      0,/*lk*/
		      0,/*crst*/
		      0xFFFF/*id*/);
}

void x264_do_mc( int i_mb_x, int i_mb_y )
{
    frameinfo_t *h = (frameinfo_t *)VRAM_FRAMEINFO;
    int16_t mvp[2];

    if( (i_mb_x + i_mb_y * h->i_mb_width) > h->i_last_mb )
	return;

    x264_analyse_mvp( h, i_mb_x, i_mb_y, mvp ); // mvp[0] mvp[1]
    motion_execute(h, i_mb_x, i_mb_y, mvp);
    *(volatile unsigned int *)VRAM_ME_DSA = 0;
    write_reg(0x13250054, (TCSM_PADDR(VRAM_ME_CHN_BASE) | 0x1) );
}

void x264_wait_mc()
{
    int16_t *mv_left = (int16_t *)VRAM_ME_LEFT_MV;
    int *cost = (int *)VRAM_ME_LEFT_COST;
    while( (*(volatile unsigned int *)VRAM_ME_DSA) != (0x80000000 | 0xFFFF) );
    int mvpa = *(int *)VRAM_ME_MVPA;
    mv_left[0] = (int8_t)(mvpa & 0xFF);
    mv_left[1] = (int8_t)((mvpa & 0xFF00)>>8);
    cost[0] = (int)((mvpa>>16) & 0xFFFF);
    volatile int *rst = (load_index & 0x1) ? VRAM_MAU_RESA : VRAM_MAU_RESA1;
    rst[0] = mvpa;
}

// VMAU
void vmau_execute( int i_mb_x, int i_mb_y )
{
    frameinfo_t *h = (frameinfo_t *)VRAM_FRAMEINFO;
    int *chnp = (int *)VRAM_MAU_CHN_BASE;
    int main_cbp = 0;
    int y_pred_mode = 0;
    int c_pred_mode = 0;

    if( (i_mb_x + i_mb_y * h->i_mb_width) > h->i_last_mb )
	return;

    if( h->frame_type == 2 /* SLICE_TYPE_I */ ) {
	main_cbp |= (MAU_Y_PREDE_MSK << MAU_Y_PREDE_SFT );//enable luma pred
	main_cbp |= (M16x16  << MAU_MTX_SFT );//enable luma 8x8

	if(i_mb_y && i_mb_x){
	    y_pred_mode = 0; //I_PRED_16x16_V;
	    c_pred_mode = 0; //I_PRED_CHROMA_DC;
	} else if(i_mb_y){
	    y_pred_mode = 5; //I_PRED_16x16_DC_TOP;
	    c_pred_mode = 5; //I_PRED_CHROMA_DC_TOP;
	} else if(i_mb_x) {
	    y_pred_mode = 4; //I_PRED_16x16_DC_LEFT;
	    c_pred_mode = 4; //I_PRED_CHROMA_DC_LEFT;
	} else {
	    y_pred_mode = 6; //I_PRED_16x16_DC_128;
	    c_pred_mode = 6; //I_PRED_CHROMA_DC_128;
	}
    } else {
	main_cbp |= (MAU_Y_ERR_MSK << MAU_Y_ERR_SFT );
	main_cbp |= (MAU_C_ERR_MSK << MAU_C_ERR_SFT );
    }

    int quant_para = ( (((int)(h->luma_qp/6) & H264_YQP_DIV6_MSK) << H264_YQP_DIV6_SFT)
		       | (((int)(h->luma_qp%6) & H264_YQP_MOD6_MSK) << H264_YQP_MOD6_SFT)
		       | (((int)(h->chroma_qp/6) & H264_CQP_DIV6_MSK) << H264_CQP_DIV6_SFT)
		       | (((int)(h->chroma_qp%6) & H264_CQP_MOD6_MSK) << H264_CQP_MOD6_SFT)
		       | (((int)(h->chroma_qp/6) & H264_C1QP_DIV6_MSK) << H264_C1QP_DIV6_SFT)
		       | (((int)(h->chroma_qp%6) & H264_C1QP_MOD6_MSK) << H264_C1QP_MOD6_SFT) );


    chnp[0] = main_cbp; //main_cbp
    chnp[1] = quant_para; //quant_para
    chnp[2] = (VRAM_ME_YRAWA + ((load_index - 1) % 3) * 512); //main_addr
    chnp[3] = TCSM_PADDR(VRAM_MAU_CHN_BASE); //ncchn_addr
    chnp[4] = 24 * 16; //main_len, id, dec_incr

    if( load_index & 0x1 )
	chnp[5] = VRAM_MAU_RESA + 4; //aux cbp
    else
	chnp[5] = VRAM_MAU_RESA1 + 4; //aux cbp

    chnp[6] = c_pred_mode;
    chnp[7] = y_pred_mode;

    write_reg(0x1328000C, TCSM_PADDR(VRAM_MAU_CHN_BASE));
    *(volatile unsigned int *)VRAM_MAU_DEC_SYNA = -1;
    *(volatile unsigned int *)VRAM_MAU_ENC_SYNA = -1;
    write_reg(0x13280040, VMAU_RUN);
}

void vmau_wait()
{
    while( (*(volatile unsigned int *)VRAM_MAU_DEC_SYNA == 0xFFFFFFFF) ||
    	   (*(volatile unsigned int *)VRAM_MAU_ENC_SYNA == 0xFFFFFFFF) );
}

int wait_mb_x, wait_mb_y;
void dblk_execute( int i_mb_x, int i_mb_y ) {

    frameinfo_t *h = (frameinfo_t *)VRAM_FRAMEINFO;
    int (*dblk_top)[2] = VRAM_DBLK_TOP_INFO; // TOP mv and TOP cbp
    int top_cbp = dblk_top[i_mb_x][1];

    int *curinfo = (load_index & 0x1) ? VRAM_MAU_RESA1 : VRAM_MAU_RESA;
    int cur_cbp = curinfo[1];
    int16_t mvx = (int8_t)(curinfo[0] & 0xFF);
    int16_t mvy = (int8_t)((curinfo[0] & 0xFF00) >> 8);

    if( (i_mb_x + i_mb_y * h->i_mb_width) > h->i_last_mb )
	return;

    int *chnp = (int *)VRAM_DBLK_CHN_BASE;

    int slice_end = ( i_mb_x == (h->i_mb_width - 1) && i_mb_y == (h->i_mb_height - 1) );
    int top_valid = (!!i_mb_y)<<30;
    int left_valid = (!!i_mb_x)<<31;
    chnp[0] = (slice_end<<31 | H264_MB_ID_MSK<<H264_MB_ID_SFT
	       | (TCSM_PADDR(VRAM_DBLK_CHN_BASE) & H264_MB_DHA_MSK) );

    if( h->enable_dblk ){
	int luma_qp = h->luma_qp;
	int chroma_qp = h->chroma_qp;

	int main_cbp = ((cur_cbp & 0xC3C3) | (cur_cbp & 0x3030) >> 2 | (cur_cbp & 0x0C0C) << 2);

	// qp0
	chnp[2] = ( (luma_qp & H264_QP_MSK )<<H264_YQP_CUR_SFT |
		    (chroma_qp & H264_QP_MSK)<<H264_UQP_CUR_SFT |
		    (luma_qp & H264_QP_MSK )<<H264_YQP_UP_SFT  |
		    (chroma_qp & H264_QP_MSK)<<H264_VQP_UP_SFT  |
		    (chroma_qp & H264_QP_MSK)<<H264_UQP_UP_SFT );

	// nnz
	if( top_valid )
	    chnp[3] = ( (main_cbp & H264_NNZ_MSK) |
			(chroma_qp & H264_QP_MSK) << H264_VQP_CUR_SFT |
			((top_cbp & 0xF000) >> 12)<< H264_TNNZ_SFT );
	else
	    chnp[3] = ( (main_cbp & H264_NNZ_MSK) |
			(chroma_qp & H264_QP_MSK) << H264_VQP_CUR_SFT |
			0xF << H264_TNNZ_SFT );

	if( h->frame_type == 2 /* SLICE_TYPE_I */ ) {
	    // curr_mb_type top_mb_type
	    chnp[1] = (left_valid | top_valid | H264_16x16<<16/*curr*/ | H264_16x16/*top*/);
	} else {
	    // curr_mb_type top_mb_type
	    chnp[1] = (left_valid | top_valid | (H264_INTER_MB | H264_16x16)<<16/*curr*/ |
		       H264_INTER_MB | 2 /*I_16x16 top*/);
	    // mv_para_addr
	    chnp[4] = (1<<26 /*mv_len*/ | (TCSM_PADDR(VRAM_DBLK_CHN_BASE+20) & 0xFFFFF)>>2);
	    chnp[5] = 0;  // cache_ref0
	    chnp[6] = 0;  // cache_ref1
	    // top_mv0
	    if( top_valid ) {
		chnp[7] = dblk_top[i_mb_x][0];
		chnp[8] = dblk_top[i_mb_x][0];
		chnp[9] = dblk_top[i_mb_x][0];
		chnp[10] = dblk_top[i_mb_x][0];
	    } else {
		chnp[7] = chnp[8] = chnp[9] = chnp[10] = 0;
	    }
	    //write_reg(VRAM_DBLK_CHN_BASE+44/48/52/56, top_mv1);
	    // curr mv
	    chnp[15] = (mvy << 16 | mvx);
	    //chnp[15] = (h->mb.mv[0][mb_4x4][1]<<16 | (h->mb.mv[0][mb_4x4][0] & 0xFFFF) );
	}
    }

    write_reg(0x13270060, 0x1); // start
    wait_mb_x = i_mb_x;
    wait_mb_y = i_mb_y;
    dblk_top[i_mb_x][0] = (mvy << 16 | mvx);
    dblk_top[i_mb_x][1] = cur_cbp;
}

void dblk_wait() {

    frameinfo_t *h = (frameinfo_t *)VRAM_FRAMEINFO;

    if( !wait_mb_x ){
	if( wait_mb_y )
	    while( *(volatile int *)VRAM_DBLK_DOUT_SYNA != ((wait_mb_y-1)<<16 | (h->i_mb_width-1)) );
    } else
	while( *(volatile int *)VRAM_DBLK_DOUT_SYNA != (wait_mb_y<<16 | (wait_mb_x-1)) );
}
