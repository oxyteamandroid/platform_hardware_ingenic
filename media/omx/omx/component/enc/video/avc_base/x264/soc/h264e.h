/*********************************************************

 ********************************************************/

#ifndef __JZM_H264E_H__
#define __JZM_H264E_H__
#include "t_vpu.h"
#include "t_motion.h"
#include "t_intpid.h"
#include "t_vmau.h"
#include "t_dblk.h"

#define SET_VMAU(a, v)      write_reg(VMAU_V_BASE+(a), (v))
#define SET_DBLK(a, v)      write_reg(DBLK_V_BASE+(a), (v))

/************************************************************
 CHN Space Allocation
 ************************************************************/
extern volatile unsigned char *tcsm1_base;
#define TCSM_VCADDR(a)       (tcsm1_base + (((unsigned)(a)) & 0xFFFF))
extern volatile unsigned char *sram_base;
#define SRAM_VCADDR(a)       (sram_base + (((unsigned)(a)) & 0xFFFF))

#include "x264_vram.h"

typedef unsigned int paddr_t;

#define __ALN32__ __attribute__ ((aligned(4)))

/*
  _H264E_SliceInfo:
  H264 Encoder Slice Level Information
 */
typedef struct _H264E_SliceInfo {
  /*basic*/
  uint8_t frame_type;
  uint8_t mb_width;
  uint8_t mb_height;
  uint8_t init_mbx;
  uint8_t init_mby;
  uint8_t last_mby;  //for multi-slice

  /*vmau scaling list*/
  uint8_t __ALN32__ scaling_list[4][16];

  /*loop filter*/
  uint8_t deblock;
  uint8_t rotate;
  int8_t alpha_c0_offset;
  int8_t beta_offset;

  /*frame buffer address: all of the buffers should be 256byte aligned!*/
  paddr_t fb[2][2];       /*{curr, ref}{tile_y, tile_c}*/
  paddr_t mau_rst;
}_H264E_SliceInfo;

/*
  H264E_SliceInit(_H264E_SliceInfo *s)
  @param s: slice information structure
 */
void H264E_SliceInit(_H264E_SliceInfo *s);

#endif /*__JZM_H264E_H__*/
