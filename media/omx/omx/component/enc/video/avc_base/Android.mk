LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

LOCAL_SRC_FILES := \
	AVCBaseEncoder.cpp \
	x264/x264.c \
	x264/common/mc.c \
	x264/common/predict.c \
	x264/common/pixel.c \
	x264/common/macroblock.c \
	x264/common/frame.c \
	x264/common/dct.c \
	x264/common/cpu.c \
	x264/common/cabac.c \
	x264/common/common.c \
	x264/common/mdate.c \
	x264/common/set.c \
	x264/common/quant.c \
	x264/common/vlc.c \
	x264/encoder/analyse.c \
	x264/encoder/me.c \
	x264/encoder/ratecontrol.c \
	x264/encoder/set.c \
	x264/encoder/macroblock.c \
	x264/encoder/cabac.c \
	x264/encoder/cavlc.c \
	x264/encoder/encoder.c \
	x264/encoder/lookahead.c \
	x264/soc/jz47xx_pmon.c \
	x264/soc/crc.c  \
	../../../dec/lume/libjzcommon/jzm_intp.c

LOCAL_C_INCLUDES := \
    $(LOCAL_PATH)/../../../dec/lume/libjzcommon \
    $(LOCAL_PATH)/x264 \
    $(LOCAL_PATH)/x264/common \
    $(LOCAL_PATH)/x264/encoder \
    $(LOCAL_PATH)/x264/soc \
    $(TOP)/hardware/ingenic/media/omx/omx/component/common \
    $(TOP)/frameworks/av/media/libstagefright/codecs/avc/common/include \
    $(TOP)/frameworks/av/media/libstagefright/include \
    $(TOP)/frameworks/native/include/media/hardware \
    $(TOP)/frameworks/native/include/media/openmax \
    $(TOP)/frameworks/av/include/media/stagefright \
    $(TOP)/hardware/ingenic/media/omx/omx/core \

LOCAL_CFLAGS := -DOSCL_IMPORT_REF= -DOSCL_UNUSED_ARG= -DOSCL_EXPORT_REF=

LOCAL_CFLAGS += $(STAGEFRIGHT_FLAGS)

LOCAL_SHARED_LIBRARIES := \
        libstagefright_foundation \
        libstagefright_omx \
        libstagefright \
        libbinder \
	libcutils \
        libutils \
        libui

LOCAL_STATIC_LIBRARIES := \
	libOMX_Basecomponent \
	libstagefright_mphwapp \

LOCAL_MODULE := libstagefright_hard_x264baseenc
LOCAL_MODULE_TAGS := optional

include $(BUILD_SHARED_LIBRARY)

LOCAL_COPY_DEPENDS_TO := etc
LOCAL_COPY_DEPENDS := x264/soc/x264_aux.bin
include $(BUILD_COPY_DEPENDS)
