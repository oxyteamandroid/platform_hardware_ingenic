/*
 * Copyright (C) 2009 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#define LOG_TAG "LUMEExtractor"
#include <utils/Log.h>

#include "include/LUMEExtractor.h"
#include "include/LUMERecognizer.h"

#include <media/stagefright/DataSource.h>
#include <media/stagefright/MediaBuffer.h>
#include <media/stagefright/MediaBufferGroup.h>
#include <MediaDebug.h>
#include <media/stagefright/MediaDefs.h>
#include <media/stagefright/MediaErrors.h>
#include <media/stagefright/MediaSource.h>
#include <media/stagefright/MetaData.h>
#include <media/stagefright/Utils.h>
#include <LUMEDefs.h>
#include <utils/String8.h>
#include "ID3.h"
#include "cutils/properties.h"

extern "C"
{
#include "stream.h"
#include "demuxer.h"
#include "stheader.h"
#include "codecs.conf.h"
}

//Convertion between StreamID and TrackID
#define STREAMID_TO_TRACKID_VIDEO(x) ((x)+1)
#define STREAMID_TO_TRACKID_AUDIO(x) ((x)+257)
#define STREAMID_TO_TRACKID_TEXT(x) ((x)+513)

#define TRACKID_TO_STREAMID_VIDEO(x) ((x)-1)
#define TRACKID_TO_STREAMID_AUDIO(x) ((x)-257)
#define TRACKID_TO_STREAMID_TEXT(x) ((x)-513)

namespace android {

class LUMESource : public MediaSource {
public:
    LUMESource(LUMESourceInfo*);

    virtual status_t start(MetaData *params = NULL) = 0;
    virtual status_t stop();
    virtual sp<MetaData> getFormat();
    virtual status_t read(MediaBuffer **buffer, const ReadOptions *options = NULL) = 0;

protected:
    virtual ~LUMESource();

    Mutex mLock;
    int32_t mFIFOLock;
    bool mStarted;
    bool mEOF;

    demuxer_t* mDemuxer;
    Mutex* mDemuxerLock;
    bool mHasVideo;
    bool mHasAudio;
    bool mHasSub;
    sp<MetaData> mMeta;
    sp<LUMEExtractor> mExtractor;

    MediaBufferGroup *mGroup;

    LUMESource(const LUMESource &);
    LUMESource &operator=(const LUMESource &);
};//LUMESource

class LUMEAudioSource : public LUMESource {
public:
    LUMEAudioSource(LUMESourceInfo*);
    virtual status_t start(MetaData *params = NULL);
    virtual status_t read(MediaBuffer **buffer, const ReadOptions *options);
private:
    MediaBuffer* mMediaBuffers[AUDIO_EXTRACTOR_BUFFER_COUNT];
};//LUMEAudioSource

class LUMEVideoSource : public LUMESource {
public:
    LUMEVideoSource(LUMESourceInfo*);
    virtual status_t start(MetaData *params = NULL);
    virtual status_t read(MediaBuffer **buffer, const ReadOptions *options);
private:
    int64_t mLastVideoPts;
    MediaBuffer* mMediaBuffers[VIDEO_EXTRACTOR_BUFFER_COUNT];
};//LUMEVideoSource

class LUMESubSource : public LUMESource {
public:
    LUMESubSource(LUMESourceInfo*);
    virtual status_t start(MetaData *params = NULL);
    virtual status_t read(MediaBuffer **buffer, const ReadOptions *options);
private:
    MediaBuffer* mMediaBuffers[SUB_EXTRACTOR_BUFFER_COUNT];
};//LUMESubSource

////////////////////////////////////////////////////////////LUMESource definition

LUMESource::LUMESource(LUMESourceInfo* info)
    : mStarted(false),
      mEOF(false),
      mDemuxer(info->demuxer),
      mDemuxerLock(info->demuxerLock),
      mHasVideo(info->hasVideo),
      mHasAudio(info->hasAudio),
      mHasSub(info->hasSub),
      mMeta(info->meta),
      mExtractor(info->extractor){
}

LUMESource::~LUMESource() {
    if(mStarted)
        stop();
}

//Release the allocated MediaBuffer
status_t LUMESource::stop() {
    Mutex::Autolock ao(mLock);

    CHECK(mStarted);

    delete mGroup;
    mGroup = NULL;

    mStarted = false;

    return OK;
}

sp<MetaData> LUMESource::getFormat() {
    Mutex::Autolock ao(mLock);

    return mMeta;
}

////////////////////////////////LUMEAudioSource definition
LUMEAudioSource::LUMEAudioSource(LUMESourceInfo* srcInfo)
    :LUMESource(srcInfo){
}

status_t LUMEAudioSource::start(MetaData *) {
    Mutex::Autolock ao(mLock);

    CHECK(!mStarted);

    mGroup = new MediaBufferGroup;
    for(int i = 0; i < AUDIO_EXTRACTOR_BUFFER_COUNT; ++i){
	MediaBuffer* buffer = new MediaBuffer(MAX_AUDIO_EXTRACTOR_BUFFER_RANGE);
	mGroup->add_buffer(buffer);
	mMediaBuffers[i] = buffer;
    }

    mMeta->setInt32(kKeyExtracterBufCount, AUDIO_EXTRACTOR_BUFFER_COUNT);

    mStarted = true;

    return OK;
}

status_t LUMEAudioSource::read(MediaBuffer **out, const ReadOptions *options){
    Mutex::Autolock ao(mLock);
    Mutex::Autolock ao1(mDemuxerLock);

    int64_t seekTimeUs = 0;
    ReadOptions::SeekMode mode;
    if (options != NULL && options->getSeekTo(&seekTimeUs, &mode)) {
	ALOGV("Audio seek to time: %lld, if -1 in a video it is OK.", seekTimeUs);
	if(!mHasVideo && seekTimeUs >= 0){
	    demux_seek(mDemuxer,(float)(seekTimeUs/MULTIPLE_S_TO_US), 0.0,1);
	}
	mEOF = false;
    }

    if(mEOF){
	ALOGV("Warning:already end of audio track");
	return ERROR_END_OF_STREAM;
    }

    *out = NULL;
    MediaBuffer* buffer = NULL;

#if 1
    bool freeBufferFound = false;
    for(int i = 0; i < AUDIO_EXTRACTOR_BUFFER_COUNT; ++i){
	if(mMediaBuffers[i]->refcount() == 0){
	    CHECK_EQ(mGroup->acquire_buffer(&buffer), OK);
	    freeBufferFound = true;
	    break;
	}
    }
    if(!freeBufferFound){
	return NO_MEMORY;
    }
#else
    CHECK_EQ(mGroup->acquire_buffer(&buffer), OK);
#endif

    demux_stream_t* ds = mDemuxer->audio;
    CHECK(NULL != ds);
    sh_audio_t *shAudio = (sh_audio_t *)ds->sh;
    CHECK(NULL != shAudio);

    unsigned char* packet = NULL;
    int packetSize = 0, totalSize = 0;
    double curPts = 0.0, nextPts = 0.0;
    int packetNum = 0;
    int64_t bufferHeadPts = 0;

    /*when two audio track have diffrent samplerate, must free old data*/
    if(mDemuxer->changeAudioFlag == 1){
    	ds_free_packs(ds);
    	mDemuxer->changeAudioFlag = 0;
    }

    while((totalSize + ds_get_packet_len(ds)) < MAX_AUDIO_EXTRACTOR_BUFFER_RANGE){
	packetSize = ds_get_packet_pts(ds, &packet, &curPts);
	if(packetSize == -1){
	    if(totalSize > 0){
		mEOF = true;
		break;
	    }

	    buffer->release();
	    buffer = NULL;

	    return ERROR_END_OF_STREAM;
	}

	memcpy((unsigned char*)buffer->data() + totalSize, packet, packetSize);
	totalSize += packetSize;

	if(packetNum++ == 0){
	    if(shAudio->i_bps)
		bufferHeadPts = (int64_t)((ds->pts + ((float)(ds->pts_bytes-packetSize))/shAudio->i_bps) * MULTIPLE_S_TO_US);
	    else
		bufferHeadPts = (int64_t)(ds->pts * MULTIPLE_S_TO_US);
	}

	//one packet per frame, packeting it will lead to fail for case that first one error.
	if(shAudio->format == 0x73627276 || shAudio->format == 0x6b6f6f63 || shAudio->format == 0x63727461 || shAudio->format == 0x43614c66 || shAudio->format == 0x162 || shAudio->format == 0x2001 || shAudio->format == 0x566F) {
	    break;
	}

	if(((int64_t)(ds_get_next_pts(ds) * MULTIPLE_S_TO_US) - bufferHeadPts) >= MAX_AUDIO_EXTRACTOR_PACKETING_PTS_GAP){
	    break;
	}else if(shAudio->i_bps){
	    double accurate_pts = ds->pts + (float)ds->pts_bytes/shAudio->i_bps;
	    if((int64_t)(accurate_pts* MULTIPLE_S_TO_US- bufferHeadPts) >= MAX_AUDIO_EXTRACTOR_PACKETING_PTS_GAP){
		break;
	    }
	}
    }

    buffer->set_range(0, totalSize);
    buffer->meta_data()->setInt64(kKeyTime, bufferHeadPts);

    *out = buffer;

    return OK;
}

//////////////////////////////LUMEVideoSource definition

LUMEVideoSource::LUMEVideoSource(LUMESourceInfo* srcInfo)
    :LUMESource(srcInfo){
    mLastVideoPts = 0;
}

status_t LUMEVideoSource::start(MetaData *) {
    Mutex::Autolock ao(mLock);

    CHECK(!mStarted);

    mGroup = new MediaBufferGroup;
    for(int i = 0; i < VIDEO_EXTRACTOR_BUFFER_COUNT; ++i){
	MediaBuffer* buffer = new MediaBuffer(MAX_VIDEO_EXTRACTOR_BUFFER_RANGE);
	mGroup->add_buffer(buffer);
	mMediaBuffers[i] = buffer;
    }

    mMeta->setInt32(kKeyExtracterBufCount, VIDEO_EXTRACTOR_BUFFER_COUNT);

    mStarted = true;
    return OK;
}

status_t LUMEVideoSource::read(MediaBuffer **out, const ReadOptions *options){
    Mutex::Autolock ao(mLock);
    Mutex::Autolock ao1(mDemuxerLock);

    int64_t seekTimeUs = 0;
    ReadOptions::SeekMode mode;
    bool inSeeking = false;
    int isthumb = 0;
    if (options != NULL && options->getSeekTo(&seekTimeUs, &mode)) {
	inSeeking = true;
	if(seekTimeUs >= 0){
	    if((mDemuxer->thumb_nseekable == 1) && ((ReadOptions::SeekMode)SEEK_WITH_IS_THUMB_MODE == mode)){
		demux_seek(mDemuxer,0.0,0.0,1);
	    }else{
		mDemuxer->thumb_eindexfind = 0;
		if((ReadOptions::SeekMode)SEEK_WITH_IS_THUMB_MODE == mode)
		    mDemuxer->thumb_eindexfind = 1;
		demux_seek(mDemuxer,(float)(seekTimeUs/MULTIPLE_S_TO_US), 0.0,1);
	    }

	    mEOF = false;
	}
    }

    if(mEOF){
	return ERROR_END_OF_STREAM;
    }

    *out = NULL;
    MediaBuffer* buffer = NULL;

#if 1
    bool freeBufferFound = false;
    for(int i = 0; i < VIDEO_EXTRACTOR_BUFFER_COUNT; ++i){
	if(mMediaBuffers[i]->refcount() == 0){
	    CHECK_EQ(mGroup->acquire_buffer(&buffer), OK);
	    freeBufferFound = true;
	    break;
	}
    }

    if(!freeBufferFound){
	return NO_MEMORY;
    }
#else
    CHECK_EQ(mGroup->acquire_buffer(&buffer), OK);
#endif

    demux_stream_t* ds = mDemuxer->video;
    CHECK(NULL != ds);
    sh_video_t *shVideo = (sh_video_t *)ds->sh;
    CHECK(NULL != shVideo);

    if(MAX_VIDEO_EXTRACTOR_BUFFER_RANGE < (int)ds->buffer_size){
        ALOGE("Error:video packet size:%d out of range:%d!!", (int)ds->buffer_size, MAX_VIDEO_EXTRACTOR_BUFFER_RANGE);
        return ERROR_OUT_OF_RANGE;
    }

    unsigned char* frame = NULL;
    float frameTime = 0.0;
    int frameSizeInvalid = 0;
    int frameSize = 0;

    while(frameSize <= 0){
	frameSize = video_read_frame(shVideo,&frameTime,&frame,0);

	if(frameSize > (int)buffer->size()){
	    frameSize = 0;
	    continue;
	}

	if(frameSize == -1){
	    if(mLastVideoPts < shVideo->stream_duration - 500000ll){
		if(frameSizeInvalid ++ < 3 ){
		    frameSize = 0;
		    continue;
		}
	    }

	    buffer->release();
	    buffer = NULL;

	    mEOF = true;

	    return ERROR_END_OF_STREAM;
	}
    }

    int64_t bufferHeadPts = (int64_t)(shVideo->pts * MULTIPLE_S_TO_US);
    if((bufferHeadPts < mLastVideoPts) && !inSeeking){
	bufferHeadPts = (mLastVideoPts +(frameTime*MULTIPLE_S_TO_US));
    }
    mLastVideoPts = bufferHeadPts;

    memcpy((unsigned char*)buffer->data(), frame, frameSize);

    buffer->set_range(0, frameSize);
    buffer->meta_data()->setInt64(kKeyTime, (int64_t)(bufferHeadPts));
    buffer->meta_data()->setInt64(kKeyStartTime, (int64_t)(mDemuxer->start_time));

    *out = buffer;

    return OK;
}

////////////////////////////////LUMESubSource definition
LUMESubSource::LUMESubSource(LUMESourceInfo* srcInfo)
    :LUMESource(srcInfo){
}

status_t LUMESubSource::start(MetaData *) {
    Mutex::Autolock ao(mLock);

    CHECK(!mStarted);

    mGroup = new MediaBufferGroup;
    for(int i = 0; i < SUB_EXTRACTOR_BUFFER_COUNT; ++i){
	MediaBuffer* buffer = new MediaBuffer(MAX_SUB_EXTRACTOR_BUFFER_RANGE);
	mGroup->add_buffer(buffer);
	mMediaBuffers[i] = buffer;
    }

    mStarted = true;

    return OK;
}

status_t LUMESubSource::read(MediaBuffer **out, const ReadOptions *options){
    Mutex::Autolock ao(mLock);
    Mutex::Autolock ao1(mDemuxerLock);

    int64_t seekTimeUs = 0;
    ReadOptions::SeekMode mode;
    if (options != NULL && options->getSeekTo(&seekTimeUs, &mode)) {
	ALOGV("Audio seek to time: %lld, if -1 in a video it is OK.", seekTimeUs);
	if(!mHasVideo && !mHasAudio && seekTimeUs >= 0){
	    demux_seek(mDemuxer,(float)(seekTimeUs/MULTIPLE_S_TO_US), 0.0,1);
	}
	mEOF = false;
    }

    if(mEOF){
	ALOGV("Warning:already end of sub track");
	return ERROR_END_OF_STREAM;
    }

    *out = NULL;
    MediaBuffer* buffer = NULL;

#if 1
    bool freeBufferFound = false;
    for(int i = 0; i < SUB_EXTRACTOR_BUFFER_COUNT; ++i){
	if(mMediaBuffers[i]->refcount() == 0){
	    CHECK_EQ(mGroup->acquire_buffer(&buffer), OK);
	    freeBufferFound = true;
	    break;
	}
    }

    if(!freeBufferFound){
	return NO_MEMORY;
    }
#else
    CHECK_EQ(mGroup->acquire_buffer(&buffer), OK);
#endif

    demux_stream_t* ds = mDemuxer->sub;
    CHECK(NULL != ds);
    sh_sub_t *shSub = (sh_sub_t *)ds->sh;
    CHECK(!(NULL == shSub && !mDemuxer->ists));

    char type = shSub ? shSub->type : 'v';
    unsigned char* packet = NULL;
    int packetSize = 0;
    double spts = 0.0, endpts=0.0;

    packetSize = ds_get_packet_sub(ds, &packet, &spts, &endpts);

    if(packetSize == -1) {
        buffer->release();
	buffer = NULL;
	mEOF = true;

	return ERROR_END_OF_STREAM;
    }else if((packetSize == -2) || (type == 'v')){
        unsigned char space[2] = "";
	memcpy((unsigned char*)buffer->data(), space, 1);
	buffer->set_range(0, 300);
	*out = buffer;

	return OK;
    }else{
	unsigned char* p = packet;
	int frommsec, fromsec, tomsec, tosec;

	if (type == 'a') {
	    int i;
	    int skip_commas = 8;
	    if (packetSize > 10 && memcmp(packet, "Dialogue: ", 10) == 0)
		skip_commas = 9;
	    for (i=0; i < skip_commas && *p != '\0'; p++){
		if (*p == ','){
		    i++;
		}
	    }

	    packet = p;

	    packetSize -= p - packet;
	    p = packet;
	}

	memcpy((unsigned char*)buffer->data(), packet, packetSize);
	buffer->set_range(0, packetSize);

	buffer->meta_data()->setInt64(kKeyTime, (int64_t)(spts*1000.0));
	//buffer->meta_data()->setInt64(kKeySpts, (int64_t)(spts*1000.0));
	//buffer->meta_data()->setInt64(kKeyEpts, (int64_t)(endpts*1000.0));

	*out = buffer;

	return OK;
    }
}
///////////////////////////////////////////////////////LUMEExtractor Definition
//Generate stream and find demuxer corresponding to the DataSource.
LUMEExtractor::LUMEExtractor(const sp<DataSource> &source)
    : mSource(source),
      mFileFormat(-1),
      mStream(NULL),
      mDemuxer(NULL),
      mTrackList(NULL),
      mTrackCount(0),
      mTrackCounted(false),
      mTrackListInited(false),
      mHasVideo(false),
      mHasAudio(false),
      mHasSub(false),
      mStarted(false){
    mPrefetcher = new LUMEPrefetcher;
    mLUMEStream = new LUMEStream(mSource.get());

    mStream = open_stream_opencore(NULL,NULL,&mFileFormat,(int)(mLUMEStream.get()));
    if(NULL == mStream){
        ALOGE("Error:stream control failed");
	return;
    }
    //mStream->Context = (void *)this;

    mDemuxer = demux_open(mStream,mFileFormat,-1,-1,-2,NULL);
    if(NULL == mDemuxer){
	ALOGE("Error:demux open failed tid = %d",gettid());
	return;
    }

    double slen = demuxer_get_time_length(mDemuxer);
    if(slen > 0.0 && slen < 5.0)
	demux_seek(mDemuxer,0.0,0.0,SEEK_ABSOLUTE);

    if(mDemuxer && mDemuxer->video && mDemuxer->video->sh){
	((sh_video_t*)(mDemuxer->video->sh))->videopicture = NULL;
	video_read_properties((sh_video_t*)mDemuxer->video->sh);
    }

    mStarted = true;

    // ALOGE("demuxer type after demux_open:%d %s",mDemuxer->type, mDemuxer->desc->name);
}


LUMEExtractor::~LUMEExtractor() {
    if(NULL != mTrackList){
        delete[] mTrackList;
        mTrackList = NULL;
    }

    if(mDemuxer && mDemuxer->video && mDemuxer->video->sh) {
        sh_video_t *shVideo = (sh_video_t *)mDemuxer->video->sh;
        if(shVideo->videobuffer)
            free(shVideo->videobuffer);
        shVideo->videobuffer = NULL;
        if(shVideo->videopicture)
            free(shVideo->videopicture);
        shVideo->videopicture = NULL;
    }

    if(NULL != mDemuxer){
        free_demuxer(mDemuxer);
        mDemuxer = NULL;
    }

    if(NULL != mStream){
        free_stream(mStream);
        mStream = NULL;
    }

    mStarted = false;
}

//Track(stream) count.
size_t LUMEExtractor::countTracks() {
    if(!mStarted)
	return 0;

    if(!mTrackCounted){
        uint32_t index = 0;

        // video tracks
        for (int i = 0; i < MAX_V_STREAMS; ++i){
            if (NULL != mDemuxer->v_streams[i]){
                ++index;
                mHasVideo = true;
            }
        }

        // audio tracks
        for (int i = 0; i < MAX_A_STREAMS; ++i){
	    if (NULL != mDemuxer->a_streams[i]){
		++index;
		mHasAudio = true;
	    }
        }

        // sub tracks
        for (int i = 0; i < MAX_S_STREAMS; ++i){
	    if (NULL != mDemuxer->s_streams[i]){
		++index;
		mHasSub = true;
	    }
        }

        mTrackCount = index;
        //mTrackCounted = true;
    }

    return mTrackCount;
}

static const char *GetMIMETypeForFourCC(uint32_t fourcc, bool audio) {
    const codecs_t *codecs = audio ? builtin_audio_codecs : builtin_video_codecs;
    bool found = false;
    for (/* NOTHING */; codecs->name; codecs++) {
	for (int j = 0; j < CODECS_MAX_FOURCC; j++) {
	    if(codecs->fourcc[j] == (unsigned int)-1) break;
	    if (codecs->fourcc[j] == fourcc) {
		found = true;
		break;
	    }
	}
	if(found) break;
    }

    if(found){
	char *tmp = codecs->dll ? codecs->dll : codecs->name;
	//ALOGE("GetMIMETypeForHandler tmp=%s name:%s type:%x",tmp, codecs->name, fourcc);
	if(!audio)
	    return MEDIA_MIMETYPE_VIDEO_LUME;
	else if (audio)
	    return MEDIA_MIMETYPE_AUDIO_LUME;
    }
    return NULL;
}

//Get the TrackList with metaData, streamID/Type stuff.
status_t LUMEExtractor::getTrackList(){
    if(mTrackListInited){
        delete[] mTrackList;
        mTrackList = NULL;
    }

    if(0 == countTracks()){
        ALOGE("Error:No track found");
        CHECK(false);
    }

    mTrackList = new TrackItem[mTrackCount];

    int index = 0;

    // video tracks
    for (int i = 0; i < MAX_V_STREAMS; ++i){
        if (NULL != mDemuxer->v_streams[i]){
            sh_video_t* shVideo = (sh_video_t*)(mDemuxer->v_streams[i]);

            mTrackList[index].streamID = i;
            mTrackList[index].videoID = shVideo->vid;
            mTrackList[index].trackID = STREAMID_TO_TRACKID_VIDEO(shVideo->vid);
            mTrackList[index].streamType = LUMEExtractor::videoType;

            sp<MetaData> meta = new MetaData;
	    const char *mime = GetMIMETypeForFourCC(shVideo->format, false);
	    meta->setCString(kKeyMIMEType, mime);
            if(shVideo->bih){
                /////TODO:(high pri)Need another "config_parser"(in opencore) to fill them.
                if(0 == shVideo->disp_w)
                    shVideo->disp_w = shVideo->bih->biWidth;
                if(0 == shVideo->disp_h)
                    shVideo->disp_h = shVideo->bih->biHeight;
            }

            meta->setInt32(kKeyWidth, shVideo->disp_w);
            meta->setInt32(kKeyHeight, shVideo->disp_h);
            meta->setInt32(kKeyDisplayWidth, shVideo->disp_w);
            meta->setInt32(kKeyDisplayHeight, shVideo->disp_h);
	    meta->setInt32(kKeyRotation,shVideo->rotation_degrees);
            meta->setInt32(kKeySampleRate, shVideo->fps);
            meta->setInt32(kKeyBitRate, shVideo->i_bps);
	    int64_t duration = demuxer_get_time_length(mDemuxer) * MULTIPLE_S_TO_US;
	    shVideo->stream_duration = duration;
            meta->setInt64(kKeyDuration, duration);
	    meta->setInt64(kKeyThumbnailTime, duration * THUMBNAIL_TIME_PERCENTAGE);
            meta->setPointer(kKeyVideoSH, (void *)shVideo);

            mTrackList[index].metaData = meta;

            ++index;
        }
    }

    // audio tracks
    for (int i = 0; i < MAX_A_STREAMS; ++i){
        if (NULL != mDemuxer->a_streams[i]){
            sh_audio_t* shAudio = (sh_audio_t*)(mDemuxer->a_streams[i]);

	    // ALOGE("LUMEExtractor::gettracklist streamID:%d, mDemuxer->aid:%d", i, mDemuxer->audio->id);

            mTrackList[index].streamID = i;
            mTrackList[index].audioID = shAudio->aid;
            mTrackList[index].trackID = STREAMID_TO_TRACKID_AUDIO(shAudio->aid);
            mTrackList[index].streamType = LUMEExtractor::audioType;

            sp<MetaData> meta = new MetaData;
	    const char *mime = GetMIMETypeForFourCC(shAudio->format, true);
	    if (shAudio->format == 0x726d6173)//amr_nb
		meta->setCString(kKeyMIMEType, MEDIA_MIMETYPE_AUDIO_AMR_NB);
	    else
		meta->setCString(kKeyMIMEType, MEDIA_MIMETYPE_AUDIO_GENERAL);
	    meta->setCString(kKeyMIMEType, mime);
            meta->setInt32(kKeySampleRate, shAudio->samplerate);
            meta->setInt32(kKeyBitsPerSample, shAudio->samplesize * 8);
            meta->setInt32(kKeyBitRate, shAudio->i_bps * 8);
            meta->setInt64(kKeyDuration, demuxer_get_time_length(mDemuxer) * MULTIPLE_S_TO_US);

            if(shAudio->wf) {
                /////TODO:(high pri)Need another "config_parser"(in opencore) to fill them.
                shAudio->channels = shAudio->wf->nChannels;
                shAudio->samplerate = shAudio->wf->nSamplesPerSec;
                /////
                meta->setInt32(kKeyBitsPerSample,shAudio->wf->wBitsPerSample);
                meta->setInt32(kKeyBitRate, shAudio->wf->nAvgBytesPerSec * 8);
                meta->setInt32(kKeySampleRate, shAudio->wf->nSamplesPerSec);
            }

	    meta->setInt32(kKeyChannelCount, shAudio->channels >= 2 ? 2 : shAudio->channels);
            meta->setPointer(kKeyAudioSH, shAudio);

	    mTrackList[index].metaData = meta;

            ++index;
        }
    }

    // sub tracks
    for (int i = 0; i < MAX_S_STREAMS; ++i){
        if (NULL != mDemuxer->s_streams[i]){
            sh_sub_t* shSub = (sh_sub_t*)(mDemuxer->s_streams[i]);

	    // ALOGE("LUMEExtractor::gettracklist streamid:%d, subid:%d", i, shSub->sid);

            mTrackList[index].streamID = i;
            mTrackList[index].subID = shSub->sid;
            mTrackList[index].trackID = STREAMID_TO_TRACKID_TEXT(shSub->sid);
            mTrackList[index].streamType = LUMEExtractor::subType;

            sp<MetaData> meta = new MetaData;
	    meta->setCString(kKeyMIMEType, MEDIA_MIMETYPE_TEXT_3GPP);//MEDIA_MIMETYPE_SUBTL_LUME);

	    //meta->setPointer(kKeySubSH, shSub);
            mTrackList[index].metaData = meta;

	    ++index;
        }
    }

    mTrackListInited = true;

    return OK;
}

sp<MediaSource> LUMEExtractor::getTrack(size_t index) {
    if(!mStarted)
	return NULL;

    if(getTrackList() != OK){
        ALOGE("Error:Failed to getTrackList");
        CHECK(false);
    }

    LUMESourceInfo srcInfo;
    srcInfo.demuxer = mDemuxer;
    srcInfo.demuxerLock = &mDemuxerLock;
    srcInfo.meta = mTrackList[index].metaData;
    srcInfo.hasVideo = mHasVideo;
    srcInfo.hasAudio = mHasAudio;
    srcInfo.hasSub = mHasSub;
    srcInfo.extractor = this;

    sp<MediaSource> source = NULL;
    switch(mTrackList[index].streamType) {
    case LUMEExtractor::audioType:
    {
        source = new LUMEAudioSource(&srcInfo);
	int changeID;
	if(mDemuxer->ists)
	    changeID = mTrackList[index].audioID;
	else
	    changeID = mTrackList[index].streamID;
	mDemuxer->a_changed_id = changeID;
	mDemuxer->changeAudioFlag = 1;
	break;
    }
    case LUMEExtractor::videoType:
    {
        source = new LUMEVideoSource(&srcInfo);
	break;
    }
    case LUMEExtractor::subType:
    {
        source = new LUMESubSource(&srcInfo);
	int changeID;
	if(mDemuxer->ists)
	    changeID = mTrackList[index].subID;
	else
	    changeID = mTrackList[index].streamID;
	mDemuxer->s_changed_id = changeID;
	break;
    }
    default:
    {
        ALOGE("Error: unrecognized stream type found!!");
        CHECK(false);
    }
    }

    if (mPrefetcher != NULL) {
	source = mPrefetcher->addSource(source);
    }

    return source;
}

sp<MetaData> LUMEExtractor::getTrackMetaData(size_t index, uint32_t flags) {
    if(!mStarted)
	return NULL;

    if(OK != getTrackList()) {
        ALOGE("Error:getTrackList failed");
        CHECK(false);
    }

    if(index >= mTrackCount){
        ALOGE("Error:Track index out of range");
        CHECK(false);
    }

    return mTrackList[index].metaData;
}

//TODO:The differences between trackMetaData and metaData? And make getMetaData the same with getTrackList?
sp<MetaData> LUMEExtractor::getMetaData() {
    if(!mStarted)
	return NULL;

    if(0 == countTracks()){
        ALOGE("Error:No track found");
        CHECK(false);
    }

    sp<MetaData> meta = new MetaData;

    if(mHasVideo)
        meta->setCString(kKeyMIMEType, MEDIA_MIMETYPE_VIDEO_LUME);
    else if(mHasAudio)
        meta->setCString(kKeyMIMEType, MEDIA_MIMETYPE_AUDIO_LUME);
    else if(mHasSub)
        meta->setCString(kKeyMIMEType, MEDIA_MIMETYPE_TEXT_3GPP);//MEDIA_MIMETYPE_SUBTL_LUME);

      char* info = NULL;
      info = demux_info_get(mDemuxer, "Author");
      if(info == NULL){
	  info = demux_info_get(mDemuxer, "Artist");
	  if(info != NULL)
	      meta->setCString(kKeyArtist, info);
      }

      info = demux_info_get(mDemuxer, "Title");
      if(info != NULL)
	  meta->setCString(kKeyTitle, info);
      info = demux_info_get(mDemuxer, "Album");
      if(info != NULL)
	  meta->setCString(kKeyAlbum, info);
      info = demux_info_get(mDemuxer, "Year");
      if(info != NULL)
	  meta->setCString(kKeyYear, info);
      info = demux_info_get(mDemuxer, "Genre");
      if(info != NULL)
	  meta->setCString(kKeyGenre, info);
      info = demux_info_get(mDemuxer, "Track");
      if(info != NULL)
	  meta->setCString(kKeyCDTrackNumber, info);

      //info = demux_info_get(mDemuxer, "udta");
      info = demux_info_get(mDemuxer, "location");
      //LOGE("udta info = %s", info);
      if(info != NULL)meta->setCString(kKeyLocation, info);

      //TODO:The info get from demux could be in form of ISO 8859-1/UTF-8/UTF-16 BE/UCS-2...which is unknown with provided by demux. however, the info got is exactly the correct bytes content.
	  char *uri = (char *)mSource->getUri().string();
      bool isHTTPStream = false;
      if (uri != NULL
	  && (!strncasecmp("http://", uri, 7)
	     || !strncasecmp("https://", uri, 8)))
	  isHTTPStream = true;
      // http stream and novideo stream can skip it
      if (!isHTTPStream && !mHasVideo) {
	  ID3 id3(mSource);
	  if (!id3.isValid()) {
	      return meta;
	  }
	  struct Map {
	      int key;
	      const char *tag1;
	      const char *tag2;
	  };
	  static const Map kMap[] = {
	      { kKeyAlbum, "TALB", "TAL" },
	      { kKeyArtist, "TPE1", "TP1" },
	      { kKeyAlbumArtist, "TPE2", "TP2" },
	      { kKeyComposer, "TCOM", "TCM" },
	      { kKeyGenre, "TCON", "TCO" },
	      { kKeyTitle, "TIT2", "TT2" },
	      { kKeyYear, "TYE", "TYER" },
	      { kKeyAuthor, "TXT", "TEXT" },
	      { kKeyCDTrackNumber, "TRK", "TRCK" },
	      { kKeyDiscNumber, "TPA", "TPOS" },
	      { kKeyCompilation, "TCP", "TCMP" },
	  };
	  static const size_t kNumMapEntries = sizeof(kMap) / sizeof(kMap[0]);

	  for (size_t i = 0; i < kNumMapEntries; ++i) {
	      ID3::Iterator *it = new ID3::Iterator(id3, kMap[i].tag1);
	      if (it->done()) {
		  delete it;
		  it = new ID3::Iterator(id3, kMap[i].tag2);
	      }

	      if (it->done()) {
		  delete it;
		  continue;
	      }

	      String8 s;
	      it->getString(&s);
	      delete it;

	      meta->setCString(kMap[i].key, s);
	  }
	  size_t dataSize;
	  String8 mime;
	  const void *data = id3.getAlbumArt(&dataSize, &mime);
	  if (data) {
	      meta->setData(kKeyAlbumArt, MetaData::TYPE_NONE, data, dataSize);
	      meta->setCString(kKeyAlbumArtMIME, mime.string());
	  }
      }


    return meta;
}

uint32_t LUMEExtractor::flags() const {
    uint32_t flags = CAN_SEEK | CAN_SEEK_BACKWARD | CAN_SEEK_FORWARD | CAN_PAUSE;

    return flags;
}

bool SniffLUME(const sp<DataSource> &source, String8 *mimeType, float *confidence, sp<AMessage> *) {
    if(LUMERecognizer::recognize(source)){
	*mimeType = MEDIA_MIMETYPE_CONTAINER_LUME;
	*confidence = 0.9f;
	return true;
    }

    return false;
}

}  // namespace android
using namespace android;
extern "C" {
MediaExtractor *createLUMEExtractor(const sp<DataSource> &source, String8 *mimeType, float *confidence) {
  if (SniffLUME(source, mimeType, confidence, NULL)) {
    // ALOGE("createLUMEExtractor mimeType=%s .....",mimeType->string());
    return new LUMEExtractor(source);
  }
  return NULL;
}
}
