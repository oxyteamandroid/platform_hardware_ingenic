/*
 * Copyright (C) 2009 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LUME_EXTRACTOR_H_

#define LUME_EXTRACTOR_H_

#include <media/stagefright/MediaExtractor.h>
#include <utils/Vector.h>
#include <utils/List.h>
#include <utils/threads.h>
#include <LUMEStream.h>
#include "LUMEPrefetcher.h"

#ifdef __cplusplus
extern "C"
{
    struct demuxer;
    typedef struct demuxer demuxer_t;	
    struct stream;
    typedef struct stream stream_t;	
}
#endif

namespace android {

struct AMessage;
class DataSource;
class SampleTable;
class String8;

class LUMEExtractor : public MediaExtractor {	
public:
    // Extractor assumes ownership of "source".
    LUMEExtractor(const sp<DataSource> &source);
    
    virtual size_t countTracks();
    virtual sp<MediaSource> getTrack(size_t index);
    virtual sp<MetaData> getTrackMetaData(size_t index, uint32_t flags);
    
    virtual sp<MetaData> getMetaData();

    virtual uint32_t flags() const;
    
protected:
    virtual ~LUMEExtractor();

private:
    enum MediaStreamType {  
	audioType,
	videoType,
	subType,//TODO:subtitle none included??
    };
    
    typedef struct{
        uint32_t trackID;
        uint32_t streamID;
        MediaStreamType streamType;
        uint32_t audioID;//Got from sh_audio_t.aid
        uint32_t videoID;//Get from sh_video_t.vid
	uint32_t subID;//Get from sh_sub_t.sid
        sp<MetaData> metaData;
    }TrackItem;

    sp<DataSource> mSource;
    sp<MetaData> mMeta;
    
    int mFileFormat;
    int mSubCounts;
    stream_t* mStream;
    
    Mutex mDemuxerLock;
    demuxer_t* mDemuxer;

    TrackItem* mTrackList;
    uint32_t mTrackCount;
    bool mTrackCounted;
    bool mTrackListInited;
    
    bool mHasVideo;
    bool mHasAudio;
    bool mHasSub;
    bool mStarted;

    sp<LUMEStream> mLUMEStream;
    sp<LUMEPrefetcher> mPrefetcher;

    status_t getTrackList();

    LUMEExtractor(const LUMEExtractor &);
    LUMEExtractor &operator=(const LUMEExtractor &);
};

struct LUMESourceInfo {
    demuxer_t* demuxer;
    Mutex* demuxerLock;
    sp<MetaData> meta;
    bool hasVideo;
    bool hasAudio;
    bool hasSub;
    sp<LUMEExtractor> extractor;
};

bool SniffLUME(const sp<DataSource> &source, String8 *mimeType, float *confidence, sp<AMessage> *);

}  // namespace android

#endif  // LUME_EXTRACTOR_H_
