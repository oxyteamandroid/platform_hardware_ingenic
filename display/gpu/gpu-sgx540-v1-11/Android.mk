LOCAL_PATH := $(call my-dir)

ifeq ($(strip $(TARGET_BOARD_PLATFORM_GPU)), SGX540)

# To add definition of BOARD_EGL_CFG:
# 1. Copy egl/egl.cfg to device/ingenic/xxx/config/
# 2. Add BOARD_EGL_CFG := device/ingenic/xxx/config/egl/egl.cfg to device/ingenic/xxx/BoardConfig.mk

file := $(TARGET_OUT)/libglslcompiler.so
$(file) : $(LOCAL_PATH)/libglslcompiler.so | $(ACP)
	$(transform-prebuilt-to-target)
GRANDFATHERED_ALL_PREBUILT += $(file)
$(INSTALLED_SYSTEMIMAGE): $(file)

file := $(TARGET_OUT)/lib/libIMGegl.so
$(file) : $(LOCAL_PATH)/libIMGegl.so | $(ACP)
	$(transform-prebuilt-to-target)
GRANDFATHERED_ALL_PREBUILT += $(file)
$(INSTALLED_SYSTEMIMAGE): $(file)

file := $(TARGET_OUT)/lib/libpvr2d.so
$(file) : $(LOCAL_PATH)/libpvr2d.so | $(ACP)
	$(transform-prebuilt-to-target)
GRANDFATHERED_ALL_PREBUILT += $(file)
$(INSTALLED_SYSTEMIMAGE): $(file)

file := $(TARGET_OUT)/lib/libpvrANDROID_WSEGL.so
$(file) : $(LOCAL_PATH)/libpvrANDROID_WSEGL.so | $(ACP)
	$(transform-prebuilt-to-target)
GRANDFATHERED_ALL_PREBUILT += $(file)
$(INSTALLED_SYSTEMIMAGE): $(file)

file := $(TARGET_OUT)/lib/libPVRScopeServices.so
$(file) : $(LOCAL_PATH)/libPVRScopeServices.so | $(ACP)
	$(transform-prebuilt-to-target)
GRANDFATHERED_ALL_PREBUILT += $(file)
$(INSTALLED_SYSTEMIMAGE): $(file)

file := $(TARGET_OUT)/lib/libsrv_init.so
$(file) : $(LOCAL_PATH)/libsrv_init.so | $(ACP)
	$(transform-prebuilt-to-target)
GRANDFATHERED_ALL_PREBUILT += $(file)
$(INSTALLED_SYSTEMIMAGE): $(file)

file := $(TARGET_OUT)/lib/libsrv_um.so
$(file) : $(LOCAL_PATH)/libsrv_um.so | $(ACP)
	$(transform-prebuilt-to-target)
GRANDFATHERED_ALL_PREBUILT += $(file)
$(INSTALLED_SYSTEMIMAGE): $(file)

file := $(TARGET_OUT)/lib/libusc.so
$(file) : $(LOCAL_PATH)/libusc.so | $(ACP)
	$(transform-prebuilt-to-target)
GRANDFATHERED_ALL_PREBUILT += $(file)
$(INSTALLED_SYSTEMIMAGE): $(file)

file := $(TARGET_OUT)/lib/libusc.so
$(file) : $(LOCAL_PATH)/libusc.so | $(ACP)
	$(transform-prebuilt-to-target)
GRANDFATHERED_ALL_PREBUILT += $(file)
$(INSTALLED_SYSTEMIMAGE): $(file)

file := $(TARGET_OUT)/lib/hw/gralloc.$(TARGET_BOOTLOADER_BOARD_NAME).so
$(file) : $(LOCAL_PATH)/hw/gralloc.xxx.so | $(ACP)
	$(transform-prebuilt-to-target)
GRANDFATHERED_ALL_PREBUILT += $(file)
$(INSTALLED_SYSTEMIMAGE): $(file)

file := $(TARGET_OUT)/lib/egl/libEGL_POWERVR_SGX540_130.so
$(file) : $(LOCAL_PATH)/egl/libEGL_POWERVR_SGX540_130.so | $(ACP)
	$(transform-prebuilt-to-target)
GRANDFATHERED_ALL_PREBUILT += $(file)
$(INSTALLED_SYSTEMIMAGE): $(file)

file := $(TARGET_OUT)/lib/egl/libGLESv1_CM_POWERVR_SGX540_130.so
$(file) : $(LOCAL_PATH)/egl/libGLESv1_CM_POWERVR_SGX540_130.so | $(ACP)
	$(transform-prebuilt-to-target)
GRANDFATHERED_ALL_PREBUILT += $(file)
$(INSTALLED_SYSTEMIMAGE): $(file)

file := $(TARGET_OUT)/lib/egl/libGLESv2_POWERVR_SGX540_130.so
$(file) : $(LOCAL_PATH)/egl/libGLESv2_POWERVR_SGX540_130.so | $(ACP)
	$(transform-prebuilt-to-target)
GRANDFATHERED_ALL_PREBUILT += $(file)
$(INSTALLED_SYSTEMIMAGE): $(file)

file := $(TARGET_OUT)/bin/pvrsrvctl
$(file) : $(LOCAL_PATH)/bin/pvrsrvctl | $(ACP)
	$(transform-prebuilt-to-target)
GRANDFATHERED_ALL_PREBUILT += $(file)
$(INSTALLED_SYSTEMIMAGE): $(file)

endif
