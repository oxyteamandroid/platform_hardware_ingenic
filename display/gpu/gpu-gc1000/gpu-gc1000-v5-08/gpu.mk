GPU_VIVANTE_LOCAL_PATH := hardware/ingenic/display/gpu/gpu-gc1000/gpu-gc1000-v5-08

# Name of gralloc.xxx.so and hwcomposer.xxx.so MUST equals to TARGET_BOARD_PLATFORM.
PRODUCT_COPY_FILES += \
	$(GPU_VIVANTE_LOCAL_PATH)/libGAL.so:system/lib/libGAL.so                                            \
	$(GPU_VIVANTE_LOCAL_PATH)/libGLSLC.so:system/lib/libGLSLC.so                                        \
	$(GPU_VIVANTE_LOCAL_PATH)/libVSC.so:system/lib/libVSC.so                                            \
	$(GPU_VIVANTE_LOCAL_PATH)/egl/libEGL_VIVANTE.so:system/lib/egl/libEGL_VIVANTE.so                    \
	$(GPU_VIVANTE_LOCAL_PATH)/egl/libGLESv1_CM_VIVANTE.so:system/lib/egl/libGLESv1_CM_VIVANTE.so        \
	$(GPU_VIVANTE_LOCAL_PATH)/egl/libGLESv2_VIVANTE.so:system/lib/egl/libGLESv2_VIVANTE.so              \
	$(GPU_VIVANTE_LOCAL_PATH)/hw/gralloc.default.so:system/lib/hw/gralloc.M200.so  	                    \
	$(GPU_VIVANTE_LOCAL_PATH)/hw/hwcomposer.default.so:system/lib/hw/hwcomposer.M200.so
