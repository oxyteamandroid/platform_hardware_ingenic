LOCAL_PATH:= $(call my-dir)
ifeq ($(strip $(TARGET_BOARD_PLATFORM_GPU)), false)

include $(CLEAR_VARS)

LOCAL_SRC_FILES:= \
	x2d.c 

LOCAL_SHARED_LIBRARIES:= \
	libutils \
	liblog \
	libbinder \
	libcutils \
	libhardware \
	libEGL \
	libhardware_legacy \
	libz	\
	libdmmu

#LOCAL_CFLAGS += -DCAMERA_INFO_MODULE=\"$(PRODUCT_MODEL)\"
#LOCAL_CFLAGS += -DCAMERA_INFO_MANUFACTURER=\"$(PRODUCT_MANUFACTURER)\"

LOCAL_C_INCLUDES += \
	external/jpeg \
	external/jhead \
	hardware/libhardware/include \
	hardware/libhardware/modules/gralloc \
	kernel/driver/misc/jz_x2d \
	hardware/ingenic/dmmu/dmmu \
	external/neven/FaceRecEm/common/src/b_FDSDK \
	hardware/ingenic/xb4780/hwcomposer-SGX540


#LOCAL_MODULE_PATH := out/target/product/warrior/root
LOCAL_MODULE_PATH := $(TARGET_OUT_SHARED_LIBRARIES)
LOCAL_MODULE_TAGS := optional

LOCAL_MODULE := libx2d

include $(BUILD_SHARED_LIBRARY)

###################################################

#LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)
LOCAL_SRC_FILES:= x2d_test.c
LOCAL_SHARED_LIBRARIES := libc \
	libdmmu \
	libx2d \
	liblog

LOCAL_C_INCLUDES += \
	hardware/ingenic/dmmu/dmmu

LOCAL_MODULE_PATH := $(TARGET_OUT_SHARED_LIBRARIES)
LOCAL_MODULE_TAGS := debug
LOCAL_CFLAGS := 
LOCAL_MODULE:= x2d_test
include $(BUILD_EXECUTABLE)
endif #($(strip $(TARGET_BOARD_PLATFORM_GPU)), false)
