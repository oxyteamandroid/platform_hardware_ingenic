#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdlib.h>		/* for convenience */
#include <stddef.h>		/* for offsetof */
#include <string.h>		/* for convenience */
#include <unistd.h>		/* for convenience */
#include <errno.h>		/* for definition of errno */
#include <stdarg.h>		/* ISO C variable arguments */
#include <cutils/log.h>
#include <system/graphics.h>

#include "x2d.h"
#include "dmmu.h"

#define SRC_W 1024
#define SRC_H 768
#define DST_W 1280
#define DST_H 720
#define SRC_SIZE SRC_W*SRC_H + SRC_W*SRC_H/2   //indicate yuv420 format
#define DST_SIZE DST_W*DST_H*4                 //indicate rgb888

#define MAXLINE 1024

struct x2d_hal_info *x2d = NULL;
static unsigned int tlb_base_phys;

static void	err_doit(int, int, const char *, va_list);

static int
err_exit(const char *fmt, ...)
{
	va_list		ap;

	va_start(ap, fmt);
	err_doit(0, 0, fmt, ap);
	va_end(ap);
	exit(-1);
}

static int
err_sys(const char *fmt, ...)
{
	va_list		ap;

	va_start(ap, fmt);
	err_doit(1, errno, fmt, ap);
	va_end(ap);
	exit(-1);
}

static void
err_doit(int errnoflag, int error, const char *fmt, va_list ap)
{
	char	buf[MAXLINE];

	vsnprintf(buf, MAXLINE, fmt, ap);
	if (errnoflag)
		snprintf(buf+strlen(buf), MAXLINE-strlen(buf), ": %s",
		  strerror(error));
	strcat(buf, "\n");
	ALOGE("%s", buf);
}

static int init_x2d_src(struct x2d_hal_info *mx2d, void *src_buf)
{
	struct src_layer (*layer)[4] = NULL;
	struct x2d_glb_info *glb_info = NULL;

	layer = mx2d->layer;
	glb_info = mx2d->glb_info;
	if (!layer || !glb_info)
		err_exit("parameter is NULL! layer: %p, glb_info: %p", layer, glb_info);

	if (!src_buf)
		err_exit("src buf is NULL");

	glb_info->layer_num = 1;
	layer[0]->format = HAL_PIXEL_FORMAT_JZ_YUV_420_B;
	layer[0]->in_width = SRC_W;
	layer[0]->in_height = SRC_H;
	layer[0]->out_width = DST_W;
	layer[0]->out_height = DST_H;

	layer[0]->addr = (unsigned int)src_buf;
	layer[0]->u_addr = (unsigned int)((char *)src_buf + SRC_W*SRC_H);
	layer[0]->v_addr = layer[0]->u_addr;

	layer[0]->y_stride = SRC_W * 16;
	layer[0]->v_stride = SRC_W * 8;

	layer[0]->glb_alpha_en = 1;
	layer[0]->global_alpha_val = 0xff;
	layer[0]->preRGB_en = 1;

	if (x2d_src_init(mx2d) < 0)
		err_exit("x2d src hal init failed");

	return 0;
}

static int init_x2d_dst(struct x2d_hal_info *mx2d, void *dst_buf)
{
	struct x2d_glb_info *glb_info = NULL;
	struct x2d_dst_info *dst_info = NULL;
	struct src_layer (*layer)[4] = NULL;

	glb_info = mx2d->glb_info;
	dst_info = mx2d->dst_info;
	layer = mx2d->layer;
	if (!glb_info || !dst_info || !layer)
		err_exit("parameter is NULL glb_info:%p, dst_info: %p, layer: %p",
				glb_info, dst_info, layer);

	layer[0]->out_w_offset = 0;
	layer[0]->out_h_offset = 0;

	glb_info->tlb_base = tlb_base_phys;
	dst_info->dst_address = (unsigned int)dst_buf;
	dst_info->dst_format = HAL_PIXEL_FORMAT_RGBA_8888;
	dst_info->dst_width = DST_W;
	dst_info->dst_height = DST_H;
	dst_info->dst_stride = DST_W * 4;
	dst_info->dst_alpha_val = 0xff;

	if (x2d_dst_init(mx2d) < 0)
		err_exit("x2d dst init failed");

	return 0;
}

int main(int argc, char **argv)
{
	int fd = -1, ret = 0, i = 0;
	void *src_buf = NULL, *dst_buf = NULL;

	ALOGE("line: %d", __LINE__);
    /* malloc buffer */
	src_buf = calloc(SRC_SIZE, sizeof(char));
	if (!src_buf)
		err_sys("src_buf calloc failed");
	dst_buf = calloc(DST_SIZE, sizeof(char));
	if (!dst_buf)
		err_sys("dst_buf calloc failed");

	for (i = 0; i < 10; i++)
		ALOGE("Before x2d convert %p: %08x", (volatile unsigned int *)dst_buf + i*1280,
			*((volatile unsigned int *)dst_buf + i*1280));

	ALOGE("line: %d", __LINE__);
	/* dmmu init */
	if ((ret = dmmu_init()) < 0)
		err_exit("dmmu_init failed");
	/* dmmu get tlb base address */
	if (dmmu_get_page_table_base_phys(&tlb_base_phys) < 0)
		err_exit("dmmu get tlb base phys failed");

	ALOGE("line: %d", __LINE__);
	/* map address */
	if ((ret = dmmu_map_user_mem(src_buf, SRC_SIZE)) < 0)
		err_exit("dmmu map src mem info failed");
	if ((ret = dmmu_map_user_mem(dst_buf, DST_SIZE)) < 0)
		err_exit("dmmu map dst mem info failed");

	ALOGE("line: %d", __LINE__);
	/* open x2d */
	if ((fd = x2d_open(&x2d)) < 0)
		err_sys("x2d open failed");

	/* init x2d src */
	if ((init_x2d_src(x2d, src_buf)) < 0)
		err_exit("init_x2d_src failed");

	/* init x2d dst */
	if ((init_x2d_dst(x2d, dst_buf)) < 0)
		err_exit("init_x2d_dst failed");

	ALOGE("line: %d", __LINE__);
	/* start x2d */
	if ((ret = x2d_start(x2d)) < 0)
		err_exit("x2d_start failed");

	ALOGE("line: %d", __LINE__);
	/* check data */
	for (i = 0; i < 10; i++)
		ALOGE("after x2d convert %p: %08x", (volatile unsigned int *)dst_buf + i*1280,
			*((volatile unsigned int *)dst_buf + i*1280));

	ALOGE("line: %d", __LINE__);
	/* unmap address */
	if ((ret = dmmu_unmap_user_mem(src_buf, SRC_SIZE)) < 0)
		err_exit("dmmu unmap src mem info failed");
	if ((ret = dmmu_unmap_user_mem(dst_buf, SRC_SIZE)) < 0)
		err_exit("dmmu unmap dst mem info failed");

	free(src_buf);
	free(dst_buf);

	ALOGE("line: %d", __LINE__);
	dmmu_deinit();
	x2d_release(&x2d);

	exit(0);
}
