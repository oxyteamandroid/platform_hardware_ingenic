#include <stdio.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <stdlib.h>

#define MAX_ALLOC_NUM	10000

struct mem_info {
	void *ptr;
	int len;
};

static struct mem_info mem_ptrs[MAX_ALLOC_NUM];

int main() {
	int i = 0;
	int count = 0;
	int count1 = 0;

	srand(time(NULL));

	printf("=====>start frag......\n");

	while (1) {
		count = 0;

		for (i = 0; i < MAX_ALLOC_NUM; i++) {
			int alloc_size = rand() % (4 * 1024);
			mem_ptrs[i].ptr = malloc(alloc_size);
			if (!mem_ptrs[i].ptr) {
				printf("===>mem exhausting......\n");
				break;
			} else {
				memset(mem_ptrs[i].ptr, 1, alloc_size);
				count++;
			}
		}

		printf("====>returning memory(count = %d)\n", count);
		sleep(5);

		for (i = 0; i < MAX_ALLOC_NUM; i++) {
			if (mem_ptrs[i].ptr) {
				free(mem_ptrs[i].ptr);
				mem_ptrs[i].ptr = NULL;
			}
		}
		count1++;	
		printf("===>frag done...count=%d   count1=%d\n",count,count1);
	}

	return 0;
}

