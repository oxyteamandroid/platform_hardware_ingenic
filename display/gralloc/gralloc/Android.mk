LOCAL_PATH := $(call my-dir)
ifeq ($(strip $(TARGET_BOARD_PLATFORM_GPU)), false)
# HAL module implemenation stored in
# hw/<OVERLAY_HARDWARE_MODULE_ID>.<ro.product.board>.so
include $(CLEAR_VARS)

LOCAL_MODULE_PATH := $(TARGET_OUT_SHARED_LIBRARIES)/hw
LOCAL_SHARED_LIBRARIES := liblog libcutils

LOCAL_SRC_FILES := 	\
	gralloc.cpp 	\
	framebuffer.cpp \
	mapper.cpp

LOCAL_MODULE := gralloc.$(TARGET_BOOTLOADER_BOARD_NAME)
LOCAL_CFLAGS:= -DLOG_TAG=\"gralloc\"

include $(BUILD_SHARED_LIBRARY)
endif
