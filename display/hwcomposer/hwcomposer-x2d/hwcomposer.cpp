#include <errno.h>
#include <malloc.h>
#include <stdlib.h>
#include <stdarg.h>
#include <fcntl.h>
#include <poll.h>
#include <sys/ioctl.h>
#include <sys/resource.h>
#include <linux/fb.h>
#include <sys/mman.h>

#include <cutils/properties.h>
#include <cutils/log.h>
#include <cutils/native_handle.h>
#include <cutils/memory.h>
#include <utils/StopWatch.h>
#include <hardware/hardware.h>
#include <hardware/gralloc.h>
#include <hardware/hwcomposer.h>
#include <EGL/egl.h>
#include <EGL/eglext.h>

#include <utils/Timers.h>
#include <hardware_legacy/uevent.h>

#include <system/graphics.h>

#include "gralloc_priv.h"
#include "dmmu.h"
#include "hwcomposer.h"
#include "hal_public.h"

#define X2DNAME "/dev/x2d"
#define FB0DEV  "/dev/graphics/fb0"
#define FB1DEV  "/dev/graphics/fb1"
#define X2D_SCALE_FACTOR 512.0
#define HW_SUPPORT_LAY_NUM 4
#define INVALID_FORMAT 0xa

#define WIDTH(rect) ((rect).right - (rect).left)
#define HEIGHT(rect) ((rect).bottom - (rect).top)
#define DWORD_ALIGN(a) (((a) >> 3) << 3)

//#define X2D_DEBUG
#undef X2D_DEBUG

#ifndef X2D_DEBUG_LOGD
#undef ALOGED
#define ALOGED //
#endif

#define LOG_TAG "HWC-JZX2D"

static int num = 0;
struct jz_layer{
    int flag;
    int y_addr;
};

struct jz_hwc_device {
    hwc_composer_device_1_t base;
    hwc_procs_t *procs;
	jz_layer tmp_layer[4];
	struct jz_x2d_config x2d_pix_glb_config;

    pthread_mutex_t lock;

    int x2d_fd;
    int render_layer_num;
	void *maped_addr[4];
    int handle_rect[4][3];
	int video_rect[3][2];

    struct jz_x2d_config x2d_cfg;

    int geometryChanged;
    int hasComposition;
    unsigned int gtlb_base;

	int x2d_timeout;

    gralloc_module_t const* gralloc_module;
    struct hwc_framebuffer_info_t *fbDev;
    alloc_device_t* grDev;
    pthread_t vsync_thread;

	void *pix_glb_buffer;
};

typedef struct jz_hwc_device jz_hwc_device_t;

static void dump_layer(hwc_layer_1_t const* l) {
    ALOGE("dump_layer: %p", l);
    ALOGE("type=%d, flags=%08x, handle=%p, tr=%02x, blend=%04x, global_alpha=%02x",
         l->compositionType, l->flags, l->handle, l->transform, l->blending, l->global_alpha);
    ALOGE("sourceCrop{%d,%d,%d,%d}, displayFrame{%d,%d,%d,%d}",
         l->sourceCrop.left,
         l->sourceCrop.top,
         l->sourceCrop.right,
         l->sourceCrop.bottom,
         l->displayFrame.left,
         l->displayFrame.top,
         l->displayFrame.right,
         l->displayFrame.bottom
         );

    hwc_rect_t const* r;
    size_t rc, numRects;
    numRects = l->visibleRegionScreen.numRects;
    r = l->visibleRegionScreen.rects;
    ALOGE("visibleRegionScreen.numRects=%d", numRects);
    /* dump visibleRegionScreen */
    for (rc=0; rc < numRects; rc++) {
        ALOGE("visibleRegionScreen rect%d (%d, %d, %d, %d)",
             rc, r->left, r->top, r->right, r->bottom);
        r++;
    }
}

void x2d_dump_cfg(struct jz_x2d_config x2d_cfg)
{
	int i=0;
	ALOGE("++++++++++++++++++++++++start dump x2d config++++++++++++++++++++++++++++++++");
    ALOGE("jz_x2d_config watchdog_cnt: %d\n tlb_base: %08x", x2d_cfg.watchdog_cnt, x2d_cfg.tlb_base);
    ALOGE("dst_address: %08x", x2d_cfg.dst_address);
    ALOGE("dst_alpha_val: %d\n dst_stride:%d\n dst_mask_val:%08x",
         x2d_cfg.dst_alpha_val, x2d_cfg.dst_stride, x2d_cfg.dst_mask_val);
    ALOGE("dst_width: %d\n dst_height:%d\n dst_bcground:%08x",
         x2d_cfg.dst_width, x2d_cfg.dst_height, x2d_cfg.dst_bcground);
    ALOGE("dst_format: %d\n dst_back_en:%08x\n dst_preRGB_en:%08x",
         x2d_cfg.dst_format, x2d_cfg.dst_back_en, x2d_cfg.dst_preRGB_en);
    ALOGE("dst_glb_alpha_en: %d\ndst_mask_en:%08x\n x2d_cfg.layer_num: %d",
         x2d_cfg.dst_glb_alpha_en, x2d_cfg.dst_mask_en, x2d_cfg.layer_num);

	for(i=0;i<x2d_cfg.layer_num;i++){
        ALOGE("layer[%d]: ++++++++++++++++++++++++++\n", i);
        ALOGE("format: %d\n transform: %d\n global_alpha_val: %d\n argb_order: %d",
             x2d_cfg.lay[i].format, x2d_cfg.lay[i].transform,
             x2d_cfg.lay[i].global_alpha_val, x2d_cfg.lay[i].argb_order);
        ALOGE("osd_mode: %d\n preRGB_en: %d\n glb_alpha_en: %d\n mask_en: %d\n msk_val=%x",
             x2d_cfg.lay[i].osd_mode, x2d_cfg.lay[i].preRGB_en,
             x2d_cfg.lay[i].glb_alpha_en, x2d_cfg.lay[i].mask_en,x2d_cfg.lay[i].msk_val);
        ALOGE("color_cov_en: %d\n in_width: %d\n in_height: %d\n out_width: %d",
             x2d_cfg.lay[i].color_cov_en, x2d_cfg.lay[i].in_width,
             x2d_cfg.lay[i].in_height, x2d_cfg.lay[i].out_width);
        ALOGE("out_height: %d\n out_w_offset: %d\n out_h_offset: %d\n v_scale_ratio: %d",
             x2d_cfg.lay[i].out_height, x2d_cfg.lay[i].out_w_offset,
             x2d_cfg.lay[i].out_h_offset, x2d_cfg.lay[i].v_scale_ratio);
        ALOGE("h_scale_ratio: %d\n yuv address addr: %08x\n u_addr: %08x\n v_addr: %08x",
             x2d_cfg.lay[i].h_scale_ratio, x2d_cfg.lay[i].addr,
             x2d_cfg.lay[i].u_addr, x2d_cfg.lay[i].v_addr);
        ALOGE("y_stride: %d\n v_stride: %d\n",
             x2d_cfg.lay[i].y_stride, x2d_cfg.lay[i].v_stride);
	}
	ALOGE("++++++++++++++++++++++++end dump x2d config++++++++++++++++++++++++++++++++");
}


static void dump_x2d_config(jz_hwc_device_t *hwc_dev)
{
    int i, ret;
    struct jz_x2d_config cfg;

    ALOGED("----------------get_x2d_config");
    ret = ioctl(hwc_dev->x2d_fd, IOCTL_X2D_GET_SYSINFO, &cfg);
    if (ret < 0) {
        ALOGED("IOCTL_X2D_GET_SYSINFO failed!\n");
    }
    ALOGED("dst  addr: 0x%x   width:  %d  height: %d",
         cfg.dst_address, cfg.dst_width, cfg.dst_height);
    ALOGED("overlay num is: %d",cfg.layer_num);
    for(i=0; i<cfg.layer_num; i++) {
        ALOGED("src layer: %d",i);
        ALOGED("addr: 0x%x   width: %d  height: %d", cfg.lay[i].addr,
             cfg.lay[i].in_width, cfg.lay[i].in_height);
        ALOGED("out width: %d  height: %d",
             cfg.lay[i].out_width, cfg.lay[i].out_height);
        ALOGED("out pos x: %d y: %d", cfg.lay[i].out_w_offset,
             cfg.lay[i].out_h_offset);
    }
}

static void jz_hwc_dump(hwc_composer_device_1_t *dev, char *buff, int buff_len)
{

}

static void jz_hwc_registerProcs(hwc_composer_device_1_t *dev,
                                 hwc_procs_t const *procs)
{
    jz_hwc_device_t *hwc_dev = (jz_hwc_device_t *)dev;

    hwc_dev->procs = (typeof(hwc_dev->procs)) procs;
}

static void deinit_dev_para(jz_hwc_device_t *hwc_dev)
{
    int i;

    hwc_dev->render_layer_num = 0;
    memset((void *)&hwc_dev->x2d_cfg, 0, sizeof(struct jz_x2d_config));
}

static int get_bpp(hwc_layer_1_t *layer, void *handle)
{
    int bpp = 0;
	int format = 0;

	if (layer->flags & HWC_INGENIC_VIDEO_LAYER_X2D) {
		format = ((struct VideoFrameInfo *)handle)->format;
	} else {

		format = ((struct private_handle_t *)handle)->format;
	}
    switch (format) {
	case HAL_PIXEL_FORMAT_RGB_565:
		bpp = 2;
		break;
	case HAL_PIXEL_FORMAT_RGBA_8888:
	case HAL_PIXEL_FORMAT_BGRA_8888:
	case HAL_PIXEL_FORMAT_RGBX_8888:
	case HAL_PIXEL_FORMAT_BGRX_8888:
		bpp = 4;
		break;
	case HAL_PIXEL_FORMAT_YV12:
	case HAL_PIXEL_FORMAT_YCrCb_420_SP:
		bpp = 16;
		break;
	case HAL_PIXEL_FORMAT_JZ_YUV_420_P:
	case HAL_PIXEL_FORMAT_JZ_YUV_420_B:
		bpp = 16;
		break;
	default:
		ALOGE("get_bpp unknown layer data format 0x%08x", format);
		bpp = 4;
		break;
    }

    return bpp;
}

static int map_lay_addr(struct src_layer *config,
                        private_handle_t *handle, hwc_layer_1_t *layer)
{
    int bpp = 0;
    int ret = 0;
    struct dmmu_mem_info mem;

    bpp = get_bpp(layer, (void *)handle);
    memset(&mem, 0, sizeof(struct dmmu_mem_info));
    mem.vaddr = (void *)config->addr;
    mem.size = handle->stride * handle->height * bpp;
    if (mem.vaddr) {
        ret = dmmu_map_user_mem(mem.vaddr, mem.size);
        if (ret < 0) {
            ALOGE("dmmu_map_user_mem in layers failed!~~~~~~~~~>");
            return -1;
        }
    } else {
        ALOGE("mem.vaddr is NULL !!!!!!!");
        return -1;
    }

    return 0;
}

static int map_video_layer_addrs(struct VideoFrameInfo *handle)
{
    if (handle->dmmu_maped) {
        ALOGED("************************** handle->dmmu_maped skip map_video_layer_addrs return.");
        return 0;
    }

    int ret = 0;
    int i;

    for (i = 0; i < handle->count; i++) {
        void * vaddr = (void *)handle->addr[i];
        int size = handle->size[i];
        if (vaddr) {
            dmmu_match_user_mem_tlb(vaddr, size);
            ret = dmmu_map_user_mem(vaddr, size);
            if (ret < 0) {
                ALOGE("dmmu_map_user_mem in layers failed!~~~~~~~~~>");
                return -1;
            }
        } else {
            ALOGE("mem.vaddr is NULL !!!!!!!");
            return -1;
        }
    }

    return 0;
}
static int unmap_addr( void *vaddr,int size)
{
    int ret = 0;
    ret = dmmu_unmap_user_mem(vaddr, size);
    if (ret < 0) {
        ALOGE("dmmu_map_user_mem failed!~~~~~~~~~>");
        return -1;
    }

    return 0;
}

static int map_addr( void *vaddr,int size)
{
    int ret = 0;

    dmmu_match_user_mem_tlb(vaddr, size);
    ret = dmmu_map_user_mem(vaddr, size);
    if (ret < 0) {
        ALOGE("dmmu_map_user_mem failed!~~~~~~~~~>");
        return -1;
    }

    return 0;
}

static int map_dst_addr(jz_hwc_device_t *hwc_dev, int i)
{
    int ret = 0;
    struct dmmu_mem_info mem;

    memset(&mem, 0, sizeof(struct dmmu_mem_info));
    mem.vaddr = (void *)hwc_dev->fbDev->addr[i];
    mem.size = hwc_dev->fbDev->fix_info.line_length * hwc_dev->fbDev->var_info.height;
    ret = dmmu_map_user_mem(mem.vaddr, mem.size);
    if (ret < 0) {
        ALOGE("dmmu_map_user_mem failed!~~~~~~~~~>");
        return -1;
    }

    return 0;
}

static void unmap_dst_addr(jz_hwc_device_t *hwc_dev)
{
    int ret;
    struct dmmu_mem_info mem;

    memset(&mem, 0, sizeof(struct dmmu_mem_info));
    mem.vaddr = (void *)hwc_dev->fbDev->addr[hwc_dev->fbDev->buf_index];
    mem.size = hwc_dev->fbDev->fix_info.line_length * hwc_dev->fbDev->var_info.height;
    ret = dmmu_unmap_user_mem(mem.vaddr, mem.size);
    if (ret < 0) {
        ALOGE("%s %s %d: dmmu_unmap_user_mem failed", __FILE__, __FUNCTION__, __LINE__);
    }
}

static void unmap_layers_addr(jz_hwc_device_t *hwc_dev, hwc_display_contents_1_t *list)
{
    int ret = 0;
    int i, j;

    for (i = 0; i < 4; i++) {
        struct dmmu_mem_info mem;

        memset(&mem, 0, sizeof(struct dmmu_mem_info));
		mem.vaddr = hwc_dev->maped_addr[i];
        ALOGED("addr[%d]=%x  w: %d, h: %d, bpp: %d~~~~~~~>",
			i,hwc_dev->maped_addr[i],	hwc_dev->handle_rect[i][0],
            hwc_dev->handle_rect[i][1], hwc_dev->handle_rect[i][2]);
        //        mem.size = hwc_dev->x2d_cfg.lay[i].in_width * hwc_dev->x2d_cfg.lay[i].in_height * 4;
        mem.size = hwc_dev->handle_rect[i][0] * hwc_dev->handle_rect[i][1] * hwc_dev->handle_rect[i][2];
        if (mem.vaddr && mem.size) {
            ret = dmmu_unmap_user_mem(mem.vaddr, mem.size);
            if (ret < 0) {
                ALOGE("%s %s %d: dmmu_unmap_user_mem failed", __FILE__, __FUNCTION__, __LINE__);
                return;
            }
        }
    }

	for (i = 0; i < 3; i++) {
        struct dmmu_mem_info mem;

        memset(&mem, 0, sizeof(struct dmmu_mem_info));
		mem.vaddr = (void *)hwc_dev->video_rect[i][0];
		mem.size = hwc_dev->video_rect[i][1];
        if (mem.vaddr && mem.size) {
            ret = dmmu_unmap_user_mem(mem.vaddr, mem.size);
            if (ret < 0) {
                ALOGE("%s %s %d: dmmu_unmap_user_mem failed", __FILE__, __FUNCTION__, __LINE__);
                //return;
            }
        } else {
            break;
        }
	}

    for (i = 0; i < 4; i++) {
		hwc_dev->maped_addr[i] = NULL;
        for (j = 0; j < 3; j++) {
            hwc_dev->handle_rect[i][j] = 0;
            hwc_dev->handle_rect[i][j] = 0;
            hwc_dev->handle_rect[i][j] = 0;
		}
	}

	for (i = 0; i < 3; i++)
		for (j = 0; j < 2; j++) {
			hwc_dev->video_rect[i][j] = 0;
			hwc_dev->video_rect[i][j] = 0;
		}
}

static int is_null_addr(hwc_layer_1_t *layer)
{
    void *addr0 = NULL;
	void *addr1 = NULL;

	if (layer->flags & HWC_INGENIC_VIDEO_LAYER_X2D) {
		addr0 = ((struct VideoFrameInfo *)layer->handle)->addr[0];
		addr1 = ((struct VideoFrameInfo *)layer->handle)->addr[1];
		addr0 = (void *)(((unsigned int)addr0 >> 12) << 12);
		addr1 = (void *)(((unsigned int)addr1 >> 12) << 12);
		if (!addr0 || !addr1)
			return 1;
	}
	return 0;
}

static int is_supported_format(hwc_layer_1_t *layer)
{
	int format;

	if (layer->flags & HWC_INGENIC_VIDEO_LAYER_X2D) {
		format = ((struct VideoFrameInfo *)layer->handle)->format;
	} else {
		format = ((private_handle_t *)layer->handle)->format;
	}

	if (format == INVALID_FORMAT)
		return 0;

    switch (format) {
	case HAL_PIXEL_FORMAT_RGB_565:
	case HAL_PIXEL_FORMAT_RGBA_8888:
	case HAL_PIXEL_FORMAT_RGBX_8888:
	case HAL_PIXEL_FORMAT_BGRX_8888:
	case HAL_PIXEL_FORMAT_BGRA_8888:
	case HAL_PIXEL_FORMAT_YCrCb_420_SP:
	case HAL_PIXEL_FORMAT_YV12:
	case HAL_PIXEL_FORMAT_JZ_YUV_420_P:
	case HAL_PIXEL_FORMAT_JZ_YUV_420_B:
		return 1;
	default:
		return 0;
	}
}

static int is_support_this_layer(hwc_layer_1_t *layer)
{
    int width, height;
	void *handle = NULL;

    //dump_layer(layer);

	handle = (void *)(layer->handle);

	if (handle) {
		if (is_null_addr(layer)) {
			ALOGE("null addr !!!");
			return 0;
		}

		if (!is_supported_format(layer)) {
			ALOGED("format not support!!!");
			return 0;
		}
	} else {
		if (!strcmp(layer->TypeID, "LayerScreenshot"))
			return 0;
	}

	if (layer->flags & HWC_INGENIC_VIDEO_LAYER_DFB) {
		ALOGE("video layer ipu direct mode!");
		return 0;
	}

	if (layer->flags & HWC_SKIP_LAYER){
		ALOGE("hwc skip layer");
		return 0;
	}
    return 1;
}

static void setup_config_dim(jz_hwc_device_t *hwc_dev,struct src_layer *config, hwc_layer_1_t *layer)
{
//	config->glb_alpha_en = 1;
//	config->global_alpha_val = layer->global_alpha;
	config->mask_en = 1;
	config->msk_val = layer->global_alpha<<24;
    config->out_width = WIDTH(layer->displayFrame);
    config->out_height = HEIGHT(layer->displayFrame);
    config->out_w_offset = layer->displayFrame.left;
    config->out_h_offset = layer->displayFrame.top;

	if ((config->out_w_offset < 0) || (config->out_w_offset > (int)hwc_dev->fbDev->var_info.width)) {
		if (config->out_w_offset < 0) {
			config->out_w_offset = 0;
		}
		if (config->out_w_offset > (int)hwc_dev->fbDev->var_info.width) {
			config->out_w_offset = hwc_dev->fbDev->var_info.width;
		}
	}
	if ((config->out_h_offset < 0) || (config->out_h_offset > (int)hwc_dev->fbDev->var_info.height)) {
		if (config->out_h_offset < 0) {
			config->out_h_offset = 0;
		}
		if (config->out_h_offset > (int)hwc_dev->fbDev->var_info.height) {
			config->out_h_offset = (int)hwc_dev->fbDev->var_info.height;
		}
	}
	if ((config->out_width + config->out_w_offset) > (int)hwc_dev->fbDev->var_info.width) {
		config->out_width = hwc_dev->fbDev->var_info.width - config->out_w_offset;
	}
	if ((config->out_height + config->out_h_offset) > (int)hwc_dev->fbDev->var_info.height) {
		config->out_height = hwc_dev->fbDev->var_info.height - config->out_h_offset;
	}
}

static void setup_pix_glb_config_addr(jz_hwc_device_t *hwc_dev,
                              struct src_layer *config, hwc_layer_1_t *layer)
{
	int bpp = 0,ret = 0;
	private_handle_t *handle = (private_handle_t *)(layer->handle);
	// ALOGE("hwcomposer ****************** width = %d,height = %d,format = %08x,stride = %d",
		//	handle->width,handle->height,handle->format,handle->stride);
	ret = hwc_dev->gralloc_module->lock(hwc_dev->gralloc_module, (buffer_handle_t)handle,
			GRALLOC_USAGE_SW_READ_OFTEN,
			0, 0, handle->width, handle->height,
			(void **)&config->addr);
	if (ret) {
		ALOGE("%s %s %d: Get source vaddr error", __FILE__, __FUNCTION__, __LINE__);
		return;
	}

    bpp = get_bpp(layer, (void *)handle);
	map_addr((void *)config->addr,handle->stride * handle->height * bpp);

	ret = hwc_dev->gralloc_module->unlock(hwc_dev->gralloc_module, (buffer_handle_t)handle);
	if (ret) {
		ALOGE("%s %s %d: Get source vaddr error", __FILE__, __FUNCTION__, __LINE__);
		return;
	}

}


static void setup_config_addr(jz_hwc_device_t *hwc_dev,
                              struct src_layer *config, hwc_layer_1_t *layer, int j)
{
    int ret = 0, offset=0, bpp=0;
	int tmp_layer_addr = 0;

	if (layer->flags & HWC_INGENIC_VIDEO_LAYER_X2D) {
		struct VideoFrameInfo *handle = (struct VideoFrameInfo *)(layer->handle);

		if (handle->format == HAL_PIXEL_FORMAT_JZ_YUV_420_B) {
			config->addr = (int)handle->addr[0];
			config->u_addr = (int)handle->addr[1];
			config->v_addr = (int)handle->addr[1];
		} else {
			config->addr = (int)handle->addr[0];
			config->u_addr = (int)handle->addr[1];
			config->v_addr = (int)handle->addr[2];
		}
		ALOGE("config->addr: %08x u_addr: %08x v_addr: %08x================>",
			 config->addr, config->u_addr, config->v_addr);
		map_video_layer_addrs(handle);
	} else {

		private_handle_t *handle = (private_handle_t *)(layer->handle);
		if((void *)config->addr == NULL ){

			// ALOGE("hwcomposer ****** width = %d,height = %d,format = %08x,stride = %d",
							//handle->width,handle->height,handle->format,handle->stride);
			ret = hwc_dev->gralloc_module->lock(hwc_dev->gralloc_module, (buffer_handle_t)handle,
					GRALLOC_USAGE_SW_READ_OFTEN,
					0, 0, handle->width, handle->height,
					(void **)&config->addr);
			if (ret) {
				ALOGE("%s %s %d: Get source vaddr error", __FILE__, __FUNCTION__, __LINE__);
				return;
			}

			hwc_dev->maped_addr[j] = (void *)config->addr;

			ret = hwc_dev->gralloc_module->unlock(hwc_dev->gralloc_module, (buffer_handle_t)handle);
			if (ret) {
				ALOGE("%s %s %d: Get source vaddr error", __FILE__, __FUNCTION__, __LINE__);
				return;
			}
			ret = map_lay_addr(config, handle, layer);
			if (ret < 0) {
				ALOGE("map_lay_addr failed!");
				return;
			}

		}else{
			ret = hwc_dev->gralloc_module->lock(hwc_dev->gralloc_module, (buffer_handle_t)handle,
					GRALLOC_USAGE_SW_READ_OFTEN,
					0, 0, handle->width, handle->height,
					(void **)&tmp_layer_addr);
			if (ret) {
				ALOGE("%s %s %d: Get source vaddr error", __FILE__, __FUNCTION__, __LINE__);
				return;
			}
			hwc_dev->maped_addr[j] = (void *)tmp_layer_addr;
			ret = hwc_dev->gralloc_module->unlock(hwc_dev->gralloc_module, (buffer_handle_t)handle);
			if (ret) {
				ALOGE("%s %s %d: Get source vaddr error", __FILE__, __FUNCTION__, __LINE__);
				return;
			}
		}

		bpp = get_bpp(layer, (void *)handle);
		if (layer->sourceCrop.left || layer->sourceCrop.top) {
			offset = ((layer->sourceCrop.top)*handle->stride + (layer->sourceCrop.left)) * bpp;
			config->addr += offset;
			ALOGED("++++++++++++++++config->addr: %08x, get_bpp(): %d",
					config->addr, get_bpp(layer, (void *)handle));
			config->addr = DWORD_ALIGN(config->addr);
			ALOGED("++++++++++++++++config->addr: %08x, get_bpp(): %d",
					config->addr, get_bpp(layer, (void *)handle));
		}
	}

}

static void setup_config_alpha(struct src_layer *config, hwc_layer_1_t *layer)
{
    switch (layer->blending) {
	case HWC_BLENDING_NONE:
		config->glb_alpha_en = 1;
		config->global_alpha_val = layer->global_alpha;
		break;
	case HWC_BLENDING_COVERAGE:
	case HWC_BLENDING_PREMULT:
            break;
    }
}

static void setup_config_transform(struct src_layer *config, hwc_layer_1_t *layer)
{
    switch (layer->transform) {
	case HWC_TRANSFORM_FLIP_H:
            ALOGED("HWC_TRANSFORM_FLIP_H~~~~");
            config->transform = X2D_H_MIRROR;
            break;
	case HWC_TRANSFORM_FLIP_V:
            ALOGED("HWC_TRANSFORM_FLIP_V~~~~");
            config->transform = X2D_V_MIRROR;
            break;
	case HWC_TRANSFORM_ROT_90:
            ALOGED("HWC_TRANSFORM_ROT_90~~~~");
            config->transform = X2D_ROTATE_90;
            break;
	case HWC_TRANSFORM_ROT_180:
            ALOGED("HWC_TRANSFORM_ROT_180~~~~");
            config->transform = X2D_ROTATE_180;
            break;
	case HWC_TRANSFORM_ROT_270:
            ALOGED("HWC_TRANSFORM_ROT_270~~~~");
            config->transform = X2D_ROTATE_270;
            break;
	default:
            ALOGED("HWC_TRANSFORM_ROT_0~~~~");
            config->transform = X2D_ROTATE_0;
            break;
    }
}

static void setup_config_format(struct src_layer *config, hwc_layer_1_t *layer)
{
	int format;

	if (layer->flags & HWC_INGENIC_VIDEO_LAYER_X2D) {
		format = ((struct VideoFrameInfo *)layer->handle)->format;
	} else {
		format = ((private_handle_t *)layer->handle)->format;
	}

    switch (format) {
	case HAL_PIXEL_FORMAT_RGB_565:
#ifdef X2D_DEBUG
		ALOGE("1------------HAL_PIXEL_FORMAT_RGB_565");
#endif
		config->argb_order = X2D_RGBORDER_RGB;
		config->format = X2D_INFORMAT_RGB565;
		config->glb_alpha_en = 1;
		config->global_alpha_val = layer->global_alpha;
		break;
	case HAL_PIXEL_FORMAT_RGBA_8888:
#ifdef X2D_DEBUG
		ALOGE("2------------HAL_PIXEL_FORMAT_RGBA_8888:");
#endif
		config->argb_order = X2D_RGBORDER_BGR;
		config->format = X2D_INFORMAT_ARGB888;
		config->preRGB_en = 1;
		config->osd_mode = X2D_OSD_MOD_SRC_OVER;

		config->glb_alpha_en = 0;
		config->global_alpha_val = layer->global_alpha;
#ifdef X2D_DEBUG
		if(layer->global_alpha < 0xFF)
			ALOGE("----------------RGBA888  global_alpha=%d",layer->global_alpha);
#endif
		break;
	case HAL_PIXEL_FORMAT_RGBX_8888:
#ifdef X2D_DEBUG
		ALOGE("3------------HAL_PIXEL_FORMAT_RGBX_8888:");
#endif
		config->argb_order = X2D_RGBORDER_BGR;
		config->format = X2D_INFORMAT_ARGB888;
		config->glb_alpha_en = 1;
		config->global_alpha_val = layer->global_alpha;
		break;
	case HAL_PIXEL_FORMAT_BGRX_8888:
#ifdef X2D_DEBUG
		config->argb_order = X2D_RGBORDER_RGB;
#endif
		config->format = X2D_INFORMAT_ARGB888;
		config->glb_alpha_en = 1;
		config->global_alpha_val = layer->global_alpha;
		break;
	case HAL_PIXEL_FORMAT_BGRA_8888:
#ifdef X2D_DEBUG
		ALOGE("4------------HAL_PIXEL_FORMAT_BGRA_8888:");
#endif
		config->argb_order = X2D_RGBORDER_RGB;
		config->format = X2D_INFORMAT_ARGB888;
		config->preRGB_en = 1;
		config->osd_mode = X2D_OSD_MOD_SRC_OVER;

		config->glb_alpha_en = 0;
		config->global_alpha_val = layer->global_alpha;
#ifdef X2D_DEBUG
		if(layer->global_alpha < 0xFF)
			ALOGE("----------------BGRA888  global_alpha=%d",layer->global_alpha);
#endif
		break;
	case HAL_PIXEL_FORMAT_YCrCb_420_SP:
		config->format = X2D_INFORMAT_NV12;
		config->glb_alpha_en = 1;
		config->global_alpha_val = layer->global_alpha;
		break;
	case HAL_PIXEL_FORMAT_YV12:
	case HAL_PIXEL_FORMAT_JZ_YUV_420_P:
		config->format = X2D_INFORMAT_YUV420SP;
		config->glb_alpha_en = 1;
		config->global_alpha_val = layer->global_alpha;
		break;
	case HAL_PIXEL_FORMAT_JZ_YUV_420_B:
		config->format = X2D_INFORMAT_TILE420;
		config->glb_alpha_en = 1;
		config->global_alpha_val = layer->global_alpha;
		break;
	default:
		ALOGE("%s %s %d: setup_config_format unknown layer data format 0x%08x",
			 __FILE__, __FUNCTION__, __LINE__, format);
		config->format = X2D_INFORMAT_NOTSUPPORT;
		break;
    }
}

static void setup_config_size(jz_hwc_device_t *hwc_dev, struct src_layer *config, hwc_layer_1_t *layer)
{
    int i, bpp;
    float v_scale, h_scale;
	float tmp_v_scale = 0, tmp_h_scale = 0;

	int in_width_flag = 0, in_height_flag = 0;
	int in_h_flag = 0, in_v_flag = 0;
    config->in_width =  WIDTH(layer->sourceCrop);
    config->in_height = HEIGHT(layer->sourceCrop);
    config->out_width = WIDTH(layer->displayFrame);
    config->out_height = HEIGHT(layer->displayFrame);
    config->out_w_offset = layer->displayFrame.left;
    config->out_h_offset = layer->displayFrame.top;

    /* ALOGE("///////////////////////////"); */
    /* dump_layer(layer); */
    /* ALOGE("hwc_dev->fbDev->buf_index = %d", hwc_dev->fbDev->buf_index); */
    /* ALOGE("\\\\\\\\\\\\\\\\\\\\\\\\\\\\"); */

    ALOGED("+++++++++++in_width: %d, in_height: %d, out_width: %d, out_height: %d",
         config->in_width, config->in_height, config->out_width, config->out_height);
    ALOGED("+++++++++++out_w_offset: %d, out_h_offset: %d", config->out_w_offset, config->out_h_offset);

	if (!(layer->flags & HWC_INGENIC_VIDEO_LAYER_X2D)) {
		tmp_h_scale = (float)config->in_width / (float)config->out_width;
	}
	if (!(layer->flags & HWC_INGENIC_VIDEO_LAYER_X2D)) {
		tmp_v_scale = (float)config->in_height / (float)config->out_height;
	}

    if ((config->out_w_offset < 0) || (config->out_w_offset > (int)hwc_dev->fbDev->var_info.width)) {
        ALOGW("w_offset: %d exceed the boundary!!!!!!!!!", config->out_w_offset);

		in_width_flag = 1;
		in_h_flag = 1;
        if (config->out_w_offset < 0) {
            config->out_w_offset = 0;
        }
        if (config->out_w_offset > (int)hwc_dev->fbDev->var_info.width) {
            config->out_w_offset = hwc_dev->fbDev->var_info.width;
        }
    }
    if ((config->out_h_offset < 0) || (config->out_h_offset > (int)hwc_dev->fbDev->var_info.height)) {
        ALOGW("h_offset: %d exceed the boundary!!!!!!!!!", config->out_h_offset);

		in_height_flag = 1;
		in_v_flag = 1;
        if (config->out_h_offset < 0) {
            config->out_h_offset = 0;
        }
        if (config->out_h_offset > (int)hwc_dev->fbDev->var_info.height) {
            config->out_h_offset = (int)hwc_dev->fbDev->var_info.height;
        }
    }
    if ((config->out_width + config->out_w_offset) > (int)hwc_dev->fbDev->var_info.width) {
		in_width_flag = 1;
        config->out_width = hwc_dev->fbDev->var_info.width - config->out_w_offset;
    }
    if ((config->out_height + config->out_h_offset) > (int)hwc_dev->fbDev->var_info.height) {
		in_height_flag = 1;
        config->out_height = hwc_dev->fbDev->var_info.height - config->out_h_offset;
    }

	if (in_h_flag == 1) {
		//ALOGE("tmp_h_scale: %f, tmp_v_scale: %f", tmp_h_scale, tmp_v_scale);
		int in_h_offset = config->in_width - config->out_width*(int)tmp_h_scale;
		private_handle_t *handle = (private_handle_t *)(layer->handle);
		bpp = get_bpp(layer, (void *)handle);

		//ALOGE("in_h_offset: %d", in_h_offset);
		if (in_h_offset < 0) {
			in_h_offset = -in_h_offset;
		}
		config->addr += in_h_offset * bpp;
		config->addr = DWORD_ALIGN(config->addr);
	}
	if (in_v_flag == 1) {
		//ALOGE("tmp_h_scale: %f, tmp_v_scale: %f", tmp_h_scale, tmp_v_scale);
		int in_v_offset = config->in_height - config->out_height*(int)tmp_v_scale;
		private_handle_t *handle = (private_handle_t *)(layer->handle);
		bpp = get_bpp(layer, (void *)handle);

		if (in_v_offset < 0) {
			in_v_offset = -in_v_offset;
		}
		config->addr += in_v_offset * handle->stride * bpp;
		config->addr = DWORD_ALIGN(config->addr);
	}

	if(in_width_flag == 1)
		config->in_width = config->out_width * (int)tmp_h_scale;
	if(in_height_flag == 1)
		config->in_height = config->out_height * (int)tmp_v_scale;
    ALOGED("-----------in_width: %d, in_height: %d, out_width: %d, out_height: %d",
         config->in_width, config->in_height, config->out_width, config->out_height);

    if (config->out_width && config->out_height) {
        switch (config->transform) {
            case X2D_H_MIRROR:
            case X2D_V_MIRROR:
            case X2D_ROTATE_0:
            case X2D_ROTATE_180:
                h_scale = (float)config->in_width / (float)config->out_width;
                v_scale = (float)config->in_height / (float)config->out_height;
                config->h_scale_ratio = (int)(h_scale * X2D_SCALE_FACTOR) - 1; /* fix upscale */
                config->v_scale_ratio = (int)(v_scale * X2D_SCALE_FACTOR) - 1; /* fix upscale */
                break;
            case X2D_ROTATE_90:
            case X2D_ROTATE_270:
                h_scale = (float)config->in_width / (float)config->out_height;
                v_scale = (float)config->in_height / (float)config->out_width;
                config->h_scale_ratio = (int)(h_scale * X2D_SCALE_FACTOR) - 1; /* fix upscale */
                config->v_scale_ratio = (int)(v_scale * X2D_SCALE_FACTOR) - 1; /* fix upscale */
                break;
            default:
                ALOGE("%s %s %d:undefined rotation degree!!!!", __FILE__, __FUNCTION__, __LINE__);
        }
    } else {
        ALOGE("layer: out_width: %d or out_height: %d is 0!", config->out_width, config->out_height);
    }

	if (layer->flags & HWC_INGENIC_VIDEO_LAYER_X2D) {
		struct VideoFrameInfo * handle = (struct VideoFrameInfo *)(layer->handle);
		ALOGED("handle->width: %d, handle->height: %d", handle->stride[0], handle->stride[1]);

		config->y_stride = handle->stride[0];
		config->v_stride = handle->stride[1];

		if (handle->format == HAL_PIXEL_FORMAT_JZ_YUV_420_B) {
			config->y_stride = handle->stride[0]>>4;
			config->v_stride = handle->stride[1]>>4;
		}
		/* YUV_420tile, width and height 16 aligned. */
		config->in_width =  (config->in_width+0xF) & ~(0xF);
		config->in_height =  (config->in_height+0xF) & ~(0xF);
	} else {
		/* ALOGE("bpp: %d iStride: %d iWidth: %d+++++++++++++++++++++", bpp, handle->iStride, handle->iWidth); */
		private_handle_t *handle = (private_handle_t *)(layer->handle);
		bpp = get_bpp(layer, (void *)handle);
		config->y_stride = handle->stride * bpp;
	}
}

static void set_dst_config(jz_hwc_device_t *hwc_dev)
{
    hwc_dev->x2d_cfg.dst_address = hwc_dev->fbDev->addr[hwc_dev->fbDev->buf_index];
    hwc_dev->x2d_cfg.dst_width = hwc_dev->fbDev->var_info.width;
    hwc_dev->x2d_cfg.dst_height = hwc_dev->fbDev->var_info.height;
    hwc_dev->x2d_cfg.dst_format =  hwc_dev->fbDev->format;
    hwc_dev->x2d_cfg.dst_stride = hwc_dev->fbDev->fix_info.line_length;

	hwc_dev->x2d_cfg.dst_glb_alpha_en = 0;
	hwc_dev->x2d_cfg.dst_alpha_val = 0xff;

	hwc_dev->x2d_cfg.dst_back_en = 1;
	hwc_dev->x2d_cfg.dst_preRGB_en = 0;

	hwc_dev->x2d_cfg.dst_mask_en = 1;
	hwc_dev->x2d_cfg.dst_bcground = 0xff000000;
}

static void set_dst_config2(jz_hwc_device_t *hwc_dev)
{
    hwc_dev->x2d_cfg.dst_address = hwc_dev->fbDev->addr[hwc_dev->fbDev->buf_index];
    hwc_dev->x2d_cfg.dst_width = hwc_dev->fbDev->var_info.width;
    hwc_dev->x2d_cfg.dst_height = hwc_dev->fbDev->var_info.height;
    hwc_dev->x2d_cfg.dst_format =  hwc_dev->fbDev->format;
    hwc_dev->x2d_cfg.dst_stride = hwc_dev->fbDev->fix_info.line_length;

	hwc_dev->x2d_cfg.dst_back_en = 1;
	hwc_dev->x2d_cfg.dst_glb_alpha_en = 0;
	hwc_dev->x2d_cfg.dst_preRGB_en = 1;
	hwc_dev->x2d_cfg.dst_mask_en = 0;
	hwc_dev->x2d_cfg.dst_alpha_val = 0xff;
}

static void set_dst_format(jz_hwc_device_t *hwc_dev)
{
	ALOGED("--bits_per_pixel=%d redoff=%d greenoff=%d blueoff=%d transp=%d",
				hwc_dev->fbDev->var_info.bits_per_pixel,
				hwc_dev->fbDev->var_info.red.offset,
				hwc_dev->fbDev->var_info.green.offset,
				hwc_dev->fbDev->var_info.blue.offset,
				hwc_dev->fbDev->var_info.transp.offset);

	switch(hwc_dev->fbDev->var_info.bits_per_pixel){
		case 16:
			ALOGW("----fb format RGB565");
			hwc_dev->fbDev->format = (X2D_OUTFORMAT_RGB565 << BIT_X2D_DST_RGB_FORMAT) | (X2D_RGBORDER_RGB << BIT_X2D_DST_RGB_ORDER);
			break;
		case 32:
			if( hwc_dev->fbDev->var_info.red.offset == 16 &&
				hwc_dev->fbDev->var_info.green.offset == 8 &&
				hwc_dev->fbDev->var_info.blue.offset == 0 &&
				hwc_dev->fbDev->var_info.transp.offset == 24){
				ALOGW("----fb format ARGB888");
				hwc_dev->fbDev->format = (X2D_OUTFORMAT_ARGB888 << BIT_X2D_DST_RGB_FORMAT) | (X2D_RGBORDER_RGB << BIT_X2D_DST_RGB_ORDER);
			}
			else if( hwc_dev->fbDev->var_info.red.offset == 0 &&
				hwc_dev->fbDev->var_info.green.offset == 8 &&
				hwc_dev->fbDev->var_info.blue.offset == 16 &&
				hwc_dev->fbDev->var_info.transp.offset == 24){
				ALOGW("----fb format ABGR888");
				hwc_dev->fbDev->format = (X2D_OUTFORMAT_ARGB888 << BIT_X2D_DST_RGB_FORMAT) | (X2D_RGBORDER_BGR << BIT_X2D_DST_RGB_ORDER);
			}else{
				ALOGW("----fb format XRGB888");
				hwc_dev->fbDev->format = (X2D_OUTFORMAT_XRGB888 << BIT_X2D_DST_RGB_FORMAT) | (X2D_RGBORDER_RGB << BIT_X2D_DST_RGB_ORDER);
			}
			break;
		default:
				ALOGE("!!!Unknow format,bits_per_pixel=%d,set default format to ARGB888",
						hwc_dev->fbDev->var_info.bits_per_pixel);
				hwc_dev->fbDev->format = (X2D_OUTFORMAT_ARGB888 << BIT_X2D_DST_RGB_FORMAT) | (X2D_RGBORDER_RGB << BIT_X2D_DST_RGB_ORDER);
			break;
	}

	hwc_dev->fbDev->format |= NEW_DST_FORMAT_BIT;
	ALOGE("hwc_dev->fbDev->format=%#x", hwc_dev->fbDev->format);
	return;
}

static int x2d_timeout_num = 0;
static void handle_pix_glb_layer(jz_hwc_device_t *hwc_dev, hwc_layer_1_t *layer,int j)
{
	int width, height;
    int bpp = 0,ret;
	int dst_stride = 0;

	width =  WIDTH(layer->sourceCrop);
	height = HEIGHT(layer->sourceCrop);

	private_handle_t *handle = (private_handle_t *)(layer->handle);
	bpp = get_bpp(layer, (void *)handle);
	if(handle->stride * height * bpp > hwc_dev->fbDev->size){
			ALOGE("Can not handle src_layer_buffer_size > fb_size for the moment");
			return;
	}


	struct jz_x2d_config *x2d_cfg = &hwc_dev->x2d_pix_glb_config;
	memset(x2d_cfg,0,sizeof(struct jz_x2d_config));
    x2d_cfg->tlb_base = hwc_dev->gtlb_base;

	/* src config*/
	/*config lay 0*/
	//ALOGE("----hand global_alpha=%d",layer->global_alpha);
	setup_pix_glb_config_addr(hwc_dev, &x2d_cfg->lay[0], layer);
	setup_config_format(&x2d_cfg->lay[0],layer);

	x2d_cfg->lay[0].transform = X2D_ROTATE_0;

	x2d_cfg->lay[0].in_width	= width;
	x2d_cfg->lay[0].in_height	= height;
	x2d_cfg->lay[0].out_width	= width;
	x2d_cfg->lay[0].out_height	= height;
	x2d_cfg->lay[0].out_w_offset = 0;
	x2d_cfg->lay[0].out_h_offset = 0;

	x2d_cfg->lay[0].mask_en = 0;
	x2d_cfg->lay[0].msk_val = 0xFF00FF00;

	x2d_cfg->lay[0].h_scale_ratio = X2D_SCALE_FACTOR - 1;
	x2d_cfg->lay[0].v_scale_ratio = X2D_SCALE_FACTOR - 1;

	x2d_cfg->lay[0].y_stride = handle->stride * bpp;

	/*config lay 1*/
	x2d_cfg->lay[1].preRGB_en = 0;
	x2d_cfg->lay[1].glb_alpha_en = 0;
	x2d_cfg->lay[1].global_alpha_val = layer->global_alpha;
	x2d_cfg->lay[1].mask_en = 1;
	x2d_cfg->lay[1].msk_val = layer->global_alpha<<24;
	x2d_cfg->lay[1].osd_mode = X2D_OSD_MOD_DST_IN;

	x2d_cfg->lay[1].out_width	= width;
	x2d_cfg->lay[1].out_height	= height;
	x2d_cfg->lay[1].out_w_offset	= 0;
	x2d_cfg->lay[1].out_h_offset	= 0;

	/* dst cconfig*/
	x2d_cfg->dst_address= (int)hwc_dev->tmp_layer[j].y_addr;
	x2d_cfg->dst_format = X2D_OUTFORMAT_ARGB888;
	x2d_cfg->dst_width	= width;
	x2d_cfg->dst_height = height;
	x2d_cfg->dst_stride	=  x2d_cfg->lay[0].y_stride;

	x2d_cfg->dst_glb_alpha_en = 0;
	x2d_cfg->dst_alpha_val = 0xff;
	x2d_cfg->dst_back_en = 0;
	x2d_cfg->dst_preRGB_en = 0;
	x2d_cfg->dst_mask_en = 0;
	x2d_cfg->dst_bcground = 0xff0000ff;

	x2d_cfg->layer_num = 2;

#ifdef X2D_DEBUG
	ALOGE("!!!!!!!!!!!!dump handle pix and glb config!!!!!!!!!!!!");
	x2d_dump_cfg(hwc_dev->x2d_pix_glb_config);
#endif

#if 1
	/*use x2d composer*/
	ret = ioctl(hwc_dev->x2d_fd, IOCTL_X2D_SET_CONFIG, x2d_cfg);
	if (ret < 0) {
		ALOGE("%s %s %d: IOCTL_X2D_SET_CONFIG failed", __FILE__, __FUNCTION__, __LINE__);
		goto err_set;
	}

	ret = ioctl(hwc_dev->x2d_fd, IOCTL_X2D_START_COMPOSE);
	if (ret < 0) {
		ALOGE("%s %s %d: IOCTL_X2D_START_COMPOSE failed", __FILE__, __FUNCTION__, __LINE__);
		dump_x2d_config(hwc_dev);
		if(10 == x2d_timeout_num++) {
			hwc_dev->x2d_timeout = 1;
			x2d_timeout_num = 0;
		}
		goto err_set;
	}

#else
	/*use cpu composer*/
	{
		int global_alpha = layer->global_alpha;
		ALOGE("------global_alpha=%d",global_alpha);
		int count = width * height;
		unsigned int *src_addr = (unsigned int *)x2d_cfg->lay[0].addr;
		unsigned int *dst_addr = (unsigned int *)hwc_dev->tmp_layer[j].y_addr;
		for(int i=0;i<count;i++){
			unsigned int src_pixel = *src_addr;
			unsigned int dst_pixel ;//= *dsr_addr;
			unsigned int A, R, G, B;
			A = src_pixel>> 24;
			A = (A*global_alpha)>>8;
			A <<=24;

			R = src_pixel & 0x00FF0000;
			R = (R*global_alpha)>>8; // / 255
			R &= 0x00FF0000;

			G = src_pixel & 0x0000FF00;
			G = (G*global_alpha)>>8;
			G &= 0x0000FF00;

			B = src_pixel & 0x000000FF;
			B = (B*global_alpha)>>8;
			B &= 0x000000FF;

			dst_pixel = A | R | G | B;
			*dst_addr = dst_pixel;

			src_addr++; dst_addr++;
		}
	}

	//memcpy((unsigned char *)hwc_dev->tmp_layer[j].y_addr,(unsigned char *)x2d_cfg.lay[0].addr,x2d_cfg.lay[0].y_stride*height*bpp);
#endif

	hwc_dev->tmp_layer[j].flag = 1;

#if 0
	num++;
	char cmd[100];
	snprintf(cmd,100,"/data/pic/%d.bmp",num);
	ALOGE("-+-cmd=%s",cmd);
	SaveBufferToBmp32(cmd,
			(unsigned char*)(hwc_dev->pix_glb_buffer), width,height,width*4, 0);
#endif
	return;

err_set:
	ALOGE("+++++++++++++++++++++++++++++++error");
    deinit_dev_para(hwc_dev);
}


static void set_layers_config(jz_hwc_device_t *hwc_dev,
                              hwc_display_contents_1_t *list, unsigned int *layer_cnt)
{
    unsigned int i = 0, j = 0;
    int ret = 0, format = 0, pix_glb_count = 0;;
	int width, height;
    unsigned int tmp_cnt = *layer_cnt;

    if (tmp_cnt > list->numHwLayers) {
        ALOGE("%s %s %d: layer_cnt: %d, list->numHwLayers: %d, layers over",
             __FILE__, __FUNCTION__, __LINE__, *layer_cnt, list->numHwLayers);
        return;
    }

    for (i = 0, j = 0; i < list->numHwLayers; i++) {
        int bpp = 0;
        hwc_layer_1_t *layer = NULL;

		if (list->numHwLayers <= tmp_cnt+i) {
			ALOGED("layers run out: %d", tmp_cnt+i);
			break;
		}
        layer = &list->hwLayers[tmp_cnt+i];
        if (layer == NULL) {
            ALOGE("%s %s %d: layer is NULL", __FILE__, __FUNCTION__, __LINE__);
            break;
        }

        *layer_cnt += 1;
		void *tmp_handle = (void *)(layer->handle);
		if (!tmp_handle) {
#if 0
			ALOGE("----handle null!!!");
//			dump_layer(layer);
			usleep(10*1000);
			continue;
#endif
		}

		width =  WIDTH(layer->sourceCrop);
		height = HEIGHT(layer->sourceCrop);
		if ((width == 1) && (height == 1)) {
			continue;
		}

        if (layer->compositionType == HWC_FRAMEBUFFER) {
            continue;
        }

		/*handle pix glb layer*/
		{

			hwc_dev->tmp_layer[j].flag = 0;
			hwc_dev->tmp_layer[j].y_addr = 0;

			if (tmp_handle) {
				if (layer->flags & HWC_INGENIC_VIDEO_LAYER_X2D) {
					format = ((struct VideoFrameInfo *)layer->handle)->format;
				} else {
					format = ((private_handle_t *)layer->handle)->format;
				}
				if((format == HAL_PIXEL_FORMAT_RGBA_8888 ||
					format == HAL_PIXEL_FORMAT_BGRA_8888) &&
					(layer->global_alpha > 0) &&
					(layer->global_alpha < 0xFF) &&
					(pix_glb_count < 2)){
#ifdef X2D_DEBUG
					ALOGE("------start handle pix glb layer total_current_layer_num=%d config_layer_num=%d  glb=%d",
							tmp_cnt+i,j,layer->global_alpha);
#endif
					hwc_dev->tmp_layer[j].y_addr =  (int)((unsigned char *)hwc_dev->pix_glb_buffer +
									pix_glb_count * hwc_dev->fbDev->size);
					handle_pix_glb_layer(hwc_dev,layer,j);
					pix_glb_count++;
#ifdef X2D_DEBUG
					ALOGE("------end handle pix glb layer finish");
#endif
				}
			}
		}

		//dump_layer(layer);
		if (tmp_handle) {
			if (layer->flags & HWC_INGENIC_VIDEO_LAYER_X2D) {
				int k;
				struct VideoFrameInfo * handle = (struct VideoFrameInfo *)(layer->handle);
				if (handle->dmmu_maped) {
					ALOGED("set_layers_config HWC_INGENIC_VIDEO_LAYER_X2D handle->dmmu_maped=%#x", handle->dmmu_maped);
				} else {
					ALOGE("HWC_INGENIC_VIDEO_LAYER_X2D~~~~~~~~~~~~~~>");
					ALOGE("bpp: %d, handle->width: %d, height: %d", bpp, handle->width, handle->height);
					for (k = 0; k < handle->count; k++) {
						hwc_dev->video_rect[k][0] = (int)handle->addr[k];
						hwc_dev->video_rect[k][1] = handle->size[k];
						ALOGE("addr: %08x, size: %d", (unsigned int)handle->addr[k], handle->size[k]);
					}
				}
			} else {
				private_handle_t *handle = (private_handle_t *)(layer->handle);
				bpp = get_bpp(layer, (void *)handle);
				hwc_dev->handle_rect[j][0] = handle->stride;
				hwc_dev->handle_rect[j][1] = handle->height;
				hwc_dev->handle_rect[j][2] = bpp;
			}

			if(hwc_dev->tmp_layer[j].flag && hwc_dev->tmp_layer[j].y_addr){
#ifdef X2D_DEBUG
				ALOGE("-------change new addr j=%d   addr=%x",j,hwc_dev->tmp_layer[j].y_addr);
#endif
				hwc_dev->x2d_cfg.lay[j].addr = hwc_dev->tmp_layer[j].y_addr;
			}

			setup_config_addr(hwc_dev, &(hwc_dev->x2d_cfg.lay[j]), layer, j);
			setup_config_alpha(&hwc_dev->x2d_cfg.lay[j], layer);
			setup_config_transform(&hwc_dev->x2d_cfg.lay[j], layer);
			setup_config_format(&hwc_dev->x2d_cfg.lay[j], layer);
			setup_config_size(hwc_dev, &hwc_dev->x2d_cfg.lay[j], layer);

			if(hwc_dev->tmp_layer[j].flag && hwc_dev->tmp_layer[j].y_addr){
				hwc_dev->x2d_cfg.lay[j].argb_order = X2D_RGBORDER_RGB;
			}

		}else{
			setup_config_dim(hwc_dev,&hwc_dev->x2d_cfg.lay[j],layer);
		}

        j++;
		if (j == HW_SUPPORT_LAY_NUM) {
			break;
		}
	}
    ALOGED("hwc_dev->x2d_cfg.layer_num j: %d--------------->", j);
    hwc_dev->x2d_cfg.layer_num = j;
}


/*int jz_hwc_set(hwc_composer_device_1_t *dev, hwc_display_t dpy,
               hwc_surface_t sur, hwc_display_contents_1_t *list)*/
int jz_hwc_set(struct hwc_composer_device_1 *dev, size_t numDisplays, hwc_display_contents_1_t** displays)
{
    int ret = 0;
	int j=0,format = 0;
    jz_hwc_device_t *hwc_dev = (jz_hwc_device_t *)dev;

    if (!numDisplays || displays == NULL) {
        ALOGD("set: empty display list");
        return 0;
    }
    
    hwc_display_contents_1_t* list = displays[0];
    hwc_display_t dpy = NULL;
    hwc_surface_t sur = NULL;

    #ifdef X2D_DEBUG
	//android::StopWatch w("+++++++++++++++hwc_set layers");
    #endif

    if (hwc_dev == NULL) {
        ALOGE("%s %s %d: invalid device", __FILE__, __FUNCTION__, __LINE__);
        return -EINVAL;
    }

	if (list != NULL) {
		dpy = list->dpy;
	    sur = list->sur;
	}

    if (dpy == NULL && sur == NULL && list == NULL) {
        // release our resources, the screen is turning off
        // in our case, there is nothing to do.
        return 0;
    }
#ifdef X2D_DEBUG
	ALOGE("-------line=%d   num hw layer=%d",__LINE__,hwc_dev->render_layer_num);
#endif
    if (hwc_dev->render_layer_num > 0) {
        int i, cycle_count = 0;
        unsigned int layer_cnt = 0;
        cycle_count = hwc_dev->render_layer_num / 4 + 1;
        if (cycle_count && !(hwc_dev->render_layer_num % 4)) {
            cycle_count -= 1;
        }

        for (i = 0; i < cycle_count; i++) {
            /* set tlb_base */
            hwc_dev->x2d_cfg.tlb_base = hwc_dev->gtlb_base;

            /* set src layers && dst x2d_cfg */
			if (!i) {
				set_dst_config(hwc_dev);
			} else {
				set_dst_config2(hwc_dev);
			}

            set_layers_config(hwc_dev, list, &layer_cnt);
#ifdef X2D_DEBUG
            x2d_dump_cfg(hwc_dev->x2d_cfg);
#endif
            ret = ioctl(hwc_dev->x2d_fd, IOCTL_X2D_SET_CONFIG, &hwc_dev->x2d_cfg);
            if (ret < 0) {
                ALOGE("%s %s %d: IOCTL_X2D_SET_CONFIG failed", __FILE__, __FUNCTION__, __LINE__);
                goto err_set;
            }
            ret = ioctl(hwc_dev->x2d_fd, IOCTL_X2D_START_COMPOSE);
            if (ret < 0) {
                ALOGE("%s %s %d: IOCTL_X2D_START_COMPOSE failed", __FILE__, __FUNCTION__, __LINE__);
                dump_x2d_config(hwc_dev);
				if(10 == x2d_timeout_num++) {
					hwc_dev->x2d_timeout = 1;
					x2d_timeout_num = 0;
				}
				goto err_set;
            }
            unmap_layers_addr(hwc_dev, list);
            memset((void *)&hwc_dev->x2d_cfg, 0, sizeof(struct jz_x2d_config));
        }
    }

#if 0
	num++;
	char cmd[100];
	snprintf(cmd,100,"/data/pic/%d.bmp",num);
	ALOGE("-+-cmd=%s",cmd);
	SaveBufferToBmp32(cmd,
			(unsigned char*)(hwc_dev->fbDev->addr[hwc_dev->fbDev->buf_index]), 800,480,800*4, 0);
#endif


	eglSwapBuffers((EGLDisplay)dpy, (EGLSurface)sur);
    hwc_dev->fbDev->buf_index = (hwc_dev->fbDev->buf_index + 1) %
            (hwc_dev->fbDev->var_info.yres_virtual / hwc_dev->fbDev->var_info.yres);
    //ALOGE("------------------------  hwc_dev->fbDev->buf_index = %d  ", hwc_dev->fbDev->buf_index);
    hwc_dev->render_layer_num = 0;
    return 0;

err_set:
    deinit_dev_para(hwc_dev);

    return ret;
}

int jz_hwc_prepare(struct hwc_composer_device_1 *dev, size_t numDisplays, hwc_display_contents_1_t** displays)
{
	int video_layer_flag = 0;
    unsigned int i;
    jz_hwc_device_t *hwc_dev = (jz_hwc_device_t *)dev;
    hwc_display_contents_1_t* list = displays[0];

    hwc_dev->render_layer_num = 0;

    if(!list || (!(list->flags & HWC_GEOMETRY_CHANGED))) {
        if(!list) {
            ALOGE("%s %s %d: !list", __FILE__, __FUNCTION__, __LINE__);
            return 0;
        }
    }

    if (hwc_dev == NULL) {
        ALOGE("%s %s %d: invalid device", __FILE__, __FUNCTION__, __LINE__);
        return -EINVAL;
    }

    for (i = 0; i < list->numHwLayers; i++) {
        hwc_layer_1_t *layer = &list->hwLayers[i];
        if (!is_support_this_layer(layer)) {
            ALOGE("Roll Back all the layers");
            return 0;
        }
		if (layer->flags & HWC_INGENIC_VIDEO_LAYER_X2D) {
			video_layer_flag = 1;
		}
    }

	if(hwc_dev->x2d_timeout) {
		for (i = 0; i < list->numHwLayers; i++) {
			hwc_layer_1_t *layer = &list->hwLayers[i];
			layer->compositionType = HWC_FRAMEBUFFER;
		}
		return 0;
	}

	/*  if (!video_layer_flag) {
      ALOGE("++++++++++++++++++++++++++++++++++++++++++++++");
    		return 0;
    	}
    */
    for (i = 0; i < list->numHwLayers; i++) {
        hwc_layer_1_t *layer = &list->hwLayers[i];
        //dump_layer(layer);
        layer->compositionType = HWC_OVERLAY;
        layer->hints = HWC_HINT_CLEAR_FB;

        hwc_dev->render_layer_num++;
    }
    ALOGED("hwc_dev->render_layer_num: %d", hwc_dev->render_layer_num);
    return 0;
}

int jz_hwc_device_close(hw_device_t *dev)
{
    jz_hwc_device_t *hwc_dev = (jz_hwc_device_t *) dev;

    unmap_dst_addr(hwc_dev);

    if (dmmu_deinit() < 0) {
        ALOGE("%s %s %d: dmmu_deinit failed", __FILE__, __FUNCTION__, __LINE__);
    }

    if (hwc_dev) {
        if (hwc_dev->x2d_fd > 0) {
            close(hwc_dev->x2d_fd);
            /* pthread will be killed when parent process exits */
            pthread_mutex_destroy(&hwc_dev->lock);
            free(hwc_dev);
        }
    }

    ALOGED("Jz hwcomposer device closed!");
    return 0;
}

void handle_vsync_uevent(jz_hwc_device_t *hwc_dev, const char *buff, int len)
{
    uint64_t timestamp = 0;
    const char *s = buff;

    if(!hwc_dev->procs || !hwc_dev->procs->vsync)
        return;

    s += strlen(s) + 1;

    while(*s) {
        if (!strncmp(s, "VSYNC=", strlen("VSYNC=")))
            timestamp = strtoull(s + strlen("VSYNC="), NULL, 0);

        s += strlen(s) + 1;
        if (s - buff >= len)
            break;
    }

    hwc_dev->procs->vsync(hwc_dev->procs, 0, timestamp);
}

static void *jz_hwc_vsync_thread(void *data)
{
    jz_hwc_device_t *hwc_dev = (jz_hwc_device_t *)data;
    char uevent_desc[4096];
    memset(uevent_desc, 0, sizeof(uevent_desc));

    setpriority(PRIO_PROCESS, 0, HAL_PRIORITY_URGENT_DISPLAY);

    uevent_init();
    while(1) {
        int len = uevent_next_event(uevent_desc, sizeof(uevent_desc) - 2);

        int vsync = !strcmp(uevent_desc, "change@/devices/platform/jz-fb.1");
	if(!vsync)
            vsync = !strcmp(uevent_desc, "change@/devices/platform/jz-fb.0");
        if(vsync)
            handle_vsync_uevent(hwc_dev, uevent_desc, len);
    }

    return NULL;
}
static int jz_hwc_eventControl(struct hwc_composer_device_1* dev,
                               int disp, int event, int enabled)
{
    jz_hwc_device_t *hwc_dev = (jz_hwc_device_t *)dev;
    int val, err;

    switch (event) {
        case HWC_EVENT_VSYNC:
            val = enabled;
            err = ioctl(hwc_dev->fbDev->fd, JZFB_SET_VSYNCINT, &val);
            if (err < 0)
                return -errno;

            return 0;
    }

    return -EINVAL;
}

static int jz_hwc_query(struct hwc_composer_device_1* dev,
                        int what, int* value)
{
    jz_hwc_device_t *hwc_dev = (jz_hwc_device_t *) dev;

    switch (what) {
        default:
            // unsupported query
            return -EINVAL;
    }
    return 0;
}

static int jz_hwc_blank(struct hwc_composer_device_1 *dev,
                        int dpy, int blank)
{
    // We're using an older method of screen blanking based on
    // early_suspend in the kernel.  No need to do anything here.
    return 0;
}

int jz_hwc_device_open(const hw_module_t *module,
                       const char *name, hw_device_t **dev)
{
    int err = 0;
    jz_hwc_device_t *hwc_dev;
    private_handle_t *dst_handle;
    int num_of_fb;
    void *fb_base = NULL;
    unsigned int t_base = 0;

    ALOGED("jz_hwc_device_open*************");

    if (strcmp(name, HWC_HARDWARE_COMPOSER)) {
        ALOGED("%s(%d): invalid device name!", __FUNCTION__, __LINE__);
        return -EINVAL;
    }

    /* alloc memory for hwc device */
    hwc_dev = (jz_hwc_device_t *)malloc(sizeof(struct jz_hwc_device));
    if (hwc_dev == NULL) {
        ALOGED("alloc mem for hwc dev failed!");
        return -ENOMEM;
    }
    memset(hwc_dev, 0, sizeof(struct jz_hwc_device));
    hwc_dev->fbDev = (struct hwc_framebuffer_info_t *)malloc(sizeof(struct hwc_framebuffer_info_t));
    if (hwc_dev->fbDev == NULL) {
        ALOGED("alloc mem for hwc dev failed!");
        return -ENOMEM;
    }
    memset(hwc_dev->fbDev, 0, sizeof(struct hwc_framebuffer_info_t));

    /* initialize variable */
    hwc_dev->base.common.tag      = HARDWARE_MODULE_TAG;
    hwc_dev->base.common.version  = HWC_DEVICE_API_VERSION_1_0;
    hwc_dev->base.common.module   = (hw_module_t *)module;

    /* initialize the procs */
    hwc_dev->base.common.close    = jz_hwc_device_close;
    hwc_dev->base.prepare         = jz_hwc_prepare;
    hwc_dev->base.set             = jz_hwc_set;
    hwc_dev->base.dump            = jz_hwc_dump;
    hwc_dev->base.registerProcs   = jz_hwc_registerProcs;
    hwc_dev->base.eventControl    = jz_hwc_eventControl;
    hwc_dev->base.query           = jz_hwc_query;
    hwc_dev->base.blank           = jz_hwc_blank;

    /* return device handle */
    *dev = &hwc_dev->base.common;

    /* init pthread mutex */
    if (pthread_mutex_init(&hwc_dev->lock, NULL)) {
        ALOGE("%s %s %d: init pthread_mutex failed", __FILE__, __FUNCTION__, __LINE__);
        goto err_mutex;
    }

    if (dmmu_init() < 0) {
        ALOGE("%s %s %d: dmmu_init failed", __FILE__, __FUNCTION__, __LINE__);
        goto err_init;
    }

    err = dmmu_get_page_table_base_phys(&t_base);
    if (err < 0 || !t_base) {
        ALOGE("%s %s %d: dmmu_get_page_table_base_phys failed", __FILE__, __FUNCTION__, __LINE__);
        goto err_getphys;
    }
    hwc_dev->gtlb_base = t_base;

    //	struct hw_module_t const* gl_module = (struct hw_module_t const*)hwc_dev->gralloc_module;
    if (hw_get_module(GRALLOC_HARDWARE_MODULE_ID, (struct hw_module_t const**)&hwc_dev->gralloc_module) == 0) {
        /* open framebuffer */
        hwc_dev->fbDev->fd = open(FB0DEV, O_RDWR);
        if (hwc_dev->fbDev->fd < 0) {
            ALOGE("%s %s %d: fb0 open error", __FILE__, __FUNCTION__, __LINE__);
            goto err_init;
        }

        /* get framebuffer's var_info */
        if (ioctl(hwc_dev->fbDev->fd, FBIOGET_VSCREENINFO, &hwc_dev->fbDev->var_info) < 0) {
            ALOGE("%s %s %d: FBIOGET_VSCREENINFO failed", __FILE__, __FUNCTION__, __LINE__);
            goto err_getinfo;
        }

        /* get framebuffer's fix_info */
        if (ioctl(hwc_dev->fbDev->fd, FBIOGET_FSCREENINFO, &hwc_dev->fbDev->fix_info) < 0) {
            ALOGE("%s %s %d: FBIOGET_FSCREENINFO failed", __FILE__, __FUNCTION__, __LINE__);
            goto err_getinfo;
        }

        hwc_dev->fbDev->var_info.width = hwc_dev->fbDev->var_info.xres;
        hwc_dev->fbDev->var_info.height = hwc_dev->fbDev->var_info.yres;
        hwc_dev->fbDev->buf_index = 0;
        hwc_dev->fbDev->bpp = hwc_dev->fbDev->var_info.bits_per_pixel / 8;
		set_dst_format(hwc_dev);

        hwc_dev->fbDev->size = hwc_dev->fbDev->fix_info.line_length * hwc_dev->fbDev->var_info.yres;
        num_of_fb = hwc_dev->fbDev->var_info.yres_virtual / hwc_dev->fbDev->var_info.yres;
        /* mmap framebuffer device */
        fb_base = mmap(0, hwc_dev->fbDev->size * num_of_fb, PROT_READ|PROT_WRITE,
                       MAP_SHARED, hwc_dev->fbDev->fd, 0);
        if (fb_base == MAP_FAILED) {
            ALOGE("%s %s %d: mmap failed", __FILE__, __FUNCTION__, __LINE__);
            goto err_getinfo;
        }

		/* buffer handle pixel and global alpha layer*/
		void *pg_addr;
		unsigned int buffer_size;
		buffer_size = 2*(hwc_dev->fbDev->size) + PAGE_SIZE;
		pg_addr = malloc(buffer_size);
		memset(pg_addr,0,buffer_size);//!!!!!!!!!!!
		hwc_dev->pix_glb_buffer = (void *)(((unsigned int)pg_addr+PAGE_SIZE - 1)&(~(PAGE_SIZE - 1)));
		ALOGED("-------page_addr=%x  addr=%x buffer_size=%d",pg_addr,hwc_dev->pix_glb_buffer,buffer_size);
		map_addr(pg_addr,buffer_size);

		/*hwc_dev->tmp_layer = (struct jz_layer *)malloc(sizeof(struct jz_layer) * 4);*/
		/*hwc_dev->x2d_pix_glb_config = (struct jz_x2d_config *)malloc(sizeof(struct jz_x2d_config));*/

        /* get && map dst fb addr */
        int i;
        for (i = 0; i < num_of_fb; i++) {
            hwc_dev->fbDev->addr[i] = (unsigned int)fb_base + (hwc_dev->fbDev->size * i);
            memset((void *)hwc_dev->fbDev->addr[i], 0x0, hwc_dev->fbDev->size);

            err = map_dst_addr(hwc_dev, i);
            if (err < 0) {
                ALOGE("%s %s %d: map_dst_addr failed!", __FILE__, __FUNCTION__, __LINE__);
                goto err_map;
            }
        }
    } else {
        ALOGE("%s %s %d: hw_get_module failed", __FILE__, __FUNCTION__, __LINE__);
        goto err_init;
    }

    /* open x2d device */
    hwc_dev->x2d_fd = open(X2DNAME, O_RDWR);
    if (hwc_dev->x2d_fd < 0) {
        ALOGE("%s %s %d: open x2d device failed", __FILE__, __FUNCTION__, __LINE__);
        goto err_getinfo;
    }

    err = pthread_create(&hwc_dev->vsync_thread, NULL, jz_hwc_vsync_thread, hwc_dev);
    if (err) {
        ALOGE("%s::pthread_create() failed : %s", __func__, strerror(err));
        goto err_pthread_create;
    }

    ALOGE("open jz hwcomposer device success!!\n");
    return 0;

err_pthread_create:
    close(hwc_dev->x2d_fd);
err_map:
    munmap(fb_base, hwc_dev->fbDev->size*num_of_fb);
err_getinfo:
    close(hwc_dev->fbDev->fd);
err_getphys:
    dmmu_deinit();
err_init:
    pthread_mutex_destroy(&hwc_dev->lock);
err_mutex:
    free(hwc_dev->fbDev);
    free(hwc_dev);
    *dev = NULL;

    return -EINVAL;
}

static struct hw_module_methods_t jz_hwc_module_methods = {
	open: jz_hwc_device_open
};

hwc_module_t HAL_MODULE_INFO_SYM = {
	common: {
		tag:                  HARDWARE_MODULE_TAG,
		version_major:        1,
		version_minor:        0,
		id:                   HWC_HARDWARE_MODULE_ID,
		name:                 "Jz4780 Hardware Composer HAL",
		author:               "sw1-ingenic",
		methods:              &jz_hwc_module_methods,
	}
};
