
#include <errno.h>
#include <pthread.h>
#include <fcntl.h>
#include <sys/epoll.h>
#include <math.h>
#include <time.h>
#include <termios.h>
#include <sys/types.h>
#include <sys/wait.h>
//#include <sys/fcntl.h>
#include <sys/socket.h>
#include <signal.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <netdb.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#define  LOG_TAG  "gps_nmea"
#include <cutils/log.h>
#include <cutils/sockets.h>
//#include <hardware_legacy/gps.h>
//#include <hardware_legacy/gps_ni.h>
#include <hardware/hardware.h>
#include <hardware/gps.h>
#include "ubx.h"

#define  GPS_DEBUG  0

#if GPS_DEBUG
#  define  D(...)   ALOGD(__VA_ARGS__)
#else
#  define  D(...)   ((void)0)
#endif

#define  LOGE(...)   ALOGE(__VA_ARGS__)
#define  LOGV(...)   ALOGV(__VA_ARGS__)

#if 0
#define  D(AAA, args...)   printf(AAA "\n", ## args)
#define  LOGE(AAA, args...)   printf(AAA "\n", ## args)
#define  LOGV(AAA, args...)   printf(AAA "\n", ## args)
#endif

/* the name of the d-controlled socket */
/* ---------------------------------- */
#define DEV_NAME     "/dev/ttyS1"

/*AssistOnline Server addr*/
#define AGPS_SERVER   "agps.u-blox.com:46434"

#define BUFFER_SIZE 8192	//!< Size of the temporary buffers
#define MAXFIELDLEN 128     //!< Maximum length for username and password

#define ANOL_UDP 0 //!< Used in getAssistNowOnlineData() to indicate UDP traffic
#define ANOL_TCP 1 //!< Used in getAssistNowOnlineData() to indicate TCP traffic
/*------------------------*/
//extern int flag = 0;

static time_t utc_mktime(struct tm *_tm);

/*****************************************************************/
/*****************************************************************/
/*****                                                       *****/
/*****       N M E A   T O K E N I Z E R                     *****/
/*****                                                       *****/
/*****************************************************************/
/*****************************************************************/

typedef struct {
    const char*  p;
    const char*  end;
} Token;

#define  MAX_NMEA_TOKENS  32

typedef struct {
    int     count;
    Token   tokens[ MAX_NMEA_TOKENS ];
} NmeaTokenizer;

static int
nmea_tokenizer_init( NmeaTokenizer*  t, const char*  p, const char*  end )
{
    int    count = 0;
    char*  q;

    // the initial '$' is optional
    if (p < end && p[0] == '$')
        p += 1;

    // remove trailing newline
    if (end > p && end[-1] == '\n') {
        end -= 1;
        if (end > p && end[-1] == '\r')
            end -= 1;
    }

    // get rid of checksum at the end of the sentecne
    if (end >= p+3 && end[-3] == '*') {
        end -= 3;
    }

    while (p < end) {
        const char*  q = p;

        q = memchr(p, ',', end-p);
        if (q == NULL)
            q = end;

        if (q >= p) {
            if (count < MAX_NMEA_TOKENS) {
                t->tokens[count].p   = p;
                t->tokens[count].end = q;
                count += 1;
            }
        }
        if (q < end)
            q += 1;

        p = q;
    }

    t->count = count;
    return count;
}

static Token
nmea_tokenizer_get( NmeaTokenizer*  t, int  index )
{
    Token  tok;
    static const char*  dummy = "";

    if (index < 0 || index >= t->count) {
        tok.p = tok.end = dummy;
    } else
        tok = t->tokens[index];

    return tok;
}

static int
str2int( const char*  p, const char*  end )
{
    int   result = 0;
    int   len    = end - p;

    for ( ; len > 0; len--, p++ )
    {
        int  c;

        if (p >= end)
            goto Fail;

        c = *p - '0';
        if ((unsigned)c >= 10)
            goto Fail;

        result = result*10 + c;
    }
    return  result;

Fail:
    return -1;
}

static double
str2float( const char*  p, const char*  end )
{
    int   result = 0;
    int   len    = end - p;
    char  temp[16];

    if (len >= (int)sizeof(temp))
        return 0.;

    memcpy( temp, p, len );
    temp[len] = 0;
    return strtod( temp, NULL );
}

/*****************************************************************/
/*****************************************************************/
/*****                                                       *****/
/*****       N M E A   P A R S E R                           *****/
/*****                                                       *****/
/*****************************************************************/
/*****************************************************************/

#define  NMEA_MAX_SIZE  83

typedef struct {
    int     pos;
    int     overflow;
    int     utc_year;
    int     utc_mon;
    int     utc_day;
    int     utc_diff;
    int     sv_status_changed;
    GpsLocation fix;
    GpsSvStatus sv_status;
    gps_location_callback  location_callback;
    gps_sv_status_callback  sv_status_callback;
    char    in[ NMEA_MAX_SIZE+1 ];
} NmeaReader;

static void
nmea_reader_update_utc_diff( NmeaReader*  r )
{
    time_t         now = time(NULL);
    struct tm      tm_local;
    struct tm      tm_utc;
    long           time_local, time_utc;

    gmtime_r( &now, &tm_utc );
    localtime_r( &now, &tm_local );

    time_local = tm_local.tm_sec +
                 60*(tm_local.tm_min +
                 60*(tm_local.tm_hour +
                 24*(tm_local.tm_yday +
                 365*tm_local.tm_year)));

    time_utc = tm_utc.tm_sec +
               60*(tm_utc.tm_min +
               60*(tm_utc.tm_hour +
               24*(tm_utc.tm_yday +
               365*tm_utc.tm_year)));

    r->utc_diff = time_utc - time_local;
}

static void
nmea_reader_init( NmeaReader*  r )
{
    memset( r, 0, sizeof(*r) );

    r->pos      = 0;
    r->overflow = 0;
    r->utc_year = -1;
    r->utc_mon  = -1;
    r->utc_day  = -1;
    r->location_callback = NULL;
    r->sv_status_callback = NULL;
    r->sv_status_changed = 0;
//    r->fix.size = sizeof(r->fix);
    nmea_reader_update_utc_diff( r );
}

static void
nmea_reader_set_callback( NmeaReader*  r, GpsCallbacks  *callbacks)
{
    if (NULL == r) {
        LOGE("NmeaReader is NULL");
        return;
    }
    if (NULL == callbacks) {
        r->location_callback = NULL;
        r->sv_status_callback = NULL;
        return;
    } else {
        r->location_callback = callbacks->location_cb;
        r->sv_status_callback = callbacks->sv_status_cb;
    }

    if (callbacks->location_cb != NULL && r->fix.flags != 0) {
        D("%s: sending latest fix to new callback", __FUNCTION__);
        r->location_callback( &r->fix );
        r->fix.flags = 0;
    }
    return;
}


static int
nmea_reader_update_time( NmeaReader*  r, Token  tok )
{
    int        hour, minute;
    double     seconds;
    struct tm  tm;
    time_t     fix_time;

    if (tok.p + 6 > tok.end)
        return -1;

    if (r->utc_year < 0) {
        // no date yet, get current one
        time_t  now = time(NULL);
        gmtime_r( &now, &tm );
        r->utc_year = tm.tm_year + 1900;
        r->utc_mon  = tm.tm_mon + 1;
        r->utc_day  = tm.tm_mday;
    }

    hour    = str2int(tok.p,   tok.p+2);
    minute  = str2int(tok.p+2, tok.p+4);
    seconds = str2float(tok.p+4, tok.end);

    tm.tm_hour  = hour;
    tm.tm_min   = minute;
    tm.tm_sec   = (int) seconds;
    tm.tm_year  = r->utc_year - 1900;
    tm.tm_mon   = r->utc_mon - 1;
    tm.tm_mday  = r->utc_day;
    tm.tm_isdst = -1;

//    fix_time = mktime( &tm ) + r->utc_diff;
    fix_time = utc_mktime(&tm);
    r->fix.timestamp = (long long)fix_time * 1000;
    return 0;
}

static int
nmea_reader_update_date( NmeaReader*  r, Token  date, Token  time )
{
    Token  tok = date;
    int    day, mon, year;

    if (tok.p + 6 != tok.end) {
        D("date not properly formatted: '%.*s'", tok.end-tok.p, tok.p);
        return -1;
    }
    day  = str2int(tok.p, tok.p+2);
    mon  = str2int(tok.p+2, tok.p+4);
    year = str2int(tok.p+4, tok.p+6) + 2000;

    if ((day|mon|year) < 0) {
        D("date not properly formatted: '%.*s'", tok.end-tok.p, tok.p);
        return -1;
    }

    r->utc_year  = year;
    r->utc_mon   = mon;
    r->utc_day   = day;

    return nmea_reader_update_time( r, time );
}


static double
convert_from_hhmm( Token  tok )
{
    double  val     = str2float(tok.p, tok.end);
    int     degrees = (int)(floor(val) / 100);
    double  minutes = val - degrees*100.;
    double  dcoord  = degrees + minutes / 60.0;
    return dcoord;
}


static int
nmea_reader_update_latlong( NmeaReader*  r,
                            Token        latitude,
                            char         latitudeHemi,
                            Token        longitude,
                            char         longitudeHemi )
{
    double   lat, lon;
    Token    tok;

    tok = latitude;
    if (tok.p + 6 > tok.end) {
        D("latitude is too short: '%.*s'", tok.end-tok.p, tok.p);
        return -1;
    }
    lat = convert_from_hhmm(tok);
    if (latitudeHemi == 'S')
        lat = -lat;

    tok = longitude;
    if (tok.p + 6 > tok.end) {
        D("longitude is too short: '%.*s'", tok.end-tok.p, tok.p);
        return -1;
    }
    lon = convert_from_hhmm(tok);
    if (longitudeHemi == 'W')
        lon = -lon;

    r->fix.flags    |= GPS_LOCATION_HAS_LAT_LONG;
    r->fix.latitude  = lat;
    r->fix.longitude = lon;
    return 0;
}


static int
nmea_reader_update_altitude( NmeaReader*  r,
                             Token        altitude,
                             Token        units )
{
    double  alt;
    Token   tok = altitude;

    if (tok.p >= tok.end)
        return -1;

    r->fix.flags   |= GPS_LOCATION_HAS_ALTITUDE;
    r->fix.altitude = str2float(tok.p, tok.end);
    return 0;
}


static int
nmea_reader_update_bearing( NmeaReader*  r,
                            Token        bearing )
{
    double  alt;
    Token   tok = bearing;

    if (tok.p >= tok.end)
        return -1;

    r->fix.flags   |= GPS_LOCATION_HAS_BEARING;
    r->fix.bearing  = str2float(tok.p, tok.end);
    return 0;
}

static int
nmea_reader_update_speed( NmeaReader*  r,
                          Token        speed )
{
    double  alt;
    Token   tok = speed;

    if (tok.p >= tok.end)
        return -1;

    r->fix.flags   |= GPS_LOCATION_HAS_SPEED;
    r->fix.speed    = str2float(tok.p, tok.end);
    return 0;
}

static int
nmea_reader_update_accuracy ( NmeaReader*  r,
        Token        hdop)
{
    double  alt;
    Token   tok = hdop;

    if (tok.p >= tok.end)
        return -1;

    r->fix.flags   |= GPS_LOCATION_HAS_ACCURACY;
    //    r->fix.accuracy = str2float(tok.p, tok.end)*10.0;
    r->fix.accuracy = str2float(tok.p, tok.end);
    return 0;
}

static int
power_save_mode(int fd)
{
	unsigned char buff[BUFFER_SIZE];
	int ret = -1;
	int bufflength = -1;
	int buffret = -1;
//	int n;
//	const unsigned char buffer_text[] = {0xB5,0x62,0x06,0x09,0x0D,0x00,0xFF,0xFF,0x00,0x00,0x00,0x00,0x00,0x00,                                                         			  0xFF,0xFF,0x00,0x00,0x03,0x1B,0x9A};  //default config
	const unsigned char buffer_rxm[] = {0xB5,0x62,0x06,0x11,0x02,0x00,0x08,0x01,0x22,0x92,                                                                             				 0xB5,0x62,0x06,0x09,0x0D,0x00,0x00,0x00,0x00,0x00,                                                                                           0xFF,0xFF,0x00,0x00,0x00,0x00,0x00,0x00,0x07,0x21,0xAF};
	D("========================power save mode==============xysun");
	D("%s: %d",__func__, __LINE__);

	bufflength = sizeof(buffer_rxm);
	ret = write(fd, buffer_rxm, bufflength);

//	bufflength = sizeof(buffer_text);
//	write(fd, buffer_text, bufflength);

	if (ret < 0)
	{
		LOGE("++++++++++++++RXM WRITE FAILD "); 
	}
	
	buffret = read(fd, buff, 100);
	D("---------buffret = %d ", buffret);
	if (buffret < 0) {

		LOGE("++++++++++++++RXM READ FAILD ");
	}

/*	for (n = 0; n < buffret; n++)
	{   
		LOGE("xysun buffer_rxm is %x", buff[n]); //text ACK
	}
*/
	return 0;
}


#if 0
static void nmea_fake_init(NmeaReader* r, NmeaTokenizer*  tzer)
{
    Token          tok;

    tok = nmea_tokenizer_get(tzer, 0);
    if ((tok.p + 5 > tok.end) && (*tok.p != (char)0xB5)) {
        D("sentence id '%.*s' too short, ignored.", tok.end-tok.p, tok.p);
        return;
    }

    tok.p += 2;
    if ( !memcmp(tok.p, "GSA", 3) ) {
        char carr[84] = "$GPGSA,A,3,104,106,107,109,,,,,,,,,100.000,100.000,100.000*33\n";
        memcpy(r->in, carr, 84); 
/*
    }
    else
    if ( !memcmp(tok.p, "GSV", 3) ) {
        char carr[84] = "$GPGSV,1,1,4,15,5,34,27,17,63,304,,19,7,313,,22,5,,*42\n";
        memcpy(r->in, carr, 84); 
*/
    }
        D("fixed Received: '%.*s'", r->pos, r->in);
        nmea_tokenizer_init(tzer, r->in, r->in + r->pos);
}
#endif

static void
nmea_reader_parse( NmeaReader*  r )
{
   /* we received a complete sentence, now parse it to generate
    * a new GPS fix...
    */
    NmeaTokenizer  tzer[1];
    Token          tok;

    D("Received: '%.*s'", r->pos, r->in);
    if (r->pos < 9) {
        D("Too short. discarded.");
        return;
    }

    nmea_tokenizer_init(tzer, r->in, r->in + r->pos);

#if GPS_DEBUG
    {
        int  n;
        D("Found %d tokens", tzer->count);
        for (n = 0; n < tzer->count; n++) {
            Token  tok = nmea_tokenizer_get(tzer,n);
            D("%2d: '%.*s'", n, tok.end-tok.p, tok.p);
        }
    }
#endif

    tok = nmea_tokenizer_get(tzer, 0);
    if ((tok.p + 5 > tok.end) && (*tok.p != (char)0xB5)) {
        D("sentence id '%.*s' too short, ignored.", tok.end-tok.p, tok.p);
        return;
    }

    // ignore first two characters.
    tok.p += 2;
    if ( !memcmp(tok.p, "GGA", 3) ) {
 
        // GPS fix
        Token  tok_time          = nmea_tokenizer_get(tzer,1);
        Token  tok_latitude      = nmea_tokenizer_get(tzer,2);
        Token  tok_latitudeHemi  = nmea_tokenizer_get(tzer,3);
        Token  tok_longitude     = nmea_tokenizer_get(tzer,4);
        Token  tok_longitudeHemi = nmea_tokenizer_get(tzer,5);
        Token  tok_altitude      = nmea_tokenizer_get(tzer,9);
        Token  tok_altitudeUnits = nmea_tokenizer_get(tzer,10);

        nmea_reader_update_time(r, tok_time);
        nmea_reader_update_latlong(r, tok_latitude,
                                      tok_latitudeHemi.p[0],
                                      tok_longitude,
                                      tok_longitudeHemi.p[0]);
        nmea_reader_update_altitude(r, tok_altitude, tok_altitudeUnits);

    } else if ( !memcmp(tok.p, "GSA", 3) ) {
        // do something ?
        Token  tok_fixStatus   = nmea_tokenizer_get(tzer, 2);
        int i;

        if (tok_fixStatus.p[0] != '\0' && tok_fixStatus.p[0] != '1') {

            Token  tok_accuracy      = nmea_tokenizer_get(tzer, 15); 

            nmea_reader_update_accuracy(r, tok_accuracy);

            r->sv_status.used_in_fix_mask = 0ul; 

            for (i = 3; i <= 14; ++i) {

                Token tok_prn  = nmea_tokenizer_get(tzer, i);
                int prn = str2int(tok_prn.p, tok_prn.end);

                D("%s: prn is %jx", __FUNCTION__, (uintmax_t)(r->sv_status.used_in_fix_mask));
                if (prn > 0 && prn < 200) {

                    if (prn <= 32) {
                        r->sv_status.used_in_fix_mask |= ((unsigned long long)1ul << (prn-1));
                    } else if (prn > 100 && prn <= 132) {
                        prn = prn - 100 + 32;
                        r->sv_status.used_in_fix_mask |= ((unsigned long long)1ul << (prn-1));
                    }
                    r->sv_status_changed = 1; 
                    D("%s: fix mask is 0x%08jX", __FUNCTION__, 
                            (uintmax_t)(r->sv_status.used_in_fix_mask));
                }
            }
        }
    } else if ( !memcmp(tok.p, "GSV", 3) ) {
        Token  tok_noSatellites  = nmea_tokenizer_get(tzer, 3);
        int    noSatellites = str2int(tok_noSatellites.p, tok_noSatellites.end);

        if (noSatellites > 0) {//receive GSV and have satellites

            Token  tok_noSentences   = nmea_tokenizer_get(tzer, 1);
            Token  tok_sentence      = nmea_tokenizer_get(tzer, 2);

            int sentence = str2int(tok_sentence.p, tok_sentence.end);
            int totalSentences = str2int(tok_noSentences.p, tok_noSentences.end);
            int curr;
            int i;

            if (sentence == 1) { 

                r->sv_status_changed = 0; 
                r->sv_status.num_svs = 0; 
            } 

            curr = r->sv_status.num_svs;

            i = 0; 

            while (i < 4 && r->sv_status.num_svs < noSatellites){

                Token  tok_prn = nmea_tokenizer_get(tzer, i * 4 + 4);
                Token  tok_elevation = nmea_tokenizer_get(tzer, i * 4 + 5);
                Token  tok_azimuth = nmea_tokenizer_get(tzer, i * 4 + 6);
                Token  tok_snr = nmea_tokenizer_get(tzer, i * 4 + 7);

                r->sv_status.sv_list[curr].prn = str2int(tok_prn.p, tok_prn.end);
                r->sv_status.sv_list[curr].elevation =
                    str2float(tok_elevation.p, tok_elevation.end);
                r->sv_status.sv_list[curr].azimuth =
                    str2float(tok_azimuth.p, tok_azimuth.end);
                r->sv_status.sv_list[curr].snr = str2float(tok_snr.p, tok_snr.end);
                r->sv_status.num_svs += 1;
                curr += 1;
                i += 1;
            }

            if (sentence == totalSentences) {
                r->sv_status_changed = 1;
            }
        } else {//received GSV but have no satallites
            memset(&r->sv_status,0,sizeof(GpsSvStatus));
            r->sv_status_changed = 1;
        }
        D("%s: GSV message with total satellites %d", __FUNCTION__, noSatellites);
    } else if ( !memcmp(tok.p, "RMC", 3) ) {
        Token  tok_time          = nmea_tokenizer_get(tzer,1);
        Token  tok_fixStatus     = nmea_tokenizer_get(tzer,2);
        Token  tok_latitude      = nmea_tokenizer_get(tzer,3);
        Token  tok_latitudeHemi  = nmea_tokenizer_get(tzer,4);
        Token  tok_longitude     = nmea_tokenizer_get(tzer,5);
        Token  tok_longitudeHemi = nmea_tokenizer_get(tzer,6);
        Token  tok_speed         = nmea_tokenizer_get(tzer,7);
        Token  tok_bearing       = nmea_tokenizer_get(tzer,8);
        Token  tok_date          = nmea_tokenizer_get(tzer,9);

        D("in RMC, fixStatus=%c", tok_fixStatus.p[0]);
        if (tok_fixStatus.p[0] == 'A')
        {
            nmea_reader_update_date( r, tok_date, tok_time );

            nmea_reader_update_latlong( r, tok_latitude,
                                           tok_latitudeHemi.p[0],
                                           tok_longitude,
                                           tok_longitudeHemi.p[0] );

            nmea_reader_update_bearing( r, tok_bearing );
            nmea_reader_update_speed  ( r, tok_speed );
        }
    } else {
        tok.p -= 2;
        D("unknown sentence '%.*s", tok.end-tok.p, tok.p);
    }
    if (r->fix.flags) {
#if GPS_DEBUG
        char   temp[256];
        char*  p   = temp;
        char*  end = p + sizeof(temp);
        struct tm   utc;

        p += snprintf( p, end-p, "sending fix" );
        if (r->fix.flags & GPS_LOCATION_HAS_LAT_LONG) {
            p += snprintf(p, end-p, " lat=%g lon=%g", r->fix.latitude, r->fix.longitude);
        }
        if (r->fix.flags & GPS_LOCATION_HAS_ALTITUDE) {
            p += snprintf(p, end-p, " altitude=%g", r->fix.altitude);
        }
        if (r->fix.flags & GPS_LOCATION_HAS_SPEED) {
            p += snprintf(p, end-p, " speed=%g", r->fix.speed);
        }
        if (r->fix.flags & GPS_LOCATION_HAS_BEARING) {
            p += snprintf(p, end-p, " bearing=%g", r->fix.bearing);
        }
        if (r->fix.flags & GPS_LOCATION_HAS_ACCURACY) {
            p += snprintf(p,end-p, " accuracy=%g", r->fix.accuracy);
        }
        gmtime_r( (time_t*) &r->fix.timestamp, &utc );
        p += snprintf(p, end-p, " time=%s", asctime( &utc ) );
        //D((char*)temp);
#endif
        if (r->location_callback) {
            r->location_callback( &r->fix );
            r->fix.flags = 0;
        }
        else {
            D("no location_callback, keeping data until needed !");
        }
    } else {
        D("have no location data");
    }

    if (r->sv_status_changed) {
        if (r->sv_status_callback) {
            r->sv_status_callback( &r->sv_status );
            r->sv_status_changed = 0;
        } else {
            D("no sv_status_callback,keeping data until needed !");
        }
    } else {
        D("have no sv_status data");
    }
}

static void
nmea_reader_addc( NmeaReader*  r, int  c )
{
    D("nmea_reader_addc start!!!");
    if (r->overflow) {
   // D("r->overflow!!");
        r->overflow = (c != '\n');
        return;
    }
   // D("r->pos!!");
    if (r->pos >= (int) sizeof(r->in)-1 ) {
        r->overflow = 1;
        r->pos      = 0;
        return;
    }
    
    r->in[r->pos] = (char)c;
    r->pos       += 1;
  //  D("nmea_reader_parse start!!!");
    if (c == '\n') {
      //  D("nmea_reader_parse !");
        nmea_reader_parse( r );
        r->pos = 0;
    }
}


/*****************************************************************/
/*****************************************************************/
/*****                                                       *****/
/*****       C O N N E C T I O N   S T A T E                 *****/
/*****                                                       *****/
/*****************************************************************/
/*****************************************************************/

/* commands sent to the gps thread */
enum {
    CMD_QUIT  = 0,
    CMD_START = 1,
    CMD_STOP  = 2
};


/* this is the state of our connection to the gps daemon */
typedef struct {
    int                     init;
    int                     fd;
    GpsCallbacks            callbacks;
    pthread_t               thread;
    int                     control[2];
//    QemuChannel             channel;
} GpsState;

static GpsState  _gps_state[1];

/*this is the state of agps daemon  add by cljiang*/
typedef struct{
	int 			init;   
	int 			fd;
	AGpsCallbacks 	callbacks;
    pthread_t       thread;
	int 			agpsSocket;
	struct sockaddr_in si_me;
	CH   connectTo[MAXFIELDLEN];
} AgpsState;

static AgpsState _agps_state[1];



typedef struct REQ_s {
	CH username[MAXFIELDLEN]; //!< Username to be sent to server
	CH password[MAXFIELDLEN]; //!< Password to be sent to server
	R8 lat; //!< Latitude of approximate location (in decimal degrees)
	R8 lon; //!< Longitude of approximate location (in decimal degrees)
	R8 alt; //!< Altitude of approximate location (in meters, can be zero)
	R8 accuracy; //!< Accuracy of lat/lon, in meters
} REQ_t;

static void
gps_state_done( GpsState*  s )
{
    // tell the thread to quit, and wait for it
    char   cmd = CMD_QUIT;
    void*  dummy;
    D("%s: %d",__func__, __LINE__);

    write( s->control[0], &cmd, 1 );
    pthread_join(s->thread, &dummy);
    D("%s: %d",__func__, __LINE__);

    // close the control socket pair
    close( s->control[0] ); s->control[0] = -1;
    close( s->control[1] ); s->control[1] = -1;

    // close connection to the QEMU GPS daemon
    close( s->fd ); s->fd = -1;
    s->init = 0;
    D("%s: %d",__func__, __LINE__);
}

static int
epoll_register( int  epoll_fd, int  fd )
{
    struct epoll_event  ev;
    int                 ret, flags;

    /* important: make the fd non-blocking */
    flags = fcntl(fd, F_GETFL);
    fcntl(fd, F_SETFL, flags | O_NONBLOCK);

    ev.events  = EPOLLIN;
    ev.data.fd = fd;
    do {
        ret = epoll_ctl( epoll_fd, EPOLL_CTL_ADD, fd, &ev );
    } while (ret < 0 && errno == EINTR);
    return ret;
}


static int
epoll_deregister( int  epoll_fd, int  fd )
{
    int  ret;
    do {
        ret = epoll_ctl( epoll_fd, EPOLL_CTL_DEL, fd, NULL );
    } while (ret < 0 && errno == EINTR);
    return ret;
}

/*static int
cold_start(int fd)
{
	const unsigned char buff1[BUFFER_SIZE];
	const unsigned char buff_cold[] = {0xB5,0x62,0x06,0x04,0x04,0x00,0xFF,0x87,0x02,0x00,0x96,0xF9};
	int cold_ret = -1;
	int bufflength = -1;
	int i;
	int ret = -1;


	D("*****************COLD START************************");
	bufflength = sizeof(buff_cold);
	cold_ret = write(fd, buff_cold, bufflength);
	if (cold_ret < 0)
	{
		LOGE("+++++++++++++++++++++write fail");    
	}

	ret = read(fd, buff1, 100);
	if (ret < 0) 
	{
		LOGE("+++++++++++++++++++++read fail"); 
	}

	for (i = 0; i < 20; i++)
	{
		D("-------------------------------");
		LOGE("xysun buff1 is %x", buff1[i]);
	}
	return 0;
}
*/

/* this is the main thread, it waits for commands from gps_state_start/stop and,
 * when started, messages from the QEMU GPS daemon. these are simple NMEA sentences
 * that must be parsed to be converted into GPS fixes sent to the framework
 */
static void
gps_state_thread( void*  arg )
{

	GpsState*   state = (GpsState*) arg;
	GpsStatus   status;
	NmeaReader  reader[1];
	int         epoll_fd   = epoll_create(2);
    int         started    = 0;
	int         gps_fd     = state->fd;
	int         control_fd = state->control[1];
    nmea_reader_init( reader );

  // register control file descriptors for polling
    epoll_register( epoll_fd, control_fd );
	epoll_register( epoll_fd, gps_fd );
	D("gps thread running");
	// now loop
	for (;;) {
		struct epoll_event   events[2];
		int                  ne, nevents;
        nevents = epoll_wait( epoll_fd, events, 2, -1 );
        if (nevents < 0) {
            if (errno != EINTR)
                LOGE("epoll_wait() unexpected error: %s", strerror(errno));
            continue;
        }
        D("gps thread received %d events", nevents);
        for (ne = 0; ne < nevents; ne++) {
            if ((events[ne].events & (EPOLLERR|EPOLLHUP)) != 0) {
                LOGE("EPOLLERR or EPOLLHUP after epoll_wait() !?");
                goto Exit;
            }
            if ((events[ne].events & EPOLLIN) != 0) {
                int  fd = events[ne].data.fd;

                if (fd == control_fd)
                {
                    char  cmd = 255;

                    int   ret;
                    D("gps control fd event");
                    do {
                        ret = read( fd, &cmd, 1 );
                    } while (ret < 0 && errno == EINTR);

                    if (cmd == CMD_QUIT) {
                        D("gps thread quitting on demand");
                        status.status = GPS_STATUS_ENGINE_OFF;
			D("%s: %d",__func__, __LINE__);
			D("----------------%p,%p, %p",state, &state->callbacks, &state->callbacks.status_cb);
                        state->callbacks.status_cb(&status);
			D("%s: %d",__func__, __LINE__);
                        goto Exit;
                    }
                    else if (cmd == CMD_START) {
                        if (!started) {
                            D("gps thread starting  location_cb=%p", state->callbacks.location_cb);
                            started = 1;
                            nmea_reader_set_callback( reader, &state->callbacks);
							status.status = GPS_STATUS_SESSION_BEGIN;
							state->callbacks.status_cb(&status);
                        }
                    }
                    else if (cmd == CMD_STOP) {
                        if (started) {
                            D("gps thread stopping");
                            started = 0;
                            nmea_reader_set_callback( reader, NULL );
                            status.status = GPS_STATUS_SESSION_END;
                            state->callbacks.status_cb(&status);
                        }
                    }
                }
                else if (fd == gps_fd)
                {
                    char  buff[32];
                    D("gps fd event");
                    for (;;) {
                        int  nn, ret;

                        ret = read( fd, buff, sizeof(buff) );
                        
						if (ret < 0) {
                            if (errno == EINTR)
                                continue;
                            if (errno != EWOULDBLOCK)
                                LOGE("error while reading from gps daemon socket: %s:", strerror(errno));
                            break;
                        }
                        D("received %d bytes: %s", ret, buff);
                        for (nn = 0; nn < ret; nn++)
                            nmea_reader_addc( reader, buff[nn] & 0xFF);
                    }

                    D("gps fd event end");
                }
				else
                {
                    LOGE("epoll_wait() returned unkown fd %d ?", fd);
                }
			}
        }
    }
Exit:
    D("%s: %d",__func__, __LINE__);
    return;
}

static void gps_state_init(GpsState *state)
{
    struct termios termios;
    int ret = -1;
	
	state->fd = open(DEV_NAME, O_RDWR | O_NONBLOCK | O_NOCTTY);
    if (state->fd < 0) { 
        LOGE("bdgps : Open %s failed !\n",DEV_NAME);
        return;
    }

    tcflush(state->fd, TCIOFLUSH);
    tcgetattr(state->fd, &termios);

    termios.c_iflag &= ~(IGNBRK | BRKINT | PARMRK | ISTRIP
            | INLCR | IGNCR | ICRNL | IXON);
    termios.c_oflag &= ~OPOST;
    termios.c_lflag &= ~(ECHO | ECHONL | ICANON | ISIG | IEXTEN);
    termios.c_cflag &= ~(CSIZE | PARENB);
    termios.c_cflag |= CS8; 

    termios.c_cflag &= ~CRTSCTS;

    tcsetattr(state->fd, TCSANOW, &termios);
	tcflush(state->fd, TCIOFLUSH);

	cfsetospeed(&termios, B9600);
	cfsetispeed(&termios, B9600);
	tcsetattr(state->fd, TCSANOW, &termios);
	
	_agps_state[0].fd = state->fd;    //agps fd   cljiang add
	power_save_mode(state->fd);
    ret = socketpair(AF_LOCAL, SOCK_STREAM, 0, state->control);
	if (ret < 0) {
		LOGE("couldn't create thread control socket pair: %s",
				strerror(errno));
		goto gsierr;
	} 
	
    
	state->thread = state->callbacks.create_thread_cb(
		    "gps_state_thread", gps_state_thread, state);
#if 0
    ret = pthread_create(&state->thread, NULL, gps_state_thread, state);
    if (ret) {
        LOGE("couldn't create gps thread: %s", strerror(errno));
        goto gsierr;

    } 
#endif
    state->init = 1;
    D(" GPS state initialized");

    return;

gsierr:
    gps_state_done(state);

    return;
}

/*
   Description : Get date from ublox server
	add by cljiang
*/

I getAssistNowOnlineData(CH * connectTo,I proto,CH * cmd,REQ_t * userInfo, CH * buffer, I bufferLength)
{
	struct sockaddr_in si_me;
	int agpsSocket;
	CH peerName[256];
	unsigned int peerPort; // UDP or TCP port to use
	CH request[256]; // Variable to hold the request string
	CH reply[BUFFER_SIZE];
	// parse host/port string
	CH * delim = strchr(connectTo,':');
	I4 totalLength = 0;
	I4 expectedLength = 1e7;
	I4 payloadLength = 0;
	CH * pPayload = NULL;
	
	// split the connectTo string into host:port parts
	if (delim)
	{
		*delim = (char)0;
		strcpy(peerName,connectTo);
		peerPort = atoi(delim+1);
		*delim = ':';
	}
	else
	{
		strcpy(peerName,connectTo);
		peerPort = 46434;
	}

	// *****************************************************
	// First, lookup IP address.
	//
	// Please note that you NEED to do a IP lookup. This, because
	// the IP number of the server may change 
	// *****************************************************

	memset((char *) &si_me, 0, sizeof(struct sockaddr_in));
	si_me.sin_family = AF_INET;
	si_me.sin_port = htons(peerPort);
	unsigned long addr = inet_addr(peerName);
	if (addr != INADDR_NONE)
	{
		// Numeric IP Address
		memcpy((char *)&si_me.sin_addr.s_addr, &addr, sizeof(addr));
	}
	else
	{
		struct hostent* hostInfo = gethostbyname(peerName);
		if (hostInfo != NULL)
		{
			memcpy((char *)&si_me.sin_addr.s_addr, hostInfo->h_addr, hostInfo->h_length);
		}
		else
		{
			D("Unable to look up %s - exiting\n",peerName);
			return 0;
		}
	}


	// *****************************************************
	// Then, open the socket and connect.
	//
	// Please note that this is the *only* place where TCP and UDP
	// are handled differently
	// *****************************************************

	if (proto == ANOL_UDP)
	{
		if ((agpsSocket=socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP))==-1)
		{
			D("unable to create UDP socket: %s\n",strerror(errno));
			return 0;
		}
	} 
	else if (proto == ANOL_TCP)
	{
		if ((agpsSocket=socket(AF_INET, SOCK_STREAM, IPPROTO_TCP))==-1)
		{
			D("unable to create TCP socket: %s\n",strerror(errno));
			return 0;
		}		
	} else {
		D("invalid protocol type %i\n",proto);
		return 0;
	}


	if (connect(agpsSocket,(struct sockaddr *)&si_me,sizeof(struct sockaddr_in))<0)
	{
		D("unable to connect: %s\n",strerror(errno));
		return 0;
	}


	// Set up a timeout for the socket
	struct timeval tv;
	tv.tv_sec = 5;
	tv.tv_usec = 0;
	if (setsockopt(agpsSocket, SOL_SOCKET, SO_RCVTIMEO, (char *)&tv,  sizeof(tv)))
	{
		D("setsockopt:");
		return 0;
	}


	// *****************************************************
	// Then, create the request string and send it
	// *****************************************************

	sprintf(request,"user=%s;pwd=%s;cmd=%s;lat=%.4f;lon=%.4f;pacc=%.0f",
		   userInfo->username,
		   userInfo->password,
		   cmd,
		   userInfo->lat,
		   userInfo->lon,
		   userInfo->accuracy);

	D("sending %s request to %s:%i (%s)\n",
		   proto == ANOL_TCP?"TCP":"UDP",
		   peerName,peerPort,
		   inet_ntoa(si_me.sin_addr)
		   );
	totalLength = write(agpsSocket,request,strlen(request));
	if (totalLength<0)
	{
		D("unable to send: %s\n",strerror(errno));
		return 0;
	}

	// *****************************************************
	// Then, read the server's replay 
	// (or run into the timeout)
	// *****************************************************

	totalLength = 0;

	// a single read call is not good enough, as the data may 
	// be received fragmented or slowly (in the TCP case) 

	// When we begin reading data, we do not know yet how much
	// we are going to get, so we initially set the expectedLength
	// to a large value.
	// Once we know more (because we have received the header),
	// we can set the expectedLength to the then known value
	while( (totalLength<expectedLength) && (totalLength<BUFFER_SIZE))
	{
		// Read Data from the socket
		I4   readLength = read(agpsSocket, reply+totalLength, BUFFER_SIZE-totalLength);
		CH * headerEnd;

		// If we have not received anything....
		if (readLength <= 0)
		{
			if (!readLength)
			{
				D("timeout while reading from socket!\n");

				return 0;
			} else {
				D("error reading from socket: %i %m!\n",readLength);
			}
			return -1;
		} 
		totalLength += readLength;
		
		// While reading from the socket, we want to parse it, in order to see whether
		// the full header has already been received.
		// If it has, extract the Payload size from the header, and 
		// set the expectedLength to that of the header+payload size
		// Also, set some pointer and counters for later use
		headerEnd=strstr(reply,"\r\n\r\n");
		if (headerEnd)
		{
			CH * pContentLength = strstr(reply,"Content-Length");
			U4 contentLength=0;
			if (sscanf(pContentLength,"Content-Length: %u",&contentLength) && (contentLength>0))
			{
				expectedLength = headerEnd - reply + contentLength + 4 /* for \r\n\r\n */;
				payloadLength  = contentLength; // store for later
				*headerEnd = (CH)0; // split into two strings
				pPayload = headerEnd + 4; // store for later
				D("setting exptected length to %i bytes\n",expectedLength);
			}
		}
	}

	// Close the socket - nothing more to do on the network side
	close(agpsSocket);


	// *****************************************************
	// Parse the reply, check for errors 
	// and return the payload 
	// *****************************************************
	if ((expectedLength == totalLength) && pPayload && payloadLength)
	{
		D("received %i bytes okay\n",totalLength);
		
		// If the header is Content-Type application/ubx, 
		// --> we can forward to the GPS receiver
		if (strstr(reply,"Content-Type: application/ubx"))
		{
			if (payloadLength>bufferLength)
			{
				D("Buffer is too small (%i), received %i bytes. \n",bufferLength,payloadLength);
				return -2;
			} else {
				D("ubx data with %i bytes payload\n",payloadLength);
				memcpy(buffer,pPayload,payloadLength);
				return payloadLength;
			}
			
		} else {
			// If the header is Content type text/plain, 
			// something went wrong
			if (strstr(reply,"Content-Type: text/plain"))
			{
				D("ERROR: %s\n",pPayload);
				return -3;
			} else {
				// Something went wrong - server does not adhere to our protocol ?!?
				return -4;
			}
		}
	} else {
		// Something went wrong - server does not adhere to our protocol? Not an AGPS Server ?!?
		return -5;
	}
}
/*--------------------------------------*/
void agps_state_thread(void* arg)
{
	AgpsState* state = (AgpsState*)arg;

	I    i; 
	REQ_t req = { "cljiang@ingenic.cn", "Hwhsrb", 30.45,114.17,0,1500e3}; //<
	I    count=0;  // Just to count the number of restarts - and to cycle between different aiding modes and protocols


	// In this example, we loop forever, sending restarts 
	// at regular intervals and deliver AssistNow data
	while(1)
	{
		CH buffer[BUFFER_SIZE];  // temporary buffer
		I  len;
		I  timeout = 20; 
		

//		tcflush(gpsDes,TCIOFLUSH); // Flush the Send/Receive queues to the GPS receiver

		D("AssistNow Online Request %i\n",++count);
		

		
		// Retrieve Assist Now Online Data
		//
		// Please note that the REQ_t structure must be filled in prior to that 
		// call (username, password, etc)
		// Also, the buffer must be large enough. 
		// -  For commands "aid", "alm" and "eph", about 1.5 kBytest is more than sufficient
		// -  For command "full", about 3.0kBytes is large enough
		// 
		// For this example application, we change between TCP and UDP protocol. 
		// Also, we change between "aid" and "full" assistance commands
		if ((len=getAssistNowOnlineData(state->connectTo,
								  count&0x01 ? ANOL_TCP:ANOL_UDP, // Switch between TCP and UDP protocol in this example code
								  "ephfull", // Switch between "full" and "aid" modes. 
								  &req,  // User credentials and approximate location 
								  buffer,BUFFER_SIZE))   >0  )
		{
			// We have successfully receiverd data from the server, which is 
			// now within 'buffer', for up to 'len' bytest
			D("-> forwarding %i bytes to GPS receiver\n",len);
			// We are sending this data right away to the GPS receiver
			write(state->fd,buffer,len);
		} else {
			// Some error occurred. See getAssistNowOnlineData() for possible error codes
			D("-> failed. error code %i\n",len);
		}

		// In this example, we repeatedly perform a assisted GPS start.
		// so, we do not really care what the GPS 
		// receiver's output is - we therefore flush the IO queues
		// once every second to avoid any buffer overflows
		while(timeout-->0)
		{
			tcflush(state->fd,TCIOFLUSH);
			sleep(1);
		}

		
		D("again\n");
	}

	// close file
	close(state->fd);
	// indicate success
	return ;
}


 static void agps_state_init(AgpsState* state )
{

	memcpy(state->connectTo,AGPS_SERVER,sizeof(AGPS_SERVER));

	D("agps fd = %d\n",state->fd);


    state->thread = state->callbacks.create_thread_cb(
		    "agps_state_thread", agps_state_thread, state);
}

/*****************************************************************/
/*****************************************************************/
/*****                                                       *****/
/*****       I N T E R F A C E                               *****/
/*****                                                       *****/
/*****************************************************************/
/*****************************************************************/
static int rfkill_id = -1;
static char *rfkill_state_path = NULL;


static int init_rfkill() {
    char path[64];
    char buf[16];
    int fd;
    int sz;
    int id;
    for (id = 0; ; id++) {
        snprintf(path, sizeof(path), "/sys/class/rfkill/rfkill%d/type", id);
        fd = open(path, O_RDONLY);
        if (fd < 0) {
            ALOGW("open(%s) failed: %s (%d)\n", path, strerror(errno), errno);
            return -1;
        }
        sz = read(fd, &buf, sizeof(buf));
        close(fd);
        if (sz >= 3 && memcmp(buf, "gps", 3) == 0) {
            rfkill_id = id;
            break;
        }
    }

    asprintf(&rfkill_state_path, "/sys/class/rfkill/rfkill%d/state", rfkill_id);
    return 0;
}

static int gps_power_ctl(int on) 
{
    int pfd = -1;
    int ret = -1; 
    const char buffer = (on ? '1' : '0');

    if (rfkill_id == -1) {
        if (init_rfkill()) goto PCTLOUT;
    }

    pfd = open(rfkill_state_path, O_WRONLY);
    if (pfd < 0) {
        LOGE("open(%s) for write failed: %s (%d)", rfkill_state_path,
             strerror(errno), errno);
        goto PCTLOUT;
    }   

    if (write(pfd, &buffer, 1) < 0) {
        LOGE("write(%s) failed: %s (%d)", rfkill_state_path, strerror(errno),
             errno);
        goto PCTLOUT;
    }   

    ret = 0;

PCTLOUT:
    if (pfd >= 0)
        close(pfd);

    return ret;
}

static int send_cmd(GpsState *s, unsigned char cmd)
{
    int ret = -1; 

    if (!s)
        return -1; 

    do {
        ret = write(s->control[0], &cmd, 1); 
    } while (ret < 0 && errno == EINTR);

    if (ret != 1) {
        D("couldn't send CMD %d command: ret=%d: %s",
                cmd, ret, strerror(errno));
        return -1; 
    }

    return 0;
}

static int gps_init_check(GpsState *s) 
{
    if (!s->init) {
        D("%s: called with uninitialized state !", __func__);
        return -1; 
    }   

    return 0;
}

static int gps_init(GpsCallbacks *callbacks)
{
    GpsState *s = _gps_state;
    int ret = -1; 
	gps_power_ctl(0);
    D("%s: %d",__func__, __LINE__);
    s->callbacks = *callbacks;
    if (!s->init)
        gps_state_init(s);
    return 0;
}

static void gps_cleanup(void)
{
    GpsState *s = _gps_state;
    D("%s: %d",__func__, __LINE__);

    if (s->init)
        gps_state_done(s);
}

static int gps_start()
{
	D("-------------gps_start------------------");
    GpsState *s = _gps_state;
    D("%s: %d",__func__, __LINE__);

	if (gps_init_check(s))
		return -1; 

	if (gps_power_ctl(1))
		return -1;

	D("--------------gps_start end-----------------");
    return send_cmd(s, CMD_START);
}

static int gps_stop()
{
    GpsState *s = _gps_state;
    D("%s: %d",__func__, __LINE__);

    if (gps_init_check(s))
        return -1; 

    gps_power_ctl(0);

    return send_cmd(s, CMD_STOP);
}

static int
gps_inject_time(GpsUtcTime time, int64_t timeReference, int uncertainty)
{
    D("%s: %d",__func__, __LINE__);
    return 0;
}

static int
gps_inject_location(double latitude, double longitude, float accuracy)
{
    D("%s: %d",__func__, __LINE__);
    return 0;
}

static void gps_delete_aiding_data(GpsAidingData flags)
{
    D("%s: called", __func__);
}

static int gps_set_position_mode(GpsPositionMode mode, GpsPositionRecurrence recurrence,               uint32_t min_interval, uint32_t preferred_accuracy, uint32_t preferred_time)
{
    D("%s: %d",__func__, __LINE__);
    if (mode == GPS_POSITION_MODE_STANDALONE || mode == GPS_POSITION_MODE_MS_BASED) {
        D("%s: called", __FUNCTION__);
        return 0;
    } else {
        D("%s: position mode error <STANDALONE or MS_BASED>!",__FUNCTION__);
        return -1;
    }

    D(" %s : called", __func__);

    return 0;
}

#define SECONDS_PER_MIN (60)
#define SECONDS_PER_HOUR (60*SECONDS_PER_MIN)
#define SECONDS_PER_DAY  (24*SECONDS_PER_HOUR)
#define SECONDS_PER_NORMAL_YEAR (365*SECONDS_PER_DAY) 
#define SECONDS_PER_LEAP_YEAR (SECONDS_PER_NORMAL_YEAR+SECONDS_PER_DAY)

static int days_per_month_no_leap[] =
{31,28,31,30,31,30,31,31,30,31,30,31};
static int days_per_month_leap[] =
{31,29,31,30,31,30,31,31,30,31,30,31};

static int is_leap_year(int year)
{
    if ((year%400) == 0)
        return 1;
    if ((year%100) == 0)
        return 0;
    if ((year%4) == 0)
        return 1;
    return 0;
}
static int number_of_leap_years_in_between(int from, int to)
{
    int n_400y, n_100y, n_4y;
    n_400y = to/400 - from/400;
    n_100y = to/100 - from/100;
    n_4y = to/4 - from/4;
    return (n_4y - n_100y + n_400y);
}

static time_t utc_mktime(struct tm *_tm)
{
    time_t t_epoch=0;
    int m; 
    int *days_per_month;
    if (is_leap_year(_tm->tm_year+1900))
        days_per_month = days_per_month_leap;
    else 
        days_per_month = days_per_month_no_leap;
    t_epoch += (_tm->tm_year - 70)*SECONDS_PER_NORMAL_YEAR; 
    t_epoch += number_of_leap_years_in_between(1970,_tm->tm_year+1900) *
        SECONDS_PER_DAY;
    for (m=0; m<_tm->tm_mon; m++) {
        t_epoch += days_per_month[m]*SECONDS_PER_DAY;
    }    
    t_epoch += (_tm->tm_mday-1)*SECONDS_PER_DAY;
    t_epoch += _tm->tm_hour*SECONDS_PER_HOUR;
    t_epoch += _tm->tm_min*SECONDS_PER_MIN;
    t_epoch += _tm->tm_sec;
    return t_epoch;

}

void assist_gps_init(AGpsCallbacks *callbacks)
{
    AgpsState *s = _agps_state;
    int ret = -1; 

    D("%s: %d",__func__, __LINE__);
    s->callbacks = *callbacks;

    if (!s->init)
        agps_state_init(s);

    return ;

}

int assist_gps_data_conn_open(const char *apn)
{
    D("%s: %d",__func__, __LINE__);
    return 0;
}

int assist_gps_data_conn_closed(void)
{
    D("%s: %d",__func__, __LINE__);
    return 0;
}

int assist_gps_data_conn_failed(void)
{
    D("%s: %d",__func__, __LINE__);
    return 0;
}

int assist_gps_set_server(AGpsType type, const char* hostname, int port)
{
    D("%s: %d",__func__, __LINE__);
    return 0;
}

static const AGpsInterface sAssistGpsInterface = {
    .init               = assist_gps_init,
    .data_conn_open     = assist_gps_data_conn_open,
    .data_conn_closed   = assist_gps_data_conn_closed,
    .data_conn_failed   = assist_gps_data_conn_failed,
    .set_server         = assist_gps_set_server,
};

const void *gps_get_extension(const char *name)
{
    D("%s: %d",__func__, __LINE__);
    if (!strcmp(name, GPS_XTRA_INTERFACE))
        return NULL;

    if (!strcmp(name, AGPS_INTERFACE))
        return &sAssistGpsInterface;

    return NULL;
}

static void gps_set_xtra_console(const char *cstr)
{ 
    GpsState *s = _gps_state;
    int n = 0;
    int ret = -1; 
    const char *p = cstr;

    if (cstr == NULL) {
        LOGE(" %s cstr is NULL", __func__);
        return ;
    }

    n = strlen(cstr);
    LOGE(" %d ", n);

    do {
        ret = write(s->fd, cstr, n); 
    } while (ret < 0 && errno == EINTR);

    if (ret != n) {
        D("%s :failed!   ret=%d: %s",
                __func__, ret, strerror(errno));
        return;
    }

    D("%s xtra SUCCESS , %s", __func__, cstr);
}

static const GpsInterface  _GpsInterface = { 
//    .size =                     sizeof(GpsInterface),
    .init =                     gps_init,
    .start =                    gps_start,
    .stop =                     gps_stop,
    .cleanup =                  gps_cleanup,
    .inject_time =              gps_inject_time,
    .inject_location =          gps_inject_location,
    .delete_aiding_data =       gps_delete_aiding_data,
    .set_position_mode =        gps_set_position_mode,
    .get_extension =            gps_get_extension,
    //.set_xtra_console =         gps_set_xtra_console,
};

const GpsInterface* gps_get_hardware_interface() 
{
    return &_GpsInterface;
}

#if 1
static int open_gps(const struct hw_module_t* module, char const* name,
                    struct hw_device_t** device)
{
    struct gps_device_t *gps_device = malloc(sizeof(struct gps_device_t));

    if (gps_device == NULL)
        return 1;

    memset(gps_device, 0, sizeof(struct gps_device_t));
    gps_device->common.tag        = HARDWARE_DEVICE_TAG;
    gps_device->common.version    = 0;  
    gps_device->common.module     = (struct hw_module_t*)module;
    gps_device->get_gps_interface = gps_get_hardware_interface;

    *device = (struct hw_device_t*)gps_device;

    return 0;
}

static struct hw_module_methods_t hw_module_methods = { 
    .open = open_gps,
};

struct hw_module_t HAL_MODULE_INFO_SYM = { 
    .tag = HARDWARE_MODULE_TAG,
    .version_major = 1,  
    .version_minor = 0,  
    .id = GPS_HARDWARE_MODULE_ID,
    .name = "bd GPS Module",
    .author = "Google",
    .methods = &hw_module_methods,
};
#endif
