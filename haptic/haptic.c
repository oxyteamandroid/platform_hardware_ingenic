/*
 * Copyright (C) 2008 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "haptic.h"
#include "drv2605.h"

static int init_haptic(void);

struct haptic_module_t HAPTIC_MODULE_INFO_SYM = {
	init: init_haptic,
};

static int init_haptic(void)
{
	return init_drv2605(&HAPTIC_MODULE_INFO_SYM);
}

