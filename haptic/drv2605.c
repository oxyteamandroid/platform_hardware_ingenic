/*
 * Copyright (C) 2008 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>

#define LOG_TAG "DRV2605"

#include <utils/Log.h>

#include "drv2605.h"

#define THE_DEVICE "/dev/drv2605"

static int fd = -1;

/* Commands */
#define HAPTIC_CMDID_PLAY_SINGLE_EFFECT     0x01
#define HAPTIC_CMDID_PLAY_EFFECT_SEQUENCE   0x02
#define HAPTIC_CMDID_PLAY_TIMED_EFFECT      0x03
#define HAPTIC_CMDID_GET_DEV_ID             0x04
#define HAPTIC_CMDID_RUN_DIAG               0x05
#define HAPTIC_CMDID_AUDIOHAPTIC_ENABLE     0x06
#define HAPTIC_CMDID_AUDIOHAPTIC_DISABLE    0x07
#define HAPTIC_CMDID_AUDIOHAPTIC_GETSTATUS  0x08

#define TOTAL_LEVEL 10
#define MAX_EFFECT 8

static int special_vibrate(int *p, int len)
{
    int i, ret;
    unsigned char WavfrmSeq[9];

    if (fd < 0)
        return -ENXIO;

    if (len > 8)
        len = 8;

    for (i = 0; i < len; i++) {
        if (p[i] == 0)
            WavfrmSeq[i + 1] = 0x10;
        else {
            WavfrmSeq[i + 1] = (p[i] & (0x7f));
        }
    }

    WavfrmSeq[0] = HAPTIC_CMDID_PLAY_SINGLE_EFFECT;

    ret = write(fd, WavfrmSeq, len + 1);
    if (ret != (len + 1)) {
        ALOGE("Write have some problem");
        return -ENXIO;
    }

    return 0;
}

int init_drv2605(struct haptic_module_t *p)
{
	fd = open(THE_DEVICE, O_RDWR);
    if(fd < 0) {
        ALOGE("drv2605 open devfs error [%s]\n", strerror(errno));
        return errno;
    }

    ALOGI("drv2605 init devfs success %p\n", p);

	p->special_vibrate = special_vibrate;

	return 0;
}

