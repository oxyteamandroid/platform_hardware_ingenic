/*
 * Copyright (C) 2008 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __HAPTIC_H
#define __HAPTIC_H

/**
 * Name of the haptic_module_info
 */
#define HAPTIC_MODULE_INFO_SYM         HMI

/**
 * Name of the haptic_module_info as a string
 */
#define HAPTIC_MODULE_INFO_SYM_AS_STR  "HMI"

struct haptic_module_t {
	/* init haptic device */
	int (*init)(void);
	int (*special_vibrate)(int *p, int len);
};

#endif

